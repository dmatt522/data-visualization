var webpack = require('webpack');
var path = require('path');
var polyfill = require('babel-polyfill');


var config = {
    entry: [
        'babel-polyfill',
        path.resolve(__dirname, 'src', 'main.jsx')
    ],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'app.bundle.js'
    },
    devServer: {
        /*clientLogLevel: "info",*/
        contentBase: path.resolve(__dirname, 'build'),
        historyApiFallback: false,
       /* hot:true*/
    },
    devtool: 'source-map',
    module:{
        loaders: [
            {
                test: /\.jsx?$/,
                include: path.resolve(__dirname, 'src'),
                loader: 'babel-loader'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};

module.exports = config;