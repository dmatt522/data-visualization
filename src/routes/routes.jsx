import React from 'react'
import { Route, IndexRoute } from 'react-router'
// routes
import PATHS from './paths'
// layouts
//import ProjectsLayout 	from '../layouts/ProjectsLayout'
// containers
//import VehicleSearchContainer from './components/containers/VehicleSearchContainer'
import RequireAuthentication 	from '../hocs/requireAuthentication'
import RequireNotAuthentication from '../hocs/requireNotAuthentication'
// layouts
import BaseLayout 		from '../shared/layouts/BaseLayout'
import AppLayout 		from '../shared/layouts/AppLayout'
import CalendarLayout 	from '../pages/calendar/CalendarLayout'
import DashboardLayout 	from '../pages/dashboard/DashboardLayout'
import ContactsLayout 	from '../pages/contacts/ContactsLayout'
import FilesLayout 	from '../pages/files/FilesLayout'
import PricingLayout 	from '../pages/pricing/PricingLayout'
import ProjectsLayout 	from '../pages/projects/ProjectsLayout'
import ReportsLayout 	from '../pages/reports/ReportsLayout'
import UsersLayout 	from '../pages/users/UsersLayout'
import VehiclesLayout 	from '../pages/vehicles/VehiclesLayout'
// components
import App 				from '../shared/components/App'
import Calendar 		from '../pages/calendar/Calendar'
import Dashboard 		from '../pages/dashboard/Dashboard'
import Contacts 		from '../pages/contacts/Contacts'
//import ContactDetails	from '../pages/contacts/ContactDetails'
//import ContactEdit		from '../pages/contacts/ContactEdit'
import Files 		from '../pages/files/Files'
import NoMatch			from '../shared/components/NoMatch'
import Pricing 			from '../pages/pricing/Pricing'
import Projects 		from '../pages/projects/Projects'
import ProjectDetails 	from '../pages/projects/ProjectDetails'
import ProjectEdit 		from '../pages/projects/ProjectEdit'
import ProjectAdd 		from '../pages/projects/ProjectAdd'
import ProjectPartVolume 	from '../pages/projects/ProjectPartVolume'
import ProjectChangeLog 	from '../pages/projects/ProjectChangeLog'
import ProjectActionPlans 	from '../pages/projects/ProjectActionPlans'
import ProjectContacts 		from '../pages/projects/ProjectContacts'
import ProjectMedia 		from '../pages/projects/ProjectMedia'
import Reports 			from '../pages/reports/Reports'
import ReportsVolumns 	from '../pages/reports/ReportsVolumns'
import Sign 			from '../pages/sign/Sign'
import Vehicles 		from '../pages/vehicles/Vehicles'
import Users 		from '../pages/users/Users'
import VehicleDetails	from '../pages/vehicles/VehicleDetails'
import VehicleEdit		from '../pages/vehicles/VehicleEdit'


//import Departments from '../components/Departments'
//import ProjectView from '../components/ProjectView'


const isLogged = (nextState, replace) => {
	console.log(nextState);
	const status = true;
	if(status === false){
		replace(PATHS.home);
	}
}

export default (

		<Route component={BaseLayout} name="BaseLayout" breadcrumbIgnore >

			<Route path={PATHS.home} component={RequireNotAuthentication(Sign)} name="Sign-In" />

			{/*
			oppure il default component quando viene chiamato PATHS.home
			<IndexRoute component={Sign} />
			*/}

			{/*<Route path={PATHS.depts} component={Departments} />*/}

			<Route component={RequireAuthentication(AppLayout)} name="AppLayout" breadcrumbIgnore >

				{/*  APP path */}
				<Route path={PATHS.app} component={App} name="Application"/>
				{/*  END APP path */}

				{/* DASHBOARD pages */}
				<Route component={DashboardLayout} name="DashboardLayout" breadcrumbIgnore >
					<Route path={PATHS.dashboard} component={Dashboard} name="Dashboard" />
				</Route>
				{/* END DASHBOARD pages */}

				{/* PROJECTS pages*/}
				<Route component={ProjectsLayout} name="ProjectsLayout" breadcrumbIgnore >
					<Route path={PATHS.projects} 		component={Projects} 		name="Projects" />
				</Route>
					<Route path={PATHS.projectDetails} 	component={ProjectDetails} 	name="ProjectDetails" />
					<Route path={PATHS.projectEdit} 	component={ProjectEdit} 	name="ProjectEdit" />
					<Route path={PATHS.projectAdd} 		component={ProjectAdd} 		name="ProjectAdd" />

					<Route path={PATHS.projectPartVolume} 	component={ProjectPartVolume} 		name="ProjectPartVolume" />
					<Route path={PATHS.projectChangeLog} 	component={ProjectChangeLog} 		name="ProjectChangeLog" />
					<Route path={PATHS.projectActionPlans} 	component={ProjectActionPlans} 		name="ProjectActionPlans" />
					<Route path={PATHS.projectContacts} 	component={ProjectContacts} 		name="ProjectContacts" />
					<Route path={PATHS.projectMedia} 		component={ProjectMedia} 			name="ProjectMedia" />
				{/* END PROJECTS pages*/}


				{/* VEHICLES pages*/}
				<Route component={VehiclesLayout} name="VehiclesLayout" breadcrumbIgnore >
					<Route path={PATHS.vehicles} 		component={Vehicles} 		name="Vehicles" />
				</Route>
					<Route path={PATHS.vehicleDetails} 	component={VehicleDetails} 	name="VehicleDetails" />
					<Route path={PATHS.vehicleEdit} 	component={VehicleEdit} 	name="VehicleEdit" />
				{/* END VEHICLES pages*/}


				{/* REPORTS pages*/}
				<Route component={ReportsLayout} name="ReportsLayout" breadcrumbIgnore >
					<Route path={PATHS.reports} component={Reports} name="Reports" />
				</Route>
				<Route path={PATHS.volumns} 	 component={ReportsVolumns} 		name="ProjectDetails" />
				<Route path={PATHS.revenues} 	 component={ProjectContacts} 		name="ProjectContacts" />
				<Route path={PATHS.lightweights} component={ProjectMedia} 			name="ProjectMedia" />
				{/* END REPORTS pages*/}


				{/* CALENDAR pages*/}
				<Route component={CalendarLayout} name="CalendarLayout" breadcrumbIgnore >
					<Route path={PATHS.calendar} component={Calendar} name="Calendar" />
				</Route>
				{/* END CALENDAR pages*/}

				{/* CONTACTS pages*/}
				<Route component={ContactsLayout} name="ContactsLayout" breadcrumbIgnore >
					<Route path={PATHS.contacts} component={Contacts} name="Contacts" />
					{/*<Route path={PATHS.contactDetails} 	component={ContactDetails} name="ContactDetails" />
					<Route path={PATHS.contactEdit}		component={ContactEdit} name="ContactEdit" />*/}
				</Route>
				{/* END CONTACTS pages*/}

				<Route component={PricingLayout} name="PricingLayout" breadcrumbIgnore>
					<Route path={PATHS.pricing} component={Pricing} name="Pricing" />
				</Route>

				<Route component={UsersLayout} name="UsersLayout" breadcrumbIgnore>
					<Route path="/users" component={Users} name="Users" />
				</Route>

				<Route component={FilesLayout} name="FilesLayout" breadcrumbIgnore>
					<Route path="/files" component={Files} name="Files" />
				</Route>


			</Route>

			<Route path="*" component={NoMatch} />

		</Route>

);