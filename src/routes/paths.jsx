import globals from '../config/globals'

const PATHS = {
	home: 				`${globals.basePath}/`,
	app: 				`${globals.basePath}/app`,
	dashboard: 			`${globals.basePath}/dashboard`,
	pricing: 			`${globals.basePath}/pricing`,
	files: 				`${globals.basePath}/files`,
	projects: 			`${globals.basePath}/projects`,
	projectDetails: 	`${globals.basePath}/projects/:id`,
	projectEdit: 		`${globals.basePath}/projects/edit/:id`,
	projectAdd: 		`${globals.basePath}/projects/add/:id`,
	projectPartVolume: 	`${globals.basePath}/projects/partvolume/:id`,
	projectChangeLog: 	`${globals.basePath}/projects/changelog/:id`,
	projectActionPlans:	`${globals.basePath}/projects/actionplans/:id`,
	projectContacts: 	`${globals.basePath}/projects/contacts/:id`,
	projectMedia: 		`${globals.basePath}/projects/media/:id`,
	vehicles: 			`${globals.basePath}/vehicles`,
	vehicleDetails: 	`${globals.basePath}/vehicles/:id`,
	vehicleEdit: 		`${globals.basePath}/vehicles/edit/:id`,
	reports:  			`${globals.basePath}/reports`,
	volumns:  			`${globals.basePath}/volumns`,
	revenues:  			`${globals.basePath}/revenues`,
	lightweights:  		`${globals.basePath}/lightweights`,
	contacts:  			`${globals.basePath}/contacts`,
	contactDetails: 	`${globals.basePath}/contacts/:id`,
	contactEdit: 		`${globals.basePath}/contacts/edit/:id`,
	calendar:    		`${globals.basePath}/calendar`,
	costModel: 			`${globals.basePath}/cost-model`,
	quotes: 			`${globals.basePath}/quotes`,
	salesForecast:		`${globals.basePath}/sales-forecast`,
	variableCompModule: `${globals.basePath}/variable-comp-module`,
	pricing: 			`${globals.basePath}/pricing`,
	goalAndObjectives: 	`${globals.basePath}/goal-and-objectives`,
	users: 				`${globals.basePath}/users`
};

export default PATHS;