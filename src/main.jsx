import React 		from 'react'
import ReactDOM 	from 'react-dom'
import promise 		from 'redux-promise'
import multi		from 'redux-multi'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, browserHistory } 		from 'react-router'
import injectTapEventPlugin 			from 'react-tap-event-plugin'

import ActionLogger from './middlewares/action_logger'
import ActionEnhancer from './middlewares/action_enhancer'

import axios from 'axios'
const AUTH_TOKEN = `Basic ${btoa('admin:1234')}`;
axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.common['Content-Type'] = 'application/json';
//console.log('['+AUTH_TOKEN+']');


import reducers from './reducers'
import routes 	from './routes/routes'

// Needed for onTouchTap http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();



const createStoreWithMiddleware = applyMiddleware(promise, multi, ActionLogger, ActionEnhancer)(createStore);

ReactDOM.render(
	<Provider store={createStoreWithMiddleware(reducers)}>
		<Router history={browserHistory} routes={routes} />
	</Provider>,
	document.getElementById('app')
);