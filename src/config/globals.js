/**
 * application level vars
 * http://api-commops.rekoware.com/api/vehicles/all/
 * http://api-commops.rekoware.com/api/vehicles/(Project_ID)/
 */
export default {
	/* path after domain: example http://www.foo.com, for the demo use: /demo */
	basePath: ''
};
