/* api calls */

const DEV 		= '//localhost:8080/data';
const STAGING_APP = '//commops.rekoware.com';
const STAGING 	= '//commops.rekoware.com/restserver/index.php/api';//'//api-commops.rekoware.com/api';
const FAKE_STG	= '//commops.rekoware.com/demo/data'
const PROD 		= '';

// fake when api is not complete
export const FAKE_API_BASE_URL	= DEV;
export const FAKE_SIGN_IN 		= `${DEV}/login.json`;
export const FAKE_PWD_RECOVERY 	= FAKE_SIGN_IN;

/**
 * API
 */
export const API_BASE_URL 			= STAGING;

//
export const UNIT_OF_MEASURE	= `${API_BASE_URL}/uom/`;
// SIGN-IN
export const USER_SIGN_IN 		= `${API_BASE_URL}/login/`; /* login/ */
export const LOGIN_CONFIG 		= `${API_BASE_URL}/login_config/`; /* login/ */
export const LOGIN_1 		= `${API_BASE_URL}/login_1/`; /* login/ */
export const LOGIN_2 		= `${API_BASE_URL}/login_2/`; /* login/ */

export const PWD_RECOVERY		= `${API_BASE_URL}/forgotpassword/`;

//
export const USERS_ROLES		= `${API_BASE_URL}/users_roles/`;

// VEHICLES
export const ALL_VEHICLES		= `${API_BASE_URL}/vehicles/all`;//`${API_BASE_URL}/vehicles.json`;//
export const SINGLE_VEHICLE		= `${API_BASE_URL}/vehicles/`; 		/* vehicles/(Project_ID)/ */
export const VEHICLE_EDIT		= ``;
export const VEHICLE_DELETE		= `${API_BASE_URL}/delete/`;

// REPORTS
export const ALL_REPORT_VOLUMNS		= `${API_BASE_URL}/reports_part_volume/?part_type=0&probability_status=0`;//`${API_BASE_URL}/allreoprtvolumns.json`;//
export const ALL_REPORT_FILTERS		= `${API_BASE_URL}/reports_charts/1`;//`${API_BASE_URL}/allreoprtvolumns.json`;//

// CONTACTS
export const ALL_CONTACTS		= `${API_BASE_URL}/contacts/all`;//`${API_BASE_URL}/directories.json`;//
export const SINGLE_CONTACT		= `${API_BASE_URL}/contacts/`; /* directories/(Contact_ID)/ */
export const CONTACT_ADD		= `${API_BASE_URL}/contacts/`; /* directories/(Contact_ID)/ */
export const CONTACT_EDIT		=  `${API_BASE_URL}/contacts/`;
export const CONTACT_DELETE		= `${API_BASE_URL}/delete/`;

// USERS
export const ALL_USERS		= `${API_BASE_URL}/users`;//`${API_BASE_URL}/directories.json`;//
export const USER_DELETE	= `${API_BASE_URL}/delete/`;
export const USER_ADD		= `${API_BASE_URL}/users/`;
export const USER_EDIT		= `${API_BASE_URL}/users/`;

// FILES
export const ALL_FILES		= `${API_BASE_URL}/files`;//`${API_BASE_URL}/directories.json`;//
export const FILE_DELETE	= `${API_BASE_URL}/delete/`;
export const FILE_ADD		= `${API_BASE_URL}/files/`;
export const FILE_EDIT		= `${API_BASE_URL}/files/`;

// PROJECTS
export const ALL_PROJECTS		= `${API_BASE_URL}/projects/all`;//`${API_BASE_URL}/projects.json`;//
export const SINGLE_PROJECT		= `${API_BASE_URL}/projects/`; /* directories/(Contact_ID)/ */
export const PROJECT_SUBMIT		= `${API_BASE_URL}/projects/`;
export const PROJECT_ADD		= `${API_BASE_URL}/projects/`;
export const PROJECT_EDIT		= `${API_BASE_URL}/projects/`;
export const PROJECT_DELETE		= `${API_BASE_URL}/delete/`;
export const PROJECT_CHART_1	= `${API_BASE_URL}/project_part_volume_vs_total_volume/`;
export const PROJECT_CONTACTS	= `${API_BASE_URL}/contacts_to_projects/`;
export const PROJECT_HISTORY	= `${API_BASE_URL}/projects_log/`;
export const PROJECT_ACTION_PLANS	= `${API_BASE_URL}/action_plans_by_project/`;
export const PROJECT_ACTIONS_PLANS_DELETE	= `${API_BASE_URL}/delete/`;
export const PROJECT_ACTIONS_PLANS_ADD	= `${API_BASE_URL}/action_plans/`;

// LOOKUPS
export const ADD_LOOKUP			= `${API_BASE_URL}/addlookup/`;
export const MATERIALS			= `${API_BASE_URL}/lookup/material`;
export const STATUS				= `${API_BASE_URL}/lookup/probability_status`;
export const PERCENT			= `${API_BASE_URL}/lookup/probability_percent`;
export const YTC				= `${API_BASE_URL}/lookup/year_moved_to_current`;
export const PART_TYPE			= `${API_BASE_URL}/lookup/part_type`;
export const MCCP_PLANT			= `${API_BASE_URL}/lookup/mcpp_plant`;
export const TIER_1				= `${API_BASE_URL}/lookup/tier_1`;
export const TIER_2				= `${API_BASE_URL}/lookup/tier_2`;
export const MOLDER				= `${API_BASE_URL}/lookup/molder`;
export const SALES_MANAGER		= `${API_BASE_URL}/lookup/sales_manager`;
export const ADE				= `${API_BASE_URL}/lookup/ade`;
export const TECH_SERVICE		= `${API_BASE_URL}/lookup/tech_service`;
export const OEM_MATERIAL		= `${API_BASE_URL}/lookup/spec`;
export const OEM_PERFORMANCE	= `${API_BASE_URL}/lookup/spec`;
export const ACTION_PLANS_PRIORITY	= `${API_BASE_URL}/lookup/action_plans_priority/`;
export const ACTION_PLANS_STATUS	= `${API_BASE_URL}/lookup/action_plans_status/`;
export const USERS				= `${API_BASE_URL}/users`;