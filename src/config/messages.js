/**
 * default messages to notify
 */
export const SIGN_IN_PROGRESS 	= 	'Authentication is in progress';
export const SIGN_IN_SUCCESS 	=	'Authentication success';
export const SIGN_IN_FAIL 		=	'Authentication error';
export const LOADING_DATA 		=	'Loading data. Please wait a moment.';
export const UPDATING_DATA 		=	'Updatinging data. Please wait a moment.';