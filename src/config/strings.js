// app
export const APP_TITLE 	= 'CommOps';
export const SGN_OUT 	= 'SignOut';


/**
 *
 * src/pages/dashboard/*
 *
 */
export const DASHBOARD_PAGE_DESCRIPTION = "dashboard page description"
/**
 *
 * src/pages/vehicles/*
 *
 */
export const VEHICLES_PAGE_DESCRIPTION 		= 'Select the search filters from each drop-down field in the "Search Filters" Panel and then click on Search. To display the entire database, click on Reset.';
export const VEHICLES_RESULT_CALL_TO_ACTION = 'Select a table row to activate the View/Edit/Delete menu.'
export const VEHICLES_FILTER_GROUP_TITLE 	= 'Search Filters';
export const VEHICLES_RESULTS_TITLE 		= 'Search Results';
//
export const VEHICLES_DETAILS_PAGE_DESCRIPTION 	= 'The Vehicle Panel displays vehicle data from user input and IHS. Edit the panel using the "Edit" link in the menu.';
export const VEHICLES_DETAILS_TITLE 			= '';
export const VEHICLES_DETAILS_PROJECTS 			= '';
//
export const VEHICLES_EDIT_PAGE_DESCRIPTION = 'Edit the desired fields and click on "Update Vehicle Data" to submit the changes.';
export const VEHICLES_EDIT_PHOTO_TITLE 		= 'Photo';
export const VEHICLES_EDIT_DATA_TITLE 		= 'Vehicle Data';
export const VEHICLES_EDIT_BUILD_TITLE 		= 'Vehicle Build';
/**
 *
 * src/pages/contacts/*
 *
 */
export const CONTACTS_PAGE_DESCRIPTION 		= 'Select the search filters from each drop-down field in the "Search Filters" Panel and then click on Search. To display the entire database, click on Reset.';
export const CONTACTS_RESULT_CALL_TO_ACTION 	= 'Select a table row to activate the View/Edit/Delete menu.'
export const CONTACTS_FILTER_GROUP_TITLE 	= 'Search Filters';
export const CONTACTS_RESULTS_TITLE 			= 'Search Results';
//
export const CONTACT_DETAILS_PAGE_DESCRIPTION = 'The Contact Details Panel lists the assigned projects. Use this panel to add or update contact comments.';
export const CONTACT_DETAILS_TITLE 			= 'Contact Details';
export const CONTACT_DETAILS_PROJECTS 		= 'Assigned Projects';
//
export const CONTACT_EDIT_PAGE_DESCRIPTION 	= 'Edit the desired fields and click on the "Update Contact" button.';
export const CONTACT_EDIT_TITLE 				= 'Contact Edit';
/**
 * src/pages/calendar/*
 */
export const CALENDAR_PAGE_DESCRIPTION	= 'calendar page description'