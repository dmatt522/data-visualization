import {
  cyan500, cyan700,
  pinkA200,
  grey100, grey300, grey400, grey500, grey900,
  redA700,
  white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';

export default {
	title:{
		paddingLeft: 16,
		color: redA700
	},
	paper:{
		padding: 16
	},
	tooltip:{
		fontSize:16,
		padding: 8
	},
	filtersGroup:{},
	resultsDisplay:{},
	filter:{
		label:{
			fontSize: 16
		}
	},
	projectDialog:{
		width:'90%',
		maxWidth:'90%',
		height:'90vh',
		minHeight:'90vh'
	},
	th:{
		/*fontSize:'1rem',
		fontWeight:'700',
		color:'#000',
		paddingLeft:8,
		paddingRight:8,*/
	},
	td:{
		/*paddingLeft:8,
		paddingRight:8*/
	},
	tableHeaderLabel:{
		/*paddingLeft:4,
		paddingRight:4,
		hoverColor:'#fff'*/
	},

};