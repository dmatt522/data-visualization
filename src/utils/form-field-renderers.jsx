import React from 'react'
import IconButton from 'material-ui/IconButton';
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'
import _ from 'lodash'


export const renderTextField = ({ input, label, type, meta: { touched,error,warning } } ) => (
	<div className="col s12 m3 form-field-wrapper">
		<label>{label}</label>
		<input {...input} id={input.name} placeholder={label} type={type} />
	</div>
);


export const renderSelectField = ({ input, label, options}) => (
	<div className="col s12 m3 form-field-wrapper">
		<label>{label}</label>
		<select {...input} className="browser-default">
		    { options.map( (opt, i) => <option key={ i } value={ opt }>{ opt }</option> ) }
		</select>
	</div>
);

export const renderTextArea = ({ input, rows, label, meta: { touched,error,warning } }) => (
	<div className="col s12 form-field-wrapper">
		<label>{label}</label>
		<textarea {...input} placeholder={label} rows={rows} className="browser-default"></textarea>
	</div>
);





export const renderTextFieldLookup = ({ input, label, type, meta: { touched,error,warning } } ) => (
	<div className="col s12 m3 form-field-wrapper">
		<label>{label}</label>
		<IconButton disabled><EditorModeEdit /></IconButton>
		<input {...input} id={input.name} placeholder={label} type={type} />
	</div>
);


export const renderSelectFieldLookup = ({ input, label, options, editModeHandler }) => (
	<div className="col s12 m3 form-field-wrapper">
		<label>{label}</label>

		<IconButton onClick={ (event) => editModeHandler(event, label, input.name) } disabled={ !_.isFunction(editModeHandler) }><EditorModeEdit /></IconButton>

		<select {...input} className="browser-default">
		    { _.concat([/*{id:'unassigned', name:'unassigned'}*/], options).map( (opt) => <option key={ opt.id } value={ opt.name }>{ opt.name }</option> ) }
		</select>
	</div>
);


export const renderDialogTextFieldLookup = ({ input, label, type='text', meta: { touched,error,warning } } ) => (
	<div className="col s12 l6 form-field-wrapper with-type-text">
		<label>{label}</label>
		<input {...input} id={input.name} type={type} />
	</div>
);


export const renderDialogSelectFieldLookup = ({ input, label, options, editModeHandler, scope='no_scope', meta: { touched,error,warning }}) => (
	<div className="col s12 l6 form-field-wrapper">
		<label>{label}</label>
		{_.isFunction(editModeHandler) && <span className="edit-mode-handler-btn" onClick={ (event) => editModeHandler(event, label, input.name, scope) }>[edit]</span>}
		<select {...input} className="browser-default">
		    { _.concat([{id:'', name:''}], options).map( (opt) => <option key={ opt.id } value={ opt.name }>{ opt.name }</option> ) }
		</select>
		{ touched && error && <span>{error}</span> }
	</div>
);





export const idSelect = ({ input, label, options, editModeHandler, scope='no_scope', meta: { touched,error,warning }}) => (
	<div className="col s12 l6 form-field-wrapper">
		<label>{label}</label>
		{_.isFunction(editModeHandler) && <span className="edit-mode-handler-btn" onClick={ (event) => editModeHandler(event, label, input.name, scope) }>[edit]</span>}
		<select {...input} className="browser-default">
			<option key={ -1 } value="empty"></option>
		    { options.map( (opt, index) => <option key={ index } value={ opt.id }>{ `${opt.name}` }</option> ) }
		</select>
		{ touched && error && <span>{error}</span> }
	</div>
);




export const userSelect = ({ input, label, options, editModeHandler, scope='no_scope', meta: { touched,error,warning }}) => (
	<div className="col s12 l6 form-field-wrapper">
		<label>{label}</label>
		{_.isFunction(editModeHandler) && <span className="edit-mode-handler-btn" onClick={ (event) => editModeHandler(event, label, input.name, scope) }>[edit]</span>}
		<select {...input} className="browser-default">
			<option key={ -1 } value="empty"></option>
		    { options.map( (opt, index) => <option key={ index } value={ opt.id }>{ `${opt.first_name} ${opt.last_name}` }</option> ) }
		</select>
		{ touched && error && <span>{error}</span> }
	</div>
);

