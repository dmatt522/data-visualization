import React from 'react'
import IconButton from 'material-ui/IconButton'
import styles from '../styles/styles'

export const genIconButton = (children, clickHandler, tooltip = '') => (
	<IconButton children={ children } onClick={ clickHandler } tooltip={ tooltip } tooltipStyles={styles.tooltip} />
);