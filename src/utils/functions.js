import React from 'react'
import _ from 'lodash'
import moment from 'moment'
import {Link} from 'react-router'
import { projectPropsDefinedById } from './const'
//import {REST_SERVER_IMG_PATH} from '../config/media'


/***********************************************
 * refactorKey
 * mapKeyToHelpText
 * _valueAt
 * _buildMediaLink
 * _normalizeLink
 * _getOptionsForField
 * _cySeries
 * _cyTableData
 * matchFilters
 * _removeColumnsByUnitOfMeasure
 * _cellValue
 * _mapInitialValues
 ***********************************************/

/**
 * refactor the key to be nicely displayed
 * @param  {[type]} key [description]
 * @return {[type]}     [description]
 *
 * used in
 * 	- ../pages/vehicles/components/ResultsTable.jsx
 * 	- ..
 */
export function refactorKey(key){
	let newKey = _.replace(key, /_/g, ' ');
	switch(newKey){
		case 'part image 1':
			return 'thumbnail';
		case 'global sales subsegment':
			return 'SubSegment';
		case 'production brand':
			return 'Brand';
		case 'production nameplate':
			return 'Nameplate';
		case 'regional sales segment':
			return 'segment';
		default:
			return newKey;
	}
}
/**
 * sets help text for each key
 * @param  {[type]} key [description]
 * @return {[type]}     [description]
 *
 * used in
 * 	- ../pages/vehicles/components/ResultsTable.jsx
 * 	- ..
 */
export function mapKeyToHelpText(key){

	//console.log('->', key);

	if(_.startsWith(key, 'cy'))
		return 'Vehicle Build - Calendar Year ' + _.split(key, /_/)[1];

	switch(key){
		case 'brand':
			return 'The badge or marque assigned to the vehicle.';
		case 'country':
			return 'The country of the vehicle’s final assembly';
		case 'global_sales_subsegment':
			return 'Global Sales Sub-Segment';
		case 'location':
			return 'Location of the the vehicle\'s final assembly plant';
		case 'manufacturer':
			return 'The company or joint venture responsible for the production of the vehicle.';
		case 'nameplate':
			return 'The vehicle\'s model name';
		case 'part_image_1':
			return 'Part thumbnail';
		case 'part_type':
			return 'The part type.';
		case 'platform':
			return 'A grouping of current generation programs and their future replacement(s) where the future replacement involves a major redesign to the vehicle';
		case 'production_brand':
			return 'The badge or marque assigned to the vehicle.';
		case 'production_nameplate':
			return 'The vehicle\'s model name';
		case 'production_plant':
			return 'The vehicle\'s final assembly plant';
		case 'program':
			return 'Identifies the actual code used by OEMs to describe a vehicle throughout the design cycle.';
		case 'vehicle_id':
			return 'The Vehicle ID';
		case 'regional_sales_segment':
			return 'Regional Sales Segment';
		case 'sop':
			return 'Start of production';
		case 'eop':
			return 'End of production';
		default:
			return '...';
	}
}

/**
 * returns an object {path: value-at-path} using obj
 * @param  {[type]} obj          [description]
 * @param  {[type]} path         [description]
 * @param  {String} defaultValue [description]
 * @return {[type]}              [description]
 */
export function _valueAt(obj, path, defaultValue = '-'){
	if(_.endsWith(obj[path], '.jpg') || _.endsWith(obj[path], '.jpeg') || _.endsWith(obj[path], '.png') || _.endsWith(obj[path], '.gif') ){
		return <img className="details-image user-details-image" src={_buildMediaLink(obj[path])} />
	}
	//if(_.indexOf(projectPropsDefinedById, path) != -1)
	//	return _.get(obj, )
	return _.get(obj, path, defaultValue);
}
/**
 * link to media resource
 * @param  {[type]} path [description]
 * @return {[type]}      [description]
 */
export function _buildMediaLink(path){
	return path;
}
/**
 * [_normalizeLink description]
 * @param  {[type]} link [description]
 * @return {[type]}      [description]
 */
export function _normalizeLink(link){
	console.log('link', link);
	// has http://
	link = _.startsWith(link, 'www') ? `http://${link}`: link;
	// if is email, add protocol
	link = (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(link)) ? "mailto:"+link : link;
	return link;
}
/**
 * extract the option field from an object in the array "filters"
 * @param  {[type]} filters [description]
 * @param  {[type]} name    [description]
 * @return {[type]}         [description]
 */
export function _getOptionsForField(filters, name){
	return _.get(_.head(_.filter(filters, function(o){ return o.name === name; })), 'options');
}
/**
 * [_cySeries description]
 * @param  {[type]} input [description]
 * @return {[type]}       [description]
 */
export function _cySeries(input){
		let output = [];
		_.forEach( input, function(value, key){
			if(_.startsWith(key, 'cy_'))
				output = _.concat(output, {key: key, value: value, label:_.upperCase(key)});
		});
		return output;//_.sortBy(output, ['key']);
}
/**
 * create data for the table in project/details dialog
 * @param  {[type]} selectedItem [description]
 * @return {[type]}              [description]
 */
export function _cyTableData(selectedItem){
	let output = [];
	let year = '';
	let currentYear = '';
	let currentObj = null;
	_.forEach( selectedItem, function(value, key){
		if(_.startsWith(key, 'cy_')){
			year = _.split(key, '_')[1];
			const name = (_.endsWith(key, 'kg')) ? 'kg': (_.endsWith(key, 'lbs')) ? 'lbs': 'tot';
			if(year != currentYear){
				if(currentObj != null){
					output = _.concat(output, [currentObj]);
				}
				currentYear = year;
				currentObj = Object.assign({}, { year:year, [name]:value });//{ year:year, [key]:value };
			}
			currentObj = Object.assign(currentObj, { [name]:value });
		}
	});
	//console.log('output',output);
	return output;
}
/**
 * [matchFilters description]
 * @param  {[type]} filters        [description]
 * @param  {[type]} enabledFilters [description]
 * @return {[type]}                [description]
 */
export function matchFilters(filters, enabledFilters){
	return filters;
/*	return filters.filter(function(value) {
		return something;
	});;*/
}

/**
 * removes from pagaData some columns according to the Unit Of Measure seleted by the user
 * @param  {[type]} pageData    [description]
 * @param  {[type]} selectedUOM [description]
 * @return {[type]}             [description]
 */
export function _removeColumnsByUnitOfMeasure(list, unselectedUOM){
	const unselectedUOMAbbr = unselectedUOM.abbr;
	const pageData = _.clone(list);
	//console.log('unselectedUOM', unselectedUOMAbbr);
	let output = [];
	let arrayOfKeys = _.keys(pageData[0]);
	arrayOfKeys = _.remove(arrayOfKeys, function(value){ return _.endsWith(value, unselectedUOMAbbr); });
	//console.log('arrayOfKeys', arrayOfKeys);
	_.forEach(pageData, function(obj){
		//const modifiedObj = _.omit(obj, arrayOfKeys);
		//console.log('modifiedObj', modifiedObj);
		output.push(_.omit(obj, arrayOfKeys));
		//console.log('output size',_.size(output));
	});
	//console.log('output', output);
	return output;
}
/**
 * [_cellValue description]
 * @param  {[type]} value [description]
 * @param  {[type]} key   [description]
 * @return {[type]}       [description]
 */
export function _cellValue(value, key = '', lookups = undefined){
	if(_.size(value) == 0 && _.startsWith(key, 'part_image')){
		return <div className="no-cell-thumb"></div>
	}
	if(_.endsWith(value, '.jpg') || _.endsWith(value, '.jpeg') || _.endsWith(value, '.png') || _.endsWith(value, '.gif') ){
		return <img className="cell-thumb" src={_buildMediaLink(value)} />
	}
	if(_.endsWith(value, '.pdf')){
		console.log(value);
		return <div className="file-cell-thumb file-cell-thumb-pdf"><a target="_blank" href={`${value}`}><i className="material-icons">description</i></a></div>
	}
	if(_.endsWith(value, '.xls')){
		console.log(value);
		return <div className="file-cell-thumb file-cell-thumb-xls"><a target="_blank" href={`${value}`}><i className="material-icons">description</i></a></div>
	}
	if(_.endsWith(value, '.txt')){
		console.log(value);
		return <div className="file-cell-thumb file-cell-thumb-txt"><a target="_blank" href={`${value}`}><i className="material-icons">description</i></a></div>
	}


	// return the value for lookups using id not value
	if(key === 'part_type' && !_.isUndefined(lookups)){
		return getName(lookups.part_type, value);
	}
	return value;
}
/**
 * [_mapInitialValues description]
 * @param  {[type]} prj     [description]
 * @param  {[type]} lookups [description]
 * @return {[type]}         [description]
 */
export function _mapInitialValues(prj, lookups){
	return Object.assign({}, prj, {
		ade: 					getUserName(lookups.ade, 				prj.ade, true),
		tech_service: 			getUserName(lookups.tech_service, 		prj.tech_service, true),
		sales_manager: 			getUserName(lookups.sales_manager,		prj.sales_manager, true),
		tier_1: 				getName(lookups.tier1, 					prj.tier_1),
		tier_2: 				getName(lookups.tier2, 					prj.tier_2),
		oem_material_spec:		getName(lookups.oem_material_spec, 		prj.oem_material_spec),
		oem_performance_spec:	getName(lookups.oem_performance_spec,	prj.oem_performance_spec),
		molder:					getName(lookups.molder, 				prj.molder),
		part_type:				getName(lookups.part_type, 				prj.part_type),
		mcpp_plant:				getName(lookups.mccp_plant, 			prj.mcpp_plant),
		probability_status:		getName(lookups.status, 				prj.probability_status),
		probability_percent:	getName(lookups.probability_percent, 	prj.probability_percent),
		year_moved_to_current:	getName(lookups.year_to_current, 		prj.year_moved_to_current),
		current_material:		getName(lookups.materials, 				prj.current_material),
		target_material:		getName(lookups.materials, 				prj.target_material),
	});
}

//sales_manager, ade e tech_service non arrivano da tabella lookup_ ma da users, quindi se vuoi il dettaglio di uno di questi chiami GET /api/users/{id}
export function getUserName(list, id){
	console.log(id, list);
	const o = _.find(list, function(item){
		//if(composite)
		//	console.log(item, 'item.id', item.id, 'id', id);
		return item.id == id;
	});
	console.log(id, o);
	//if(composite)
	//	return `${o.first_name} ${o.last_name}`;
	return o ? `${o.first_name} ${o.last_name}` : '---';
}
export function getName(list, id){
	//console.log(id, composite, list);
	const o = _.find(list, function(item){
		//if(composite)
		//	console.log(item, 'item.id', item.id, 'id', id);
		return item.id == id;
	});
	//console.log(id, o);
	//if(composite)
	//	return `${o.first_name} ${o.last_name}`;
	return o ? o.name : '';
}
export function getRoleName(list, id){
	//console.log(id, composite, list);
	const o = _.find(list, function(item){
		//if(composite)
		//	console.log(item, 'item.id', item.id, 'id', id);
		return item.role_id == id;
	});
	//console.log(id, o);
	//if(composite)
	//	return `${o.first_name} ${o.last_name}`;
	return o ? o.name : '';
}


export function userFromId(prop, value, users){
	//if(prop === 'submitted_by' || prop === 'assigned_to'){
			// todo: get from state.user
			//console.log(getName(lookups['action_plans_status'], value));
		return 'X'; //getName(lookups['action_plans_status'], value);
	//}
}


export function stringFromId(prop, value, lookups, usersList = []){
	// if defined by id ... transform in string value
	if(_.includes(lookups.definedById, prop)){
		//console.log('---->',prop, value);
		if(prop === 'ade' || prop === 'sales_manager' || prop === 'tech_service')
			return getUserName(lookups[prop], value);
		if(prop === 'role_id')
			return getRoleName(lookups[prop], value);
		if(prop === 'priority'){
			//console.log(getName(lookups['action_plans_priority'], value));
			return getName(lookups['action_plans_priority'], value);
		}
		if(prop === 'plan_status'){
			//console.log(getName(lookups['action_plans_status'], value));
			return getName(lookups['action_plans_status'], value);
		}
		if(prop === 'submitted_by' || prop === 'assigned_to'){
			return getUserName(usersList, value);
		}
		return getName(lookups[prop], value);
	}

	// return formatted data: date_time_submitted can be false so check it!!!
	if(prop === 'date' || prop === 'date_time_submitted' || prop === 'date_submitted' || prop === 'deadline')
		return moment.unix(value).format('MMMM Do YYYY');//MMMM Do YYYY, hh:mm:ss a
	// if it's an image ....
	if(_.endsWith(value, '.jpg') || _.endsWith(value, '.jpeg') || _.endsWith(value, '.png') || _.endsWith(value, '.gif') )
		return (<img className="cell-thumb" src={_buildMediaLink(value)} />)
	if(prop === 'comment')
		return _.truncate(value, {length:80, separator:' '});
	return value;
}


export function dateFromIntToString(value){
	return moment.unix(value).format('MMMM Do YYYY');
}

/**
 * init values for forms
 * @param  {[type]} data     [description]
 * @param  {[type]} toRemove fields to remove from default values
 * @return {[type]}          [description]
 */
export function createInitValues(data, toRemove=[]){
	let output = {};
	_.forEach(data, function(value, key){
		if(!_.includes(toRemove,key))
			output[key] = value;
	})
	console.log('initial values', output);
	return output;
}