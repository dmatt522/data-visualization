import React, {Component} 	from 'react';
import Pagination 			from 'react-js-pagination'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'
import ResultsDisplayToolbar from './ResultsDisplayToolbar'
import ResultsTable from './ResultsTable'

import { paginationChange, deleteContact } from '../actions/index'

class ResultsDisplay extends Component{
	constructor(props){
		super(props);
		this.state = {
			disableTools: true,
		};
		this.paginationChangeHandler = this.paginationChangeHandler.bind(this);
		this.enableToolbarTools = this.enableToolbarTools.bind(this);
	}

	paginationChangeHandler(pageNumber){
		// change the page
		this.props.paginationChange(pageNumber);
	}
	enableToolbarTools(rows){
		//console.log('disableTools', (_.size(rows) == 0));
		this.setState({ disableTools: (_.size(rows) == 0) });
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >

				<div className="comp-resultsdisplay">
					<ResultsDisplayToolbar
						toggleColumns={ this.toggleColumns }
						disableTools={ this.state.disableTools }
						actionHandler={ this.props.toolbarActionHandler} />
					<ResultsTable
						defaultColumns={this.props.defaultColumns}
						data={ this.props.data.pageData }
						enableToolbarTools={ this.enableToolbarTools } />
					<div className="pagination-container">
						<Pagination
							activePage={ this.props.data.activePage }
							itemsCountPerPage={ this.props.data.itemsPerPage }
							totalItemsCount={ _.size(this.props.data.filteredList) }
							pageRangeDisplayed={ 5 }
							onChange={ this.paginationChangeHandler }
							/>
					</div>

				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
ResultsDisplay.propTypes = {};
ResultsDisplay.defaultProps = {
	defaultColumns:[]
};

export default ResultsDisplay;