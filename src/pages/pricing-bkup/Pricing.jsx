import React, {Component} from 'react'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import Dialog from 'material-ui/Dialog'

import FiltersGroup 	from './components/FiltersGroup'
import ResultsDisplay 	from './components/ResultsDisplay'
import Details 			from './components/dialog/Details'
import Delete 			from './components/dialog/Delete'

import PageTitleBar 		from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'

import styles from '../../styles/styles'

function whoIsTheChild(name, closeDialogHandler){
	switch(name){
		case 'delete':
			return <Delete closeHandler={ closeDialogHandler } />;
		case 'details':
			return <Details closeHandler={ closeDialogHandler } />;
		default:
			return <Details closeHandler={ closeDialogHandler } />;
	}
}

class Pricing extends Component{
	constructor(props){
		super(props);
		this.state = {
			openDialog:false,
			actionName:''
		};
	}
	// close dialog
	closeDialogHandler(){
		this.setState({openDialog:false});
	}
	//
	toolbarActionHandler(obj){
		this.setState({openDialog:true, actionName:obj.actionName});
	}
	resetFiltersHandler(){
		//this.props.resetFilterProjects();
	}
	applyFiltersHandler(){
		//this.props.applyFilterProjects();
	}
	bufferFiltersHandler(filter){
		//this.props.bufferFilterProjects(filter);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {
		//this.props.resetFilterPricing();
	}

	render(){
		// section to show in dialog
		const dialogChild = whoIsTheChild(this.state.actionName, this.closeDialogHandler);

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false}
			>
				<div className="comp-pricing page pricing-page">

					<PageTitleBar title="Pricing" />

					<ul className="collapsible" data-collapsible="expandable">
						{/* start filters here */}
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Search" />
							</div>
						  	<div className="collapsible-body">
								<FiltersGroup />
						  	</div>
						</li>
						{/* end filters here */}
						{/* start table here */}
						<li>
						  	<div className="collapsible-header active">
								<CollapsibleHeader  title="Results" /></div>
						  	<div className="collapsible-body">
								<ResultsDisplay />
						  	</div>
						</li>
						{/* end table here */}
					</ul>
				</div>

				{/* start dialog */}
				<Dialog
					autoScrollBodyContent={true}
					children={dialogChild}
					open={this.state.openDialog}
					onRequestClose={ () => this.setState({openDialog:false}) }
					contentStyle={styles.projectDialog} />
				{/* end dialog */}


			</ReactCSSTransitionGroup>
		);
	}
}
Pricing.propTypes = {};
Pricing.defaultProps = {};

export default Pricing;