import React, {Component} from 'react';
import {connect} from 'react-redux'

import DialogToolbar from './DialogToolbar'

import CollapsibleHeader from '../../../../shared/components/CollapsibleHeader'

import { _valueAt, stringFromId } from '../../../../utils/functions'

class Details extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}
	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		const data = this.props.contacts.selectedItem;

		return(
			<div className="dialog-details">
				<DialogToolbar title="Contact details" closeHandler={this.closeHandler} />
				<ul className="collapsible project-details" data-collapsible="expandable">
						{/* start filters here */}
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Media" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">
									<div className="col s12"><img src={ data.photo } className="responsive-img" /></div>
								</div>
						  	</div>
						</li>
						{/* end filters here */}
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Info" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">
									<div className="col s12 m6"><span>First name</span> {_valueAt(data, 'first_name')}</div>
									<div className="col s12 m6"><span>Last name</span> {_valueAt(data, 'last_name')}</div>
									<div className="col s12 m6"><span>Company name</span> {_valueAt(data, 'company_name')}</div>
									<div className="col s12 m6"><span>Title</span> {_valueAt(data, 'title')}</div>
									<div className="col s12 m6"><span>Department</span> {_valueAt(data, 'department')}</div>
									<div className="col s12 m6"><span>Address</span> {_valueAt(data, 'address')}</div>
									<div className="col s12 m6"><span>Address 2</span> {_valueAt(data, 'address_2')}</div>
									<div className="col s12 m6"><span>Postal code</span> {_valueAt(data, 'zip_code_postal_code')}</div>
									<div className="col s12 m6"><span>City</span> {_valueAt(data, 'city_town')}</div>
									<div className="col s12 m6"><span>State province</span> {_valueAt(data, 'state_province')}</div>
									<div className="col s12 m6"><span>Country</span> {_valueAt(data, 'country')}</div>
									<div className="col s12 m6"><span>Email</span> {_valueAt(data, 'email')}</div>
								</div>
						  	</div>
						</li>

						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Phone numbers" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">
									<div className="col s12 m6"><span>Phone work</span> {_valueAt(data, 'phone_work')}</div>
									<div className="col s12 m6"><span>Fax</span> {_valueAt(data, 'phone_fax')}</div>
									<div className="col s12 m6"><span>Phone cell</span> {_valueAt(data, 'phone_cell')}</div>
									<div className="col s12 m6"><span>Phone home</span> {_valueAt(data, 'phone_home')}</div>
								</div>
						  	</div>
						</li>


						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Social" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">
									<div className="col s12 m6"><span>Website</span> {_valueAt(data, 'website_url')}</div>
									<div className="col s12 m6"><span>Twitter</span> {_valueAt(data, 'twitter')}</div>
									<div className="col s12 m6"><span>LinkedIn</span> {_valueAt(data, 'linkedIn')}</div>
									<div className="col s12 m6"><span>Google</span> {_valueAt(data, 'google')}</div>
									<div className="col s12 m6"><span>Facebook</span> {_valueAt(data, 'facebook')}</div>
								</div>
						  	</div>
						</li>

						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Notes" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">
									<div className="col s12">{_valueAt(data, 'notes')}</div>
								</div>
						  	</div>
						</li>


				</ul>

			</div>
		);
	}
}
Details.propTypes = {};
Details.defaultProps = {};

function mapStateToProps({contacts, lookups}){
	return {contacts, lookups};
}
export default connect(mapStateToProps)(Details);