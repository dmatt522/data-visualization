import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'

import _ from 'lodash'

import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
import DialogToolbar from './DialogToolbar'

import {editContact, fetchContacts} from '../../actions/index'
import {notify} from '../../../../shared/actions/index'

function validate(values){
	//console.log('---->', values);
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	if(_.size(values.first_name) === 0)
		errors.first_name = 'First name is Required';
	if(_.size(values.last_name) === 0)
		errors.last_name = 'Last name is Required';
	if(!pattern.test(values.email))
		errors.email = 'Please use a valid email address';
	return errors;
}

const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div>
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)
const renderTextArea = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	    <label>Write notes here</label>
	    <div>
	    	<textarea name="notes"></textarea>
	    </div>
	</div>
)


class Edit extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}

	onFormSubmit(formData){
		console.log('Edit.onFormSubmit', formData);
		this.props.editContact(formData);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		const contacts = this.props.contacts;
		if(prevProps.contacts.operationCodeSuccess !== contacts.operationCodeSuccess){
			this.props.fetchContacts();
			this.props.notify({message:'User edited'});
			this.props.closeHandler();
		}
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;
		return(
			<div className="dialog-edit">
				<DialogToolbar title="Edit user data" closeHandler={this.closeHandler} />
				<div className="row dialog-addnew-form-container">
					<form onSubmit={handleSubmit(this.onFormSubmit)}>

						<Field name="first_name" label="First name" component={renderField} type="text" required={true} />
						<Field name="last_name" label="Last name" component={renderField} type="text" required={true} />
						<Field name="email" label="Email" component={renderField} type="email" required={true} />
						<Field name="company_name" label="Company" component={renderField} type="text" required={true} />
						<Field name="title" label="Title" component={renderField} type="text" />
						<Field name="department" label="Department" component={renderField} type="text" />
						<Field name="address" label="Address" component={renderField} type="text" />
						<Field name="address_2" label="Address 2" component={renderField} type="text" />
						<Field name="zip_code_postal_code" label="Postal code" component={renderField} type="text" />
						<Field name="state_province" label="State province" component={renderField} type="text" />
						<Field name="city_town" label="City" component={renderField} type="text" />
						<Field name="country" label="Country" component={renderField} type="text" />
						<Field name="phone_work" label="Phone work" component={renderField} type="text" />
						<Field name="phone_fax" label="Fax" component={renderField} type="text" />
						<Field name="phone_cell" label="Phone cell" component={renderField} type="text" />
						<Field name="phone_home" label="Phone home" component={renderField} type="text" />
						<Field name="website_url" label="Website" component={renderField} type="text" />
						<Field name="twitter" label="Twitter" component={renderField} type="text" />
						<Field name="linkedIn" label="LinkedIn" component={renderField} type="text" />
						<Field name="google" label="Google" component={renderField} type="text" />
						<Field name="facebook" label="Facebook" component={renderField} type="text" />
						<div>
									<div>
								    	<label>Write notes here</label>
								    	<Field name="notes" label="Notes" component="textarea"/>
								    </div>
								</div>


						<RaisedButton type="button" label="Reset" primary={false} disabled={ submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ submitting }  />
					</form>
				</div>
			</div>
		);
	}
}
Edit.propTypes = {};
Edit.defaultProps = {};

function createInitValues(data, toRemove){
	let output = {};
	_.forEach(data, function(value, key){
		if(!_.includes(toRemove,key))
			output[key] = value;
	})
	console.log('output', output);
	return output;
}

function mapStateToProps(state){
	return{
		contacts: state.contacts,
		user: state.user,
		initialValues: createInitValues(state.contacts.selectedItem, ['type'])
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({ editContact, fetchContacts, notify }, dispatch);
}

Edit = reduxForm({form:'editContactDataForm', validate})(Edit);
//Edit = ReduxFormConsumer(Edit, 'editContactDataForm', validate);
export default connect(mapStateToProps, mapDispatchToProps)(Edit);