import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import CollapsibleHeader from '../../../../shared/components/CollapsibleHeader'

import _ from 'lodash'

import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
import DialogToolbar from './DialogToolbar'

import {submitNewContact} from '../../actions/index'
import {notify} from '../../../../shared/actions/index'

function validate(values){
	//console.log('---->', values);
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	if(_.size(values.first_name) === 0)
		errors.first_name = 'Required';
	if(_.size(values.last_name) === 0)
		errors.last_name = 'Required';
	if(!pattern.test(values.email))
		errors.email = 'Invalid email';
	if(_.size(values.company_name) === 0)
		errors.company_name = 'Required';

	return errors;
}

const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div>
	{/*<div className="col s12 m6 l4">*/}
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)

const renderTextArea = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	{/*<div className="col s12">*/}
		<div>
	    	<label>Write notes here</label>
	    	<textarea {...input}></textarea>
	    </div>
	</div>
)



const renderRadio = ({input, children, label}) => (
	<div>
		<label>{label}</label>
		<RadioButtonGroup {...input} children={children}
			defaultSelected="0"
			onChange={ (event, value) => console.log(value) } />
	</div>
)


class AddNew extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}

	onFormSubmit(formData){
		//console.log(this, 'submitting form data', formData);
		this.props.submitNewContact(formData);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
		console.log('this.props',this.props.contacts.operationCode9);
		console.log('prevProps',prevProps.contacts.operationCode9);
		const contacts = this.props.contacts;
		if(prevProps.contacts.operationCode9 !== contacts.operationCode9){
			this.props.notify({message:'The contact already exists'});
		}

		if(prevProps.contacts.operationCodeSuccess !== contacts.operationCodeSuccess){
			this.props.notify({message:'Contact created'});
			this.props.closeHandler();
		}
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;
		return(
			<div className="dialog-addnew">
				<DialogToolbar title="Add new contact" closeHandler={this.closeHandler} />
				<div className="dialog-addnew-form-container">
				<form onSubmit={handleSubmit(this.onFormSubmit)}>

					<Field name="contact_id" component="input" type="hidden" />

					{/*<ul className="collapsible project-details" data-collapsible="expandable">
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Info" />
							</div>
						  	<div className="collapsible-body">*/}
								<div className="row">
									<Field name="first_name" label="First name" component={renderField} type="text" required={true} />
									<Field name="last_name" label="Last name" component={renderField} type="text" required={true} />
									<Field name="email" label="Email" component={renderField} type="email" required={true} />
									<Field name="company_name" label="Company" component={renderField} type="text" required={true} />
									<Field name="title" label="Title" component={renderField} type="text" />
									<Field name="department" label="Department" component={renderField} type="text" />
									<Field name="address" label="Address" component={renderField} type="text" />
									<Field name="address_2" label="Address 2" component={renderField} type="text" />
									<Field name="zip_code_postal_code" label="Postal code" component={renderField} type="text" />
									<Field name="state_province" label="State province" component={renderField} type="text" />
									<Field name="city_town" label="City" component={renderField} type="text" />
									<Field name="country" label="Country" component={renderField} type="text" />
								{/*</div>
						  	</div>
						</li>
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Phone numbers" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">*/}
									<Field name="phone_work" label="Phone work" component={renderField} type="text" />
									<Field name="phone_fax" label="Fax" component={renderField} type="text" />
									<Field name="phone_cell" label="Phone cell" component={renderField} type="text" />
									<Field name="phone_home" label="Phone home" component={renderField} type="text" />
								{/*</div>
						  	</div>
						</li>
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Social" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">*/}
									<Field name="website_url" label="Website" component={renderField} type="text" />
									<Field name="twitter" label="Twitter" component={renderField} type="text" />
									<Field name="linkedIn" label="LinkedIn" component={renderField} type="text" />
									<Field name="google" label="Google" component={renderField} type="text" />
									<Field name="facebook" label="Facebook" component={renderField} type="text" />
								{/*</div>
						  	</div>
						</li>
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Notes" />
							</div>
						  	<div className="collapsible-body">
								<div className="row">*/}
								<div>
									<div>
								    	<label>Write notes here</label>
								    	<Field name="notes" label="Notes" component="textarea"/>
								    </div>
								</div>

								</div>
						  	{/*</div>
						</li>


					</ul>*/}





						<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
					</form>
				</div>
			</div>
		);
	}
}
AddNew.propTypes = {};
AddNew.defaultProps = {};

function mapStateToProps(state){
	return{
		contacts: state.contacts,
		user: state.user,
		initialValues: {
			contact_id:'0'
		}
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({submitNewContact, notify}, dispatch);
}

AddNew = reduxForm({form:'addNewForm', validate})(AddNew);
//AddNew = ReduxFormConsumer(AddNew, 'addNewForm', validate);
export default connect(mapStateToProps, mapDispatchToProps)(AddNew);