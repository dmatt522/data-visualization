import React, {Component} from 'react';

class ContactsLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<div className="page-layout users-page-layout">
				{this.props.children}
			</div>
		);
	}
}
ContactsLayout.propTypes = {};
ContactsLayout.defaultProps = {};

export default ContactsLayout;