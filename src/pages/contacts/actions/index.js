import axios 	from 'axios'
import globals 	from '../../../config/globals'
import {
	ALL_CONTACTS,
	CONTACT_DELETE,
	CONTACT_ADD,
	CONTACT_EDIT
	} from '../../../config/api'
import {
	FETCH_CONTACTS,
	PAGINATION_CONTACTS_CHANGE,
	SORT_CONTACTS,
	APPLY_FILTERS_CONTACTS,
	RESET_FILTER_CONTACTS,
	BUFFER_FILTER_CONTACTS,
	SELECT_CONTACT,
	DELETE_CONTACT,
	ADD_CONTACT,
	EDIT_CONTACT
	} from '../../../actions/types'



// warning: use api urls not locals

/**
 * fetches all vehicles data
 * @return {[type]} [description]
 */
export function fetchContacts(){

	//const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
	//const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
	//const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
	const request = axios.get(ALL_CONTACTS);
	//console.log('Action: fetchVehicles', request);
	//debugger;
	return {
		type: FETCH_CONTACTS,
		payload: request
	};
}


export function paginationChange(page){
	return {
		type:PAGINATION_CONTACTS_CHANGE,
		payload: page
	}
}

export function sortList(sortKey){
	return {
		type: SORT_CONTACTS,
		payload: sortKey
	}
}

export function resetFilters(){
	return{
		type: RESET_FILTER_CONTACTS,
		payload: {}
	}
}
export function applyFilters(){
	return{
		type:APPLY_FILTERS_CONTACTS,
		payload:{}
	}
}
export function bufferFilter(filter){
	return{
		type: BUFFER_FILTER_CONTACTS,
		payload: filter
	}
}

export function selectItem(rows){
	console.log('action selectItem', rows);
	return {
		type: SELECT_CONTACT,
		payload: rows
	}
}

export function deleteContact(selectedItem){
	const request = axios.post(CONTACT_DELETE, {scope:'contacts', id:selectedItem.contact_id});
	console.log(request);
	return {
		type: DELETE_CONTACT,
		payload: request,
		selectedItem:selectedItem
	}
}

export function submitNewContact(formData){
	const request = axios.post(CONTACT_ADD, formData);
	console.log('submitNewContact action formData', request);
	return {
		type: ADD_CONTACT,
		payload: request
	}
}


export function editContact(formData){
	console.log('Contacts-actions-editContact', formData);
	const request = axios.post(CONTACT_EDIT, formData);
	return {
		type: EDIT_CONTACT,
		payload: request
	}
}