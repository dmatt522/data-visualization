import React from 'react';
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'

const LayoutToolbar = ({ styles }) => {
    return (
		<Toolbar>
			<ToolbarGroup firstChild={ true }>
				<ToolbarTitle className="page-title" text="Contacts" />
			</ToolbarGroup>
			<ToolbarGroup lastChild={ true }>
				<IconMenu
					iconButtonElement={ <IconButton><MoreVertIcon /></IconButton> }
					anchorOrigin={{horizontal: 'right', vertical: 'top'}}
				    targetOrigin={{horizontal: 'right', vertical: 'top'}} >
				    <MenuItem primaryText="item 1" onTouchTap={ () => console.log('') } />
				    <MenuItem primaryText="item 2" onTouchTap={ () => console.log('') }/>
				</IconMenu>
			</ToolbarGroup>
		</Toolbar>
    );
};

export default LayoutToolbar;
