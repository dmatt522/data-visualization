import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReduxFormConsumer from '../../hocs/reduxFormConsumer'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { Field } from 'redux-form'

import RaisedButton from 'material-ui/RaisedButton'

import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar	from '../../shared/toolbars/PageTitleBar'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import CollapsibleHeader from '../../shared/components/CollapsibleHeader'
//import SimpleText from '../../shared/ui/SimpleText'
import BaseComponent 	from '../../shared/components/BaseComponent'

// actions
//
// texts
import {
	CONTACT_EDIT_PAGE_DESCRIPTION,
	CONTACT_EDIT_TITLE
} from '../../config/strings'

import { renderTextField, renderTextArea } from '../../utils/form-field-renderers'
import { _valueAt, _normalizeLink, refactorKey } 	from '../../utils/functions'

import globals from '../../config/globals'

class ContactEdit extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}

onFormSubmit(data){
		console.log('ContactEdit form submit', data);
		// call action creator ...
	}

	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}

	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;
		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<section className="comp-contact-editl page contact-edit-page">

					<Breadcrumbs />

					<PageTitleBar title={ CONTACT_EDIT_TITLE } />

					{/*<SimpleText text={ CONTACT_EDIT_PAGE_DESCRIPTION } />*/}
					<div className="row" style={{marginBottom:0}}>
						<div className="col s12">
							<h5 className="details-title">{ _valueAt(this.props.contacts.selectedItem, 'first_name') } { _valueAt(this.props.contacts.selectedItem, 'last_name') }</h5>
						</div>
					</div>

					<form className="contact-edit-form">
						<ul className="collapsible" data-collapsible="expandable">
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title="Image" />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
							           	<div className="col s12 m2">
											<img src={`${globals.basePath}/images/profile_placeholder.jpg`} className="responsive-img form-edit-image" />
										</div>
										<div className="col s12 m4 file-field input-field file-upload">
											<div className="btn">
												<span>File</span>
												<Field type="file" component="input" name="photo" />
											</div>
											<div className="file-path-wrapper">
												<input className="file-path validate" type="text" />
											</div>
										</div>
						        	</div>
						        </div>
						    </li>
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title="Name & Address" />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
							           	<Field name="first_name" type="text" component={renderTextField} label="First Name" />
							           	<Field name="last_name" type="text" component={renderTextField} label="Last Name" />
							           	<Field name="title" type="text" component={renderTextField} label="Title" />
							           	<Field name="department" type="text" component={renderTextField} label="Department" />
							           	<Field name="address" type="text" component={renderTextField} label="Address" />
							           	<Field name="address_2" type="text" component={renderTextField} label="Address 2" />
							           	<Field name="city_town" type="text" component={renderTextField} label="City_Town" />
							           	<Field name="zip_code_postal_code" type="text" component={renderTextField} label="Postal Code" />
							           	<Field name="country" type="text" component={renderTextField} label="Country" />
							           	<Field name="notes" component={renderTextArea} rows="6" label="Notes" />
						        	</div>
						        </div>
						    </li>
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title="Phone" />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
						            	<Field name="phone_work" type="tel" component={renderTextField} label="Work" />
						            	<Field name="phone_cell" type="tel" component={renderTextField} label="Cell" />
						            	<Field name="phone_home" type="tel" component={renderTextField} label="Home" />
						            	<Field name="phone_fax" type="tel" component={renderTextField} label="Fax" />
						        	</div>
						        </div>
						    </li>
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title="Web" />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
							            <Field name="email" type="email" component={renderTextField} label="Email" />
							            <Field name="website_url" type="url" component={renderTextField} label="Website" />
							            <Field name="linkedIn" type="url" component={renderTextField} label="LinkedIn" />
							            <Field name="facebook" type="url" component={renderTextField} label="Facebook" />
							            <Field name="google" type="url" component={renderTextField} label="Google" />
							            <Field name="twitter" type="url" component={renderTextField} label="Twitter" />
						        	</div>
						        </div>
						    </li>
						</ul>
						<div className="row" style={{textAlign:'center'}}>
							<div className="col s10 offset-s1">
								<RaisedButton type="button" label="Reset Changes" primary={false} disabled={ pristine || submitting } onClick={reset} />
								<RaisedButton type="submit" label="Update Vehicle Data" primary={true} disabled={ pristine || submitting }  />
							</div>
						</div>
					</form>


				</section>
			</ReactCSSTransitionGroup>
		);
	}
}
ContactEdit.propTypes = {};
ContactEdit.defaultProps = {};

function mapStateToProps(state){
	return {
		contacts: state.contacts,
		initialValues: state.contacts.selectedItem
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({}, dispatch);
}
ContactEdit = ReduxFormConsumer(ContactEdit, 'contactEditForm');
export default connect(mapStateToProps, mapDispatchToProps)(ContactEdit);