import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
//import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import { Link } from 'react-router'

import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar	from '../../shared/toolbars/PageTitleBar'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import ToolsToolbar from '../../shared/toolbars/ToolsToolbar'
import TooltippedIconButton from '../../shared/ui/TooltippedIconButton'
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'
import CollapsibleHeader from '../../shared/components/CollapsibleHeader'
//import SimpleText from '../../shared/ui/SimpleText'
import BaseComponent 	from '../../shared/components/BaseComponent'

import globals 		from '../../config/globals'
import { _valueAt, _normalizeLink, refactorKey } 	from '../../utils/functions'
// actions
//
// texts
import {
	CONTACT_DETAILS_PAGE_DESCRIPTION,
	CONTACT_DETAILS_TITLE,
	CONTACT_DETAILS_PROJECTS
} from '../../config/strings'

function createColumn(item, name){
	return <div className="col s12 m6"><span className="as-label">{refactorKey(name)}</span>{_valueAt(item, name)}</div>
}

function createSocialLink(item, name, iconName){
	//console.log(_valueAt(item, name));
	if(_valueAt(item, name) == '')
		return '';

	return <a href={_normalizeLink(_valueAt(item, name))} target="_blank"><span className={iconName}></span></a>
}



class ContactDetails extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}


	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}

	componentWillMount() {

	}

	render(){
		console.log('ContactDetails routes', this.props.routes);
		const item = this.props.contacts.selectedItem;

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<section className="comp-contact-detail page contact-detail-page">

					<Breadcrumbs />

					<PageTitleBar title={ CONTACT_DETAILS_TITLE }>
						<Link to={`${globals.basePath}/contacts/edit/${_valueAt(item, 'contact_id')}`}>
							<TooltippedIconButton text="Edit"><EditorModeEdit /></TooltippedIconButton>
						</Link>
					</PageTitleBar>

					{/*<SimpleText text={ CONTACT_DETAILS_PAGE_DESCRIPTION } />*/}
					<div className="row" style={{marginBottom:0}}>
						<div className="col s12">
							<h5 className="details-title">{ _valueAt(item, 'first_name') } { _valueAt(item, 'last_name') }</h5>
						</div>
					</div>




					<div className="row">
						<div className="col s12">
						<ul className="collapsible" data-collapsible="expandable">
						{/* contact  details*/}
						<li>
						  <div className="collapsible-header active"><CollapsibleHeader  title={ CONTACT_DETAILS_TITLE } /></div>
						  <div className="collapsible-body">
						  	<div className="row">
						  		{/* image */}
						  		<div className="col s12 m7 l4">
									<img src={`${globals.basePath}/images/profile_placeholder.jpg`} className="responsive-img" />
								</div>

								<div className="col s12 m5 l8 item-details-zone">

									<div className="row">
										{createColumn(item, 'title')}
										{createColumn(item, 'department')}
										{createColumn(item, 'company_name')}
										{createColumn(item, 'address')}
										{createColumn(item, 'address_2')}
									</div>
									<h5>Phone</h5>
									<div className="row">
										{createColumn(item, 'phone_work')}
										{createColumn(item, 'phone_cell')}
										{createColumn(item, 'phone_fax')}
										{createColumn(item, 'phone_home')}
									</div>
									<h5>Links</h5>
									<div className="item-details-zone_socials">
										{createSocialLink(item, 'email', 'icon-mail')}
										{createSocialLink(item, 'website_url', 'icon-earth')}
										{createSocialLink(item, 'linkedIn', 'icon-linkedin')}
										{createSocialLink(item, 'facebook', 'icon-facebook2')}
										{createSocialLink(item, 'google', 'icon-google-plus2')}
										{createSocialLink(item, 'twitter', 'icon-twitter2')}
										{/*<a href={`mailto:${_valueAt(item, 'Email')}`}><span className="icon-mail"></span></a>
										<a href={_normalizeLink(_valueAt(item, 'Website_URL'))} target="_blank"><span className="icon-earth"></span></a>
										<a href={_normalizeLink(_valueAt(item, 'LinkedIn'))} target="_blank"><span className="icon-linkedin"></span></a>
										<a href={_normalizeLink(_valueAt(item, 'Facebook'))} target="_blank"><span className="icon-facebook2"></span></a>
										<a href={_normalizeLink(_valueAt(item, 'Google'))} target="_blank"><span className="icon-google-plus2"></span></a>
										<a href={_normalizeLink(_valueAt(item, 'Twitter'))} target="_blank"><span className="icon-twitter2"></span></a>*/}
									</div>


								</div>

						  	</div>
						  </div>
						</li>
						{/* contact projects */}
						<li>
						  <div className="collapsible-header active"><CollapsibleHeader  title={ CONTACT_DETAILS_PROJECTS } /></div>
						  <div className="collapsible-body">

						  </div>
						</li>
					  </ul>
					</div>
					</div>
				</section>
			</ReactCSSTransitionGroup>
		);
	}
}
ContactDetails.propTypes = {};
ContactDetails.defaultProps = {};

function mapStateToProps({contacts}){
	return { contacts: contacts };
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({}, dispatch );
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactDetails);