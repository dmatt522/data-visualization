import React, {Component} 			from 'react'
import { connect } 							from 'react-redux'
import { bindActionCreators } 	from 'redux'
import { Field } 								from 'redux-form'
import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import _ from 'lodash'

import Dialog from 'material-ui/Dialog'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'

import ReduxFormConsumer 	from '../../hocs/reduxFormConsumer'
import BaseComponent 			from '../../shared/components/BaseComponent'
import InputField 				from '../../shared/input/InputField'
import HelpButton 				from '../../shared/buttons/HelpButton'
import ComposableToolbar 	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar 	from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'
//import ProjectLookup			from '../../shared/dialogs/ProjectLookup'

import { submitProject, fetchProjects } from './actions/index'

import {notify} from '../../shared/actions/index'
import { newItem } from '../../shared/actions/lookups'
import globals from '../../config/globals'
import { _valueAt, _cySeries } from '../../utils/functions'
import { renderDialogTextFieldLookup, renderDialogSelectFieldLookup, idSelect, userSelect } from '../../utils/form-field-renderers'


const st = {
	cfa:{
		paddingTop: 16
	}
};
/*
const createColumn = (label, item, prop, unit='') => {
	return 	(
		<div className="col s12 m4 l3">
			<span className="as-label">{label}</span> { `${_valueAt(item, prop)}${unit}` }
	    </div>
	)
};*/

/*
checks if data are valid
 */
function _isValidSubmission(formData){
	const partType = formData.part_type;
	const currentMaterial = formData.current_material;
	console.log('@>','partType', partType, 'currentMaterial', currentMaterial);
	if(_.isUndefined(partType) || _.size(partType) == 0 || _.isUndefined(currentMaterial) || _.size(currentMaterial) == 0)
		return false;
	console.log('valid submission');
	return true;
}
/*
checks if there's another prj with the same part_type, current_material, vehicle_id
 */
function _projectExists(projects, formData){
	const partType = formData.part_type;
	const currentMaterial = formData.current_material;
	const vehicleId = formData.vehicle_id;
	console.log('->', 'partType', partType, 'currentMaterial', currentMaterial);
	let result = [];
	result = _.filter(projects, function(project){
		if(project.vehicle_id === vehicleId && project.current_material === currentMaterial && project.part_type === partType){
			return project;
		}
	});


	console.log('_projectExists', result);
	if(_.size(result) > 0)
		return true;
	return false;
}


class ProjectAdd extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {
			showDialog: false,
			title:'',
			fieldScope:'',
			fieldValue: ''
		};

		this.openDialog = this.openDialog.bind(this);
		this.updateLookup = this.updateLookup.bind(this);
		this.setFieldValue = this.setFieldValue.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}


	componentWillReceiveProps(nextProps) {
		//console.log('project add componentWillReceiveProps ', nextProps);

	    if(_.size(this.props.projects.list) !== _.size(nextProps.projects.list)){
	    	console.log('componentWillReceiveProps', _.size(this.props.projects.list), _.size(nextProps.projects.list));
	    	console.log('go to projects page');
	    	this.props.notify({message:'Project succesfully added!'});
	    	this.context.router.push('/projects');
	    }

	}

	componentDidMount() {
	    $('select').material_select();
	    $('.collapsible').collapsible({accordion : false });
	}
	componentDidUpdate(prevProps, prevState) {
		$('select').material_select();
		$('.collapsible').collapsible({accordion : false });
		console.log('this.props',this.props.projects.operationCode9);
		console.log('prevProps',prevProps.projects.operationCode9);
		if(!_.isUndefined(this.props.projects.operationCode9) && prevProps.projects.operationCode9 !== this.props.projects.operationCode9){
			this.props.notify({message:'The project already exists'});
		}
	}
	onFormSubmit(formData){
		console.log('submitting new prj', formData);
		this.props.notify({message:'Submitting new project'});

		if(_isValidSubmission(formData) && !_projectExists(this.props.projects, formData)){
			// submit the new project
			this.props.submitProject(formData);
			// refresh project_reducer
			_.delay(function(me){
				me.props.fetchProjects();
			}, 4000, this);
		}
		else{
			//console.log('send a message for add project submission cancealed');
			this.props.notify({message:'Please fill required fields: Part Type, Current Material'});
		}
	}
	setFieldValue(event){
		console.log('event.target.value', event.target.value);
		this.setState({fieldValue: event.target.value});
	}
	updateLookup(){
		this.props.newItem({scope: this.state.fieldScope, value: this.state.fieldValue});
		this.props.notify({message:'Data submission in progress'});
		this.setState({ showDialog:false, fieldValue:'', fieldScope:'', dialogTitle:'' });
		_.delay(function(me){
			me.setState({ optionsRefreshEnforcer: _.now() })
		}, 3000, this);
	}
	openDialog(event, title, fieldName, scope){
		event.preventDefault();
		//console.log(title, fieldName);
		this.setState({ showDialog:true, dialogTitle:title, fieldScope:scope  });
		//this.lookupDialog.openFromParent({title: title});
	}

	render(){
		const { handleSubmit, pristine, reset, submitting, lookups } = this.props;
		const item = this.props.vehicles.selectedItem;
		const years = _cySeries(item);
		const dialogActions = [
			<FlatButton label="Confirm" primary={ true } onTouchTap={ this.updateLookup } />,
			<FlatButton label="Cancel" primary={ false } onTouchTap={ () => this.setState({ showDialog:false }) } />
		];


		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<div className="comp-projectadd page projectadd-page">

				<PageTitleBar title="Add project" />

				{/* START copy of vehicle details */}

					<div className="comp-vehicledetails page vehiclesdetails-page">
						<div className="row comp-vehicledetails">
							<div className="col s12">
								<h5 className="details-title">{ _valueAt(item, 'manufacturer') } { _valueAt(item, 'production_brand') }</h5>
							</div>
						</div>
						<div className="row">
							<div className="col s12 m4">
								<img src={ item.vehicle_image } className="responsive-img" />
							</div>
							<div className="col s12 m8">
							<ul className="collapsible" data-collapsible="expandable">
							    <li>
							        <div className="collapsible-header">
							            <CollapsibleHeader title="Info" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
							                <div className="col s12 m6">
												<span>Manufacturer</span> { _valueAt(item, 'manufacturer') }
											</div>
											<div className="col s12 m6">
												<span>Brand</span> { _valueAt(item, 'production_brand') }
											</div>
											<div className="col s12 m6">
												<span>Subsegment</span> { _valueAt(item, 'global_sales_subsegment') }
											</div>
											<div className="col s12 m6">
												<span>Segment</span> { _valueAt(item, 'regional_sales_segment') }
											</div>

											<div className="col s12 m6">
												<span>Nameplate</span> { _valueAt(item, 'production_nameplate') }
											</div>
											<div className="col s12 m6">
												<span>Platform</span> { _valueAt(item, 'platform') }
											</div>
											<div className="col s12 m6">
												<span>Program</span> { _valueAt(item, 'program') }
											</div>

											<div className="col s12 m6">
												<span>Production Plant</span> { _valueAt(item, 'production_plant') }
											</div>
											<div className="col s12 m6">
												<span>Country</span> { _valueAt(item, 'country') }
											</div>
											<div className="col s12 m6">
												<span>Location</span> { _valueAt(item, 'location') }
											</div>
											<div className="col s12 m6">
												<span>Start of Production</span> { _valueAt(item, 'sop') }
											</div>
											<div className="col s12 m6">
												<span>End of Production</span> { _valueAt(item, 'eop') }
											</div>
							            </div>
							        </div>
							    </li>
							    <li>
							        <div className="collapsible-header">
							            <CollapsibleHeader title="Build" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
											{
												years.map( (item, i) => <div className="col s12 m6" key={ i }><span>{ item.label }</span> <span>{ item.value }</span></div> )
											}
							            </div>
							        </div>
							    </li>
							</ul>

						</div>
						</div>
					</div>

				{/* END copy of vehicle details */}

					<div>
						{/*<ComposableToolbar title="Add project" />*/}

						<div className="row">
							<div className="col s12">
							<form onSubmit={handleSubmit(this.onFormSubmit)}>

								{/* hidden fields */}
								<Field type="hidden" name="vehicle_id" component="input" />
								<Field type="hidden" name="submitted_by" component="input" />


								<ul className="collapsible" data-collapsible="expandable">
									{/* media */}
									<li>
										<div className="collapsible-header active">
								            <CollapsibleHeader title="Media" />
								        </div>
								        <div className="collapsible-body">
								        	<div className="row">
								        		<div className="col s12 m3">
									            	content here
									            </div>
								        	</div>
								        </div>
									</li>
									{/* commercial */}
									<li>
										<div className="collapsible-header active">
								            <CollapsibleHeader title="Commercial" />
								        </div>
								        <div className="collapsible-body">
								            <div className="row">
								            	{lookups.probability_status && <Field name="probability_status" options={lookups.probability_status} component={idSelect} label="Status" /> }



								            	{lookups.probability_percent && <Field name="probability_percent" options={lookups.probability_percent} component={idSelect} label="Probability (%)" />}

								            	{lookups.year_moved_to_current && <Field name="year_moved_to_current" options={lookups.year_moved_to_current} component={idSelect} label="Year Moved to Current" />}

								            	{lookups.current_material && <Field name="current_material" options={lookups.current_material} component={idSelect} label="Current Material" editModeHandler={ this.openDialog } scope="material" />}

								            	<Field name="current_material_cost_per_lb" component={renderDialogTextFieldLookup} label="Current Material Price (&/lb)" />

								            	{lookups.mcpp_material && <Field name="mcpp_material" options={lookups.mcpp_material} component={idSelect} label="Target Material" editModeHandler={ this.openDialog } scope="material" />}

								           		<Field name="mcpp_target_price" component={renderDialogTextFieldLookup} label="Target Material Price($/lb)" />

								            </div>
								        </div>
									</li>
									{/* production */}
									<li>
										<div className="collapsible-header active">
								            <CollapsibleHeader title="Production" />
								        </div>
								        <div className="collapsible-body">
								        	<div className="row">
									            	{lookups.part_type && <Field name="part_type" options={lookups.part_type} component={idSelect} label="Part Type" editModeHandler={ this.openDialog } scope="part_type" />}
									            	<Field name="part_weight_lbs" component={renderDialogTextFieldLookup} label="Part Weight (lbs)" />
									            	{lookups.mcpp_plant && <Field name="mcpp_plant" options={lookups.mcpp_plant} component={idSelect} label="MCPP Plant" editModeHandler={ this.openDialog } scope="mcpp_plant" />}
									            	{lookups.tier_1 && <Field name="tier_1" options={lookups.tier_1} component={idSelect} label="Tier 1" editModeHandler={ this.openDialog } scope="tier_1" />}
									            	{lookups.tier_2 && <Field name="tier_2" options={lookups.tier_2} component={idSelect} label="Tier 2" editModeHandler={ this.openDialog } scope="tier_2" />}
									            	{lookups.molder && <Field name="molder" options={lookups.molder} component={idSelect} label="Molder" editModeHandler={ this.openDialog }  scope="molder"/>}
								        	</div>
								        </div>
									</li>
									{/* MCPP Team */}
									<li>
										<div className="collapsible-header active">
								            <CollapsibleHeader title="MCPP Team" />
								        </div>
								        <div className="collapsible-body">
								        	<div className="row">
								        		{lookups.sales_manager && <Field name="sales_manager" options={lookups.sales_manager} component={userSelect} label="Sales Manager" scope="sales_manager" />}
								        		{lookups.ade && <Field name="ade" options={lookups.ade} component={userSelect} label="ADE" scope="ade" />}
								        		{lookups.tech_service && <Field name="tech_service" options={lookups.tech_service} component={userSelect} label="Tech Service" scope="tech_service" />}
								        	</div>
								        </div>
									</li>
									{/* Specifications */}
									<li>
										<div className="collapsible-header active">
								            <CollapsibleHeader title="Specifications" />
								        </div>
								        <div className="collapsible-body">
								        	<div className="row">
								        		{lookups.oem_material_spec && <Field name="oem_material_spec" options={lookups.oem_material_spec} component={idSelect} label="OEM Material Spec" editModeHandler={ this.openDialog } scope="spec" />}
								        		{lookups.oem_performance_spec && <Field name="oem_performance_spec" options={lookups.oem_performance_spec} component={idSelect} label="OEM Performance Spec" editModeHandler={ this.openDialog } scope="spec" />}

								        	</div>
								        </div>
									</li>

								</ul>
								{/* buttons*/}
								<div className="row" style={{textAlign:'center'}}>
									<div className="col s10 offset-s1">
										<RaisedButton type="button" label="Reset Changes" primary={false} disabled={ pristine || submitting } onClick={reset} />
										<RaisedButton type="submit" label="Create Project" primary={true} disabled={ pristine || submitting }  />
									</div>
								</div>
								</form>


							</div>
						</div>

					</div>
					{/* start dialog form component
					<ProjectLookup ref={ (c) => this.lookupDialog = c } />*/}
					<Dialog
						title={ this.state.dialogTitle }
						modal={ true }
						open={ this.state.showDialog }
						actions={ dialogActions }>
						{/*<div>
						<strong>Add item to Dropdown Menu</strong>
						<p>Add a new item to the dropdown list by filling in the field where indicated and clicking on "Add to Dropdown Menu". The new entry will appear in the dropdown menu when you return to the Add Project Panel.</p>
						</div>*/}
						{/*{ console.log('fieldScope', this.state.fieldScope, 'fieldValue', this.state.fieldValue) }*/}
						<form onSubmit={ function(event){event.preventDefault();} }>
							<input type="hidden" name="scope" value={this.state.fieldScope} />
							<input type="text" name="value" value={this.state.fieldValue} placeholder="New item here" onChange={ this.setFieldValue }/>
						</form>
					</Dialog>
					{/* finish dialog form component */}
				</div>


			</ReactCSSTransitionGroup>
		);
	}
}
ProjectAdd.propTypes = {};
ProjectAdd.defaultProps = {};
ProjectAdd.contextTypes = {
		router: React.PropTypes.object
};

function mapStateToProps(state){
	return {
		projects: state.projects,
		vehicles: state.vehicles,
		lookups: state.lookups,
		user: state.user,
		initialValues: {
			vehicle_id: state.vehicles.selectedItem.vehicle_id,
			submitted_by: state.user.userInfo.user_id
		}
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		submitProject,
		fetchProjects,
		/*fetchMaterials,
		fetchPartType,
		fetchMcppPlant,
		fetchStatus,
		fetchProbabilityPercent,
		fetchYTC,
		fetchTier1,
		fetchTier2,
		fetchMolder,
		fetchSalesManager,
		fetchAde,
		fetchTechService,
		fetchOemMaterial,
		fetchOemPerformance,*/
		notify,
		newItem
	}, dispatch);
}
/*
function validate(values) {
	const errors = {};
	console.log('values.part_type',values.part_type);
	if(_.isUndefined(values.part_type) || _.size(values.part_type) == 0)
		errors.part_type = 'This field is required';
	if(_.isUndefined(values.current_material) || _.size(values.current_material) == 0)
		errors.current_material = 'This field is required';
	return errors;
}
*/

ProjectAdd = ReduxFormConsumer(ProjectAdd, 'projectAddForm'/*, validate*/);
export default connect(mapStateToProps,mapDispatchToProps)(ProjectAdd);