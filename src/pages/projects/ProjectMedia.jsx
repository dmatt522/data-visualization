import React, {Component} 		from 'react'
import {connect} from 'react-redux'
import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import _ from 'lodash'

import BaseComponent 		from '../../shared/components/BaseComponent'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import PageTitleBar 	from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'
import { _valueAt } from '../../utils/functions'

class ProjectMedia extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}


	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}

	componentDidUpdate(prevProps, prevState) {
	    $('.collapsible').collapsible({accordion : false });
	}


	render(){
		const item = this.props.projects.selectedItem;
		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >

				<section className="comp-projectdetails page projectdetails-page">

					<Breadcrumbs />
					<PageTitleBar title="Project Media" />
					<div className="row">
					<div className="col s12 div-as-title">{`${_valueAt(item, 'part_type')} Project`}</div>
					<div className="col s12 ">
					<ul className="collapsible" data-collapsible="expandable">
						<li>
							<div className="collapsible-header active">
								<CollapsibleHeader  title="title" />
							</div>
						  	<div className="collapsible-body">
								<div className="row"><div className="col s12">content</div></div>
						  	</div>
						</li>
						<li>
							<div className="collapsible-header active">
								<CollapsibleHeader  title="title" />
							</div>
						  	<div className="collapsible-body">
						  		<div className="row"><div className="col s12">content</div></div>

						  	</div>
						</li>
					</ul>
				</div>
				</div>
				</section>

			</ReactCSSTransitionGroup>
		);
	}
}
ProjectMedia.propTypes = {};
ProjectMedia.defaultProps = {};

function mapStateToProps({projects}){
	return {projects};
}
export default connect(mapStateToProps)(ProjectMedia);