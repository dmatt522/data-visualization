import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field } from 'redux-form'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import _ from 'lodash'
import RaisedButton from 'material-ui/RaisedButton'
import ReduxFormConsumer from '../../hocs/reduxFormConsumer'
import PageTitleBar from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader from '../../shared/components/CollapsibleHeader'
import { openProjectLookupDialog } 	from '../../shared/actions/index'
import {
	fetchMaterials,
	fetchPartType,
	fetchMcppPlant,
	fetchStatus,
	fetchProbabilityPercent,
	fetchYTC,
	fetchTier1,
	fetchTier2,
	fetchMolder,
	fetchSalesManager,
	fetchAde,
	fetchTechService,
	fetchOemMaterial,
	fetchOemPerformance, } from '../../shared/actions/lookups'
import styles from '../../styles/styles'
import globals from '../../config/globals'
import { renderTextFieldLookup, renderSelectFieldLookup } from '../../utils/form-field-renderers'
import { _valueAt, _getOptionsForField, _cySeries } 	from '../../utils/functions'

class ProjectEdit extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.openDialog = this.openDialog.bind(this);
	}
	openDialog(event, title){
		event.preventDefault();

	}
	onFormSubmit(formData){

	}
	componentDidMount() {
	  $('select').material_select();
		$('.collapsible').collapsible({accordion : false });
		if(_.isEmpty(this.props.lookups.materials))
	    	this.props.fetchMaterials();
	    if(_.isEmpty(this.props.lookups.part_type))
	    	this.props.fetchPartType();
	    if(_.isEmpty(this.props.lookups.mccp_plant))
	    	this.props.fetchMcppPlant();
	    if(_.isEmpty(this.props.lookups.status))
	    	this.props.fetchStatus();
	    if(_.isEmpty(this.props.lookups.status))
	    	this.props.fetchProbabilityPercent();
	    if(_.isEmpty(this.props.lookups.year_to_current))
	    	this.props.fetchYTC();
	    if(_.isEmpty(this.props.lookups.tier1))
	    	this.props.fetchTier1();
	    if(_.isEmpty(this.props.lookups.tier1))
	    	this.props.fetchTier2();
	    if(_.isEmpty(this.props.lookups.molder))
	    	this.props.fetchMolder();
	    if(_.isEmpty(this.props.lookups.sales_manager))
	    	this.props.fetchSalesManager();
	    if(_.isEmpty(this.props.lookups.ade))
	    	this.props.fetchAde();
	    if(_.isEmpty(this.props.lookups.tech_service))
	    	this.props.fetchTechService();
	    if(_.isEmpty(this.props.lookups.oem_material_spec))
	    	this.props.fetchOemMaterial();
	    if(_.isEmpty(this.props.lookups.oem_performance_spec))
	    	this.props.fetchOemPerformance();
	}
	componentDidUpdate(prevProps, prevState) {
	  $('select').material_select();
		$('.collapsible').collapsible({accordion : false });
	}

	render(){
		const { handleSubmit, pristine, reset, submitting, projects, lookups } = this.props;
		const item = projects.selectedItem;
		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<div className="comp-projectedit page projectedit-page">
					<PageTitleBar title="Edit project" />
					<form onSubmit={ handleSubmit(this.onFormSubmit) }>
						<div className="row">
						<div className="col s12 div-as-title">{`${_valueAt(item, 'part_type')} Project`}</div>
							<div className="col s12">
								<ul className="collapsible" data-collapsible="expandable">

									{/* media */}
									<li>
										<div className="collapsible-header active">
							        <CollapsibleHeader title="Media" />
							      </div>
							      <div className="collapsible-body">
							        <div className="row">
							        	<div className="col s12 m6"> img placeholder </div>
												<div className="col s12 m6"> img placeholder </div>
											</div>
							      </div>
									</li>
									{/* commercial */}
									<li>
										<div className="collapsible-header active">
							        <CollapsibleHeader title="Commercial" />
							      </div>
							      <div className="collapsible-body">
							        <div className="row">
							        		{lookups.status && <Field name="status" options={lookups.status} component={renderSelectFieldLookup} label="Status" /> }
								            	{lookups.probability_percent && <Field name="probability_percent" options={lookups.probability_percent} component={renderSelectFieldLookup} label="Probability (%)" />}
								            	{lookups.year_to_current && <Field name="year_moved_to_current" options={lookups.year_to_current} component={renderSelectFieldLookup} label="Year Moved to Current" />}
								            	{lookups.materials && <Field name="current_material" options={lookups.materials} component={renderSelectFieldLookup} label="Current Material" editModeHandler={ this.openDialog } />}
								            	<Field name="current_material_cost_per_lb" component={renderTextFieldLookup} label="Current Material Price (&/lb)" />
								            	{lookups.materials && <Field name="mccp_material" options={lookups.materials} component={renderSelectFieldLookup} label="Target Material" editModeHandler={ this.openDialog } />}
								           		<Field name="mcpp_target_price" component={renderTextFieldLookup} label="Target Material Price($/lb)" />
											</div>
							      </div>
									</li>
									{/* production */}
									<li>
										<div className="collapsible-header active">
							        <CollapsibleHeader title="Vehicle Build" />
							      </div>
							      <div className="collapsible-body">
							        <div className="row">
							        		{lookups.part_type && <Field name="part_type" options={lookups.part_type} component={renderSelectFieldLookup} label="Part Type" editModeHandler={ this.openDialog } />}
									            	<Field name="part_weight" component={renderTextFieldLookup} label="Part Weight (lbs)" />
									            	{lookups.mccp_plant && <Field name="mccp_plant" options={lookups.mccp_plant} component={renderSelectFieldLookup} label="MCCP Plant" editModeHandler={ this.openDialog } />}
									            	{lookups.tier1 && <Field name="tier_1" options={lookups.tier1} component={renderSelectFieldLookup} label="Tier 1" editModeHandler={ this.openDialog } />}
									            	{lookups.tier2 && <Field name="tier_2" options={lookups.tier2} component={renderSelectFieldLookup} label="Tier 2" editModeHandler={ this.openDialog } />}
									            	{lookups.molder && <Field name="molder" options={lookups.molder} component={renderSelectFieldLookup} label="Molder" editModeHandler={ this.openDialog } />}
											</div>
							      </div>
									</li>
									{/* mcpp team */}
									<li>
										<div className="collapsible-header active">
							        <CollapsibleHeader title="MCCP Team" />
							      </div>
							      <div className="collapsible-body">
							        <div className="row">
							        		{lookups.sales_manager && <Field name="sales_manager" options={lookups.sales_manager} component={renderSelectFieldLookup} label="Sales Manager" editModeHandler={ this.openDialog } />}
								        		{lookups.ade && <Field name="ade" options={lookups.ade} component={renderSelectFieldLookup} label="ADE" editModeHandler={ this.openDialog } />}
								        		{lookups.tech_service && <Field name="tech_service" options={lookups.tech_service} component={renderSelectFieldLookup} label="Tech Service" editModeHandler={ this.openDialog } />}
											</div>
							      </div>
									</li>
									{/* specifications */}
									<li>
										<div className="collapsible-header active">
							        <CollapsibleHeader title="Specifications" />
							      </div>
							      <div className="collapsible-body">
							        <div className="row">

							        		{lookups.oem_material_spec && <Field name="oem_material_spec" options={lookups.oem_material_spec} component={renderSelectFieldLookup} label="OEM Material Spec" editModeHandler={ this.openDialog } />}
								        		{lookups.oem_performance_spec && <Field name="oem_performance_spec" options={lookups.oem_performance_spec} component={renderSelectFieldLookup} label="OEM Performance Spec" editModeHandler={ this.openDialog } />}
											</div>
							      </div>
									</li>


								</ul>
								<div className="row" style={{textAlign:'center'}}>
									<div className="col s10 offset-s1">
										<RaisedButton type="button" label="Reset Changes" primary={false} disabled={ pristine || submitting } onClick={reset} />
										<RaisedButton type="submit" label="Update Project Data" primary={true} disabled={ pristine || submitting }  />
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
ProjectEdit.propTypes = {};
ProjectEdit.defaultProps = {};

function mapStateToProps(state){
	return {
		lookups: state.lookups,
		projects: state.projects,
		initialValues: state.projects.selectedItem
	}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		fetchMaterials,
		fetchPartType,
		fetchMcppPlant,
		fetchStatus,
		fetchProbabilityPercent,
		fetchYTC,
		fetchTier1,
		fetchTier2,
		fetchMolder,
		fetchSalesManager,
		fetchAde,
		fetchTechService,
		fetchOemMaterial,
		fetchOemPerformance,
		openProjectLookupDialog
	}, dispatch);
}
ProjectEdit = ReduxFormConsumer(ProjectEdit, 'projectEditForm');
export default connect(mapStateToProps, mapDispatchToProps)(ProjectEdit);