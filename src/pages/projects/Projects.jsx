import React, {Component} 		from 'react'
import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'
import { connect } 				from 'react-redux'
import { bindActionCreators }	from 'redux'

import _ from 'lodash'

import Dialog from 'material-ui/Dialog'

import Chart from './components/dialog/Chart'
import Contacts from './components/dialog/Contacts'
import Delete from './components/dialog/Delete'
import Details from './components/dialog/Details'
import DetailsMulti from './components/dialog/DetailsMulti'
import Files from './components/dialog/Files'
import History from './components/dialog/History'
import ActionPlans from './components/dialog/ActionPlans'

import ResultsDisplay 		from './components/ResultsDisplay'
import BaseComponent 		from '../../shared/components/BaseComponent'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import ComposableToolbar 	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar 		from '../../shared/toolbars/PageTitleBar'
import FiltersGroup 		from './components/FiltersGroup.jsx'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'

// actions
import {
	fetchProjects,
	bufferFilterProjects,
	resetFilterProjects,
	applyFilterProjects } from './actions/index'
/*import {
	fecthAllLookups,
	fetchMaterials,
	fetchPartType,
	fetchMcppPlant,
	fetchStatus,
	fetchProbabilityPercent,
	fetchYTC,
	fetchTier1,
	fetchTier2,
	fetchMolder,
	fetchSalesManager,
	fetchAde,
	fetchTechService,
	fetchOemMaterial,
	fetchOemPerformance } from '../../shared/actions/lookups'*/
// shared actions
import { notify } from '../../shared/actions/index'
import styles from '../../styles/styles'

function whoIsTheChild(name, closeDialogHandler){
	switch(name){
		case 'details_multi':
			return <DetailsMulti closeHandler={ closeDialogHandler } />;
		case 'plans':
			return <ActionPlans closeHandler={ closeDialogHandler } />;
		case 'delete':
			return <Delete closeHandler={ closeDialogHandler } />;
		case 'details':
			return <Details closeHandler={ closeDialogHandler } />;
		case 'files':
			return <Files closeHandler={ closeDialogHandler } />;
		case 'chart':
			return <Chart closeHandler={ closeDialogHandler } />;
		case 'history':
			return <History closeHandler={ closeDialogHandler } />;
		case 'contacts':
			return <Contacts closeHandler={ closeDialogHandler } />;
		default:
			return <Details closeHandler={ closeDialogHandler } />;
	}
}

// transform the lookup[...] to be readeable by the filter
function refactorOptions(options){
	return options.map( function(opt){ return {id:opt.id, name: `${opt.first_name} ${opt.last_name}`}; } );
}


class Projects extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {
			openDialog:false,
			actionName:''
		};
		this.resetFiltersHandler = this.resetFiltersHandler.bind(this);
		this.applyFiltersHandler = this.applyFiltersHandler.bind(this);
		this.bufferFiltersHandler = this.bufferFiltersHandler.bind(this);
		this.toolbarActionHandler = this.toolbarActionHandler.bind(this);
		this.closeDialogHandler = this.closeDialogHandler.bind(this);
	}

	closeDialogHandler(){
		this.setState({openDialog:false});
	}
	toolbarActionHandler(obj){
		//console.log('action', obj.actionName);

		this.setState({openDialog:true, actionName:obj.actionName});
	}
	resetFiltersHandler(){
		this.props.resetFilterProjects();
	}
	applyFiltersHandler(){
		this.props.applyFilterProjects();
	}
	bufferFiltersHandler(filter){
		this.props.bufferFilterProjects(filter);
	}

	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	    if(_.isEmpty(this.props.projects.list)){
			this.props.fetchProjects();
		}
	}

	componentDidUpdate(prevProps, prevState) {
	    $('.collapsible').collapsible({accordion : false });
	    //this.refs.filtersGroupRef.setLoadingStateFromParent(false);
	}

	componentWillUnmount() {
		//console.log('projects willunmount');
	    this.props.resetFilterProjects();
	}


	render(){
		//console.log('this.state.actionName',this.state.actionName);
		const dialogChild = whoIsTheChild(this.state.actionName, this.closeDialogHandler);
		const lookups = this.props.lookups;
		const projectFilters = [
			{label: 'Part Type', 	name:'part_type', 		options:lookups.part_type},
			{label: 'Status', 	name:'probability_status', 		options:lookups.probability_status},
			{label: 'Current Material', 	name:'current_material', 		options:lookups.current_material},
			{label: 'Target Material', 	name:'mcpp_material', 		options:lookups.mcpp_material},
			{label: 'MCPP Plant', 	name:'mcpp_plant', 		options:lookups.mccp_plant},
			{label: 'Tier 1', 	name:'tier_1', 		options:lookups.tier_1},
			{label: 'Tier 2', 	name:'tier_2', 		options:lookups.tier_2},
			{label: 'Molder', 	name:'molder', 		options:lookups.molder},
			{label: 'Year moved to current', 	name:'year_moved_to_current', 		options:lookups.year_moved_to_current},
			{label: 'Performance Spec', 	name:'oem_performance_spec', 		options:lookups.oem_performance_spec},
			{label: 'Material Spec', 	name:'oem_material_spec', 		options:lookups.oem_material_spec},

			{label:'Sales Manager', name:'sales_manager', 	options: refactorOptions(lookups.sales_manager)},
			{label:'ADE', 			name:'ade', 			options: refactorOptions(lookups.ade)},
			{label:'Tech Service', 	name:'tech_service', 	options: refactorOptions(lookups.tech_service)}
		];



		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >

				<div className="comp-projects page projects-page">

					<Breadcrumbs />

					<PageTitleBar title="Projects" />

					{/*<SimpleText text={ VEHICLES_PAGE_DESCRIPTION } />*/}

					<ul className="collapsible" data-collapsible="expandable">

						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Search by vehicle" />
							</div>
						  	<div className="collapsible-body">
								{/*<SimpleText text="lorem ipsum dolor" />*/}
								<FiltersGroup
									data={ this.props.projects }
									ref="filtersGroupByVehicleRef"
									title="title"
									showButtonsAndRecap={false}
									filters={this.props.projects.filtersByVehicle}
									hasFiltersId={false}
									onResetHandler={ this.resetFiltersHandler }
									onApplyHandler={ this.applyFiltersHandler }
									onSelectChange={ (filter) => this.props.bufferFilterProjects(filter) } />
						  	</div>
						</li>
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Search by project" />
							</div>
						  	<div className="collapsible-body">
								{/*<SimpleText text="lorem ipsum dolor" />*/}
								<FiltersGroup
									ref="filtersGroupByProjectRef"
									title="title"
									data={ this.props.projects }
									filters={ projectFilters }
									hasFiltersId={true}
									onResetHandler={ this.resetFiltersHandler }
									onApplyHandler={ this.applyFiltersHandler }
									onSelectChange={ (filter) => this.props.bufferFilterProjects(filter) } />
						  	</div>
						</li>
						<li>
						  	<div className="collapsible-header active">
								<CollapsibleHeader  title="Results" /></div>
						  	<div className="collapsible-body">
								{/*<SimpleText text={VEHICLES_RESULT_CALL_TO_ACTION} />*/}
								<ResultsDisplay
									title="title"
									toolbarActionHandler={ this.toolbarActionHandler }
									data={ this.props.projects } />
						  	</div>
						</li>
					</ul>
				</div>

				<Dialog
					autoScrollBodyContent={true}
					children={dialogChild}
					open={this.state.openDialog}
					onRequestClose={ () => this.setState({openDialog:false}) }
					contentStyle={styles.projectDialog} />

			</ReactCSSTransitionGroup>
		);
	}
}
Projects.propTypes = {};
Projects.defaultProps = {};

function mapStateToProps(state){
	return {
		lookups: state.lookups,
		projects: state.projects
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
			bufferFilterProjects,
			resetFilterProjects,
			applyFilterProjects,
			fetchProjects,
			notify
		},
		dispatch
	);
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects);