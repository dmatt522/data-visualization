import React, {Component} from 'react'
import styles from '../../styles/styles'
class ProjectsLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<div className="page-layout projects-page-layout">
				{this.props.children}
			</div>
		);
	}
}
ProjectsLayout.propTypes = {};
ProjectsLayout.defaultProps = {};
export default ProjectsLayout;