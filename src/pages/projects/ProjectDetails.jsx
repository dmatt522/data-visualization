import React, {Component} 	from 'react'
import { connect } 			from 'react-redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import { Link } from 'react-router'

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'

import TestDialog 			from './components/TestDialog'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import ComposableToolbar 	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar 	from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader	from '../../shared/components/CollapsibleHeader'
import TooltippedIconButton from '../../shared/ui/TooltippedIconButton'
import styles 				from '../../styles/styles'

import globals from '../../config/globals'
import paths from '../../routes/paths'
import { _valueAt } from '../../utils/functions'


const dialogsData = [
	{title: 'Part Volume', 		id:'vol'},
	{title: 'Change Log', 		id:'log'},
	{title: 'Action Plans', 	id:'act'},
	{title: 'Project Contacts', id:'pct'},
	{title: 'Images', 			id:'img'},
	{title: 'Files', 			id:'fls'}/*,
	{title: 'Edit', 			id:'edt'}*/
];

const createColumn = (label, item, prop, unit='') => {
	return 	(
		<div className="col s12 m4 l3">
			<span className="as-label">{label}</span> { `${_valueAt(item, prop)}${unit}` }
	    </div>
	)
};


class ProjectDetails extends Component{
	constructor(props){
		super(props);
		this.state = {
			currentDialog: ''
		};

		this.openDialog = this.openDialog.bind(this);
		this.closeDialog = this.closeDialog.bind(this);
	}

	openDialog(id){
		console.log(`open ${id}`);
		this.setState({ currentDialog: id });
	}

	closeDialog(){
		this.setState({ currentDialog: '' });
	}

	componentDidMount() {
	    $('.tooltipped').tooltip({delay: 50});
	    $('.collapsible').collapsible({accordion : false });
	}
	componentDidUpdate(prevProps, prevState) {
	    $('.tooltipped').tooltip({delay: 50});
	    $('.collapsible').collapsible({accordion : false });
	}

	render(){

		const modals = dialogsData.map( (item) => {
			return (
				<TestDialog
					key={ item.id }
					title={ item.title }
					open={ item.id === this.state.currentDialog }
					closeDialog={ this.closeDialog } />
			)
		});

		const item = this.props.projects.selectedItem;

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<section className="comp-projectdetails page projectdetails-page">
					<Breadcrumbs />

					<PageTitleBar title="Project Details">
						<IconMenu
							iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
						    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
						    targetOrigin={{horizontal: 'right', vertical: 'top'}}
						    >
						    <MenuItem primaryText="Part Volume"
						    	containerElement={
						    		<Link to={`${_.replace(paths.projectPartVolume, /:id/, _valueAt(item, 'project_id'))}`}/>
						    	} />
						    <MenuItem primaryText="Change Log"
						    	containerElement={
						    		<Link to={`${_.replace(paths.projectChangeLog, /:id/, _valueAt(item, 'project_id'))}`}/>
						    	} />
						    <MenuItem primaryText="Action Plans"
						    	containerElement={
						    		<Link to={`${_.replace(paths.projectActionPlans, /:id/, _valueAt(item, 'project_id'))}`}/>
						    	} />
						    <MenuItem primaryText="Project Contacts"
						    	containerElement={
						    		<Link to={`${_.replace(paths.projectContacts, /:id/, _valueAt(item, 'project_id'))}`}/>
						    	} />
						    <MenuItem primaryText="Media"
						    	containerElement={
						    		<Link to={`${_.replace(paths.projectMedia, /:id/, _valueAt(item, 'project_id'))}`}/>
						    	} />
						</IconMenu>
						<Link to={`${_.replace(paths.projectEdit, /:id/, _valueAt(item, 'project_id'))}`}>
							<IconButton><EditorModeEdit /></IconButton>
						</Link>
					</PageTitleBar>

					<div className="row comp-projectdetails">

						<div className="col s12 div-as-title">{`${_valueAt(item, 'part_type')} Project`}</div>

						<div className="col s12">

							<ul className="collapsible" data-collapsible="expandable">
								{/* media */}
								<li>
									<div className="collapsible-header active">
							            <CollapsibleHeader title="Media" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
							            	<div className="col s12 m6">
												img placeholder
											</div>
											<div className="col s12 m6">
												img placeholder
											</div>

							            </div>
							        </div>
								</li>
							{/* commercial */}
								<li>
									<div className="collapsible-header active">
							            <CollapsibleHeader title="Commercial" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
							            	{createColumn('Status', item, 'probability_status')}
							            	{createColumn('Probability', item, 'probability_percent', '%')}
							            	{createColumn('Year Moved to Current', item, 'year_moved_to_current')}
							            	{createColumn('Current Material', item, 'current_material')}
							            	{createColumn('Current Material Price ($/lb)', item, 'current_material_cost_per_lb')}
							            	{createColumn('Target Material', item, 'mcpp_material')}
							            	{createColumn('Target Material Price ($/lb)', item, '')}
							            </div>
							        </div>
								</li>
								{/* production */}
								<li>
									<div className="collapsible-header active">
							            <CollapsibleHeader title="Production" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
								            {createColumn('Part Type', item, 'part_type')}
								            {createColumn('Part Weight', item, 'part_weight_lbs')}
								            {createColumn('MCPP Plant', item, 'mcpp_plant')}
								            {createColumn('Tier 1', item, 'tier_1')}
								            {createColumn('Tier 2', item, 'tier_2')}
								            {createColumn('Molder', item, 'molder')}
							            </div>
							        </div>
								</li>
								{/* mcpp team */}
								<li>
									<div className="collapsible-header active">
							            <CollapsibleHeader title="MCPP Team" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
							            	{createColumn('Sales Manager', item, 'sales_manager')}
							            	{createColumn('ADE', item, 'ade')}
							            	{createColumn('Tech Service ', item, 'tech_service')}
							            </div>
							        </div>
								</li>
								{/* specifications */}
								<li>
									<div className="collapsible-header active">
							            <CollapsibleHeader title="Specifications" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
							            	{createColumn('OEM Material Spec', item, 'oem_material_spec')}
							            	{createColumn('OEM Performance Spec', item, 'oem_performance_spec')}
							            </div>
							        </div>
								</li>



							</ul>

						</div>
					</div>

					{ modals }

				</section>
			</ReactCSSTransitionGroup>
		);
	}
}
ProjectDetails.propTypes = {};
ProjectDetails.defaultProps = {};

function mapStateToProps({projects}){
	return {projects};
}

export default connect(mapStateToProps)(ProjectDetails);