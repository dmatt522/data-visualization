import React, {Component} 	from 'react';


class Filter extends Component{
	constructor(props){
		super(props);
		this.state = {
			value: 'any'
		};
		this.handleSelectChange = this.handleSelectChange.bind(this);
	}


	/**
	 * [componentWillReceiveProps description]
	 * @param  {[type]} nextProps [description]
	 * @return {[type]}           [description]
	 */
	componentWillReceiveProps(nextProps) {
	    if(nextProps.bufferSize == 0)
	    	this.setState({ value:'any' });

	}

	handleSelectChange(event){
		console.log('event', event);
		this.setState({ value:event.target.value });
		this.props.onChange({name:this.props.name, value: event.target.value});
	}

	render(){

		if(!this.props.hasFiltersId){
			return (
				<div className="browser-default-input-field">
				    <label>{ this.props.label }</label>
				    <select className="browser-default" value={ this.state.value } onChange={ this.handleSelectChange }>
				    	<option key="any" value="any">any</option>
					    {
			    			this.props.options.map( (opt, i) => <option key={ i } value={ opt }>{ opt }</option> )
			    		}
				    </select>
				  </div>
			);
		}

		return(
			<div className="browser-default-input-field">
			    <label>{ this.props.label }</label>
			    <select className="browser-default" value={ this.state.value } onChange={ this.handleSelectChange }>
			    	<option key="any" value="any">any</option>
				    {
		    		this.props.options.map( (opt, i) => <option key={ i } value={ opt.id }>{ opt.name }</option> )
		    		}
			    </select>
			  </div>
		);
	}
}
Filter.propTypes = {};
Filter.defaultProps = {};


export default Filter;