import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';

class TestDialog extends Component{
	constructor(props){
		super(props);
	}


	render(){
		const actions = [
	      <FlatButton
	        label="Cancel"
	        primary={true}
	        onTouchTap={ this.props.closeDialog }
	      />,
	      <FlatButton
	        label="Submit"
	        primary={true}
	        disabled={true}
	        onTouchTap={ this.props.closeDialog }
	      />,
	    ];

		return(
			<Dialog
	          title={ this.props.title }
	          actions={actions}
	          modal={true}
	          open={this.props.open}
	          onRequestClose={ this.props.closeDialog }
	        >
	        content here
	        </Dialog>
		);
	}
}
TestDialog.propTypes = {};
TestDialog.defaultProps = {};

export default TestDialog;