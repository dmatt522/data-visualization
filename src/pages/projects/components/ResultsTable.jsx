import React, { Component, PropTypes } 	from 'react'
import { connect } 						from 'react-redux'
import { bindActionCreators } 			from 'redux'

import Avatar 		from 'material-ui/Avatar'
import FlatButton 	from 'material-ui/FlatButton'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';

import _ from 'lodash'

// this component actions to dispatch
import { sortList, selectProject, selectMultiProject } from '../actions/index'

// utils functions
import { refactorKey, mapKeyToHelpText, _cellValue } from '../../../utils/functions'

import HelpButton from '../../../shared/buttons/HelpButton'
import styles from '../../../styles/styles'


// data I do not watnt to display in table


function buildColumnClassName(hiddenColumns, key){
	return (_.indexOf(hiddenColumns, key) > -1) ? `${key} columnHide`: `${key}`;
}

class ResultsTable extends Component{
	constructor(props){
		super(props);
		this.state = {};

		this.headerClickHandler = this.headerClickHandler.bind(this);
		this.rowSelectionHandler = this.rowSelectionHandler.bind(this);
	}

	headerClickHandler(key){
		this.props.sortList(key);
	}

	rowSelectionHandler(rows){
		this.props.enableToolbarTools(rows);
		console.log('rows =', rows);
		if(rows === 'all'){
			let array = [];
			for(let i=0; i<_.size(this.props.data);i++){
				array.push(i);
			}
			this.props.selectMultiProject(array);
			return;
		}
		if(_.size(rows) === 0)
			return;
		if(_.size(rows) > 1)
			this.props.selectMultiProject(rows);
		else
			this.props.selectProject(rows);
	}

	componentDidMount() {
		$('.tooltipped').tooltip({delay: 50});
	}
	componentDidUpdate(prevProps, prevState) {
	    $('.tooltipped').tooltip({delay: 50});
	}
	componentWillUnmount() {
		$('.tooltipped').tooltip('remove');
	}

	render(){
		// no data to render ...
		//
		if(_.isEmpty(this.props.data)){
			return (
				<div className="no-search-result">Your search matched 0 items.</div>
			);
		}

		// data to render
		//
		// remove some props from the objects representing the single vehicle
		//const propsToRemove = ['project_id', 'vehicle_id', 'part_image_1', 'part_image_2', 'part_image_3'];
		//const displayData = this.props.data.map( (item) => {
		//	return _.omit(item, propsToRemove);
		//} );
		// use only defaultColumns
		const displayData = this.props.data.map( (item) => {
			return _.pick(item, this.props.defaultColumns);
		} );
		const columnWidth = _.floor(100 / _.size(displayData)) ;

		return(
			<Table
				className="project-result-display-table"
				fixedHeader={ false }
				fixedFooter={ false }
				multiSelectable={true}
				onRowSelection={ this.rowSelectionHandler } >
				<TableHeader>
		        	<TableRow>
		        		{
		        			_.map(displayData[0], (value, key) => {
		        				return (
		        					<TableHeaderColumn aria-label={ mapKeyToHelpText(key) } className={ `${key} hint--bottom hint--medium hint--bounce` } key={ key }>
		        						{/*<FlatButton
		        								hoverColor="transparent"
		        								labelStyle={{textDecoration:'underline',whiteSpace:'nowrap',textOverflow:'ellipsis', padding:0}}
		        								primary={true}
		        								label=
		        								onClick={ (event) => this.headerClickHandler(key) } />*/}
		        						<span className="tableHeaderColumnText" onClick={ (event) => this.headerClickHandler(key) }>{ _.upperFirst(refactorKey(key)) }</span>
		        					</TableHeaderColumn>
		        				);
		        			})
		        		}
			      	</TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} deselectOnClickaway={true} >
			    	{
		        		displayData.map((item, i) => {
		        			return (
		        				<TableRow key={ i } style={{cursor:'pointer'}}>
		        					{
		        						_.map(item, (value, key) =>  <TableRowColumn className={ key } key={ key }>{ _cellValue(value, key, this.props.lookups) }</TableRowColumn> )
		        					}
		        				</TableRow>
		        			)
		        		})
		        	}
			    </TableBody>
			    {/*<TableFooter>
			    	<TableRow>
		              <TableRowColumn>a</TableRowColumn>
		              <TableRowColumn>b</TableRowColumn>
		              <TableRowColumn>c</TableRowColumn>
		            </TableRow>
			    </TableFooter>*/}
			</Table>
		);
	}
}
ResultsTable.displayName = 'ResultsTable';
ResultsTable.propTypes = {};
ResultsTable.defaultProps={

}
function mapStateToProps({lookups}){
	return { lookups };
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({ sortList, selectProject, selectMultiProject }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultsTable);
