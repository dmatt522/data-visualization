import React, {Component} 		from 'react'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import IconButton	 			from 'material-ui/IconButton'
import Pagination 				from 'react-js-pagination'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'
import ResultsTable 			from './ResultsTable'
import ResultsDisplayToolbar	from './ResultsDisplayToolbar'
//import ToolbarResultsDisplay 	from '../../../shared/components/ToolbarResultsDisplay'
import ResultsCollapsible 		from '../../../shared/components/ResultsCollapsible'
import Preloader 				from '../../../shared/components/Preloader'
import NoResult 				from '../../../shared/components/NoResult'

import _ from 'lodash'
// actions
import { paginationChange, deleteProject } from '../actions/index'
import { notify } from '../../../shared/actions/index'
import { UPDATING_DATA } from '../../../config/messages'
// styles
import styles 	from '../../../styles/styles'
import globals from '../../../config/globals'

import { _removeColumnsByUnitOfMeasure } from '../../../utils/functions'

/**
 * todo: inline editing mode in the result row
 */

class ResultsDisplay extends Component{
	constructor(props){
		super(props);
		this.state = {
			disableTools: true,
			loading:true,
			toggledColumns:false,
			numSelectedRows:0
		};

		this.paginationChangeHandler = this.paginationChangeHandler.bind(this);
		this.enableToolbarTools = this.enableToolbarTools.bind(this);
		this.deleteHandler = this.deleteHandler.bind(this);
		this.toggleColumns = this.toggleColumns.bind(this);
	}

	toggleColumns(){
		// do not use hidden columns, now use defaultColumns
		return;
		const selectors = _.join(_.map(this.props.hiddenColumns, (selector) => `.${selector}`), ',');
		$(selectors).toggleClass('columnHide');
	}

	paginationChangeHandler(pageNumber){
		//this.props.notify(UPDATING_DATA);
		// change the page
		this.props.paginationChange(pageNumber);
	}

	// enable/disable toolbar menu to view/edit ..
	enableToolbarTools(rows){
		//console.log('disableTools', (_.size(rows) == 0));
		this.setState({ disableTools: (_.size(rows) == 0), numSelectedRows: _.size(rows) });
	}

	deleteHandler(event){
		this.props.deleteProject({
			scope: 'Projects',
			id: this.props.data.selectedItem['project_id']
		});
	}

	toolbarActionHandler(){

	}

	componentWillMount() {
	    //console.log('ResultsDisplay.componentWillMount');
	    if(!_.isEmpty(this.props.data.pageData))
	    	this.setState({loading:false});
	}
	componentDidMount() {
	    //console.log('ResultsDisplay.componentDidMount');

	}
	componentWillReceiveProps(nextProps) {
	    //console.log('ResultsDisplay.componentWillReceiveProps');
	    //console.log('@ current', this.props.data.pageData);
	    //console.log('# next', nextProps.data.pageData);
	    if(!_.isEqual(this.props.data.pageData, nextProps.data.pageData)){
	    	//console.log('pageData changed ...');
	    	this.setState({loading:true});
	    }

	}
	componentWillUpdate(nextProps, nextState) {
	    //console.log('ResultsDisplay.componentWillUpdate');

	}
	componentDidUpdate(prevProps, prevState) {
	    //console.log('ResultsDisplay.componentDidUpdate');
		if(!_.isEqual(this.props.data.pageData, prevProps.data.pageData)){
	    	//console.log('pageData changed ...');
	    	this.setState({loading:false});
	    }
	}
	componentWillUnmount() {
	    //console.log('ResultsDisplay.componentWillUnmount');
	    this.setState({loading:false});
	}

	render(){
		//console.log('ResultsDisplay.render', 'is loading?', this.state.loading);

		/*if(_.isUndefined(this.state.loading) || this.state.loading){
			return (
				<Preloader />
			);
		}*/

		if(_.isEmpty(this.props.data)){
			return (
				<NoResult />
			);
		}

		//const tableData = _removeColumnsByUnitOfMeasure(this.props.data.pageData, this.props.settings.unselectedUOM);

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >

				<div className="comp-resultsdisplay">

					{/*<ToolbarResultsDisplay
						title="Search Results"
						toggleColumns={ this.toggleColumns }
						disableTools={ this.state.disableTools }
						deleteHandler={ this.deleteHandler }
						actionPaths={{
							view: `${globals.basePath}/projects/${this.props.data.selectedItem['project_id']}`,
							edit: `${globals.basePath}/projects/edit/${this.props.data.selectedItem['project_id']}`,
							delete: "#"
						}} />*/}
					<ResultsDisplayToolbar
						toggleColumns={ this.toggleColumns }
						disableTools={ this.state.disableTools }
						numSelectedRows={this.state.numSelectedRows}
						actionHandler={ this.props.toolbarActionHandler} />


					<ResultsTable
						defaultColumns={this.props.defaultColumns}
						data={ this.props.data.pageData/*_removeColumnsByUnitOfMeasure(this.props.data.pageData, this.props.settings.unselectedUOM)*/ }
						enableToolbarTools={ this.enableToolbarTools } />

					<div className="pagination-container">
						<Pagination
							activePage={ this.props.data.activePage }
							itemsCountPerPage={ this.props.data.itemsPerPage }
							totalItemsCount={ _.size(this.props.data.filteredList) }
							pageRangeDisplayed={ 5 }
							onChange={ this.paginationChangeHandler }
							/>
					</div>

				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
ResultsDisplay.propTypes = {};
ResultsDisplay.defaultProps = {
	hiddenColumns:[
		'probability_status',
		'probability_percent',
		'year_moved_to_current',
		'current_material',
		'current_material_cost_per_lb',
		'target_material',
		'mcpp_target_price',
		'part_weight_lbs',
		'mcpp_plant',
		'tier_1',
		'tier_2',
		'molder',
		'sales_manager',
		'ade',
		'tech_service',
		'oem_performance_spec',
		'oem_material_spec',
		'sop',
		'eop'
	],
	defaultColumns:[
		'part_image_1',
		'part_type',
		'brand',
		'manufacturer',
		'nameplate',
		'platform',
		'program'
	]
};



function mapStateToProps({settings}){
	return {settings};
}
// dispatch actions
function mapDispatchToProps(dispatch){
	return bindActionCreators({ paginationChange, deleteProject, notify }, dispatch);
}
// connect to redux, only as dispatcher
export default connect(mapStateToProps, mapDispatchToProps)(ResultsDisplay);