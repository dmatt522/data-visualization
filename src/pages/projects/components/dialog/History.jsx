import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
//import {Toolbar, ToolbarGroup,ToolbarTitle} from 'material-ui/Toolbar'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';

import _ from 'lodash'
import DialogTitlebar from '../../../sharedcomps/dialog/DialogTitlebar'
import {refactorKey, stringFromId} from '../../../../utils/functions'

import {fetchProjectHistory} from '../../actions/index'

class History extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
	}

	closeHandler(){
		console.log('closeHandler');
		this.props.closeHandler();

	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {
		this.props.fetchProjectHistory(this.props.projects.selectedItem.project_id);
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const logs = this.props.projects.selectedItemHistory;
		const headerColumns = _.map(logs[0], function(value, key){
			return key;
		});
		//console.log('lookups.definedById',this.props.lookups.definedById);
		const tableLayout = {tableLayout:'auto'};

		return(
			<div className="projectHistoryTableWrapper">
				<DialogTitlebar title="Project History" closeHandler={ this.closeHandler }/>
				<Table
					className="projectHistoryTable"
					bodyStyle={{overflow:'visible'}}
					style={tableLayout}
					fixedHeader={ false }
					fixedFooter={ false }>
					<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
			        	<TableRow>
			        			{
			        				headerColumns.map((value, i) =>(
			        						<TableHeaderColumn key={i}>
					        					<span className="tableHeaderColumnText">{_.upperFirst(refactorKey(value))}</span>
					        				</TableHeaderColumn>
			        					)
			        				)
			        			}
				      	</TableRow>
				    </TableHeader>

				    <TableBody displayRowCheckbox={false}>
				    	{
				    		logs.map((log, index) =>
				    			(
				    				<TableRow key={index}>
				    					{
				    						_.map(log, (value, key) => (<TableRowColumn key={value}>{stringFromId(key, value, this.props.lookups)}</TableRowColumn>)
				    						)
				    					}
				    				</TableRow>
				    			)
				    		)
				    	}
				    </TableBody>
				</Table>
			</div>
		);
	}
}
History.propTypes = {};
History.defaultProps = {};

function mapStateToProps({projects, lookups}){
	return {projects, lookups};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({fetchProjectHistory}, dispatch);
}
export default connect(mapStateToProps,mapDispatchToProps)(History);