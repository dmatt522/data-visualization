import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field } from 'redux-form'

import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import { Toolbar, ToolbarGroup, ToolbarTitle, ToolbarSeparator } from 'material-ui/Toolbar'
import Toggle from 'material-ui/Toggle'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import Chart from 'material-ui/svg-icons/editor/show-chart'
import Edit from 'material-ui/svg-icons/editor/mode-edit'
import Eye from 'material-ui/svg-icons/action/visibility'
import Close from 'material-ui/svg-icons/navigation/close'

import TitleHeader from '../../../../shared/components/TitleHeader'
import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
//import ComposableToolbar from '../../../../shared/toolbars/ComposableToolbar'
import TooltippedIconButton from '../../../../shared/ui/TooltippedIconButton'

import { newItem } from '../../../../shared/actions/lookups'
import {notify} from '../../../../shared/actions/index'

import { fetchProjects,submitProject, editProject, fetchProjectChart1} 	from '../../actions/index'

import {
	_valueAt,
	_getOptionsForField,
	_cySeries,
	_cyTableData,
	_mapInitialValues,
	getUserName,
	getName } from '../../../../utils/functions'

import { genIconButton } from '../../../../utils/components-generators'

import {
	renderDialogTextFieldLookup,
	renderDialogSelectFieldLookup,
	userSelect,
	idSelect } from '../../../../utils/form-field-renderers'

const createColumn = (label, item, prop, unit='') => {
	return 	(
		<div className="col s12 m4">
			<span>{label} </span> { `${_valueAt(item, prop)}${unit}` }
	    </div>
	)
};
/*
const createItem = (label, item, prop, unit='') => {
	return 	(
		<div className="column-item">
			<span className="as-label">{label}</span>
			<span>{ `${_valueAt(item, prop)}${unit}` }</span>
	    </div>
	)
}
*/

class Details extends Component{
	constructor(props){
		super(props);
		this.state = {
			editMode:false,
			showLookupDialog:false,
			lookupDialogTitle:'',
			fieldScope:'',
			fieldValue: '',
			openChart: false
		};

		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.openDialog = this.openDialog.bind(this);
		this.updateLookup = this.updateLookup.bind(this);
		this.onEditModeToggle = this.onEditModeToggle.bind(this);
		this.onChartClickHandler = this.onChartClickHandler.bind(this);
		this.closeHandler = this.closeHandler.bind(this);
		this.setFieldValue = this.setFieldValue.bind(this);
	}

	closeHandler(){
		this.props.closeHandler();
	}
	onChartClickHandler(event){
		this.setState({openChart:true});
	}
	onEditModeToggle(){
		//event.preventDefault();
		this.setState({editMode:!this.state.editMode});
	}
	openDialog(event, title, fieldName, scope){
		event.preventDefault();
		//console.log(title, fieldName);
		this.setState({ showLookupDialog:true, lookupDialogTitle:title, fieldScope: scope, fieldName:fieldName });
		//this.lookupDialog.openFromParent({title: title});
	}
	setFieldValue(event){
		//console.log('event.target.value', event.target.value);
		this.setState({fieldValue: event.target.value});
	}
	updateLookup(){
		console.log({scope: this.state.fieldScope, value: this.state.fieldValue});
		this.props.newItem({scope: this.state.fieldScope, value: this.state.fieldValue});
		this.props.notify({message:'Data submission in progress'});
		this.setState({ showLookupDialog:false, fieldValue:'', fieldScope:'', dialogTitle:'' });
		_.delay(function(me){
			me.setState({ optionsRefreshEnforcer: _.now() })
		}, 3000, this);
	}
	onFormSubmit(formData){
		//console.log('submitting', formData);
		this.props.submitProject(formData);
		this.props.editProject(this.props.projects.selectedItem, formData)
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {
		//console.log('details will mount', this.props.projects.selectedItem);
		this.cyTableData = _cyTableData(this.props.projects.selectedItem);
	}
	componentDidMount() {
		//console.log('details did mount', this.props.projects.selectedItem.project_id);
		$('.tooltipped').tooltip({delay: 50});
		$('.collapsible').collapsible({accordion : false });
		this.props.fetchProjectChart1(this.props.projects.selectedItem.project_id);
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {
		//console.log('details will update', nextProps);
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
		$('.tooltipped').tooltip({delay: 50});


		const projects = this.props.projects;
		if(prevProps.projects.operationCode9 !== projects.operationCode9){
			//this.props.notify({message:'User already exits'});
		}
		if(prevProps.projects.operationCodeSuccess !== projects.operationCodeSuccess){
			this.props.fetchProjects();
			this.props.notify({message:'Project data updated'});
			this.props.closeHandler();
		}
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting, projects, lookups } = this.props;
		const item = projects.selectedItem;//_mapInitialValues( projects.selectedItem, this.props.lookups );
		const uomAbbr = this.props.settings.selectedUOM.abbr;
		const ico = this.state.editMode ? <Eye />: <Edit />;
		const dialogChartActions = [
			<FlatButton label="Close" primary={ true } onTouchTap={ () => this.setState({ openChart:false }) } />
		];
		const lookupDialogActions = [
			<FlatButton label="Confirm" primary={ true } onTouchTap={ this.updateLookup } />,
			<FlatButton label="Cancel" primary={ true } onTouchTap={ () => this.setState({ showLookupDialog:false }) } />
		];
		return(
			<div className="dialog-content project-details">
				{/*<ComposableToolbar title={`${_valueAt(item, 'part_type')} Project`} tgstyle={{width:200, marginRight:16}}>
					<Toggle label="Edit mode" onToggle={ this.onEditModeToggle } style={{paddingTop:16}}/>
					<ToolbarSeparator />
					<IconButton children={<Chart />} onClick={ (event) => console.log(event) } />
				</ComposableToolbar>*/}

				<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text={`${getName(lookups.part_type, _valueAt(item, 'part_type'))} Project`} />

					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						{/*<IconButton children={ <Chart /> } onClick={ (event) => this.onChartClickHandler(event) } tooltip="View Chart"/>
						<IconButton children={ ico } onClick={ (event) => this.onEditModeToggle(event) } tooltip="Toggle Details/Edit" />
						<IconButton children={ <Close /> } onClick={ (event) => this.closeHandler(event) } tooltip="Close"/>
						*/}
						{ genIconButton( <Chart />, this.onChartClickHandler, 'Chart') }
						{ genIconButton( ico, this.onEditModeToggle, 'Toggle Details/Edit') }
						{ genIconButton( <Close />, this.closeHandler, 'Close') }

					</ToolbarGroup>
				</Toolbar>


			{/* start view details */}
				<div className={`dialog-view-edit ${this.state.editMode ? 'dialog-close-mode': 'dialog-open-mode'}`}>



{/* start collapsible */}
<ul className="collapsible project-details" data-collapsible="expandable">
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Vehicle Data" />
        </div>
        <div className="collapsible-body">
            <div className="row">
					{createColumn('Manufacturer', item, 'manufacturer')}
					{createColumn('Platform', item, 'platform')}
					{createColumn('Production Plant', item, 'production_plant')}
					{createColumn('Location', item, 'location')}
					{createColumn('Manufacturer', item, 'manufacturer')}
					{createColumn('Start of Production', item, 'sop')}
					{createColumn('End of Production', item, 'eop')}
            </div>
        </div>
    </li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Commercial" />
        </div>
        <div className="collapsible-body">
            <div className="row">
                <div className="col s12 m4">
					<span>Status </span>
					{ `${getName(lookups.probability_status, _valueAt(item, 'probability_status'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>Probability </span>
					{ `${getName(lookups.probability_percent, _valueAt(item, 'probability_percent'))}%` }
	    		</div>
				<div className="col s12 m4">
					<span>Year Moved to Current </span>
					{ `${getName(lookups.year_to_current, _valueAt(item, 'year_moved_to_current'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>Current Material </span>
					{ `${getName(lookups.current_material, _valueAt(item, 'current_material'))}` }
	    		</div>
				{createColumn('Current Material Price ($/lb)', item, 'current_material_cost_per_lb')}
	    		<div className="col s12 m4">
					<span>Target Material </span>
					{ `${getName(lookups.mcpp_material, _valueAt(item, 'mcpp_material'))}` }
	    		</div>
				{createColumn('Target Material Price ($/lb)', item, '')}


			{/*
				{createColumn('Probability', item, 'probability_percent', '%')}
                {createColumn('Status', item, 'probability_status')}
				{createColumn('Year Moved to Current', item, 'year_moved_to_current')}
				{createColumn('Current Material', item, 'current_material')}
				{createColumn('Target Material', item, 'mcpp_material')}
			*/}
            </div>
        </div>
    </li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Production" />
        </div>
        <div className="collapsible-body">
            <div className="row">
					{createColumn('Part Weight', item, 'part_weight_lbs')}
					{/*{createColumn('Tier 1', item, 'tier_1')}
					{createColumn('MCPP Plant', item, 'mcpp_plant')}
					{createColumn('Tier 2', item, 'tier_2')}
					{createColumn('Molder', item, 'molder')}*/}

					<div className="col s12 m4">
						<span>MCPP Plant </span>
						{ `${getName(lookups.mcpp_plant, _valueAt(item, 'mcpp_plant'))}` }
	    			</div>
					<div className="col s12 m4">
						<span>Tier 1 </span>
						{ `${getName(lookups.tier1, _valueAt(item, 'tier_1'))}` }
	    			</div>
					<div className="col s12 m4">
						<span>Tier 2 </span>
						{ `${getName(lookups.tier2, _valueAt(item, 'tier_2'))}` }
	    			</div>
	    			<div className="col s12 m4">
						<span>Molder </span>
						{ `${getName(lookups.molder, _valueAt(item, 'molder'))}` }
	    			</div>
            </div>
        </div>
    </li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="MCPP Team" />
        </div>
        <div className="collapsible-body">
            <div className="row">
            	<div className="col s12 m4">
					<span>Sales Manager </span>
					{ `${getUserName(lookups.sales_manager, _valueAt(item, 'sales_manager'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>ADE </span>
					{ `${getUserName(lookups.ade, _valueAt(item, 'ade'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>Tech Service </span>
					{ `${getUserName(lookups.tech_service, _valueAt(item, 'tech_service'))}` }
	    		</div>

	    		{/*
				{createColumn('Sales Manager', item, 'sales_manager')}
				{createColumn('ADE', item, 'ade')}
				{createColumn('Tech Service ', item, 'tech_service')}
	    		*/}

            </div>
        </div>
    </li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Specifications" />
        </div>
        <div className="collapsible-body">
            <div className="row">
            	<div className="col s12 m4">
					<span>OEM Material Spec </span>
					{ `${getName(lookups.oem_material_spec, _valueAt(item, 'oem_material_spec'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>OEM Performance Spec </span>
					{ `${getName(lookups.oem_performance_spec, _valueAt(item, 'oem_performance_spec'))}` }
	    		</div>
            </div>
        </div>
    </li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Builds & Volumes" />
        </div>
        <div className="collapsible-body">
            <div className="row">
                <div className="col s12">
					<Table className="prj-details-table" style={{width:'100%'}} selectable={false}>
						<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
							<TableRow>
								<TableHeaderColumn style={{width:'33%'}}>Calendar Year</TableHeaderColumn>
								<TableHeaderColumn style={{width:'33%'}}>Vehicle Builds</TableHeaderColumn>
								<TableHeaderColumn style={{width:'33%'}}>Part Volume</TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody displayRowCheckbox={false}>
						{
							this.cyTableData.map( (row, index) => (
								<TableRow key={ index }>
									<TableRowColumn>{ row.year }</TableRowColumn>
									<TableRowColumn>{ row.tot }</TableRowColumn>
									<TableRowColumn>{ `${row[uomAbbr]} (${uomAbbr})` }</TableRowColumn>
								</TableRow>
							))
						}
						</TableBody>
					</Table>
				</div>
            </div>
        </div>
    </li>

</ul>
{/* finish collapsible */}


					{/* details vehicle data
					<div className="row dialog-view-edit-row-vehicle-data">
						<div className="col s12">
							<h5>Vehicle Data</h5>
						</div>
						<div className="col s12 m4">
							<span>Manufacturer</span> { _valueAt(item, 'manufacturer') }
						</div>
						<div className="col s12 m4">
							<span>Platform</span> { _valueAt(item, 'platform') }
						</div>
						<div className="col s12 m4">
							<span>Production Plant</span> { _valueAt(item, 'production_plant') }
						</div>
						<div className="col s12 m4">
							<span>Location</span> { _valueAt(item, 'location') }
						</div>
						<div className="col s12 m4">
							<span>Start of Production</span> { _valueAt(item, 'sop') }
						</div>
						<div className="col s12 m4">
							<span>End of Production</span> { _valueAt(item, 'eop') }
						</div>
					</div>*/}
					{/* details row 1
					<div className="row dialog-view-edit-row-project-data">
						<div className="col s12 m4">
							<h5>Commercial</h5>
							{createItem('Status', item, 'probability_status')}
							{createItem('Probability', item, 'probability_percent', '%')}
							{createItem('Year Moved to Current', item, 'year_moved_to_current')}
							{createItem('Current Material', item, 'current_material')}
							{createItem('Current Material Price ($/lb)', item, 'current_material_cost_per_lb')}
							{createItem('Target Material', item, 'mcpp_material')}
							{createItem('Target Material Price ($/lb)', item, '')}
						</div>
						<div className="col s12 m4">
							<h5>Production</h5>
							{createItem('Part Weight', item, 'part_weight_lbs')}
							{createItem('MCPP Plant', item, 'mcpp_plant')}
							{createItem('Tier 1', item, 'tier_1')}
							{createItem('Tier 2', item, 'tier_2')}
							{createItem('Molder', item, 'molder')}
						</div>
						<div className="col s12 m4">
							<h5>MCPP Team</h5>
							{createItem('Sales Manager', item, 'sales_manager')}
							{createItem('ADE', item, 'ade')}
							{createItem('Tech Service ', item, 'tech_service')}
							<h5>Specifications</h5>
							{createItem('OEM Material Spec', item, 'oem_material_spec')}
							{createItem('OEM Performance Spec', item, 'oem_performance_spec')}
						</div>
					</div>*/}
					{/* details row 2
					<div className="row dialog-view-edit-row-table-data">
						<div className="col s12">
							<Table className="prj-details-table" style={{width:'100%'}} selectable={false}>
								<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
									<TableRow>
										<TableHeaderColumn><h5>Calendar Year</h5></TableHeaderColumn>
										<TableHeaderColumn><h5>Vehicle Builds</h5></TableHeaderColumn>
										<TableHeaderColumn><h5>Part Volume</h5></TableHeaderColumn>
									</TableRow>
								</TableHeader>
								<TableBody displayRowCheckbox={false}>
									{
										this.cyTableData.map( (row, index) => (
											<TableRow key={ index }>
												<TableRowColumn>{ row.year }</TableRowColumn>
												<TableRowColumn>{ row.tot }</TableRowColumn>
												<TableRowColumn>{ `${row[uomAbbr]} (${uomAbbr})` }</TableRowColumn>
											</TableRow>
										))
									}
								</TableBody>
							</Table>
						</div>
					</div>*/}

				</div>



			{/* finish view details */}


			{/* start edit form */}
				<div className={`dialog-view-edit ${this.state.editMode ? 'dialog-open-mode': 'dialog-close-mode'}`}>
				<form onSubmit={handleSubmit(this.onFormSubmit)}>
					{/* to edit set project_id
					<Field type="hidden" name="project_id" value={_valueAt(item, 'project_id')} component="input" /> */}

						<div className="row">
							{lookups.status && <Field name="probability_status" options={lookups.status} component={idSelect} label="Status" /> }
							{lookups.year_to_current && <Field name="year_moved_to_current" options={lookups.year_to_current} component={idSelect} label="Year Moved to Current" />}
							{lookups.current_material && <Field name="current_material" options={lookups.current_material} component={idSelect} label="Current Material" editModeHandler={ this.openDialog } scope="material" />}
							<Field name="current_material_cost_per_lb" type="text" component={renderDialogTextFieldLookup} label="Current Material Price (&/lb)" />
							{lookups.mcpp_material && <Field name="mcpp_material" options={lookups.mcpp_material} component={idSelect} label="Target Material" editModeHandler={ this.openDialog } scope="material" />}
							<Field name="mcpp_target_price" type="text" component={renderDialogTextFieldLookup} label="Target Material Price($/lb)" />
						</div>
						<div className="row">
							{lookups.part_type && <Field name="part_type" options={lookups.part_type} component={idSelect} label="Part Type" editModeHandler={ this.openDialog } scope="part_type" />}
							<Field name="part_weight_lbs" type="text" component={renderDialogTextFieldLookup} label="Part Weight (lbs)" />
							{lookups.mcpp_plant && <Field name="mcpp_plant" options={lookups.mcpp_plant} component={idSelect} label="MCPP Plant" editModeHandler={ this.openDialog } scope="mcpp_plant" />}
							{lookups.tier1 && <Field name="tier_1" options={lookups.tier_1} component={idSelect} label="Tier 1" editModeHandler={ this.openDialog } scope="tier_1" />}
							{lookups.tier2 && <Field name="tier_2" options={lookups.tier_2} component={idSelect} label="Tier 2" editModeHandler={ this.openDialog } scope="tier_2" />}
							{lookups.molder && <Field name="molder" options={lookups.molder} component={idSelect} label="Molder" editModeHandler={ this.openDialog } scope="molder" />}
						</div>
						<div className="row">
							{lookups.sales_manager && <Field name="sales_manager" options={lookups.sales_manager} component={userSelect} label="Sales Manager" />}
							{lookups.ade && <Field name="ade" options={lookups.ade} component={userSelect} label="ADE"/>}
							{lookups.tech_service && <Field name="tech_service" options={lookups.tech_service} component={userSelect} label="Tech Service" />}
						</div>
						<div className="row">
							{lookups.oem_material_spec && <Field name="oem_material_spec" options={lookups.oem_material_spec} component={idSelect} label="OEM Material Spec" editModeHandler={ this.openDialog } scope="spec" />}
							{lookups.oem_performance_spec && <Field name="oem_performance_spec" options={lookups.oem_performance_spec} component={idSelect} label="OEM Performance Spec" editModeHandler={ this.openDialog } scope="spec" />}
						</div>
						{/* buttons*/}
								<div className="row" style={{textAlign:'center'}}>
									<div className="col s10 offset-s1">
										<RaisedButton type="button" label="Reset Changes" primary={false} disabled={ pristine || submitting } onClick={reset} />
										<RaisedButton type="submit" label="Update Project Data" primary={true} disabled={ pristine || submitting }  />
									</div>
								</div>


				</form>
				</div>
			{/* finish edit form */}

			{/* start lookup edit dialog */}

				<Dialog
					title={this.state.lookupDialogTitle}
					modal={true}
					open={this.state.showLookupDialog}
					actions={lookupDialogActions}>
					{/*<div>
						<p>Add a new item to the dropdown list by filling in the field where indicated and clicking on "Add to Dropdown Menu". The new entry will appear in the dropdown menu when you return to the Add Project Panel.</p>
					</div>*/}
					<form onSubmit={ function(event){event.preventDefault();} }>
						<input type="hidden" name="scope" value={this.state.fieldScope} />
						<input type="text" name="value" value={this.state.fieldValue} placeholder="New item here" onChange={ this.setFieldValue }/>
					</form>
				</Dialog>

			{/* finish lookup edit dialog */}

			{/* start chart dialog */}
				<Dialog
					title="Chart"
					modal={true}
					open={this.state.openChart}
					actions={dialogChartActions}>
				</Dialog>
			{/* finish chart dialog */}

			</div>
		);
	}
}
Details.propTypes = {};
Details.defaultProps = {};

function mapStateToProps(state){
	return {
		settings: state.settings,
		lookups: state.lookups,
		projects: state.projects,
		initialValues: removeVehicleData(state.projects.selectedItem, state.lookups)
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		fetchProjects,
		submitProject,
		editProject,
		fetchProjectChart1,
		newItem,
		notify
	}, dispatch);
}


function removeVehicleData(o, lookups){
	let toRemove = ['brand', 'date_time_submitted', 'eop', 'manufacturer', 'nameplate', 'part_weight_kg', 'platform', 'program', 'sop', 'segment', 'subsegment', 'status', 'target_material', 'vehicle_image'];
	let cleaned = _.omitBy(o, function(value, key){ return _.startsWith(key, 'cy_')});
	// const values = _mapInitialValues( _.omit(cleaned, toRemove), lookups );
	const values = _.omit(cleaned, toRemove);
	//console.log('init values', values);
	return values;
}

Details = ReduxFormConsumer(Details, 'projectEditForm');
export default connect(mapStateToProps, mapDispatchToProps)(Details);