import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field } from 'redux-form'

import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import { Toolbar, ToolbarGroup, ToolbarTitle, ToolbarSeparator } from 'material-ui/Toolbar'
import Toggle from 'material-ui/Toggle'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import Chart from 'material-ui/svg-icons/editor/show-chart'
import Edit from 'material-ui/svg-icons/editor/mode-edit'
import Eye from 'material-ui/svg-icons/action/visibility'
import Close from 'material-ui/svg-icons/navigation/close'

import TitleHeader from '../../../../shared/components/TitleHeader'
import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
//import ComposableToolbar from '../../../../shared/toolbars/ComposableToolbar'
import TooltippedIconButton from '../../../../shared/ui/TooltippedIconButton'

import { newItem } from '../../../../shared/actions/lookups'

import { submitProject, editProject, fetchProjectChart1} 	from '../../actions/index'

import {
	_valueAt,
	_getOptionsForField,
	_cySeries,
	_cyTableData,
	_mapInitialValues,
	getUserName,
	getName } from '../../../../utils/functions'

import { genIconButton } from '../../../../utils/components-generators'

import {
	renderDialogTextFieldLookup,
	renderDialogSelectFieldLookup,
	userSelect,
	idSelect } from '../../../../utils/form-field-renderers'

const createColumn = (label, item, prop, unit='') => {
	return 	(
		<div className="col s12 m4">
			<span>{label}</span>: { `${_valueAt(item, prop)}${unit}` }
	    </div>
	)
};
/*
const createItem = (label, item, prop, unit='') => {
	return 	(
		<div className="column-item">
			<span className="as-label">{label}</span>
			<span>{ `${_valueAt(item, prop)}${unit}` }</span>
	    </div>
	)
}
*/

class DetailsMulti extends Component{
	constructor(props){
		super(props);
		this.state = {
			editMode:false,
			showLookupDialog:false,
			lookupDialogTitle:'',
			fieldScope:'',
			fieldValue: '',
			openChart: false
		};

		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.openDialog = this.openDialog.bind(this);
		this.updateLookup = this.updateLookup.bind(this);
		this.onEditModeToggle = this.onEditModeToggle.bind(this);
		this.onChartClickHandler = this.onChartClickHandler.bind(this);
		this.closeHandler = this.closeHandler.bind(this);
		this.setFieldValue = this.setFieldValue.bind(this);
	}

	closeHandler(){
		this.props.closeHandler();
	}
	onChartClickHandler(event){

	}
	onEditModeToggle(){

	}
	openDialog(event, title, fieldName, scope){

	}
	setFieldValue(event){

	}
	updateLookup(){

	}
	onFormSubmit(formData){
		//console.log('submitting', formData);
		this.props.submitProject(formData);
		this.props.editProject(this.props.projects.selectedItem, formData)
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {
		//console.log('details will mount', this.props.projects.selectedItem);
		//this.cyTableData = _cyTableData(this.props.projects.selectedItem);
	}
	componentDidMount() {
		//console.log('details did mount', this.props.projects.selectedItem.project_id);
		$('.tooltipped').tooltip({delay: 50});
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {
		//console.log('details will update', nextProps);
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
		$('.tooltipped').tooltip({delay: 50});
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting, projects, lookups } = this.props;
		const items = projects.selectedMultiItem;
		/*const itemsCyData = items.map(function(item){
			return _cyTableData(item);
		});*/
		//const item = projects.selectedItem;//_mapInitialValues( projects.selectedItem, this.props.lookups );
		const uomAbbr = this.props.settings.selectedUOM.abbr;
		const ico = this.state.editMode ? <Eye />: <Edit />;


		return(
			<div className="dialog-content project-details">
				<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text="Details" />
					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						{ genIconButton( <Close />, this.closeHandler, 'Close') }
					</ToolbarGroup>
				</Toolbar>


			{/* start view details */}
				<div>



{/* start collapsible */}
<ul className="collapsible project-details" data-collapsible="expandable">

	{
		items.map( (item, index) => {
			return (
    <li key={index}>
        <div className="collapsible-header">
            <TitleHeader title={`${getName(lookups.part_type, _valueAt(item, 'part_type'))} Project`} />
        </div>
        <div className="collapsible-body">
            <div className="row">
            	<div className="col s12">Vehicle Data</div>
				{createColumn('Manufacturer', item, 'manufacturer')}
				{createColumn('Platform', item, 'platform')}
				{createColumn('Production Plant', item, 'production_plant')}
				{createColumn('Location', item, 'location')}
				{createColumn('Manufacturer', item, 'manufacturer')}
				{createColumn('Start of Production', item, 'sop')}
				{createColumn('End of Production', item, 'eop')}
            </div>
            <div className="row">
            	<div className="col s12">Commercial</div>
                <div className="col s12 m4">
					<span>Status</span>:
					{ `${getName(lookups.probability_status, _valueAt(item, 'probability_status'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>Probability</span>:
					{ `${getName(lookups.probability_percent, _valueAt(item, 'probability_percent'))}%` }
	    		</div>
				<div className="col s12 m4">
					<span>Year Moved to Current</span>:
					{ `${getName(lookups.year_to_current, _valueAt(item, 'year_moved_to_current'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>Current Material</span>:
					{ `${getName(lookups.materials, _valueAt(item, 'current_material'))}` }
	    		</div>
				{createColumn('Current Material Price ($/lb)', item, 'current_material_cost_per_lb')}
	    		<div className="col s12 m4">
					<span>Target Material</span>:
					{ `${getName(lookups.materials, _valueAt(item, 'mcpp_material'))}` }
	    		</div>
				{createColumn('Target Material Price ($/lb)', item, '')}
            </div>
            <div className="row">
            	<div className="col s12">Production</div>
					{createColumn('Part Weight', item, 'part_weight_lbs')}
					<div className="col s12 m4">
						<span>MCPP Plant</span>:
						{ `${getName(lookups.mcpp_plant, _valueAt(item, 'mcpp_plant'))}` }
	    			</div>
					<div className="col s12 m4">
						<span>Tier 1</span>:
						{ `${getName(lookups.tier1, _valueAt(item, 'tier_1'))}` }
	    			</div>
					<div className="col s12 m4">
						<span>Tier 2</span>:
						{ `${getName(lookups.tier2, _valueAt(item, 'tier_2'))}` }
	    			</div>
	    			<div className="col s12 m4">
						<span>Molder</span>:
						{ `${getName(lookups.molder, _valueAt(item, 'molder'))}` }
	    			</div>
            </div>
            <div className="row">
            	<div className="col s12">MCPP Team</div>
            	<div className="col s12 m4">
					<span>Sales Manager</span>:
					{ `${getUserName(lookups.sales_manager, _valueAt(item, 'sales_manager'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>ADE</span>:
					{ `${getUserName(lookups.ade, _valueAt(item, 'ade'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>Tech Service</span>:
					{ `${getUserName(lookups.tech_service, _valueAt(item, 'tech_service'))}` }
	    		</div>
            </div>
            <div className="row">
            	<div className="col s12">Specifications</div>
            	<div className="col s12 m4">
					<span>OEM Material Spec</span>:
					{ `${getName(lookups.oem_material_spec, _valueAt(item, 'oem_material_spec'))}` }
	    		</div>
	    		<div className="col s12 m4">
					<span>OEM Performance Spec</span>:
					{ `${getName(lookups.oem_performance_spec, _valueAt(item, 'oem_performance_spec'))}` }
	    		</div>
            </div>
            <div className="row">
            	<div className="col s12">Builds & Volumes</div>
                <div className="col s12">
					<Table className="prj-details-table" style={{width:'100%'}} selectable={false}>
						<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
							<TableRow>
								<TableHeaderColumn style={{width:'33%'}}>Calendar Year</TableHeaderColumn>
								<TableHeaderColumn style={{width:'33%'}}>Vehicle Builds</TableHeaderColumn>
								<TableHeaderColumn style={{width:'33%'}}>Part Volume</TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody displayRowCheckbox={false}>
						{
							_cyTableData(item).map( (row, index) => (
								<TableRow key={ index }>
									<TableRowColumn>{ row.year }</TableRowColumn>
									<TableRowColumn>{ row.tot }</TableRowColumn>
									<TableRowColumn>{ `${row[uomAbbr]} (${uomAbbr})` }</TableRowColumn>
								</TableRow>
							))
						}
						</TableBody>
					</Table>
				</div>
            </div>
        </div>
    </li>

			)
		})
	}
</ul>
{/* finish collapsible */}

				</div>



			{/* finish view details */}





			</div>
		);
	}
}
DetailsMulti.propTypes = {};
DetailsMulti.defaultProps = {};

function mapStateToProps(state){
	return {
		settings: state.settings,
		lookups: state.lookups,
		projects: state.projects
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({}, dispatch);
}


function removeVehicleData(o, lookups){
	let toRemove = ['brand', 'date_time_submitted', 'eop', 'manufacturer', 'nameplate', 'part_weight_kg', 'platform', 'program', 'sop', 'segment', 'subsegment', 'status', 'target_material', 'vehicle_image'];
	let cleaned = _.omitBy(o, function(value, key){ return _.startsWith(key, 'cy_')});
	// const values = _mapInitialValues( _.omit(cleaned, toRemove), lookups );
	const values = _.omit(cleaned, toRemove);
	//console.log('init values', values);
	return values;
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsMulti);