import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import Close from 'material-ui/svg-icons/navigation/close'

import { _valueAt } from '../../../../utils/functions'

import { deleteProject } from '../../actions/index'

class Delete extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	closeHandler(event){
		this.props.closeHandler();
	}
	deletionHandler(event){
		this.props.deleteProject(this.props.projects.selectedItem);
		this.props.closeHandler();
	}
	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const item = this.props.projects.selectedItem;
		return(
			<div>

				<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text="Delete Project" />
					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						<IconButton children={ <Close /> } onClick={ (event) => this.closeHandler(event) } />
					</ToolbarGroup>
				</Toolbar>

				<div className="row">
					<div className="col s12" style={{color: '#000'}}>
						<strong>Confirm deletion of project:</strong>
						<div>
							{ `Manufacturer: ${_valueAt(item, 'manufacturer')}` }<br />
							{ `Brand: ${_valueAt(item, 'brand')}` }<br />
						</div>
					</div>
					<div className="col s12">
						<RaisedButton label="No" primary={false} onClick={ (event) => this.closeHandler(event) }/>
						<RaisedButton label="Yes" primary={true} onClick={ (event) => this.deletionHandler(event) }/>
					</div>

				</div>
			</div>
		);
	}
}
Delete.propTypes = {};
Delete.defaultProps = {};

function mapStateToProps({projects}){
	return {projects};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		deleteProject
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Delete);