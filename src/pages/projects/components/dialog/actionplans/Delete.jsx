import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import RaisedButton from 'material-ui/RaisedButton'
import DialogTitlebar from '../../../../sharedcomps/dialog/DialogTitlebar'
import {deleteProjectActionPlans} from '../../../actions/index'

class Delete extends React.Component {


  constructor(props) {
    super(props);
    this.state = {

    }
    this.rejectDeletion = this.rejectDeletion.bind(this);
    this.confirmDeletion = this.confirmDeletion.bind(this);

  }

  rejectDeletion(){
  	console.log('rejectDeletion');
  	this.props.closeHandler();
  	//this.setState({open:false});
  }

  confirmDeletion(){
  	if(this.props.theThing.item)
	  	this.props.deleteProjectActionPlans(this.props.theThing.item);
	this.props.closeHandler();
	//this.setState({open:false});
  }

  render() {
  	const item = this.props.theThing.item;
    return (
      <div>
      	<DialogTitlebar title="Delete Action Plan" closeHandler={ this.rejectDeletion }/>
      	<div style={{padding:16}}>
      		Confirm deletion of "{item.title}" action plan?
      	</div>
      	<div>
      		<RaisedButton label="No" primary={false} onClick={ (event) => this.rejectDeletion() } />
			<RaisedButton label="Yes" primary={true} onClick={ (event) => this.confirmDeletion() }/>
      	</div>
      </div>
    );
  }
}

function mapStateToProps({theThing}){
	return {theThing};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({deleteProjectActionPlans}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Delete);