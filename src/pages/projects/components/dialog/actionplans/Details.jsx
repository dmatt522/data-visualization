import React, {Component} from 'react'
import {connect} from 'react-redux'
import DialogTitlebar from '../../../../sharedcomps/dialog/DialogTitlebar'
import {stringFromId, getUserName} from '../../../../../utils/functions'
function _dateToString(int){
	return new Date(int * 1000).toDateString();
}

class Details extends Component {

  constructor(props) {
    super(props);
    this.rejectAction = this.rejectAction.bind(this);
  }

   rejectAction(){
  	console.log('rejectAction');
  	this.props.closeHandler();
  	//this.setState({open:false});
  }

  render() {
  	const item = this.props.theThing.item;
  	const lookups = this.props.lookups;
    const users = this.props.users;
    return (
      	<div className="detailsActonPlanDialog">
      		<DialogTitlebar title="Edit Action Plan" closeHandler={ this.rejectAction }/>
    		<div style={{padding:16}}>
    			<div className="row">
    				<div className="col s12"><span>Title </span> {item.title}</div>
    				<div className="col s12"><span style={{display:'block'}}>Comment </span> {item.comment}</div>
    				<div className="col s12 m6"><span>Priority </span> {stringFromId('priority', item.priority, lookups)}</div>
    				<div className="col s12 m6"><span>Deadline </span> {_dateToString(item.deadline)}</div>
    				<div className="col s12 m6"><span>Assigned to </span> {getUserName(users.list, item.assigned_to)}</div>
    				<div className="col s12 m6"><span>Status </span> {stringFromId('plan_status', item.plan_status, lookups)}</div>
    				<div className="col s12 m6"><span>Date submitted </span> {_dateToString(item.date_submitted)}</div>
    				<div className="col s12 m6"><span>Opened by </span> {getUserName(users.list, item.submitted_by)}</div>
    			</div>

    		</div>
    	</div>
    );
  }
}

function mapStateToProps({theThing, lookups, users}){
	return {theThing, lookups, users};
}
export default connect(mapStateToProps, null)(Details);