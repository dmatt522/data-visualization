import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm} from 'redux-form'
import _ from 'lodash'
import moment from 'moment'
import DatePicker from 'material-ui/DatePicker'
import RaisedButton from 'material-ui/RaisedButton'
import DialogTitlebar from '../../../../sharedcomps/dialog/DialogTitlebar'

import {editProjectActionPlans} from '../../../actions/index'
import {getUserName, dateFromIntToString} from '../../../../../utils/functions'

import {renderField,renderTextArea,renderSelect,renderDateField} from '../../../../sharedcomps/forms/render_functions'

const validate = (values) => {
	const errors = {};
	if(_.size(values.title) === 0)
		errors.title = 'Title is required'
	if(_.size(values.comment) === 0)
		errors.comment = 'Comments are required'
	if(_.size(values.plan_status) === 0 || values.plan_status === 'unassigned')
		errors.plan_status = 'Status is required'
	return errors;
}
function _createOptions(list){
	const items = _.concat([{id:'',name:''}], list);
	return items.map( (item, i) => <option key={i} value={item.id}>{item.name}</option> );
}

function _createUserOptions(list){
	//console.log('-------', list);
	const items = _.concat([{id:'unassigned',first_name:'',last_name:''}], list);
	return items.map( (item, i) => {
		let text = `${_.upperFirst(item.first_name)} ${_.upperFirst(item.last_name)}`;
		return <option key={i} value={item.id}>{text}</option>
	} );
}

class Edit extends Component {


  constructor(props) {
    super(props);
    this.state = {
    	date:undefined
    }
    this.rejectAction = this.rejectAction.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.setDate = this.setDate.bind(this);
  }

  setDate(e, date){
		console.log('date:', date);
		console.log('moment date:',moment(date, 'x'));
		console.log('js date:',new Date(date).valueOf()/1000);
		this.setState({date:new Date(date).valueOf()/1000});
		//this.setState({date:moment(date, 'x')});
	}

  rejectAction(){
  	console.log('rejectAction');
  	this.props.closeHandler();
  	//this.setState({open:false});
  }

  onFormSubmit(formData){
  	const data = Object.assign({}, formData, {deadline:this.state.date});
  	console.log('onFormSubmit', data);
  	this.props.editProjectActionPlans(data);
  }


  render() {
  	const { handleSubmit, pristine, reset, submitting, users, lookups } = this.props;

    return (
    	<div>
      		<DialogTitlebar title="Edit Action Plan" closeHandler={ this.rejectAction }/>
    		<div style={{padding:16}}>
    			<form className="editForm" onSubmit={handleSubmit(this.onFormSubmit)}>
    				<Field name="id" component="input" type="hidden"/>
    				<Field name="title" label="Title" component={renderField} type="text" required={true} />
    				<Field name="assigned_to" label="Assigned to" component={renderSelect} children={_createUserOptions(users.list)} />
					<Field name="plan_status" label="Status" component={renderSelect} children={_createOptions(lookups.action_plans_status)} required={true} />
					<Field name="priority" label="Priority" component={renderSelect} children={_createOptions(lookups.action_plans_priority)} />
					<div style={{marginBottom:20}}>
					    <label>Deadline</label>
					    <div>
					    	<DatePicker key="datepick" hintText="Select a date" mode="landscape" onChange={this.setDate} />
					    </div>
					</div>
					<Field name="comment" label="Comments" component={renderTextArea} label="Comments" required={true} />
					<div style={{marginBottom:20, marginTop:20}}>
						<label>Date submitted</label> {dateFromIntToString(this.props.theThing.item.date_submitted)}</div>
					<div style={{marginBottom:30}}>
						<label>Opened by</label> {getUserName(this.props.users.list, this.props.theThing.item.submitted_by)}</div>
					<div className="right-align">
						<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
					</div>
    			</form>
    		</div>
    	</div>
    );
  }
}

function mapStateToProps(state){
	const ap = state.theThing.item;
	return {
		users:state.users,
		lookups:state.lookups,
		theThing: state.theThing,
		initialValues:{
			id:state.theThing.item.id,
			project_id: state.projects.selectedItem.project_id,
			user_id:state.user.userInfo.user_id,
			title: state.theThing.item.title,
			assigned_to:state.theThing.item.assigned_to,
			plan_status:state.theThing.item.plan_status,
			priority:state.theThing.item.priority,
			comment: state.theThing.item.comment
		}
	}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({editProjectActionPlans}, dispatch);
}


Edit = reduxForm({form:'editProjectActionPlan', validate})(Edit);
export default connect(mapStateToProps, mapDispatchToProps)(Edit);