import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm} from 'redux-form'
import _ from 'lodash'
import moment from 'moment'
import DatePicker from 'material-ui/DatePicker'
import RaisedButton from 'material-ui/RaisedButton'
import DialogTitlebar from '../../../../sharedcomps/dialog/DialogTitlebar'
import {addProjectActionPlans} from '../../../actions/index'
import {renderField,renderTextArea,renderSelect,renderDateField} from '../../../../sharedcomps/forms/render_functions'

const validate = (values) => {
	const errors = {};
	if(_.size(values.title) === 0)
		errors.title = 'Title is required'
	if(_.size(values.comment) === 0)
		errors.comment = 'Comments are required'
	if(_.size(values.plan_status) === 0 || values.plan_status === 'unassigned')
		errors.plan_status = 'Status is required'
	return errors;
}
function _createOptions(list){
	const items = _.concat([{id:'',name:''}], list);
	return items.map( (item, i) => <option key={i} value={item.id}>{item.name}</option> );
}

function _createUserOptions(list){
	//console.log('-------', list);
	const items = _.concat([{id:'unassigned',first_name:'',last_name:''}], list);
	return items.map( (item, i) => {
		let text = `${_.upperFirst(item.first_name)} ${_.upperFirst(item.last_name)}`;
		return <option key={i} value={item.id}>{text}</option>
	} );
}

class AddNew extends Component {


  constructor(props) {
    super(props);
    this.state = {
    	date:undefined
    }
    this.rejectAction = this.rejectAction.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.setDate = this.setDate.bind(this);
  }

  setDate(e, date){
		console.log('date:', date);
		console.log('moment date:',moment(date, 'x'));
		console.log('js date:',new Date(date).valueOf()/1000);
		this.setState({date:new Date(date).valueOf()/1000});
		//this.setState({date:moment(date, 'x')});
	}

  rejectAction(){
  	console.log('rejectAction');
  	this.props.closeHandler();
  	//this.setState({open:false});
  }

  onFormSubmit(formData){
  	const data = Object.assign({}, formData, {deadline:this.state.date});
  	console.log('onFormSubmit', data);
  	this.props.addProjectActionPlans(data);
  }

  render() {
  	const { handleSubmit, pristine, reset, submitting, users, lookups } = this.props;

    return (
      <div>
      		<DialogTitlebar title="New Action Plan" closeHandler={ this.rejectAction }/>
    		<div style={{padding:16}}>
    			<form className="addForm" onSubmit={handleSubmit(this.onFormSubmit)}>
    				<Field name="id" component="input" type="hidden" />
					<Field name="project_id" component="input" type="hidden" />
					<Field name="submitted_by" component="input" type="hidden" />
    				<Field name="title" label="Title" component={renderField} type="text" required={true} />
    				<Field name="assigned_to" label="Assigned to" component={renderSelect} children={_createUserOptions(users.list)} />
					<Field name="plan_status" label="Status" component={renderSelect} children={_createOptions(lookups.action_plans_status)} required={true} />
					<Field name="priority" label="Priority" component={renderSelect} children={_createOptions(lookups.action_plans_priority)} />
					<div style={{marginBottom:20}}>
					    <label>Deadline</label>
					    <div>
					    	<DatePicker key="datepick" hintText="Select a date" mode="landscape" onChange={this.setDate} />
					    </div>
					</div>
					<Field name="comment" label="Comments" component={renderTextArea} label="Comments" required={true} />
					<div className="right-align" style={{marginTop:20}}>
						<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
					</div>
    			</form>
    		</div>
    	</div>
    );
  }
}
function mapStateToProps(state){
	return {
		users:state.users,
		lookups:state.lookups,
		initialValues:{
			id:'0',
			project_id: state.projects.selectedItem.project_id,
			submitted_by: state.user.userInfo.user_id
		}
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({addProjectActionPlans}, dispatch);
}
AddNew = reduxForm({form:'newActionPlan', validate})(AddNew);
export default connect(mapStateToProps, mapDispatchToProps)(AddNew);