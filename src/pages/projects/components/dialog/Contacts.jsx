import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'
import SelectField from 'material-ui/SelectField'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import IconButton 			from 'material-ui/IconButton'
import IconMenu 			from 'material-ui/IconMenu'
import MenuItem 			from 'material-ui/MenuItem'
import MoreVertIcon 		from 'material-ui/svg-icons/navigation/more-vert'

import _ from 'lodash'

import ResultsDisplayToolbar from '../ResultsDisplayToolbar'
import CollapsibleHeader from '../../../../shared/components/CollapsibleHeader'

import {
	fetchContactsToProjects,
	addContactToProject,
	removeContactFromProject} from '../../actions/index'

import { notify } from '../../../../shared/actions/index'

function _ofType(list, value, key = 'type'){
	return _.filter(list, function(item){ return item[key] == value; });
}

function _selectedContacts(list, rows){
	let output = [];
	_.forEach(rows, function(index){
		console.log('-->', index);
		output.push(_.nth(list, index));
	});
	return output;
}
/*
function _defaultToolbar(addHandler, removeHandler, type){
	return (<Toolbar style={{backgroundColor:'#fff'}}>
	<ToolbarGroup firstChild={true} />
		<ToolbarGroup lastChild={true}>
			<IconMenu
				iconButtonElement={ <IconButton><MoreVertIcon /></IconButton> }
				anchorOrigin={{horizontal: 'right', vertical: 'top'}}
				targetOrigin={{horizontal: 'right', vertical: 'top'}} >
				<MenuItem primaryText="Add New" onTouchTap={ (e) => addHandler(type) } />
				<MenuItem primaryText="Remove" onTouchTap={ (e) => removeHandler(type) } />
			</IconMenu>
		</ToolbarGroup>
	</Toolbar>);
}
function _defaultTable(list, rowSelectionHandler){
	return (<Table onRowSelection={ rowSelectionHandler }>
					<TableHeader>
			        	<TableRow>
			        		<TableHeaderColumn><span className="tableHeaderColumnText">First Name</span></TableHeaderColumn>
			        		<TableHeaderColumn><span className="tableHeaderColumnText">Last Name</span></TableHeaderColumn>
			        		<TableHeaderColumn><span className="tableHeaderColumnText">E-mail</span></TableHeaderColumn>
			        		<TableHeaderColumn><span className="tableHeaderColumnText">Phone Work</span></TableHeaderColumn>
				      	</TableRow>
				    </TableHeader>
				    <TableBody  showRowHover={true}>
				    	{
				    		list.map(function(item, index){
						    	return (
						    		<TableRow key={index}>
						    			<TableRowColumn>{item.first_name}</TableRowColumn>
						    			<TableRowColumn>{item.last_name}</TableRowColumn>
						    			<TableRowColumn>{item.email}</TableRowColumn>
						    			<TableRowColumn>{item.phone_work}</TableRowColumn>
						        	</TableRow>
						        )
				    		})
				    	}
				    </TableBody>
				</Table>)
}
*/

class Contacts extends Component{


	constructor(props){
		super(props);
		this.state = {
			open: false,
			selectedType: 'u',
			selectedValue: -1
		};

		this.removableUser = {};
		this.teamRowSelectionHandler = this.teamRowSelectionHandler.bind(this);
		this.contactsRowSelectionHandler = this.contactsRowSelectionHandler.bind(this);
		this.removeHandler = this.removeHandler.bind(this);
		this.addHandler = this.addHandler.bind(this);
		this.confirmUserToProject = this.confirmUserToProject.bind(this);
		this.onSelectChange = this.onSelectChange.bind(this);
		this.onTypeChange = this.onTypeChange.bind(this);
	}

	onSelectChange(event, index, value){
		console.log('person', value, index);
		const list = (this.state.selectedType === 'u') ? this._userList : this._contactList;
		this.selectedContactForAdd = list[index];
		this.setState({selectedValue:index});
		console.log('this.selectedContactForAdd', this.selectedContactForAdd);
	}
	onTypeChange(event, value){
		console.log('type', value);
		this.setState({selectedType: value});
	}
	confirmUserToProject(){
		if(this.selectedContactForAdd){
			const who = this.selectedContactForAdd;
			console.log('confirmUserToProject', this.state.selectedType, who);
			let id = (who.type === 'u') ? who.id : who.contact_id;
			this.props.addContactToProject({
				project_id:	this.props.projects.selectedItem.project_id,
				contact_id: id,
				type: who.type
			});
		}
		this.selectedContactForAdd = undefined;
		this.setState({selectedValue: -1, open:false});
	}
	addHandler(){
		console.log('addHandler');
		//this.setState({open:true});
	}
	removeHandler(){
		// single row selection
		/*if(this.selectedContact){
			const who = this.selectedContact;
			console.log('remove: pid', this.props.projects.selectedItem.project_id, who);
			let id = (who.type === 'u') ? who.user_id : who.contact_id;
			this.props.removeContactFromProject({
				project_id:	this.props.projects.selectedItem.project_id,
				contact_id: id,
				type: who.type
			});
		}
		this.selectedContact = undefined;*/
		// multi row selection
		if(this.selectedContacts){
			const action = this.props.removeContactFromProject;
			const pid = this.props.projects.selectedItem.project_id;
			_.forEach(this.selectedContacts, function(who){
				let id = (who.type === 'u') ? who.id : who.contact_id;
				action({
					project_id:	pid,
					contact_id: id,
					type: who.type
				});
			});
		}
		this.selectedContacts = undefined;
	}

	teamRowSelectionHandler(rows){
		console.log('rows', rows);
		if(_.size(rows) == 0)
			return;
		//this.selectedContact = _.clone(this._users[_.head(rows)]);
		// multi row selection
		this.selectedContacts = _selectedContacts(this._users, rows);
	}
	contactsRowSelectionHandler(rows){
		console.log('rows', rows);
		if(_.size(rows) == 0)
			return;
		//this.selectedContact = _.clone(this._contacts[_.head(rows)]);
		// multi row selection
		this.selectedContacts = _selectedContacts(this._contacts, rows);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {
		console.log('contacts will mount');
	}
	componentDidMount() {
		console.log('contacts did mount');
		$('.collapsible').collapsible({accordion : false });
		this.props.fetchContactsToProjects(this.props.projects.selectedItem.project_id);
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {

		// in tutto il componente Contacts viene usata la versione dello state ordinata per cognome/nome
		this._userList = this.orderListByLastName(nextProps.users.list);
		this._contactList = this.orderListByLastName(nextProps.contacts.list);
		//console.log('contacts will update', this._userList, this._contactList);
	}
	componentDidUpdate(prevProps, prevState) {
		console.log('contacts did update');
		$('.collapsible').collapsible({accordion : false });
		console.log('this.props',this.props.projects.operationCode9);
		console.log('prevProps',prevProps.projects.operationCode9);
		if(!_.isUndefined(this.props.projects.operationCode9) && prevProps.projects.operationCode9 !== this.props.projects.operationCode9)
			this.props.notify({message:'The contact already exists'});
		//this.props._listOfContacts = this.props.projects.selectedItemContacts;
		//this.props._users = _ofType(this.props.projects.selectedItemContacts, 'u');
		//this.props._contacts = _ofType(this.props.projects.selectedItemContacts, 'c');
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {
		console.log('contacts will unmount');
	}


	orderListByLastName(list){
		return _.sortBy(list, ['last_name', 'first_name']);
	}


	createSelectOptions(){
		const list = (this.state.selectedType === 'u') ? this._userList : this._contactList;
		//console.log(this.state.selectedType, '->', 'number of persons', _.size(list), list);
		const selectOptions = [];
		for(let i = 0; i < _.size(list); i++){
			selectOptions.push(<MenuItem value={i} key={i} primaryText={`${list[i].last_name}, ${list[i].first_name}`} />);
		}
		return selectOptions;
	}

	render(){

		this._users = _ofType(this.props.projects.selectedItemContacts, 'u');
		this._contacts = _ofType(this.props.projects.selectedItemContacts, 'c');

		const dialogActions = [
			<FlatButton label="Cancel" primary={true} onTouchTap={(e) => this.setState({open:false})} />,
			<FlatButton label="Confirm" primary={true} onTouchTap={(e) => this.confirmUserToProject()} />
		];




		return(
			<div>
			<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text="Project Contacts" />
					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						<IconMenu
							iconButtonElement={ <IconButton><MoreVertIcon /></IconButton> }
							anchorOrigin={{horizontal: 'right', vertical: 'top'}}
							targetOrigin={{horizontal: 'right', vertical: 'top'}} >
							<MenuItem primaryText="Add New" onTouchTap={ (e) => this.setState({open:true}) } />
							<MenuItem primaryText="Remove Selected" onTouchTap={ (e) => this.removeHandler() } />
						</IconMenu>
					</ToolbarGroup>
				</Toolbar>
				<ul className="collapsible" data-collapsible="expandable">
					<li>
						<div className="collapsible-header active">
							<CollapsibleHeader  title="MCPP Team" />
						</div>
						<div className="collapsible-body">
							<Table onRowSelection={ this.teamRowSelectionHandler } multiSelectable={true}>
								<TableHeader>
						        	<TableRow>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">First Name</span></TableHeaderColumn>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">Last Name</span></TableHeaderColumn>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">E-mail</span></TableHeaderColumn>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">Phone Work</span></TableHeaderColumn>
							      	</TableRow>
							    </TableHeader>
							    <TableBody  showRowHover={true}>
							    	{
							    		this._users.map(function(item, index){
									    	return (
									    		<TableRow key={index}>
									    			<TableRowColumn>{item.first_name}</TableRowColumn>
									    			<TableRowColumn>{item.last_name}</TableRowColumn>
									    			<TableRowColumn>{item.email}</TableRowColumn>
									    			<TableRowColumn>{item.phone_work}</TableRowColumn>
									        	</TableRow>
									        )
							    		})
							    	}
							    </TableBody>
							</Table>
						</div>
					</li>
					<li>
						<div className="collapsible-header active">
							<CollapsibleHeader  title="Contacts" />
						</div>
						<div className="collapsible-body">
							<Table onRowSelection={ this.contactsRowSelectionHandler }  multiSelectable={true}>
								<TableHeader>
						        	<TableRow>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">First Name</span></TableHeaderColumn>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">Last Name</span></TableHeaderColumn>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">E-mail</span></TableHeaderColumn>
						        		<TableHeaderColumn><span className="tableHeaderColumnText">Phone Work</span></TableHeaderColumn>
							      	</TableRow>
							    </TableHeader>
							    <TableBody  showRowHover={true}>
							    	{
							    		this._contacts.map(function(item, index){
									    	return (
									    		<TableRow key={index}>
									    			<TableRowColumn>{item.first_name}</TableRowColumn>
									    			<TableRowColumn>{item.last_name}</TableRowColumn>
									    			<TableRowColumn>{item.email}</TableRowColumn>
									    			<TableRowColumn>{item.phone_work}</TableRowColumn>
									        	</TableRow>
									        )
							    		})
							    	}
							    </TableBody>
							</Table>
						</div>
					</li>
				</ul>


				<Dialog
					title="Add a contact to this project"
					modal={true}
					open={this.state.open}
					actions={dialogActions}
				>
					<RadioButtonGroup name="typeOfContact" defaultSelected={this.state.selectedType} onChange={this.onTypeChange}>
						<RadioButton value="u" label="MCPP Team" />
						<RadioButton value="c" label="Contact" />
					</RadioButtonGroup>
					<SelectField value={this.state.selectedValue} onChange={this.onSelectChange} maxHeight={240} floatingLabelText="Select a person" floatingLabelFixed={true} autoWidth={true} floatingLabelStyle={{fontSize:'1rem',color:'#000'}}>
						{this.createSelectOptions()}
					</SelectField>
				</Dialog>



			</div>
		);
	}
}
Contacts.propTypes = {};
Contacts.defaultProps = {};

function mapStateToProps({projects, contacts, users}){
	return {projects, contacts, users};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		fetchContactsToProjects,
		addContactToProject,
		removeContactFromProject,
		notify
	}, dispatch);
}
export default connect(mapStateToProps,mapDispatchToProps)(Contacts);