import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm} from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import _ from 'lodash'
import Dialog 		from 'material-ui/Dialog'
import MenuItem 	from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import DetailsIcon	from 'material-ui/svg-icons/image/details'
import DeleteIcon 	from 'material-ui/svg-icons/action/delete'
import AddIcon 		from 'material-ui/svg-icons/content/add'
import EditIcon		from 'material-ui/svg-icons/editor/mode-edit'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';

//import NewActionPlan from '../forms/NewActionPlan'
//import DeleteActionPlan from '../forms/DeleteActionPlan'

import DialogTitlebar from '../../../sharedcomps/dialog/DialogTitlebar'
import BaseToolbarAdvanced from '../../../sharedcomps/comps/BaseToolbarAdvanced'
import Details from './actionplans/Details'
import Delete from './actionplans/Delete'
import Edit from './actionplans/Edit'
import AddNew from './actionplans/AddNew'

import {fetchProjectActionPlans, deleteProjectActionPlans,selectProjectActionPlan} from '../../actions/index'
import {notify, setContextForEditDeleteDetails, clearContextForEditDeleteDetails} from '../../../../shared/actions/index'

import {refactorKey, stringFromId} from '../../../../utils/functions'

import styles from '../../../../styles/styles'

function whoIsTheChild(name, closeDialogHandler){
	switch(name){
		case 'add':
			return <AddNew closeHandler={ closeDialogHandler } />;
		case 'delete':
			return <Delete closeHandler={ closeDialogHandler } />;
		case 'details':
			return <Details closeHandler={ closeDialogHandler } />;
		case 'edit':
			return <Edit closeHandler={ closeDialogHandler } />;
		default:
			return <Details closeHandler={ closeDialogHandler } />;
	}
}

class ActionPlans extends Component{
	constructor(props){
		super(props);
		this.state = {
			open:false,
			openDelete:false,
			selectedActionPlan:undefined,
			openDialog:false,
			actionName:'',
			isEditDeleteDetailsDisabled:false
		};
		this.closeHandler = this.closeHandler.bind(this);
		this.closeDialogHandler = this.closeDialogHandler.bind(this);
		//this.openDialog = this.openDialog.bind(this);
		//this.openDeleteDialog = this.openDeleteDialog.bind(this);
		//this.removeHandler = this.removeHandler.bind(this);
		this.rowSelectionHandler = this.rowSelectionHandler.bind(this);
		this.toolbarActionHandler = this.toolbarActionHandler.bind(this);
	}

	closeHandler(){
		console.log('closeHandler');
		this.props.closeHandler();

	}

	closeDialogHandler(){
		console.log('closeDialogHandler');
		this.setState({openDialog:false});
		this.props.clearContextForEditDeleteDetails();
		// reset reducer prop to avoid accidental operations on the previous selected item
		//this.props.selectProjectActionPlan({});
		//this.props.resetFilters();
		//this.props.fetchContacts();
	}


	toolbarActionHandler(event, actionName){
		console.log('Contacts toolbarActionHandler', actionName);
		this.setState({openDialog:true, actionName:actionName});
	}


	removeHandler(){
		//console.log('removeHandler this.selectedToDelete',this.selectedToDelete);
		//if(this.state.selectedActionPlan)
		//	this.props.deleteProjectActionPlans(this.state.selectedActionPlan);
		//this.closeDeleteDialogHandler();
	}

	rowSelectionHandler(rows){
		//this.setState({isEditDeleteDetailsDisabled: (_.size(rows) != 1)});
		if(_.size(rows) === 0){
			this.props.clearContextForEditDeleteDetails();
		}
		if(_.size(rows) === 1){
			const plan = this.props.projects.selectedItemActionPlans[_.head(rows)];
			console.log('ActionPlans.rowSelectionHandler', plan);
			this.props.setContextForEditDeleteDetails(plan);
			//this.props.selectProjectActionPlan(plan);
		}
		console.log('ActionPlans.rowSelectionHandler', rows);
			//this.setState({selectedActionPlan:this.props.projects.selectedItemActionPlans[_.head(rows)]});
			//this.selectedToDelete = this.props.projects.selectedItemActionPlans[_.head(rows)];
		/*if(_.isString(rows) && rows === 'all'){

			return;
		}
		else{
			if(_.size(rows) === 0)
				//this.selectedToDelete = undefined;
				return;

		}*/
	}

	componentDidMount() {
		this.props.fetchProjectActionPlans(this.props.projects.selectedItem.project_id);
	}

	componentDidUpdate(prevProps, prevState) {
		const p = this.props.projects;
		console.log('p.operationCodeSuccess',p.operationCodeSuccess);
		console.log('p.operationCodeError',p.operationCodeError);
		//const error = this.props.projects.operationCodeError;
		if(prevProps.projects.operationCodeSuccess !== p.operationCodeSuccess){
			this.props.notify({message:'Operation success!'});
			this.props.fetchProjectActionPlans(this.props.projects.selectedItem.project_id);
			this.setState({openDialog:false});
			//this.setState({open:false});
		}
		if(prevProps.projects.operationCodeError !== p.operationCodeError){
			this.props.notify({message:'Operation fail!'});
			this.setState({openDialog:false});
		}
	}

	render(){
		const users = this.props.users;
		const tableLayout = {tableLayout:'auto'};
		//let plans = this.props.projects.selectedItemActionPlans;

		// remove status prop
		const plans = _.map(this.props.projects.selectedItemActionPlans, function(item){
			return _.omit(item, 'status');
		});

		const headerColumns = _.map(plans[0], function(value, key){
			return key;
		});
		const selectedProjectActionPlan = this.props.projects.selectedProjectActionPlan;
		/*const menuItems = [
			<MenuItem key="0" primaryText="Add New" onTouchTap={ (e) => this.openDialog() } />,
			<MenuItem key="1" primaryText="Remove Selected" onTouchTap={ (e) => this.openDeleteDialog() } />
		];*/
		const dialogChild = whoIsTheChild(this.state.actionName, this.closeDialogHandler);
		// more vert menu
		const menuItems = [
			<MenuItem key="details" leftIcon={<DetailsIcon />} primaryText="Details" onTouchTap={ (e) => this.toolbarActionHandler(e, 'details') } />,
			<MenuItem key="edit" leftIcon={<EditIcon />} primaryText="Edit" onTouchTap={ (e) => this.toolbarActionHandler(e, 'edit') } />,
			<MenuItem key="delete" leftIcon={<DeleteIcon />} primaryText="Delete" onTouchTap={ (e) => this.toolbarActionHandler(e, 'delete') } />
		];

		// more icons
		const extraIcons = [
			<IconButton key="add" onClick={ (e) => this.toolbarActionHandler(e, 'add') }>
				<AddIcon />
			</IconButton>
		];
		//const dialogActions = [ ];
		/*const dialogDeleteActions = [
			<RaisedButton label="No" primary={false} onClick={(e)=> this.closeDeleteDialogHandler()}/>,
			<RaisedButton label="Yes" primary={true} onClick={(e)=> this.removeHandler()}/>
		];*/

		return(
			<div>
				<DialogTitlebar title="Project Action Plans" closeHandler={ this.closeHandler }/>
				<BaseToolbarAdvanced
					children={menuItems}
					extraIcons={extraIcons} />
				<Table
					className="projectActionPlansTable"
					bodyStyle={{overflow:'visible'}}
					style={tableLayout}
					fixedHeader={ false }
					fixedFooter={ false }
					onRowSelection={this.rowSelectionHandler}
				>
					<TableHeader>
			        	<TableRow>
			        			{
			        				headerColumns.map((value, i) =>(
			        						<TableHeaderColumn key={i}>
					        					<span className="tableHeaderColumnText">{_.upperFirst(refactorKey(value))}</span>
					        				</TableHeaderColumn>
			        					)
			        				)
			        			}
				      	</TableRow>
				    </TableHeader>

				    <TableBody showRowHover={true} deselectOnClickaway={true}>
				    	{
				    		plans.map((plan, index) =>
				    			(
				    				<TableRow selectable={true} key={index}>
				    					{
				    						_.map(plan, (value, key) => (<TableRowColumn key={value}>{stringFromId(key, value, this.props.lookups, users.list)}</TableRowColumn>)
				    						)
				    					}
				    				</TableRow>
				    			)
				    		)
				    	}
				    </TableBody>
				</Table>

				{/* start dialog */}
				<Dialog
					autoScrollBodyContent={true}
					children={dialogChild}
					open={this.state.openDialog}
					onRequestClose={ this.closeDialogHandler }
					contentStyle={styles.projectDialog} />
				{/* end dialog */}


				{/*<NewActionPlan open={this.state.open}/>*/}

				{/*<Dialog
					open={this.state.open}
					modals={true}
					onRequestClose={this.closeDialogHandler}
				>
					<DialogTitlebar title="Add a new Action Plan" closeHandler={ this.closeDialogHandler }/>
					<NewActionPlan closeContainerDialogHandler={this.closeDialogHandler}/>
				</Dialog>*/}



				{/*<Dialog
					open={this.state.openDelete}
					actions={dialogDeleteActions}
					modals={true}
					title="Delete Action Plan"
					onRequestClose={this.closeDeleteDialogHandler}
				>
					<div style={{padding:16}}>Confirm deletion?</div>
				</Dialog>*/}



			</div>
		);
	}
}
ActionPlans.propTypes = {};
ActionPlans.defaultProps = {};

function mapStateToProps({projects, lookups, users}){
	return {projects, lookups, users}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		fetchProjectActionPlans,
		deleteProjectActionPlans,
		selectProjectActionPlan,
		setContextForEditDeleteDetails,
		clearContextForEditDeleteDetails,
		notify
	}, dispatch);
}

ActionPlans = reduxForm({form:'addSelectedProjectActionPlan'})(ActionPlans);
export default connect(mapStateToProps, mapDispatchToProps)(ActionPlans);