import React, {Component} from 'react';

import DialogTitlebar from '../../../sharedcomps/dialog/DialogTitlebar'

class Files extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
	}

	closeHandler(){
		console.log('closeHandler');
		this.props.closeHandler();
	}

	render(){
		return(
			<div>
				<DialogTitlebar title="Project Files" closeHandler={ this.closeHandler }/>
			</div>
		);
	}
}
Files.propTypes = {};
Files.defaultProps = {};

export default Files;