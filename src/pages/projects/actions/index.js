import axios from 'axios'
//import globals from '../../../config/globals'

// api end points
import {
	ALL_PROJECTS,
	SINGLE_PROJECT,
	PROJECT_EDIT,
	PROJECT_DELETE,
	PROJECT_ADD,
	PROJECT_SUBMIT,
	PROJECT_CHART_1,
	PROJECT_CONTACTS,
	PROJECT_HISTORY,
	PROJECT_ACTION_PLANS,
	PROJECT_ACTIONS_PLANS_DELETE,
	PROJECT_ACTIONS_PLANS_ADD
} from '../../../config/api'
// action ypes
import {
	FETCH_PROJECTS,
	FETCH_PROJECTS_CHART_1,
	BUFFER_FILTERS_PROJECTS,
	APPLY_FILTERS_PROJECTS,
	RESET_FILTER_PROJECTS,
	PAGINATION_PROJECTS_CHANGE,
	SELECT_PROJECT,
	SELECT_MULTI_PROJECT,
	SORT_PROJECTS,
	EDIT_PROJECT,
	EDIT_PROJECT_LOCAL,
	SUBMIT_PROJECT,
	DELETE_PROJECT,
	RESET_PROJECTS_STATE,
	FETCH_CONTACTS_TO_PROJECT,
	ADD_CONTACT_TO_PROJECT,
	REMOVE_CONTACT_FROM_PROJECT,
	FETCH_PROJECT_HISTORY,
	FETCH_PROJECT_ACTION_PLANS,
	DELETE_PROJECT_ACTIONS_PLANS,
	ADD_PROJECT_ACTIONS_PLANS,
	SELECT_ACTION_PLAN
} from '../../../actions/types'


export function fetchProjects(){
	const request = axios.get(ALL_PROJECTS);
	//console.log('request', request);
	return {
		type: FETCH_PROJECTS,
		payload: request
	}
}

export function fetchProjectChart1(id){
	const request = axios.get(`${PROJECT_CHART_1}${id}`);
	return {
		type: FETCH_PROJECTS_CHART_1,
		payload: request
	}
}


/*
export function reset(){
	return {
		type: RESET_PROJECTS_STATE,
		payload: {}
	}
}*/

export function bufferFilterProjects(data){
	return {
		type: BUFFER_FILTERS_PROJECTS,
		payload: data
	}
}

export function resetFilterProjects(){
	return {
		type: RESET_FILTER_PROJECTS,
		payload: {}
	}
}

export function applyFilterProjects(){
	return {
		type: APPLY_FILTERS_PROJECTS,
		payload: {}
	}
}

export function paginationChange(page){
	return {
		type:PAGINATION_PROJECTS_CHANGE,
		payload: page
	}
}
export function sortList(sortKey){
	return {
		type: SORT_PROJECTS,
		payload: sortKey
	}
}

export function selectProject(rows){
	console.log('selectProject', rows);
	return {
		type: SELECT_PROJECT,
		payload: rows
	}
}

export function selectMultiProject(rows){
	return {
		type: SELECT_MULTI_PROJECT,
		payload: rows
	}
}


export function deleteProject(selectedItem){
	const request = axios.post(PROJECT_DELETE, {scope:'projects', id:selectedItem.project_id});
	return {
		type: DELETE_PROJECT,
		payload: request,
		selectedItem: selectedItem
	}
}

export function editProject(selectedItem, formData){
	return {
		type: EDIT_PROJECT_LOCAL,
		payload: formData,
		selectedItem: selectedItem
	}
}


/**
 * submit a project to REST (for edit or add pourpose)
 * no need to separate the endpoint cause the difference is made
 * by the submission of a project_id value
 * @param  {[type]} formData [description]
 * @return {[type]}          [description]
 */
export function submitProject(formData){
	const request = axios.post(PROJECT_SUBMIT, formData);
	console.log('action creator submitProject');
	return {
		type: SUBMIT_PROJECT,
		payload: request
	}
}

export function addProjectToVehicle(formData){
	const request = axios.post(PROJECT_ADD, formData);
	console.log('action creator addProjectToVehicle', formData);
	return {
		type: SUBMIT_PROJECT,
		payload: formData
	}
}

export function editProjectData(formData){
	const request = axios.post(PROJECT_EDIT, formData);
	return {
		type: EDIT_PROJECT,
		payload: request
	}
}


export function fetchContactsToProjects(projectId){
	//console.log('dinamicizzare il projectId');
	// todo: dinamicizzare il projectId
	//const projectId = '67';
	const request = axios.get(`${PROJECT_CONTACTS}${projectId}`);
	return {
		type: FETCH_CONTACTS_TO_PROJECT,
		payload: request
	}
}

/**
 *
 *  start - project action plans
 *
 */
export function selectProjectActionPlan(selectedItem={}){
	console.log('action', selectedItem);
	return {
		type: SELECT_ACTION_PLAN,
		payload: selectedItem
	}
}

export function fetchProjectActionPlans(projectId){
	//const projectId = '71';
	const request = axios.get(`${PROJECT_ACTION_PLANS}${projectId}`);
	return {
		type: FETCH_PROJECT_ACTION_PLANS,
		payload: request
	}
}

export function deleteProjectActionPlans(selectedItem){
	const request = axios.post(PROJECT_ACTIONS_PLANS_DELETE, {scope:"projects_action_plans",id:selectedItem.id});
	return {
		type: DELETE_PROJECT_ACTIONS_PLANS,
		payload: request
	}
}

export function addProjectActionPlans(formData){
	console.log('actions', formData);
	const request = axios.post(PROJECT_ACTIONS_PLANS_ADD, formData);
	return {
		type: ADD_PROJECT_ACTIONS_PLANS,
		payload: request
	}
}

export function editProjectActionPlans(formData){
	console.log('actions', formData);
	const request = axios.post(PROJECT_ACTIONS_PLANS_ADD, formData);
	return {
		type: ADD_PROJECT_ACTIONS_PLANS,
		payload: request
	}
}
/**
 *
 *  end - project action plans
 *
 */


export function addContactToProject(data){
	console.log('action addContactToProject', data);
	const request = axios.post(PROJECT_CONTACTS, Object.assign({}, data, {delete:'0'}));
	return {
		type: ADD_CONTACT_TO_PROJECT,
		payload: request
	}
}


export function removeContactFromProject(data){
	//console.log('removeContactFromProject', Object.assign({}, data, {delete:'1'}));
	const request = axios.post( PROJECT_CONTACTS, Object.assign({}, data, {delete:'1'}) );
	return {
		type: REMOVE_CONTACT_FROM_PROJECT,
		payload: request
	};
}

export function fetchProjectHistory(projectId){
	console.log('dinamicizzare il projectId');
	// todo: dinamicizzare il projectId
	//const projectId = '68';
	const request = axios.get(`${PROJECT_HISTORY}${projectId}`);
	return {
		type: FETCH_PROJECT_HISTORY,
		payload: request
	}
}

