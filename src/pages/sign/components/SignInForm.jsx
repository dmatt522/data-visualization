import React, {Component} from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// material-ui
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'

// actions creators
import { signIn } from '../actions/index'

/**
 * todo: check height of textfield when focus in
 */


class SignInForm extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		console.log('logged ' + this.props.user.logged + ' name '+this.props.user.name);
		if(this.props.user.logged){
			return (<div>welcome { this.props.user.name }</div>);
		}
		return(
			<div style={ {width: '90%', margin: '0 auto 32px'} }>
				<form>
					<div>
						<TextField
							hintText="Please enter your email"
		    				fullWidth={ true } />
					</div>
					<div style={ {marginBottom: 24} }>
					    <TextField
		    				hintText="Please enter your password"
		    				type="password"
		    				fullWidth={ true } />
					</div>
	    			<div>
		    			<RaisedButton
		    				label="Submit"
		    				fullWidth={ true }
		    				primary={ true }
		    				onClick={ () => this.props.signIn( {logged: !this.props.user.logged, name: 'Ciccillo'} ) } />
					</div>
				</form>
			</div>
		);
	}
}
SignInForm.propTypes = {};
SignInForm.defaultProps = {};

function mapStateToProps(state){
	return {
		user: state.user
	}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({signIn: signIn}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm);