import React, {Component} from 'react'
import { reduxForm, Field } from 'redux-form'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import { SIGN_IN_PROGRESS } from '../../../config/messages'
// default notify messages
//import messages from '../../../config/messages'
// actions to dispatch
//import { ActionNotifier } from '../../../hocs/actionNotifier'
import { signIn } from '../actions/index'
import { notify } from '../../../shared/actions/index'


class SignInForm2 extends Component{

	constructor(props){
		super(props);

		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	onFormSubmit(data){
		//console.log('SignInForm2.onFormSubmit ', data);
		this.props.notify({message:SIGN_IN_PROGRESS});
		this.props.signIn(data);
	}

	render(){

		const { handleSubmit, pristine, reset, submitting } = this.props;

		return(
				<form className="singin-form2"  encType="application/json" onSubmit={ handleSubmit(this.onFormSubmit) }>
					<div className="row">

						<div className="col s12">
							<label>Email</label>
							<Field name="username" type="text" placeholder="Please enter your email" component="input" />
						</div>

						<div className="col s12">
							<label>Password</label>
							<Field name="password" type="password" placeholder="Please enter your password" component="input" />
						</div>

						<div className="col s12 center-align">
							<div className="row">
								<div className="col s6">
									<RaisedButton type="button"
										fullWidth={ true } label="Reset" primary={false}
										disabled={ pristine || submitting } onClick={ reset }  />
								</div>
								<div className="col s6">
									<RaisedButton type="submit"
										fullWidth={ true } label="Sign In" primary={true}
										disabled={ pristine || submitting }  />
								</div>
							</div>
						</div>
					</div>
				</form>
		);
	}
}
SignInForm2.propTypes = {};
SignInForm2.defaultProps = {};

// connect: mapStateToProps, mapDispatchToProps
// reduxForm: form config, mapStateToProps, mapDispatchToProps
function mapStateToProps(state){
	return {};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({ signIn, notify }, dispatch);
}
SignInForm2 = connect(mapStateToProps, mapDispatchToProps)(SignInForm2);
SignInForm2 = reduxForm({ form: 'loginForm' })(SignInForm2);
export default SignInForm2;

// user types something in ... record it on application state
/*
state === {                     <- application state
	form:{ 						<- see rootReducer
		SingInForm:{ 			<- this form
			email: '...',
			password: '...'
		}
	}
}

*/