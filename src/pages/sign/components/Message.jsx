import React, { PropTypes } from 'react';

const Message = ({ title, text }) => {
    return (
        <div>
        	<p><strong>{title}</strong></p>
			<p>{text}</p>
        </div>
    );
};

Message.displayName = 'Message';

Message.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
};

export default Message;
