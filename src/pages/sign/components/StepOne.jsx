import React, {Component} from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import _ from 'lodash'
import {loginStep1} from '../actions/index'


const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div className="col s12">
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)


const validate = (values) => {
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	if(!pattern.test(values.username))
		errors.username = 'Invalid Email';
	if(_.size(values.password)===0)
		errors.password = 'Password is required';
	return errors;
}

class StepOne extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	onFormSubmit(formData){
		//console.log('form submitting', formData);
		this.props.loginStep1(formData);
	}


	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;

		return(

			<form onSubmit={handleSubmit(this.onFormSubmit)} >
				<Field name="username" type="text" label="Email" required={true} component={renderField} />
				<Field name="password" type="password" label="Password" required={true} component={renderField} />
				<div className="center-align">
					<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
					<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
				</div>
			</form>
		);
	}
}
StepOne.propTypes = {};
StepOne.defaultProps = {};

function mapStateToProps({auth}){
	return {auth}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({loginStep1}, dispatch);
}
StepOne = reduxForm({form:'stepOneAuthForm', validate})(StepOne);
export default connect(mapStateToProps, mapDispatchToProps)(StepOne);