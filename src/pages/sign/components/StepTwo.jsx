import React, {Component} from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import _ from 'lodash'
import {loginStep2} from '../actions/index'


const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div className="col s12">
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)


const validate = (values) => {
	const errors = {};
	if(_.size(values.code)===0)
		errors.code = 'Code is required';
	return errors;
}

class StepTwo extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	onFormSubmit(formData){
		//console.log('form submitting', formData);
		this.props.loginStep2(formData);
	}


	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;

		return(

			<form onSubmit={handleSubmit(this.onFormSubmit)} >
				<Field name="username" type="hidden" component="input" />
				<Field name="userid" type="hidden" component="input" />
				<Field name="code_exp" type="hidden" component="input" />
				<Field name="code" type="text" label="Code" required={true} component={renderField} />
				<div className="center-align">
					<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
					<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
				</div>
			</form>
		);
	}
}
StepTwo.propTypes = {};
StepTwo.defaultProps = {};

function mapStateToProps(state){
	return {
		user: state.user,
		initialValues:{
			userid: state.user.authInfo.userid,
			username:state.user.authInfo.username,
			code_exp:state.user.authInfo.code_exp
		}
	}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({loginStep2}, dispatch);
}
StepTwo = reduxForm({form:'stepTwoAuthForm', validate})(StepTwo);
export default connect(mapStateToProps, mapDispatchToProps)(StepTwo);