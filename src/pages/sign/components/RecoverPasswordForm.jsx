import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { bindActionCreators } from 'redux'
import RaisedButton from 'material-ui/RaisedButton'

import { resetPassword } from '../actions/index'

class RecoverPasswordForm extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	onFormSubmit(data){
		console.log('RecoverPasswordForm.onFormSubmit', data);
		this.props.resetPassword(data);
	}

	render(){
		const { handleSubmit } = this.props;
		return(
				<form onSubmit={ handleSubmit(this.onFormSubmit) }>
					<div>
		          		<label>Email</label>
		          		<Field name="email" type="text" placeholder="Please enter your email" component="input" />
						{/*<input id="email" type="email" className="validate" placeholder="Please enter your email" />*/}
					</div>
					<div>
						<RaisedButton type="submit" label="Send" fullWidth={ true } primary={ true } />
					</div>
				</form>
		);
	}
}
RecoverPasswordForm.propTypes = {};
RecoverPasswordForm.defaultProps = {};


function mapDispatchToProps(dispatch){
	return bindActionCreators({ resetPassword }, dispatch);
}
// now can dispatch actions
RecoverPasswordForm = connect(null, mapDispatchToProps)(RecoverPasswordForm);
// now can use redux-form
RecoverPasswordForm = reduxForm({form:'passwordRecovery'})(RecoverPasswordForm);

export default RecoverPasswordForm;