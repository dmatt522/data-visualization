import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Stepper, Step, StepLabel} from 'material-ui/Stepper'
import StepOne from './StepOne'
import StepTwo from './StepTwo'

import {notify} from '../../../shared/actions/index'

const styles = {
	title:{
		textAlign:'center',
		fontWeight:700,
		color:'#cd0000'
	}
}

class TwoFactorAuth extends Component{
	constructor(props){
		super(props);
		this.state = {
			stepIndex:0,
			finished:false
		};
		this.handleNext = this.handleNext.bind(this);
		this.handlePrev = this.handlePrev.bind(this);
		this.getStepContent = this.getStepContent.bind(this);
	}

	handleNext(){
		//const {stepIndex} = this.state;
		this.setState({stepIndex: this.state.stepIndex + 1});
	}
	handlePrev(){
		//const {stepIndex} = this.state;
		this.setState({stepIndex: this.state.stepIndex - 1});
	}

	getStepContent(stepIndex){
		switch(stepIndex){
			case 0:
				return <StepOne />;
			case 1:
				return <StepTwo />;
			default:
				return <StepOne />;
		}
	}

	componentWillReceiveProps(nextProps) {
		console.log('this.state.stepIndex', this.state.stepIndex);
		if(this.state.stepIndex === 0){
			if(nextProps.user.authStep1Complete){
				console.log('authStep1Complete', nextProps.user.authStep1Complete);
				//this.props.notify({message:'Login authentication success'});
				this.handleNext();
			}
			else{
				this.props.notify({message:'Login authentication fail'});
			}

		}
		if(this.state.stepIndex === 1){
			if(nextProps.user.authStep2Complete){
				console.log('authStep1Complete', nextProps.user.authStep2Complete);
				this.props.notify({message:'Code authentication success'});
				//this.handleNext();
			}
			else{
				this.props.notify({message:'Code authentication fail'});
			}

		}

	}


	render(){
		//const {stepIndex, finished} = this.state;
		//

		return(
			<div style={ {width: '100%', maxWidth: 700, margin: 'auto'} }>
				<p style={styles.title}>Authentication</p>
				<Stepper activeStep={this.state.stepIndex}>
					<Step>
						<StepLabel>Login authentication</StepLabel>
					</Step>
					<Step>
						<StepLabel>Code authentication</StepLabel>
					</Step>
				</Stepper>
				<div>
					<div>{this.getStepContent(this.state.stepIndex)}</div>
				</div>
			</div>
		);
	}
}
TwoFactorAuth.propTypes = {};
TwoFactorAuth.defaultProps = {};
function mapStateToProps({user}){
	return {user}
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({notify}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(TwoFactorAuth);