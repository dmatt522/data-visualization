import React, {Component} from 'react'
import { connect } from 'react-redux'

import SignInForm2 from './SignInForm2'

class SignInPanel extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		/*if(this.props.user.logged){
			return (
				<div>welcome back { this.props.user.name }</div>
			)
		}*/
		return(
			<div style={ {width: '90%', margin: '0 auto 32px'} }>
				<SignInForm2 />
			</div>
		);
	}
}
SignInPanel.propTypes = {};
SignInPanel.defaultProps = {};

function mapStateToProps(state){
	return {
		user: state.user,
		loginForm: state.form.loginForm,
		auth: state.auth
	}
}

export default connect(mapStateToProps)(SignInPanel);