import React, {Component} 		from 'react'
import { Link } 				from 'react-router'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
//import ActionNotifier from '../../hocs/actionNotifier'

import FlatButton 	from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog 		from 'material-ui/Dialog'

import HeroSlider 			from '../../shared/ui/HeroSlider'
import Message 				from './components/Message'
import SignInPanel 			from './components/SignInPanel'
import TwoFactorAuth from './components/TwoFactorAuth'
import RecoverPasswordForm 	from './components/RecoverPasswordForm'

import { SIGN_IN_SUCCESS, SIGN_IN_FAIL } from '../../config/messages'
import { notify } from '../../shared/actions/index'
import globals 	from '../../config/globals'
import PATHS 	from '../../routes/paths'

// init fetchings before app starts
import {loginConfig} from './actions/index'
import { fecthAllLookups } from '../../shared/actions/lookups'
import { fetchProjects } from '../projects/actions/index'
import { fetchContacts } from '../contacts/actions/index'
import { fetchUsers } from '../users/actions/index'
import { fetchFiles } from '../files/actions/index'
import { fetchVehicles } from '../vehicles/actions/index'


/**
 * bug: ogni volta che viene ricreata la pagina si crea anche un nuovo elemento indicators che va sopra il precedente
 */

const info = {
	title: 'Welcome to the MPA CommOps Portal.',
	text: 'When logging into the Mitsubishi Chemical Performance Polymer Comm Opps Portal, you agree to the Terms of Use.'
};

const slides = [
	{title:'No word was spoken', subtitle:'No word was spoken until we had left the city far', image:`${globals.basePath}/images/Home-slideshow1.jpg`},
	{title:'This was beyond', subtitle:'This was beyond the boys, and they let it slip by', image:`${globals.basePath}/images/auto-stock.jpg`},
	{title:'It would run on', subtitle:'It would run on, plain and clear and', image:`${globals.basePath}/images/slideshow-stock2.jpg`}
];

class Sign extends Component{
	constructor(props){
		super(props);
		this.state = { open:false };
	}

	componentDidMount() {
		$('.slider').slider({full_width: true});
		this.props.loginConfig();
		this.props.fecthAllLookups();
		this.props.fetchContacts();
		this.props.fetchUsers();
		this.props.fetchFiles();
		this.props.fetchProjects();
		this.props.fetchVehicles();

	}
	componentDidUpdate(prevProps, prevState) {
		$('.slider').slider({full_width: true});
	}

	componentWillReceiveProps(nextProps) {
		//console.log('... componentWillReceiveProps()', nextProps.user.reset , this.props.user.reset);

		// ha solo chiesto un recovery mail
		if(nextProps.user.reset > this.props.user.reset)
			this.setState({open:false});

		// ha eseguito un login
		/*if(nextProps.user.reset === this.props.user.reset){
			(nextProps.user.logged)
			? this.props.notify({message:SIGN_IN_SUCCESS})
			: this.props.notify({message:SIGN_IN_FAIL});
		}*/
	}

	render(){

		const panel = (this.props.user.authType === '1') ? <TwoFactorAuth /> : <SignInPanel /> ;
		console.log('type',this.props.user.authType, 'panel', panel);

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<section className="comp-signin page signin-page">

					{/*<HeroSlider slides={ slides } />*/}

					<div className="page-content" style={{marginTop:20}}>

						<div className="row">
							{/*<div className="col l6 s12">
								<Message { ...info } />
							</div>*/}
							<div className="col s12 m6 l4 offset-m3 offset-l4">
									<div style={{textAlign:'center', marginBottom:24}}>
										<img src="http://placehold.it/117x60" style={{verticalAlign:'top'}} />
									</div>
									<div style={{padding:'24px 0'}}>
										{ panel }
									</div>
				  		  			<RaisedButton
				  		  				label="Forgot Password?"
				  		  				style={ {width: '90%', margin: 'auto', display:'block'} }
				  		  				onTouchTap={ () => this.setState({ open:true }) } />
							</div>
						</div>

					</div>

					<Dialog
						title="Password Recovery"
						modal={ true }
						open={ this.state.open }
						actions={ <FlatButton
									label="Cancel"
									primary={ true }
									onTouchTap={ () => this.setState({ open:false }) } /> } >
						<RecoverPasswordForm />
					</Dialog>
				</section>
			</ReactCSSTransitionGroup>
		);
	}
}
Sign.propTypes = {};
Sign.defaultProps = {};

// recive user state from application store
function mapStateToProps({user}){
	return { user };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		notify,
		loginConfig,
		fecthAllLookups,
		fetchContacts,
		fetchUsers,
		fetchFiles,
		fetchProjects,
		fetchVehicles
	}, dispatch);
}
//Sign = ActionNotifier(Sign);
export default connect(mapStateToProps, mapDispatchToProps)(Sign);