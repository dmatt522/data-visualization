import globals from '../../../config/globals'
import {
	SIGN_IN,
	SIGN_OUT,
	FORGOT_PASSWORD,
	LOGIN_TYPE,
	AUTH_1,
	AUTH_2
	} from '../../../actions/types'
import {
	USER_SIGN_IN,
	PWD_RECOVERY,
	FAKE_SIGN_IN,
	FAKE_PWD_RECOVERY,
	LOGIN_CONFIG,
	LOGIN_1,
	LOGIN_2
	} from '../../../config/api'

import axios from 'axios'

/**
 *	--------------------------------------------------------
 *	GET
 * 	--------------------------------------------------------
 */


/**
 * 	--------------------------------------------------------
 *	POST
 * 	--------------------------------------------------------
 */

/**
 * signIn action creator
 * @param  {object} formData [contains form data]
 * @return {object}          [action object]
 *
 * payload is a Promise object that will pass through redux-promise middleware
 */
export function signIn(formData){
	//console.log(window.location.protocol);
	/*const request = axios.get(FAKE_SIGN_IN);
	const request = axios({
		url: USER_SIGN_IN,
		method: 'post',
		headers: {
			'Content-Type':'application/json',
			'X-Requested-With': 'XMLHttpRequest'
		},
		//transformRequest: [ (formData) => JSON.stringify(formData)],
		data: JSON.stringify([formData])
		//responseType: 'json'
	});*/

	const request = axios.post(USER_SIGN_IN, formData);

	//console.log('ActionCreator', request);
	return {
		type: SIGN_IN,
		payload: request
	}
}

export function loginConfig(){
	const request = axios.get(LOGIN_CONFIG);
	return {
		type: LOGIN_TYPE,
		payload: request
	}
}

export function loginStep1(formData){
	const request = axios.post(LOGIN_1, formData);
	//console.log('ActionCreator', request);
	return {
		type: AUTH_1,
		payload: request
	}
}

export function loginStep2(formData){
	const request = axios.post(LOGIN_2, formData);
	//console.log('ActionCreator', request);
	return {
		type: AUTH_2,
		payload: request
	}
}


/**
 * user logout
 * @return {[type]} [description]
 */
export function signOut(){
	console.log('signOut');
	return {
		type: SIGN_OUT,
		payload: {}
	};
}



/**
 * resetPassword action creator
 * @param  {object} formData [contains form data]
 * @return {object}          [action object]
 *
 * payload is a Promise object that will pass through redux-promise middleware
 */
export function resetPassword(formData){
	//const request = axios.get(FAKE_PWD_RECOVERY);
	//const request = axios.post(PWD_RECOVERY, formData); // retruns a Promise

	const request = axios({
		url: PWD_RECOVERY,
		method: 'post',
		headers: {
			'Content-Type':'application/json',
			'X-Requested-With': 'XMLHttpRequest'
		},
		//transformRequest: [ (formData) => JSON.stringify(formData)],
		data: JSON.stringify([formData])
		//responseType: 'json'
	});


	console.log('Request:', formData, request);
	return {
		type: FORGOT_PASSWORD,
		payload: request
	};
}