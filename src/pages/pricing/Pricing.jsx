import React, {Component} from 'react'
import {connect} from 'react-redux'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import Dialog from 'material-ui/Dialog'
import MenuItem 			from 'material-ui/MenuItem'
import DetailsIcon		 		from 'material-ui/svg-icons/image/details'

import FiltersGroup 	from  '../sharedcomps/FiltersGroup'
import ResultsDisplay 	from '../sharedcomps/ResultsDisplay'
import Details 			from './components/dialog/Details'
import Delete 			from './components/dialog/Delete'

import PageTitleBar 		from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'

import styles from '../../styles/styles'

function whoIsTheChild(name, closeDialogHandler){
	switch(name){
		case 'delete':
			return <Delete closeHandler={ closeDialogHandler } />;
		case 'details':
			return <Details closeHandler={ closeDialogHandler } />;
		default:
			return <Details closeHandler={ closeDialogHandler } />;
	}
}

class Pricing extends Component{
	constructor(props){
		super(props);
		this.state = {
			openDialog:false,
			actionName:''
		};

		this.toolbarActionHandler = this.toolbarActionHandler.bind(this);
		this.paginationChangeHandler = this.paginationChangeHandler.bind(this);
	}

	// close dialog
	closeDialogHandler(){
		this.setState({openDialog:false});
	}

	rowSelectionHandler(rows){
		console.log('rowSelectionHandler', rows);
	}

	// results display toolbar menu actions
	toolbarActionHandler(event, obj){
		console.log('Pricing toolbarActionHandler', obj);
		this.setState({openDialog:true, actionName:obj.actionName});
	}
	// pagination click handler
	paginationChangeHandler(pageNum){
		console.log('Pricing paginationChangeHandler', pageNum);
	}
	// enable/disable toolbar menu to view/edit ..
	enableToolbarTools(rows){
		//console.log('disableTools', (_.size(rows) == 0));
		//this.setState({ disableTools: (_.size(rows) == 0) });
	}

	resetFiltersHandler(){
		//this.props.resetFilterProjects();
	}
	applyFiltersHandler(){
		//this.props.applyFilterProjects();
	}
	bufferFiltersHandler(filter){
		//this.props.bufferFilterProjects(filter);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {
		//this.props.resetFilterPricing();
	}

	render(){
		// section to show in dialog
		const dialogChild = whoIsTheChild(this.state.actionName, this.closeDialogHandler);

		const menuItems = [
			<MenuItem key="abc" leftIcon={<DetailsIcon />} primaryText="Details/Edit" onTouchTap={ (e) => this.toolbarActionHandler(e, 'details') } />
		];

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false}
			>
				<div className="comp-pricing page pricing-page">

					<PageTitleBar title="Pricing" />

					<ul className="collapsible" data-collapsible="expandable">
						{/* start filters here */}
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Search" />
							</div>
						  	<div className="collapsible-body">
								<FiltersGroup />
						  	</div>
						</li>
						{/* end filters here */}
						{/* start table here */}
						<li>
						  	<div className="collapsible-header active">
								<CollapsibleHeader  title="Results" /></div>
						  	<div className="collapsible-body">
								<ResultsDisplay
									data={ this.props.projects }
									menuItems={ menuItems }
									defaultColumns={ this.props.defaultColumns }
									displaySelectAll={true}
									adjustForCheckbox={true}
									displayRowCheckbox={true}
									multiSelectable={false}
									rowSelectionHandler={ this.rowSelectionHandler }
									toolbarActionHandler={ this.toolbarActionHandler }
									paginationChangeHandler={ this.paginationChangeHandler }
								/>
						  	</div>
						</li>
						{/* end table here */}
					</ul>
				</div>

				{/* start dialog */}
				<Dialog
					autoScrollBodyContent={true}
					children={dialogChild}
					open={this.state.openDialog}
					onRequestClose={ () => this.setState({openDialog:false}) }
					contentStyle={styles.projectDialog} />
				{/* end dialog */}


			</ReactCSSTransitionGroup>
		);
	}
}
Pricing.propTypes = {};
Pricing.defaultProps = {
	defaultColumns:[
		'part_image_1',
		'part_type',
		'brand',
		'manufacturer',
		'nameplate',
		'platform',
		'program'
	]
};

function mapStateToProps({projects, user}){
	return {projects, user};
}
export default connect(mapStateToProps)(Pricing);