import React, {Component} from 'react';

class PricingLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<div className="page-layout pricing-page-layout">
				{this.props.children}
			</div>
		);
	}
}
PricingLayout.propTypes = {};
PricingLayout.defaultProps = {};

export default PricingLayout;