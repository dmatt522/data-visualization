import React, {Component} from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton 			from 'material-ui/IconButton'
import Close from 'material-ui/svg-icons/navigation/close'

class DialogToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		return(
			<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text={this.props.title} />
					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						<IconButton children={ <Close /> } onClick={ this.props.closeHandler } />
					</ToolbarGroup>
				</Toolbar>
		);
	}
}
DialogToolbar.propTypes = {};
DialogToolbar.defaultProps = {
	title:'Dialog Title',
	closeHandler:function(event){console.log(event);}
};

export default DialogToolbar;