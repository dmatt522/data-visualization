import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import MenuItem from 'material-ui/MenuItem'
import SelectField from 'material-ui/SelectField'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import _ from 'lodash'

import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
import DialogToolbar from './DialogToolbar'

import {editUser,fetchUsers} from '../../actions/index'
import {createInitValues} from '../../../../utils/functions'
import {notify} from '../../../../shared/actions/index'

function validate(values){
	//console.log('---->', values);
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	if(_.size(values.first_name) === 0)
		errors.first_name = 'First name is Required';
	if(_.size(values.last_name) === 0)
		errors.last_name = 'Last name is Required';
	if(!pattern.test(values.email))
		errors.email = 'Please use a valid email address';
	if(_.size(values.password) === 0)
		errors.password = 'Password is Required';
	return errors;
}

const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div>
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)

const renderRadio = ({input, children, label}) => (
	<div>
		<label>{label}</label>
		<RadioButtonGroup {...input} children={children}
			defaultSelected={input.value}
			onChange={ (event, value) => input.onChange(value) } />
	</div>
)

const renderSelectField = ({ input, label, meta: { touched, error }, children }) => (
 	<div>
	 	<SelectField
	    	floatingLabelText={label}
	    	errorText={touched && error}
	    	{...input}
	    	onChange={(event, index, value) => input.onChange(value)}
	    	children={children} />
    </div>
)

class Edit extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}

	onFormSubmit(formData){
		console.log('Edit.onFormSubmit', this.props.editUser);
		this.props.editUser(formData);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		const users = this.props.users;
		if(prevProps.users.operationCode9 !== users.operationCode9){
			//this.props.notify({message:'User already exits'});
		}
		if(prevProps.users.operationCodeSuccess !== users.operationCodeSuccess){
			this.props.fetchUsers();
			this.props.notify({message:'User edited'});
			this.props.closeHandler();
		}
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;
		const roleChildren = [
			<MenuItem key="1" value="1" primaryText="Admin"/>,
			<MenuItem key="2" value="2" primaryText="Manager"/>,
			<MenuItem key="3" value="3" primaryText="Supervisor"/>,
			<MenuItem key="4" value="4" primaryText="Employee"/>
		];
		return(
			<div className="dialog-edit">
				<DialogToolbar title="Edit user data" closeHandler={this.closeHandler} />
				<div className="row dialog-addnew-form-container">
					<form onSubmit={handleSubmit(this.onFormSubmit)}>

						<Field name="submitted_by" component="input" type="hidden" />
						<Field name="id" component="input" type="hidden" />

						<Field name="first_name" label="First name" component={renderField} type="text" required={true} />
						<Field name="last_name" label="Last name" component={renderField} type="text" required={true} />
						<Field name="email" label="Email" component={renderField} type="email" required={true} />
						<Field name="password" label="Password" component={renderField} type="password" required={true} />

						<Field name="ade" label="Is ADE" component={renderRadio} onChange={ (value) => console.log(value) }>
				      		<RadioButton value="1" key="ade1" label="Yes" />
							<RadioButton value="0" key="ade0" label="No" />
				      	</Field>

				      	<Field name="tech_service" label="Is tech service" component={renderRadio}>
							<RadioButton value="1" key="tc1" label="Yes" />
				      		<RadioButton value="0" key="tc0" label="No" />
				      	</Field>

				      	<Field name="sales_manager" label="Is tech service" component={renderRadio}>
							<RadioButton value="1" key="tc1" label="Yes" />
				      		<RadioButton value="0" key="tc0" label="No" />
				      	</Field>

				      	<Field name="role_id" component={renderSelectField} label="Role">
				      		{ roleChildren }
				      	</Field>

						<RaisedButton type="button" label="Reset" primary={false} disabled={ submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ submitting }  />

					</form>
				</div>
			</div>
		);
	}
}
Edit.propTypes = {};
Edit.defaultProps = {};

function mapStateToProps(state){
	return{
		users: state.users,
		user: state.user,
		initialValues: createInitValues(state.users.selectedItem, ['type'])
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({ editUser,fetchUsers,notify }, dispatch);
}

Edit = reduxForm({form:'editUserDataForm', validate})(Edit);
//Edit = ReduxFormConsumer(Edit, 'editUserDataForm', validate);
export default connect(mapStateToProps, mapDispatchToProps)(Edit);