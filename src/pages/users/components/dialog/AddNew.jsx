import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'
import MenuItem from 'material-ui/MenuItem'
import SelectField from 'material-ui/SelectField'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import _ from 'lodash'

import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
import DialogToolbar from './DialogToolbar'

import {submitNewUser,fetchUsers} from '../../actions/index'
import {notify} from '../../../../shared/actions/index'

function validate(values){
	//console.log('---->', values);
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	if(_.size(values.first_name) === 0)
		errors.first_name = 'First name is required';
	if(_.size(values.last_name) === 0)
		errors.last_name = 'Last name is required';
	if(!pattern.test(values.email))
		errors.email = 'Please use a valid email address';
	if(_.size(values.password) === 0)
		errors.password = 'Password is required';
	return errors;
}

const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div>
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)

const renderRadio = ({input, children, label}) => (
	<div>
		<label>{label}</label>
		<RadioButtonGroup {...input} children={children}
			defaultSelected="0"
			onChange={ (event, value) => input.onChange(value) } />
	</div>
)

const renderSelectField = ({ input, label, meta: { touched, error }, children }) => (
 	<div>
	 	<SelectField
	    	floatingLabelText={label}
	    	errorText={touched && error}
	    	{...input}
	    	onChange={(event, index, value) => input.onChange(value)}
	    	children={children} />
    </div>
)


class AddNew extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}

	onFormSubmit(formData){
		console.log(this, 'submitting form data', formData);

		this.props.submitNewUser(formData);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {
		console.log('this.props',this.props.users.operationCodeSuccess);
		console.log('prevProps',prevProps.users.operationCodeSuccess);
		const users = this.props.users;
		if(prevProps.users.operationCode9 !== users.operationCode9){
			this.props.notify({message:'User already exits'});
		}
		if(prevProps.users.operationCodeSuccess !== users.operationCodeSuccess){
			this.props.fetchUsers();
			this.props.notify({message:'User created'});
			this.props.closeHandler();
		}
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;
		const roleChildren = [
			<MenuItem key="1" value="1" primaryText="Admin"/>,
			<MenuItem key="2" value="2" primaryText="Manager"/>,
			<MenuItem key="3" value="3" primaryText="Supervisor"/>,
			<MenuItem key="4" value="4" primaryText="Employee"/>
		];
		return(
			<div className="dialog-addnew">
				<DialogToolbar title="Add new user" closeHandler={this.closeHandler} />
				<div className="row dialog-addnew-form-container">
					<form onSubmit={handleSubmit(this.onFormSubmit)}>

						<Field name="submitted_by" component="input" type="hidden" />
						<Field name="id" component="input" type="hidden" />

						<Field name="first_name" label="First name" component={renderField} type="text" required={true}/>
						<Field name="last_name" label="Last name" component={renderField} type="text" required={true} />
						<Field name="email" label="Email" component={renderField} type="email" required={true} />
						<Field name="password" label="Password" component={renderField} type="password" required={true} />
						<Field name="phone_work" label="Phone work" component={renderField} type="text" />

				      	<Field name="ade" label="Is ADE" component={renderRadio} onChange={ (value) => console.log(value) }>
				      		<RadioButton value="1" key="ade1" label="Yes" />
							<RadioButton value="0" key="ade0" label="No" />
				      	</Field>

				      	<Field name="tech_service" label="Is tech service" component={renderRadio}>
							<RadioButton value="1" key="tc1" label="Yes" />
				      		<RadioButton value="0" key="tc0" label="No" />
				      	</Field>

				      	<Field name="sales_manager" label="Is tech service" component={renderRadio}>
							<RadioButton value="1" key="tc1" label="Yes" />
				      		<RadioButton value="0" key="tc0" label="No" />
				      	</Field>

				      	<Field name="role_id" component={renderSelectField} label="Role">
				      		{ roleChildren }
				      	</Field>

						<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
					</form>
				</div>
			</div>
		);
	}
}
AddNew.propTypes = {};
AddNew.defaultProps = {};

function mapStateToProps(state){
	return{
		users: state.users,
		user: state.user,
		roles: state.lookups.roles,
		initialValues: { submitted_by: state.user.userInfo.user_id, id:'0', sales_manager:'0', ade:'0', tech_service:'0' }
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({fetchUsers, submitNewUser, notify}, dispatch);
}

AddNew = reduxForm({form:'addNewForm', validate})(AddNew);
//AddNew = ReduxFormConsumer(AddNew, 'addNewForm', validate);
export default connect(mapStateToProps, mapDispatchToProps)(AddNew);