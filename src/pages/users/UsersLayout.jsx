import React, {Component} from 'react';

class UsersLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<div className="page-layout users-page-layout">
				{this.props.children}
			</div>
		);
	}
}
UsersLayout.propTypes = {};
UsersLayout.defaultProps = {};

export default UsersLayout;