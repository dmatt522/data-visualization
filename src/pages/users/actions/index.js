import axios 	from 'axios'
import globals 	from '../../../config/globals'
import {
	ALL_USERS,
	USER_DELETE,
	USER_ADD,
	USER_EDIT
	} from '../../../config/api'
import {
	FETCH_USERS,
	PAGINATION_USERS_CHANGE,
	SORT_USERS,
	APPLY_FILTERS_USERS,
	RESET_FILTER_USERS,
	BUFFER_FILTER_USERS,
	SELECT_USER,
	DELETE_USER,
	ADD_USER,
	EDIT_USER
	} from '../../../actions/types'



// warning: use api urls not locals

/**
 * fetches all vehicles data
 * @return {[type]} [description]
 */
export function fetchUsers(){

	//const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
	//const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
	//const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
	const request = axios.get(ALL_USERS);
	//console.log('Action: fetchVehicles', request);
	//debugger;
	return {
		type: FETCH_USERS,
		payload: request
	};
}


export function paginationChange(page){
	return {
		type:PAGINATION_USERS_CHANGE,
		payload: page
	}
}

export function sortList(sortKey){
	return {
		type: SORT_USERS,
		payload: sortKey
	}
}

export function resetFilters(){
	return{
		type: RESET_FILTER_USERS,
		payload: {}
	}
}
export function applyFilters(){
	return{
		type:APPLY_FILTERS_USERS,
		payload:{}
	}
}
export function bufferFilter(filter){
	return{
		type: BUFFER_FILTER_USERS,
		payload: filter
	}
}

export function selectItem(rows){
	console.log('action selectItem', rows);
	return {
		type: SELECT_USER,
		payload: rows
	}
}

export function deleteUser(selectedItem){
	const request = axios.post(USER_DELETE, {scope:'users', id:selectedItem.id});
	console.log(request);
	return {
		type: DELETE_USER,
		payload: request,
		selectedItem:selectedItem
	}
}

export function submitNewUser(formData){
	console.log('submitNewUser action formData', formData);
	const request = axios.post(USER_ADD, formData);
	return {
		type: ADD_USER,
		payload: request
	}
}

export function editUser(formData){
	//console.log('Users-actions-editUser', formData);
	const request = axios.post(USER_EDIT, formData);
	return {
		type: EDIT_USER,
		payload: request
	}
}