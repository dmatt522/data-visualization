import React from 'react'
import {RadioButtonGroup} from 'material-ui/RadioButton'
import SelectField from 'material-ui/SelectField'
/**
 * text input
 * @param  {[type]} options.input    [description]
 * @param  {[type]} options.label    [description]
 * @param  {[type]} options.type     [description]
 * @param  {[type]} options.required [description]
 * @param  {[type]} options.meta:    {            touched, error } [description]
 * @return {[type]}                  [description]
 */
export const renderField = ({ input, label, type, required, meta: { touched, error } }) => (
	<div>
	{/*<div className="col s12 m6 l4">*/}
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)


export const renderDateField = ({ input, label, type, children,required, meta: { touched, error } }) => (
	<div>
	{/*<div className="col s12 m6 l4">*/}
	    <label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	{children}
	    </div>
	</div>
)





/**
 * textarea
 * @param  {[type]} options.input [description]
 * @param  {[type]} options.label [description]
 * @param  {[type]} options.type  [description]
 * @param  {[type]} options.meta: {            touched, error } [description]
 * @return {[type]}               [description]
 */
export const renderTextArea = ({ input, label, type, required, meta: { touched, error } }) => (
	<div>
	{/*<div className="col s12">*/}
		<div>
	    	<label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    	<textarea {...input} rows="8"></textarea>
	    </div>
	</div>
)

/**
 * radio button group
 * @param  {[type]} {input, children,     label}) [description]
 * @return {[type]}          [description]
 */
export const renderRadio = ({input, children, label}) => (
	<div>
		<label>{label}</label>
		<RadioButtonGroup {...input} children={children}
			defaultSelected={input.value}
			onChange={ (event, value) => console.log(value) } />
	</div>
)

/**
 * select
 * @param  {[type]} options.input [description]
 * @param  {[type]} options.label [description]
 * @param  {[type]} options.meta: {            touched,      error [description]
 * @param  {[type]} children      })           [description]
 * @return {[type]}               [description]
 */
export const renderSelectField = ({ input, label, meta: { touched, error }, children }) => (
 	<div>
	 	<SelectField
	    	floatingLabelText={label}
	    	errorText={touched && error}
	    	{...input}
	    	onChange={(event, index, value) => input.onChange(value)}
	    	children={children} />
    </div>
)

export const renderSelect = ({ input, label, children, required, meta: { touched, error } }) => (
	<div>
		<label>{label} {required && <span className="required-field">*</span>} {touched && error && <span className="form-error-msg">{error}</span>}</label>
		<select {...input} className="browser-default" style={{borderBottom:'1px solid #999',marginBottom:20}}>
			{children}
		</select>
	</div>
)