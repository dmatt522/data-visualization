import React, {Component} from 'react';

class ResultsDisplayToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		return(
			<div>ResultsDisplayToolbar</div>
		);
	}
}
ResultsDisplayToolbar.propTypes = {};
ResultsDisplayToolbar.defaultProps = {};

export default ResultsDisplayToolbar;