import React, {Component} from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton 			from 'material-ui/IconButton'
import Close from 'material-ui/svg-icons/navigation/close'

class DialogTitlebar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text={this.props.title} />
					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						<IconButton children={ <Close /> } onClick={ this.props.closeHandler } />
					</ToolbarGroup>
				</Toolbar>
		);
	}
}
DialogTitlebar.propTypes = {};
DialogTitlebar.defaultProps = {
	title:'Dialog Title',
	closeHandler:function(event){console.log(event);}
};

export default DialogTitlebar;