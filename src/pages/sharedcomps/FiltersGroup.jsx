import React, {Component} from 'react'

import RaisedButton from 'material-ui/RaisedButton'
import Search from 'material-ui/svg-icons/action/search'
import Clear from 'material-ui/svg-icons/content/clear'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import FilterSelect from './comps/FilterSelect'

class FiltersGroup extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		// use filters component prop or data.filters
		const filters = this.props.filters || this.props.data.filters;



		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={true}
				transitionEnterTimeout={500}
				transitionLeave={false}
			>
				<div className="filtersgroup-selects">
					<div className="row">
						{
							filters.map((item, i) => {
								return (
									<div className="col s12 m4 l3 filter-col" key={i}>
										<label>{item.label}</label>
										<FilterSelect
											onChangeHandler={ this.props.onChangeHandler }
											defaultValue="any"
											bufferSize={ _.size(this.props.data.filtersBuffer) }
											data={ item }
										/>
										{/*<select className="browser-default" onChange={ (event) => this.props.onChangeHandler({name:item.name, value: event.target.value}) }>
											<option key="any" value="any">any</option>
											{
												item.options.map( (opt, i) => <option key={ i } value={ opt }>{ opt }</option> )
											}
										</select>*/}
									</div>
								);
							})
						}
					</div>
				</div>
				<div className="filtersgroup-buttons">
					<RaisedButton label="Reset" primary={false} onClick={ this.props.onResetHandler } icon={<Clear />} />
					<RaisedButton label="Search" primary={true} onClick={ this.props.onApplyHandler } icon={<Search />} />
				</div>
				<div className="filtersgroup-recap">
					<div className="filtersgroup-recap-label">Selected Filters: { _.size(this.props.data.filtersBuffer) > 0 ? '' : 'no filter selected yet.'}</div>
					<ul className="filtersgroup-recap-list">
						{
							this.props.data.filtersBuffer.map((item) => <li className="selected-filters-item" key={item.name}><span>{_.upperFirst(_.replace(item.name, /_/g, ' '))}</span> <span>{item.value}</span></li>)
						}
					</ul>
				</div>

			</ReactCSSTransitionGroup>
		);
	}
}
FiltersGroup.propTypes = {};
FiltersGroup.defaultProps = {};

export default FiltersGroup;