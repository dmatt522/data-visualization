import React, {Component} 	from 'react';
import Pagination 			from 'react-js-pagination'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

//import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'



//import ResultsDisplayToolbar from './ResultsDisplayToolbar'
//import ResultsTable from './ResultsTable'
//import NoResult from './comps/NoResult'
import ResultsDisplayTable from './comps/ResultsDisplayTable'
import ResultsDisplayToolbar from './comps/ResultsDisplayToolbar'
//import { paginationChange, deleteContact } from '../actions/index'
// utils functions
import { refactorKey, mapKeyToHelpText, _cellValue } from '../../utils/functions'








class ResultsDisplay extends Component{
	constructor(props){
		super(props);
		this.state = {
			contextMenuDisabled: true
		};
		//this.onRowSelection = this.onRowSelection.bind(this);
		this._onRowSelection = this._onRowSelection.bind(this);
	}


	_onRowSelection(rows){
		this.setState({contextMenuDisabled: (_.size(rows) === 0) });
		this.props.rowSelectionHandler(rows);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		//console.log('nextProps',nextProps, 'this.props', this.props);
		//if(nextProps.data === this.props.data)
		//	return false;
		return true;
	}
	componentWillUpdate(nextProps, nextState) {
		//console.log('ResultsDisplay.componentWillUpdate', nextState);
	}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		/*const data = this.props.data.pageData;

		if(_.isUndefined(this.props.data) || _.isEmpty(this.props.data)){
			return (
				<NoResult />
			);
		}*/



		/*const displayData = data.map( (item) => {
			return _.pick(item, this.props.defaultColumns);
		} );*/

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >

				<div className="comp-resultsdisplay">

					{/* start results display toolbar */}
					<ResultsDisplayToolbar children={ this.props.menuItems } extraIcons={this.props.extraIcons} disabled={ this.state.contextMenuDisabled } />
					{/*<Toolbar style={{backgroundColor:'#fff'}}>
						<ToolbarGroup firstChild={true} />
						<ToolbarGroup lastChild={true}>
							<IconMenu
								iconButtonElement={ <IconButton disabled={ this.state.contextMenuDisabled } ><MoreVertIcon /></IconButton> }
								children={ this.props.menuItems }
								anchorOrigin={{horizontal: 'right', vertical: 'top'}}
								targetOrigin={{horizontal: 'right', vertical: 'top'}} >
							</IconMenu>
						</ToolbarGroup>
					</Toolbar>*/}
					{/* end results display toolbar */}


					{/* start results table */}
					<ResultsDisplayTable
						data={ this.props.data.pageData }
						defaultColumns={ this.props.defaultColumns }
						hasHeaderHints={this.props.hasHeaderHints}
						headerClickHandler={this.props.headerClickHandler}
						rowSelectionHandler={this._onRowSelection}
					/>

					{/*<Table
						className="result-display-table"
						fixedHeader={ false }
						fixedFooter={ false }
						onRowSelection={ this._onRowSelection }
						multiSelectable={this.props.multiSelectable}
					>
						<TableHeader displaySelectAll={this.props.displaySelectAll} adjustForCheckbox={this.props.adjustForCheckbox}>
							<TableRow>
								{ tableRowHeaders(displayData, this.props.headerClickHandler) }
							</TableRow>
						</TableHeader>
						<TableBody showRowHover={true} displayRowCheckbox={this.props.displayRowCheckbox}>
							{
				        		displayData.map((item, i) => {
				        			return (
				        				<TableRow key={ i } style={{cursor:'pointer'}}>
				        					{
				        						_.map(item, (value, key) =>  <TableRowColumn className={ key } key={ key }>{ _cellValue(value, key) }</TableRowColumn> )
				        					}
				        				</TableRow>
				        			)
				        		})
				        	}
						</TableBody>
					</Table>*/}
					{/* end results table */}



					{/*<ResultsTable
						defaultColumns={this.props.defaultColumns}
						data={this.props.data.pageData}
						enableToolbarTools={this.props.enableToolbarTools}
					/>*/}
					<div className="pagination-container">
						<Pagination
							activePage={ this.props.data.activePage }
							itemsCountPerPage={ this.props.data.itemsPerPage }
							totalItemsCount={ _.size(this.props.data.filteredList) }
							pageRangeDisplayed={ 5 }
							onChange={ this.props.paginationChangeHandler }
						/>
					</div>

				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
ResultsDisplay.propTypes = {};
ResultsDisplay.defaultProps = {
	displaySelectAll:true,
	adjustForCheckbox:true,
	displayRowCheckbox:true,
	multiSelectable:false
};

export default ResultsDisplay;