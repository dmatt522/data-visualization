import React, {Component} from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import _ from 'lodash'
import NoResult from './NoResult'
import { refactorKey, mapKeyToHelpText, _cellValue } from '../../../utils/functions'


function tableRowHeaders(displayData, headerClickHandler, hasHeaderHints){
	return (
		_.map(displayData[0], (value, key) => {
		    return (
		    	hasHeaderHints
		    	?
		        <TableHeaderColumn aria-label={ mapKeyToHelpText(key) } className={ `${key} hint--bottom hint--medium hint--bounce` } key={ key }>
		        	<span className="tableHeaderColumnText" onClick={ (event) => headerClickHandler(key) }>{ _.upperFirst(refactorKey(key)) }</span>
		        </TableHeaderColumn>
		    	:
		        <TableHeaderColumn className={ `${key}` } key={ key }>
		        	<span className="tableHeaderColumnText" onClick={ (event) => headerClickHandler(key) }>{ _.upperFirst(refactorKey(key)) }</span>
		        </TableHeaderColumn>
		    );
		})
	);
}


class ResultsDisplayTable extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this._onRowSelection = this._onRowSelection.bind(this);
	}

	_onRowSelection(rows){
		this.props.rowSelectionHandler(rows);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		if(nextProps.data === this.props.data)
			return false
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){


		if(_.isUndefined(this.props.data) || _.isEmpty(this.props.data)){
			return (
				<NoResult />
			);
		}

		const data = this.props.data;


		const displayData = data.map( (item) => {
			return _.pick(item, this.props.defaultColumns);
		} );


		return(
			<Table
						className="result-display-table"
						fixedHeader={ false }
						fixedFooter={ false }
						onRowSelection={ this._onRowSelection }
						multiSelectable={this.props.multiSelectable}
					>
						<TableHeader displaySelectAll={this.props.displaySelectAll} adjustForCheckbox={this.props.adjustForCheckbox}>
							<TableRow>
								{ tableRowHeaders(displayData, this.props.headerClickHandler, this.props.hasHeaderHints) }
							</TableRow>
						</TableHeader>
						<TableBody showRowHover={true} displayRowCheckbox={this.props.displayRowCheckbox}>
							{
				        		displayData.map((item, i) => {
				        			return (
				        				<TableRow key={ i } style={{cursor:'pointer'}}>
				        					{
				        						_.map(item, (value, key) =>  <TableRowColumn className={ key } key={ key }>{ _cellValue(value, key) }</TableRowColumn> )
				        					}
				        				</TableRow>
				        			)
				        		})
				        	}
						</TableBody>
					</Table>
		);
	}
}
ResultsDisplayTable.propTypes = {};
ResultsDisplayTable.defaultProps = {
	displaySelectAll:true,
	adjustForCheckbox:true,
	displayRowCheckbox:true,
	multiSelectable:false,
	hasHeaderHints:true
};

export default ResultsDisplayTable;