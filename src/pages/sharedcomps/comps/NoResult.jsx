import React, { PropTypes } from 'react';

const NoResult = ({  }) => {
    return (
        <div className="no-search-result">Your search matched 0 items.</div>
    );
};

NoResult.displayName = 'NoResult';

NoResult.propTypes = {

};

export default NoResult;
