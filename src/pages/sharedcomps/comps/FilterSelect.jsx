import React, {Component} from 'react';

class FilterSelect extends Component{
	constructor(props){
		super(props);
		this.state = {
			value:'any'
		};

		this.changeHandler = this.changeHandler.bind(this);
	}

	changeHandler(event){
		this.setState({value:event.target.value});
		this.props.onChangeHandler({name:this.props.data.name, value: event.target.value})
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {
		if(nextProps.bufferSize == 0)
	    	this.setState({ value:'any' });
	}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		return(
			<select
				className="browser-default"
				onChange={ this.changeHandler }
				value={this.state.value}
			>
				<option key={this.props.defaultValue} value={this.props.defaultValue}>{this.props.defaultValue}</option>
				{
					this.props.data.options.map( (opt, i) => <option key={ i } value={ opt }>{ opt }</option> )
				}
			</select>
		);
	}
}
FilterSelect.propTypes = {};
FilterSelect.defaultProps = {
	defaultValue:'any'
};

export default FilterSelect;