import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconMenu 			from 'material-ui/IconMenu'
import IconButton 			from 'material-ui/IconButton'
import MoreVertIcon 		from 'material-ui/svg-icons/navigation/more-vert'

import _ from 'lodash'

class BaseToolbarAdvanced extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}


	render(){
		return(
			<Toolbar style={{backgroundColor:'#fff'}}>
						<ToolbarGroup firstChild={true} />
						<ToolbarGroup lastChild={true}>
							{this.props.extraIcons}
							<IconMenu
								iconButtonElement={ <IconButton disabled={ _.isEmpty(this.props.theThing.item) } ><MoreVertIcon /></IconButton> }
								children={ this.props.children }
								anchorOrigin={{horizontal: 'right', vertical: 'top'}}
								targetOrigin={{horizontal: 'right', vertical: 'top'}} >
							</IconMenu>
						</ToolbarGroup>
					</Toolbar>
		);
	}
}
BaseToolbarAdvanced.propTypes = {};
BaseToolbarAdvanced.defaultProps = {};

function mapStateToProps({theThing}){
	return {theThing};
}
export default connect(mapStateToProps, null)(BaseToolbarAdvanced);