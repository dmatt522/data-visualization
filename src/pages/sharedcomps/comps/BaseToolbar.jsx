import React, {Component} from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconMenu 			from 'material-ui/IconMenu'
import IconButton 			from 'material-ui/IconButton'
import MoreVertIcon 		from 'material-ui/svg-icons/navigation/more-vert'

class BaseToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}


	render(){
		return(
			<Toolbar style={{backgroundColor:'#fff'}}>
						<ToolbarGroup firstChild={true} />
						<ToolbarGroup lastChild={true}>
							{this.props.extraIcons}
							<IconMenu
								iconButtonElement={ <IconButton disabled={ this.props.disabled } ><MoreVertIcon /></IconButton> }
								children={ this.props.children }
								anchorOrigin={{horizontal: 'right', vertical: 'top'}}
								targetOrigin={{horizontal: 'right', vertical: 'top'}} >
							</IconMenu>
						</ToolbarGroup>
					</Toolbar>
		);
	}
}
BaseToolbar.propTypes = {};
BaseToolbar.defaultProps = {};

export default BaseToolbar;