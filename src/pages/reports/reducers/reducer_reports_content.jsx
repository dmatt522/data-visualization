/**
 * toggle components on section stage
 */
import Types from '../../../config/action_types'
const INITIAL_STATE = {
	analysis: true,
	projects: true,
	gantt: true,
	goscale: true,
	leader: true,
	volume: true
};

export default function(state = INITIAL_STATE, action) {
	switch(action.type){
		case Types.REPORTS_CONTENT:
			//console.log('1)', action.payload);
			state[action.payload.id] = action.payload.status;
			//console.log('2)', state);
			return state;
		default:
			return state;
	}
}