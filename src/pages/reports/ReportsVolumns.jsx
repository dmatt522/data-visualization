import React, {Component} 		from 'react'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import Dialog from 'material-ui/Dialog'

import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar			from '../../shared/toolbars/PageTitleBar'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import BaseComponent 	from '../../shared/components/BaseComponent'
import FiltersGroup 	from '../../shared/search/FiltersGroup'
import CollapsibleHeader from '../../shared/components/CollapsibleHeader'
import Chart from '../../shared/chart/Chart'

import _ from 'lodash'

// actions
import { notify } from '../../shared/actions/index'
// shared actions
import { LOADING_DATA, UPDATING_DATA } from '../../config/messages'
// Titles
import {
    VEHICLES_PAGE_DESCRIPTION,
    VEHICLES_RESULT_CALL_TO_ACTION,
    VEHICLES_RESULTS_TITLE,
    VEHICLES_FILTER_GROUP_TITLE } from '../../config/strings'

// actions
import {
    fetchReportsVolumns,
	fetchReportsFilters,
    resetFilterReports,
    applyFilterReports,
    bufferFilterReports } from './actions/index'
// Css Styles
import styles from '../../styles/styles'



class ReportsVolumns extends BaseComponent{
	constructor(props){
		super(props);
        this.state = {};
        this.applyFiltersHandler = this.applyFiltersHandler.bind(this);
        this.resetFiltersHandler = this.resetFiltersHandler.bind(this);
	}


	applyFiltersHandler(){
        this.props.applyFilterReports();
	}
	resetFiltersHandler(){
        this.props.resetFilterReports();
	}

	didUpdateCallback(c){
	}

	componentDidMount() {
        $('.collapsible').collapsible({accordion : false });
        if(_.isEmpty(this.props.reports.list)){
            this.props.fetchReportsVolumns();
        }
		if(_.isEmpty(this.props.reports.filters)){
			this.props.fetchReportsFilters();
		}
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}
	shouldComponentUpdate(nextProps, nextState) {
		//console.log('Reports.shouldComponentUpdate');
		return true;
	}
	componentWillUnmount() {
	}

	render(){
		console.log('ReportsVolumns Render');
        console.log(this.props.reports.list);
        console.log(this.props.reports.filters);
		/*if(this.state.loading){
		 return <div>'loading...'</div>;
		 }*/

		return(
        <ReactCSSTransitionGroup
        transitionName="testTransition"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false} >
        <div className="comp-vehicles page vehicles-page">

			<Breadcrumbs />

			<PageTitleBar title="Reports" />

			{/*<SimpleText text={ VEHICLES_PAGE_DESCRIPTION } />*/}

			<ul className="collapsible" data-collapsible="expandable">
				<li>
					<div className="collapsible-header">
						<CollapsibleHeader  title={VEHICLES_FILTER_GROUP_TITLE} />
					</div>
					<div className="collapsible-body">
						{/*<SimpleText text="lorem ipsum dolor" />*/}
						<FiltersGroup
						ref="filtersGroupRef"
						title={ VEHICLES_FILTER_GROUP_TITLE }
						data={ this.props.reports }
						onResetHandler={ this.resetFiltersHandler }
						onApplyHandler={ this.applyFiltersHandler }
						onSelectChange={ (filter) => this.props.bufferFilterReports(filter) } />
					</div>
				</li>
				<li>
					<div>
						<Chart
                            data= { this.props.reports.filteredList } chartType = { this.props.reports.chartType }
                            />
					</div>
				</li>
			</ul>
		</div>

		</ReactCSSTransitionGroup>
		);
	}
}
ReportsVolumns.propTypes = {};
ReportsVolumns.defaultProps = {
    numCollapsible: 2
};
ReportsVolumns.contextTypes = {
    router: React.PropTypes.object
};
// recive data from app state reducer
function mapStateToProps({ reports, contacts }){
    //console.log('contacts', contacts);
    return { reports };
}
// dispatch actions
function mapDispatchToProps(dispatch){
    return bindActionCreators(
        {
            fetchReportsVolumns,
			fetchReportsFilters,
			bufferFilterReports,
            resetFilterReports,
            applyFilterReports
        },
        dispatch
    );
}
// connect to redux
export default connect(mapStateToProps, mapDispatchToProps)(ReportsVolumns);