import React, {Component} from 'react'
import BaseComponent from '../../shared/components/BaseComponent'
import Paper from 'material-ui/Paper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import ToolbarExpand from '../../shared/components/ToolbarExpand'
import styles from '../../styles/styles'
import globals from '../../config/globals'

const papers = [
	{title:'Analysis', id:'anl', image:`${globals.basePath}/images/g2.png`},
	{title:'Target Part Volume', id:'psm', image:`${globals.basePath}/images/g5.png`},
	{title:'Material Volume', id:'vol', image:`${globals.basePath}/images/g3.png`},
	{title:'Gantt Chart', id:'gch', image:`${globals.basePath}/images/g6.png`},
	{title:'G & O\'Scale', id:'gos', image:`${globals.basePath}/images/g7.png`},
	{title:'Leader Summary', id:'lsm', image:`${globals.basePath}/images/g8.png`}
];


class ReportsRevenues extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}

	handleOpenClose(id){
		console.log(id);
	}

	render(){

		const items = papers.map( (item, i) => {
			return (
				<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
					<div className="col l4 m6 s12" key={ i }>
						<Paper style={ {minHeight: 560} }>
							<ToolbarExpand
								key={ item.id }
								title={ item.title }
								clickHandler={ () => { this.handleOpenClose(item.id) } } />

								<div style={{padding:16, overflow:'hidden'}}> content for { item.title }
								{ (item.image) ? <div><img style={{maxWidth:'100%'}} src={item.image} /></div> : <div></div> }

								</div>


						</Paper>
					</div>
				</ReactCSSTransitionGroup>
			);
		} );

		return(
			<section>
				<div className="row">
					{ items }
				</div>
			</section>
		);
	}
}
Reports.propTypes = {};
Reports.defaultProps = {};

export default Reports;