import axios 	from 'axios'
import globals 	from '../../../config/globals'
import {
	ALL_REPORT_VOLUMNS,
    ALL_REPORT_FILTERS
} from '../../../config/api'
import {
	FETCH_REPORTS_VOLUMNS,
	FETCH_REPORTS_FILTERS,
	FETCH_REPORTS_REVENUES,
	FETCH_REPORTS_LIGHTWEIGHTS_,
	BUFFER_FILTERS_REPORTS,
	APPLY_FILTERS_REPORTS,
	RESET_FILTER_REPORTS
} from '../../../actions/types'



// warning: use api urls not locals

/**
 * fetches all reports chart data
 * @return {[type]} [description]
 */
export function fetchReportsVolumns(){

	//const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
	//const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
	//const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
	const request = axios.get(ALL_REPORT_VOLUMNS);
	//console.log('Action: fetchVehicles', request);
	//debugger;
	return {
		type: FETCH_REPORTS_VOLUMNS,
		payload: request
	};
}

/**
 * fetches all reports chart data
 * @return {[type]} [description]
 */
export function fetchReportsFilters(){

    //const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
    //const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
    //const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
    const request = axios.get(ALL_REPORT_FILTERS);
    //console.log('Action: fetchVehicles', request);
    //debugger;
    return {
        type: FETCH_REPORTS_FILTERS,
        payload: request
    };
}

/**
 * add/change the value of a display vehicle filter
 * @param  {object} data {label, value}
 * @return {[type]}      [description]
 */
export function bufferFilterReports(data){
	//console.log('Action: filterVehicles', data);
	return {
		type: BUFFER_FILTERS_REPORTS,
		payload: data
	};
}

/**
 * [resetFilterVehicles description]
 * @return {[type]} [description]
 */
export function resetFilterReports(){
	//console.log('Action: resetFilterVehicles');
	return {
		type: RESET_FILTER_REPORTS,
		payload: {}
	};
}

/**
 * [applyFilterVehichles description]
 * @return {[type]} [description]
 */
export function applyFilterReports(){
	//console.log('Action: applyFilterVehichles');
	return {
		type: APPLY_FILTERS_REPORTS,
		payload: {}
	};
}
