// dispatch 'dashboardContent' action to application state
// to show/hide contents in dashboard component
//
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reportsContent } from '../actions/'

import Drawer from 'material-ui/Drawer'
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui/svg-icons/navigation/close'

import ControlledToggle from '../../../shared/ui/ControlledToggle'

const style = {
	foo: {
		paddingLeft: 12,
		paddingRight: 12
	}
}

class ContextDrawer extends Component{
	constructor(props){
		super(props);
		//this.state = {};
	}

	onToggleChange(data){
		console.log('onToggleChange', data.id, data.status);
		this.props.reportsContent(data);
	}

	render(){
		return(
			<Drawer
				openSecondary={true}
				open={this.props.open}
				>
				<IconButton onClick={this.props.toggleDrawer}><CloseIcon/></IconButton>
				<div style={style.foo}>
					<ControlledToggle
						label="Analysis" id="analysis" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Project Summary" id="project" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Gantt Chart" id="gantt" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="G & O'Scale" id="goscale" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Leader Summary" id="leader" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Material Volume" id="volume" onToggle={this.onToggleChange.bind(this)} />
				</div>
			</Drawer>
		);
	}
}
ContextDrawer.propTypes = {};
ContextDrawer.defaultProps = {};

function mapDispatchToProps(dispatch){
	return bindActionCreators({reportsContent}, dispatch);
}

export default connect(null, mapDispatchToProps)(ContextDrawer);