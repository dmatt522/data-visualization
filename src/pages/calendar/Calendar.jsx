import React, {Component} from 'react'
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import TooltippedIconButton from '../../shared/ui/TooltippedIconButton'
import PageTitleBar  from '../../shared/toolbars/PageTitleBar'
import SimpleText    from '../../shared/ui/SimpleText'

import { CALENDAR_PAGE_DESCRIPTION } from '../../config/strings'
//import EventCalendar from 'react-event-calendar'

BigCalendar.momentLocalizer(moment);

const events = [
    {
    'title': 'All Day Event',
    'allDay': true,
    'start': new Date(2016, 9, 3),
    'end': new Date(2016, 9, 4)
  },
  {
    'title': 'Long Event',
    'start': new Date(2016, 9, 3),
    'end': new Date(2016, 9, 10)
  }
];

class Calendar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
      <ReactCSSTransitionGroup
        transitionName="testTransition"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false} >
  			<section className="comp-calendar page calendar-page">

          {/*<Breadcrumbs items={ ['dashboard', 'calendar'] } />*/}
          <PageTitleBar title="Calendar" />
          {/*<PageDescription text={ CALENDAR_PAGE_DESCRIPTION } />*/}


            <div className="row">
                      <div className="col s12">
                          <BigCalendar
                            selectable
                            events={events}
                            defaultView='week'
                            scrollToTime={new Date(1970, 1, 1, 6)}
                            defaultDate={new Date()}
                            onSelectEvent={event => console.log(event.title)}
                            onSelectSlot={(slotInfo) => console.log(
                              `selected slot:
                              start -> ${slotInfo.start.toLocaleString()}
                              end -> ${slotInfo.end.toLocaleString()}` )}
                          />
                      </div>
                  </div>
  			</section>
      </ReactCSSTransitionGroup>
		);
	}
}
Calendar.propTypes = {};
Calendar.defaultProps = {};

export default Calendar;