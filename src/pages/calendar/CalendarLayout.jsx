import React, {Component} from 'react';

//import LayoutToolbar from './components/LayoutToolbar'

class Calendarlayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
			<div className="page-layout calendar-page-layout">
				{/*<LayoutToolbar />*/}
				{this.props.children}
			</div>
		);
	}
}
Calendarlayout.propTypes = {};
Calendarlayout.defaultProps = {};

export default Calendarlayout;