import Types from '../../../config/action_types'

export function dashboardContent(props){
	return {
		type:Types.DASHBOARD_CONTENT,
		payload: props
	};
}