import Types from '../../../config/action_types'

const INITIAL_STATE = {
	projects: true,
	analysis: true,
	tasks: true,
	directory: true,
	calendar: true,
	goscale: true
};

export default function(state = INITIAL_STATE, action) {
	switch(action.type){
		case Types.DASHBOARD_CONTENT:
			//console.log('1)', action.payload);
			state[action.payload.id] = action.payload.status;
			//console.log('2)', state);
			return state;//{...state, INITIAL_STATE['action.payload']:action.toggled}
		default:
			return state;
	}
}