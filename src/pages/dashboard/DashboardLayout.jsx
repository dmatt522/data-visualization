import React, {Component} from 'react';

import Breadcrumbs from 'react-router-breadcrumbs'

import LayoutToolbar from './components/LayoutToolbar'

import routes from '../../routes/routes'
import styles from '../../styles/styles'

/**
 * todo: use Bradcrumbs in the application ... maybe <Breadcrumbs routes={routes} />
 */

class DashboardLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};

		this.expandAll = this.expandAll.bind(this);
		this.closeAll = this.closeAll.bind(this);
	}

	expandAll(){
		console.log('expandAll');
	}
	closeAll(){
		console.log('closeAll');
	}
	/*toggleDrawer(){
		this.setState({openDrawer:!this.state.openDrawer});
	}*/

	render(){
		return(
			<div className="page-layout dashboard-page-layout">

				{/*<LayoutToolbar
					styles={styles}
					expandAll={ this.expandAll }
					closeAll={ this.closeAll } />*/}
				{this.props.children}
			</div>
		);
	}
}
DashboardLayout.propTypes = {};
DashboardLayout.defaultProps = {};

export default DashboardLayout;