import React, {Component} from 'react';
//import BaseComponent from '../../shared/components/BaseComponent'
import Paper from 'material-ui/Paper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import TooltipConsumer from '../../hocs/tooltipConsumer'
import PageTitleBar from '../../shared/toolbars/PageTitleBar'
import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
//import SimpleText 		from '../../shared/ui/SimpleText'
import ToolbarExpand from '../../shared/components/ToolbarExpand'
import FakeList from '../../shared/components/FakeList'
import styles from '../../styles/styles'

import globals from '../../config/globals'
import { DASHBOARD_PAGE_DESCRIPTION } from '../../config/strings'

const papers = [
	{title:'Projects', id:'prj', child:'ok'},
	{title:'Material Volume', id:'vol', image:`${globals.basePath}/images/g1.png`},
	{title:'G & O\'Scale', id:'gos', image:`${globals.basePath}/images/g4.png`},
	{title:'Tasks', id:'tsk', child:'ok'},
	{title:'Directory', id:'dir', child:'ok'},
	{title:'Calendar', id:'cal'}
];

class Dashboard extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	handleOpenClose(id){
		//console.log(id);
	}

	render(){

		const items = papers.map( (item, i) => {
			return (
				<div className="col l4 m6 s12" key={ i }>
					<Paper style={ {minHeight: 560} }>
						<ToolbarExpand
							key={ item.id }
							title={ item.title }
							clickHandler={ () => { this.handleOpenClose(item.id) } } />

							<div style={{padding:16, overflow:'hidden'}}>
							{ (item.image) ? <div><img style={{maxWidth:'100%'}} src={item.image} /></div> : <div></div> }

							{ (item.child) ? <FakeList />: <div></div>}

							</div>


					</Paper>
				</div>
			);
		} );

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<section>
					<PageTitleBar title="Dashboard"/>
					{/*<SimpleText text={ DASHBOARD_PAGE_DESCRIPTION } />*/}
					<div className="row">
						{ items }
					</div>
				</section>
			</ReactCSSTransitionGroup>
		);
	}
}
Dashboard.propTypes = {};
Dashboard.defaultProps = {};

export default TooltipConsumer(Dashboard);