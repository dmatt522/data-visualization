// dispatch 'dashboardContent' action to application state
// to show/hide contents in dashboard component
//
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { dashboardContent } from '../actions'

import Drawer from 'material-ui/Drawer'
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui/svg-icons/navigation/close'

import ControlledToggle from '../../../shared/ui/ControlledToggle'

const style = {
	foo: {
		paddingLeft: 12,
		paddingRight: 12
	}
}

class ContextDrawer extends Component{
	constructor(props){
		super(props);
		//this.state = {};
	}

	onToggleChange(data){
		console.log('onToggleChange', data.id, data.status);
		this.props.dashboardContent(data);
	}

	render(){
		return(
			<Drawer
				openSecondary={true}
				open={this.props.open}
				>
				<IconButton onClick={this.props.toggleDrawer}><CloseIcon/></IconButton>
				<div style={style.foo}>
					<ControlledToggle
						label="Projects" id="projects" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Analysis" id="analysis" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Tasks" id="tasks" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Directory" id="directory" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="Calendar" id="calendar" onToggle={this.onToggleChange.bind(this)} />
					<ControlledToggle
						label="G & O'Scale" id="goscale" onToggle={this.onToggleChange.bind(this)} />
				</div>
			</Drawer>
		);
	}
}
ContextDrawer.propTypes = {};
ContextDrawer.defaultProps = {};

function mapDispatchToProps(dispatch){
	return bindActionCreators({dashboardContent}, dispatch);
}

export default connect(null, mapDispatchToProps)(ContextDrawer);