import React from 'react';
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'
//import TooltippedIconButton from '../../../shared/ui/TooltippedIconButton'

const LayoutToolbar = ({ styles, expandAll, closeAll }) => {
    return (
		<Toolbar>
			<ToolbarGroup firstChild={true}>
				<ToolbarTitle className="page-title" text="Dashboard" />
			</ToolbarGroup>
			<ToolbarGroup lastChild={ true }>
				<IconMenu
					iconButtonElement={ <IconButton><MoreVertIcon /></IconButton> }
					anchorOrigin={{horizontal: 'right', vertical: 'top'}}
				    targetOrigin={{horizontal: 'right', vertical: 'top'}} >
				    <MenuItem primaryText="Expand All" onTouchTap={ expandAll } />
				    <MenuItem primaryText="Close All" onTouchTap={ closeAll }/>
				</IconMenu>
			</ToolbarGroup>
		</Toolbar>
    );
};

export default LayoutToolbar;
