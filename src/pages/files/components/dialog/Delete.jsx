import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import RaisedButton from 'material-ui/RaisedButton'

import DialogToolbar from './DialogToolbar'

import { _valueAt } from '../../../../utils/functions'
import {deleteItem} from '../../actions/index'

class Delete extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.deletionHandler = this.deletionHandler.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}
	deletionHandler(event){
		if(!_.isEmpty(this.props.files.selectedItem))
			this.props.deleteItem(this.props.files.selectedItem);
		this.props.closeHandler();
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		const item = this.props.files.selectedItem;

		return(
			<div className="dialog-delete">
				<DialogToolbar title="Delete file" closeHandler={this.closeHandler} />
				<div className="row">
					<div className="col s12" style={{color: '#000'}}>
						Confirm deletion of <strong>{ `${_valueAt(item, 'title')}` }</strong>?
					</div>
					<div className="col s12">
						<RaisedButton label="No" primary={false} onClick={ (event) => this.closeHandler(event) }/>
						<RaisedButton label="Yes" primary={true} onClick={ (event) => this.deletionHandler(event) }/>
					</div>
				</div>
			</div>
		);
	}
}
Delete.propTypes = {};
Delete.defaultProps = {};

function mapStateToProps({files}){
	return {files};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		deleteItem
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Delete);