import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'

import _ from 'lodash'

import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
import DialogToolbar from './DialogToolbar'

//import {editUser} from '../../actions/index'

function validate(values){
	//console.log('---->', values);
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	/*if(_.size(values.first_name) === 0)
		errors.first_name = 'First name is Required';
	if(_.size(values.last_name) === 0)
		errors.last_name = 'Last name is Required';
	if(!pattern.test(values.email))
		errors.email = 'Please use a valid email address';*/
	return errors;
}

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	    <label>{label} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)

class Edit extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}

	onFormSubmit(formData){
		console.log('Edit.onFormSubmit', this.props.editUser);
		this.props.editUser(formData);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const { handleSubmit, pristine, reset, submitting } = this.props;
		return(
			<div className="dialog-edit">
				<DialogToolbar title="Edit file data" closeHandler={this.closeHandler} />
				<p>content here</p>
				{/*<div className="row dialog-addnew-form-container">
					<form onSubmit={handleSubmit(this.onFormSubmit)}>

						<Field name="submitted_by" component="input" type="hidden" />
						<Field name="id" component="input" type="hidden" />

						<Field name="first_name" label="First name" component={renderField} type="text" />
						<Field name="last_name" label="Last name" component={renderField} type="text" />
						<Field name="email" label="Email" component={renderField} type="email" />

						<RaisedButton type="button" label="Reset" primary={false} disabled={ submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ submitting }  />
					</form>
				</div>*/}
			</div>
		);
	}
}
Edit.propTypes = {};
Edit.defaultProps = {};

function mapStateToProps(state){
	return{
		users: state.files,
		user: state.user,
		initialValues: {}
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({ /*editUser*/ }, dispatch);
}

Edit = reduxForm({form:'editFileDataForm', validate})(Edit);
//Edit = ReduxFormConsumer(Edit, 'editUserDataForm', validate);
export default connect(mapStateToProps, mapDispatchToProps)(Edit);