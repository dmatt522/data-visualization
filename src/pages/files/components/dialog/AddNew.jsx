import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Field, reduxForm } from 'redux-form'
import RaisedButton from 'material-ui/RaisedButton'

import _ from 'lodash'

import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
import DialogToolbar from './DialogToolbar'

import {submitNew} from '../../actions/index'
import {stringFromId} from '../../../../utils/functions'

function validate(values){
	//console.log('---->', values);
	const pattern = /\S+@\S+\.\S+/;
	const errors = {};
	if(_.size(values.first_name) === 0)
		errors.first_name = 'First name is Required';
	if(_.size(values.last_name) === 0)
		errors.last_name = 'Last name is Required';
	if(!pattern.test(values.email))
		errors.email = 'Please use a valid email address';
	return errors;
}

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
	    <label>{label} {touched && error && <span className="form-error-msg">{error}</span>}</label>
	    <div>
	    	<input {...input} placeholder={label} type={type}/>
	    </div>
	</div>
)

const renderSelect = ({name, label, options}) => (
	<div>
	    <label>{label}</label>
		<select name={name} className="browser-default">
			<option key="-1" value="none">none</option>
		    { options.map( (opt, i) => <option key={ i } value={ opt.id }>{ opt.name }</option> ) }
		</select>
	</div>
);

const renderFile = ({input, label, type}) => (
	<div>
	<label>{label}</label>
	{/*<input {...input} type={type} />*/}
	<div className="file-field input-field">
      <div className="btn">
        <span>File</span>
        <input {...input} type={type} />
      </div>
      <div className="file-path-wrapper">
        <input className="file-path validate" type="text" />
      </div>
    </div>
    </div>
);

class AddNew extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}

	onFormSubmit(formData){
		console.log(this, 'submitting form data', formData);

		this.props.submitNew(formData);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		const { handleSubmit, pristine, reset, submitting } = this.props;
		let options = [];
		_.forEach(this.props.projects.list, (item) => {
			options.push({id:item.project_id, name: item.project_id});
		});
		//const options = this.props.projects.list.map((item) => {
			//let partType = stringFromId('part_type', item.part_type, this.props.lookups);
			//return {id:item.project_id, name: `${partType} (${item.manufacturer} - ${item.brand})`};
			//return {id:item.project_id, name: item.project_id};
		//});
		console.log('-->', options);

		return(
			<div className="dialog-container dialog-addnew">
				<DialogToolbar title="Add new file" closeHandler={this.closeHandler} />
				<div className="row dialog-addnew-form-container">
					<form onSubmit={handleSubmit(this.onFormSubmit)} enctype="multipart/form-data">

						<Field name="user_id" component="input" type="hidden" />
						<Field name="id" component="input" type="hidden" />



						<Field name="title" label="Title" component={renderField} type="text" />

						{/*<Field name="project_id" label="Project" component={renderSelect} options={options} />*/}
							<div>
							    <label>Project</label>
							    <Field name="project_id" component="select" className="browser-default">

									<option key="nokey" value="none">none</option>
								    { options.map( (opt, i) => <option key={ i } value={ opt.id }>{ opt.name }</option> ) }

								</Field>
							</div>

						<Field name="media" label="Select a file" component={renderFile} type="file" />



						<RaisedButton type="button" label="Reset" primary={false} disabled={ pristine || submitting } onClick={reset} />
						<RaisedButton type="submit" label="Submit" primary={true} disabled={ pristine || submitting }  />
					</form>
				</div>
			</div>
		);
	}
}
AddNew.propTypes = {};
AddNew.defaultProps = {};

function mapStateToProps(state){
	return{
		files: state.files,
		projects:state.projects,
		user: state.user,
		lookups: state.lookups,
		initialValues: {
			user_id: state.user.userInfo.user_id,
			id:'0'
		}
	};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({submitNew}, dispatch);
}

AddNew = reduxForm({form:'uploadNewFile', validate})(AddNew);
//AddNew = ReduxFormConsumer(AddNew, 'addNewForm', validate);
export default connect(mapStateToProps, mapDispatchToProps)(AddNew);