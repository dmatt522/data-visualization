import React, {Component} from 'react';
import {connect} from 'react-redux'

import DialogToolbar from './DialogToolbar'

import { _valueAt, stringFromId } from '../../../../utils/functions'

class Details extends Component{
	constructor(props){
		super(props);
		this.state = {};
		this.closeHandler = this.closeHandler.bind(this);
	}

	closeHandler(event){
		this.props.closeHandler();
	}
	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		//const data = this.props.files.selectedItem;

		return(
			<div className="dialog-files">
				<DialogToolbar title="File details" closeHandler={this.closeHandler} />
				<p>content here</p>
				{/*<div className="row dialog-details-container">
					<div className="col s12 m6"><span>First name</span> {_valueAt(data, 'first_name')}</div>
					<div className="col s12 m6"><span>Last name</span> {_valueAt(data, 'last_name')}</div>
					<div className="col s12 m6"><span>Phone work</span> {_valueAt(data, 'phone_work')}</div>
					<div className="col s12 m6"><span>Email</span> {_valueAt(data, 'email')}</div>
					<div className="col s12 m6"><span>Sales manager</span> {stringFromId('sales_manager', data.sales_manager, this.props.lookups)}</div>
					<div className="col s12 m6"><span>Tech service</span> {stringFromId('tech_service', data.tech_service, this.props.lookups)}</div>
				</div>*/}
			</div>
		);
	}
}
Details.propTypes = {};
Details.defaultProps = {};

function mapStateToProps({files, lookups}){
	return {files, lookups};
}
export default connect(mapStateToProps)(Details);