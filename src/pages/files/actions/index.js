import axios 	from 'axios'
import globals 	from '../../../config/globals'
import {
	ALL_FILES,
	FILE_DELETE,
	FILE_ADD,
	FILE_EDIT
	} from '../../../config/api'
import {
	FETCH_FILES,
	PAGINATION_FILES_CHANGE,
	SORT_FILES,
	APPLY_FILTERS_FILES,
	RESET_FILTER_FILES,
	BUFFER_FILTER_FILES,
	SELECT_FILE,
	DELETE_FILE,
	ADD_FILE,
	EDIT_FILE
	} from '../../../actions/types'



// warning: use api urls not locals

/**
 * fetches all vehicles data
 * @return {[type]} [description]
 */
export function fetchFiles(){

	//const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
	//const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
	//const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
	const request = axios.get(ALL_FILES);
	//console.log('Action: fetchVehicles', request);
	//debugger;
	return {
		type: FETCH_FILES,
		payload: request
	};
}


export function paginationChange(page){
	return {
		type:PAGINATION_FILES_CHANGE,
		payload: page
	}
}

export function sortList(sortKey){
	return {
		type: SORT_FILES,
		payload: sortKey
	}
}

export function resetFilters(){
	return{
		type: RESET_FILTER_FILES,
		payload: {}
	}
}
export function applyFilters(){
	return{
		type:APPLY_FILTERS_FILES,
		payload:{}
	}
}
export function bufferFilter(filter){
	return{
		type: BUFFER_FILTER_FILES,
		payload: filter
	}
}

export function selectItem(rows){
	console.log('action selectItem', rows);
	return {
		type: SELECT_FILE,
		payload: rows
	}
}

export function deleteItem(selectedItem){
	const request = axios.post(FILE_DELETE, {scope:'files', id:selectedItem.id});
	console.log(request);
	return {
		type: DELETE_FILE,
		payload: request,
		selectedItem:selectedItem
	}
}

export function submitNew(formData){
	//const config = { headers: { 'Content-Type': '' } };
	const config = { headers: { 'Content-Type': 'multipart/form-data' } };
	//const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
	const request = axios.post(FILE_ADD, formData, config);
	return {
		type: ADD_FILE,
		payload: request
	}
}

export function editItem(formData){
	const request = axios.post(FILE_EDIT, formData);
	return {
		type: EDIT_FILE,
		payload: request
	}
}