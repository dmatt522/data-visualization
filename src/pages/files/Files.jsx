import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import _ from 'lodash'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import Dialog 		from 'material-ui/Dialog'
import MenuItem 	from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import DetailsIcon	from 'material-ui/svg-icons/image/details'
import DeleteIcon 	from 'material-ui/svg-icons/action/delete'
import AddIcon 		from 'material-ui/svg-icons/content/add'
import EditIcon		from 'material-ui/svg-icons/editor/mode-edit'

import FiltersGroup 	from  '../sharedcomps/FiltersGroup'
import ResultsDisplay 	from '../sharedcomps/ResultsDisplay'
import Details 			from './components/dialog/Details'
import Delete 			from './components/dialog/Delete'
import Edit 			from './components/dialog/Edit'
import AddNew 			from './components/dialog/AddNew'

import PageTitleBar 		from '../../shared/toolbars/PageTitleBar'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'

import {
	paginationChange,
	sortList,
	bufferFilter,
	selectItem,
	resetFilters,
	applyFilters } from './actions/index'

import styles from '../../styles/styles'

function whoIsTheChild(name, closeDialogHandler){
	switch(name){
		case 'add':
			return <AddNew closeHandler={ closeDialogHandler } />;
		case 'delete':
			return <Delete closeHandler={ closeDialogHandler } />;
		case 'details':
			return <Details closeHandler={ closeDialogHandler } />;
		case 'edit':
			return <Edit closeHandler={ closeDialogHandler } />;
		default:
			return <Details closeHandler={ closeDialogHandler } />;
	}
}

class Files extends Component{
	constructor(props){
		super(props);
		this.state = {
			openDialog:false,
			actionName:''
		};

		this.resetFiltersHandler = this.resetFiltersHandler.bind(this);
		this.applyFiltersHandler = this.applyFiltersHandler.bind(this);
		this.bufferFiltersHandler = this.bufferFiltersHandler.bind(this);
		this.toolbarActionHandler = this.toolbarActionHandler.bind(this);
		this.headerClickHandler = this.headerClickHandler.bind(this);
		this.paginationChangeHandler = this.paginationChangeHandler.bind(this);
		this.rowSelectionHandler = this.rowSelectionHandler.bind(this);
		this.closeDialogHandler = this.closeDialogHandler.bind(this);
	}

	// close dialog
	closeDialogHandler(){
		this.setState({openDialog:false});
	}

	rowSelectionHandler(rows){
		console.log('Files.rowSelectionHandler', rows);
		//if(_.size(rows) == 0)
		//	return;
		// call action
		this.props.selectItem(rows);
	}

	// results display toolbar menu actions
	toolbarActionHandler(event, actionName){
		console.log('Files toolbarActionHandler', actionName);
		this.setState({openDialog:true, actionName:actionName});
	}
	// pagination click handler
	paginationChangeHandler(pageNum){
		console.log('Files paginationChangeHandler', pageNum);
		this.props.paginationChange(pageNum);
	}
	headerClickHandler(key){
		console.log('click on', key);
		this.props.sortList(key);
	}
	// enable/disable toolbar menu to view/edit ..
	/*enableToolbarTools(rows){
		//console.log('disableTools', (_.size(rows) == 0));
		this.setState({ disableTools: (_.size(rows) == 0) });
	}*/

	resetFiltersHandler(){
		console.log('resetFiltersHandler');
		this.props.resetFilters();
	}
	applyFiltersHandler(){
		console.log('applyFiltersHandler');
		this.props.applyFilters();
	}
	bufferFiltersHandler(filter){
		console.log('filterChangeHandler', filter.name, filter.value);
		this.props.bufferFilter(filter);
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {
		//console.log('componentWillUpdate nextProps',nextProps);
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {
		//console.log('componentWillReceiveProps nextProps',nextProps.users.selectedItem);
	}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {
		//this.props.resetFilterPricing();
	}

	render(){
		// section to show in dialog
		const dialogChild = whoIsTheChild(this.state.actionName, this.closeDialogHandler);
		//console.log('Files.render', dialogChild, this.state.actionName);

		// more vert menu
		const menuItems = [
			<MenuItem key="details" leftIcon={<DetailsIcon />} primaryText="Details" onTouchTap={ (e) => this.toolbarActionHandler(e, 'details') } />,
			<MenuItem key="edit" leftIcon={<EditIcon />} primaryText="Edit" onTouchTap={ (e) => this.toolbarActionHandler(e, 'edit') } />,
			<MenuItem key="delete" leftIcon={<DeleteIcon />} primaryText="Delete" onTouchTap={ (e) => this.toolbarActionHandler(e, 'delete') } />
		];

		// more icons
		const extraIcons = [
			<IconButton key="add" tooltip="Add new" tooltipStyles={styles.tooltip} onClick={ (e) => this.toolbarActionHandler(e, 'add') }>
				<AddIcon />
			</IconButton>];

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false}
			>
				<div className="comp-files page files-page">

					<PageTitleBar title="Files" />

					<ul className="collapsible" data-collapsible="expandable">
						{/* start filters here */}
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title="Search" />
							</div>
						  	<div className="collapsible-body">
								<FiltersGroup
									data={ this.props.files }
									onChangeHandler={ this.bufferFiltersHandler }
									onResetHandler={ this.resetFiltersHandler }
									onApplyHandler={ this.applyFiltersHandler }
								/>
						  	</div>
						</li>
						{/* end filters here */}

						{/* start table here */}
						<li>
						  	<div className="collapsible-header active">
								<CollapsibleHeader  title="Results" /></div>
						  	<div className="collapsible-body">
								<ResultsDisplay
									data={ this.props.files }
									menuItems={ menuItems }
									extraIcons={ extraIcons }
									defaultColumns={ this.props.defaultColumns }
									hasHeaderHints={false}
									displaySelectAll={true}
									adjustForCheckbox={true}
									displayRowCheckbox={true}
									multiSelectable={false}
									rowSelectionHandler={ this.rowSelectionHandler }
									toolbarActionHandler={ this.toolbarActionHandler }
									headerClickHandler={ this.headerClickHandler }
									paginationChangeHandler={ this.paginationChangeHandler }
								/>
						  	</div>
						</li>
						{/* end table here */}
					</ul>
				</div>

				{/* start dialog */}
				<Dialog
					autoScrollBodyContent={true}
					children={dialogChild}
					open={this.state.openDialog}
					onRequestClose={ () => this.setState({openDialog:false}) }
					contentStyle={styles.projectDialog} />
				{/* end dialog */}


			</ReactCSSTransitionGroup>
		);
	}
}
Files.propTypes = {};
//  nome, path, tipo e un icona
Files.defaultProps = {
	defaultColumns:[
		'url',
		'title',
		'type'
	]
};

function mapStateToProps({files, user}){
	return {files, user};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({
		paginationChange,
		sortList,
		selectItem,
		bufferFilter,
		resetFilters,
		applyFilters
	}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Files);