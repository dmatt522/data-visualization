import React, {Component} from 'react';

class FilesLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<div className="page-layout users-page-layout">
				{this.props.children}
			</div>
		);
	}
}
FilesLayout.propTypes = {};
FilesLayout.defaultProps = {};

export default FilesLayout;