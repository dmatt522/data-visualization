import React, {Component} from 'react';

//import LayoutToolbar from './components/LayoutToolbar'

import styles from '../../styles/styles'

class ContactsLayout extends Component{
	constructor(props){
		super(props);
		this.state = {
			/*openDrawer: false*/
		};
	}

	/*toggleDrawer(){
		this.setState({openDrawer:!this.state.openDrawer});
	}*/

	render(){
		return(
			<div className="page-layout contacts-page-layout">
				{this.props.children}
			</div>
		);
	}
}
ContactsLayout.propTypes = {};
ContactsLayout.defaultProps = {};

export default ContactsLayout;