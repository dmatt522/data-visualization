import axios 	from 'axios'
import globals 	from '../../../config/globals'
import {
	ALL_CONTACTS,
	SINGLE_CONTACT } from '../../../config/api'
import {
	FETCH_CONTACTS,
	BUFFER_FILTERS_CONTACTS,
	APPLY_FILTERS_CONTACTS,
	RESET_FILTER_CONTACTS,
	PAGINATION_CONTACTS_CHANGE,
	SORT_CONTACTS,
	SELECT_CONTACT
	} from '../../../actions/types'



// warning: use api urls not locals

/**
 * fetches all vehicles data
 * @return {[type]} [description]
 */
export function fetchContacts(){

	//const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
	//const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
	//const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
	const request = axios.get(ALL_CONTACTS);
	//console.log('Action: fetchVehicles', request);
	//debugger;
	return {
		type: FETCH_CONTACTS,
		payload: request
	};
}

/**
 * add/change the value of a display vehicle filter
 * @param  {object} data {label, value}
 * @return {[type]}      [description]
 */
export function bufferFilterContacts(data){
	//console.log('Action: filterVehicles', data);
	return {
		type:BUFFER_FILTERS_CONTACTS,
		payload: data
	};
}

/**
 * [resetFilterVehicles description]
 * @return {[type]} [description]
 */
export function resetFilterContacts(){
	//console.log('Action: resetFilterVehicles');
	return {
		type: RESET_FILTER_CONTACTS,
		payload: {}
	};
}

/**
 * [applyFilterVehichles description]
 * @return {[type]} [description]
 */
export function applyFilterContacts(){
	//console.log('Action: applyFilterVehichles');
	return {
		type: APPLY_FILTERS_CONTACTS,
		payload: {}
	};
}

/**
 * [paginationChange description]
 * @param  {[type]} page [description]
 * @return {[type]}      [description]
 */
export function paginationChange(page){
	//console.log('Action: paginationChange');
	return {
		type: PAGINATION_CONTACTS_CHANGE,
		payload: page
	};
}

/**
 * [sortList description]
 * @param  {[type]} sortKey [description]
 * @return {[type]}         [description]
 */
export function sortList(sortKey){
	//console.log('Action: sortList', sortKey);
	return {
		type: SORT_CONTACTS,
		payload: sortKey
	}

}


/**
 * [selectVehicle description]
 * @param  {array of integers} rows: integers are the indexes of the selected row/rows
 * @return {[type]}      [description]
 */
export function selectContact(rows){
	//console.log('Action: selectItem', rows);
	return {
		type: SELECT_CONTACT,
		payload: rows
	}
}