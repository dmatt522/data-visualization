import React, {Component} 		from 'react'
import { connect }				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import { Link } 	from 'react-router'
import ContentAdd from 'material-ui/svg-icons/content/add'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import TooltippedIconButton from '../../shared/ui/TooltippedIconButton'
import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar	from '../../shared/toolbars/PageTitleBar'
//import PageDescription 	from '../../shared/ui/PageDescription'
//import SimpleText	 		from '../../shared/ui/SimpleText'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import CollapsibleHeader 	from '../../shared/components/CollapsibleHeader'
import FiltersGroup 		from '../../shared/search/FiltersGroup'
import ResultsDisplay 		from './components/ResultsDisplay'
import BaseComponent 		from '../../shared/components/BaseComponent'

import globals from '../../config/globals'

// actions
import {
	fetchContacts,
	resetFilterContacts,
	applyFilterContacts,
	bufferFilterContacts
	} from './actions/index'
// texts
import {
	CONTACTS_PAGE_DESCRIPTION,
	CONTACTS_RESULT_CALL_TO_ACTION,
	CONTACTS_RESULTS_TITLE,
	CONTACTS_FILTER_GROUP_TITLE
	} from '../../config/strings'


class Contacts extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}


	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	    this.props.fetchContacts();
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
		//this.refs.filtersGroupRef.setLoadingStateFromParent(false);
	}

	componentWillMount() {
	}

	componentWillReceiveProps(nextProps) {

	}

	render(){
		//console.log('Contacts routes', this.props.routes);
		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<section className="comp-contacts page contacts-page">

					<Breadcrumbs />

					<PageTitleBar title="Directory">
						<Link to={`${globals.basePath}/contacts/add/`}>
							<TooltippedIconButton text="Add Contact"><ContentAdd /></TooltippedIconButton>
						</Link>
					</PageTitleBar>

					{/*<SimpleText text={ CONTACTS_PAGE_DESCRIPTION } className="vevefeche" />*/}

					<ul className="collapsible" data-collapsible="expandable">
						<li>
						  	<div className="collapsible-header active">
						  		<CollapsibleHeader  title={ CONTACTS_FILTER_GROUP_TITLE } />
						  	</div>
						  	<div className="collapsible-body">
								<FiltersGroup
									title={ CONTACTS_FILTER_GROUP_TITLE }
									data={ this.props.contacts }
									ref="filtersGroupRef"
									onResetHandler={ () => this.props.resetFilterContacts() }
									onApplyHandler={ () => this.props.applyFilterContacts() }
									onSelectChange={ (filter) => this.props.bufferFilterContacts(filter) }/>
						  	</div>
						</li>
						<li>
						  	<div className="collapsible-header active"><CollapsibleHeader  title={ CONTACTS_RESULTS_TITLE } /></div>
						  	<div className="collapsible-body">
						  		{/*<SimpleText text={ CONTACTS_RESULT_CALL_TO_ACTION } />*/}
								<ResultsDisplay
									title={ CONTACTS_RESULTS_TITLE }
									data={ this.props.contacts } />
						  	</div>
						</li>
					</ul>
				</section>
			</ReactCSSTransitionGroup>
		);
	}
}
Contacts.propTypes = {};
Contacts.defaultProps = {};

function mapStateToProps({contacts}){
	return { contacts: contacts };
}
function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
			fetchContacts,
			resetFilterContacts,
			applyFilterContacts,
			bufferFilterContacts
		},
		dispatch
	);
}

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);