import React, {Component} from 'react'
import {Link} from 'react-router'

import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton 			from 'material-ui/IconButton'
import IconMenu 			from 'material-ui/IconMenu'
import MenuItem 			from 'material-ui/MenuItem'
import FileDownloadIcon 	from 'material-ui/svg-icons/file/file-download'
import ReorderIcon 			from 'material-ui/svg-icons/action/reorder'
import MoreVertIcon 		from 'material-ui/svg-icons/navigation/more-vert'
import Delete 				from 'material-ui/svg-icons/action/delete'
import Details		 		from 'material-ui/svg-icons/image/details'
import Chart		 		from 'material-ui/svg-icons/editor/show-chart'
import History		 		from 'material-ui/svg-icons/action/history'
import Contract				from 'material-ui/svg-icons/action/compare-arrows'
import Expand 				from 'material-ui/svg-icons/action/swap-horiz'
import Attachments			from 'material-ui/svg-icons/file/attachment'
import Folder				from 'material-ui/svg-icons/file/folder'

class ResultsDisplayToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {
			isExpanded: false
		};

		this.expanderButtonClickHandler = this.expanderButtonClickHandler.bind(this);
		this.actionHandler = this.actionHandler.bind(this);
	}

	expanderButtonClickHandler(event){
		event.preventDefault();
		this.setState({isExpanded:!this.state.isExpanded});
		this.props.toggleColumns();
	}

	// menu item click sent to Projects to display dialog box
	actionHandler(event, actionName){
		//console.log('event', event.actionName);
		event.preventDefault();
		this.props.actionHandler({actionName:actionName});
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		//const btn = this.state.isExpanded ? <Contract /> : <Expand />;

		return(
			<Toolbar style={{backgroundColor:'#fff'}}>
				<ToolbarGroup firstChild={true} />
				<ToolbarGroup lastChild={true}>
					{/*<IconButton onClick={ this.expanderButtonClickHandler }>{ btn }</IconButton>*/}
					<IconButton><FileDownloadIcon /></IconButton>
					<IconMenu
						iconButtonElement={
							<IconButton
								className="tooltipped"
								data-position="bottom" data-delay="50" data-tooltip="Actions"
								disabled={ this.props.disableTools } >
								<MoreVertIcon />
							</IconButton> }
						anchorOrigin={{horizontal: 'right', vertical: 'top'}}
						targetOrigin={{horizontal: 'right', vertical: 'top'}} >
						<MenuItem leftIcon={<Details />} primaryText="Details/Edit"
							onTouchTap={ (e) => this.actionHandler(e, 'details') } />
						<MenuItem leftIcon={<Delete />} primaryText="Delete"
							onTouchTap={ (e) => this.actionHandler(e, 'delete') } />
						<MenuItem leftIcon={<Chart />} primaryText="Chart"
							onTouchTap={ (e) => this.actionHandler(e, 'chart') } />
						<MenuItem leftIcon={<Folder />} primaryText="LTC"
							onTouchTap={ (e) => this.actionHandler(e, 'ltc') } />
						<MenuItem leftIcon={<History />} primaryText="History"
							onTouchTap={ (e) => this.actionHandler(e, 'history') } />
						<MenuItem leftIcon={<Attachments />} primaryText="Files"
							onTouchTap={ (e) => this.actionHandler(e, 'files') } />
						<MenuItem leftIcon={<Folder />} primaryText="Contacts"
							onTouchTap={ (e) => this.actionHandler(e, 'contacts') } />
					</IconMenu>
				</ToolbarGroup>
			</Toolbar>
		);
	}
}
ResultsDisplayToolbar.propTypes = {};
ResultsDisplayToolbar.defaultProps = {};

export default ResultsDisplayToolbar;