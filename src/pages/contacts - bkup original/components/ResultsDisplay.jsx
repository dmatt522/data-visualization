import React, {Component} 		from 'react'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import IconButton	 			from 'material-ui/IconButton'
import Pagination 				from 'react-js-pagination'

import ResultsTable 			from './ResultsTable'
import ToolbarResultsDisplay 	from '../../../shared/components/ToolbarResultsDisplay'
import ResultsCollapsible 		from '../../../shared/components/ResultsCollapsible'

import _ from 'lodash'
// actions
import { paginationChange } from '../actions/index'
// styles
import styles 	from '../../../styles/styles'
import globals from '../../../config/globals'

/**
 * todo: inline editing mode in the result row
 */

class ResultsDisplay extends Component{
	constructor(props){
		super(props);
		this.state = {
			disableTools: true
		};

		this.paginationChangeHandler = this.paginationChangeHandler.bind(this);
		this.enableToolbarTools = this.enableToolbarTools.bind(this);
	}

	paginationChangeHandler(pageNumber){
		// change the page
		this.props.paginationChange(pageNumber);
	}

	// enable/disable toolbar menu to view/edit ..
	enableToolbarTools(rows){
		//console.log('disableTools', (_.size(rows) == 0));
		this.setState({ disableTools: (_.size(rows) == 0) });
	}

	render(){


		if(_.isEmpty(this.props.data)){
			return (
				<div className="no-search-result">Your search matched 0 items.</div>
			);
		}

		return(
			<div className="comp-resultsdisplay">

				<ToolbarResultsDisplay
					title="Search Results"
					disableTools={ this.state.disableTools }
					actionPaths={{
						view: `${globals.basePath}/contacts/${this.props.data.selectedItem['contact_id']}`,
						edit: `${globals.basePath}/contacts/edit/${this.props.data.selectedItem['contact_id']}`,
						delete: "#"
					}} />

				<ResultsTable
					defaultColumns={this.props.defaultColumns}
					data={ this.props.data.pageData }
					enableToolbarTools={ this.enableToolbarTools } />

				<div className="pagination-container">
					<Pagination
						activePage={ this.props.data.activePage }
						itemsCountPerPage={ this.props.data.itemsPerPage }
						totalItemsCount={ _.size(this.props.data.filteredList) }
						pageRangeDisplayed={ 5 }
						onChange={ this.paginationChangeHandler }
						/>
				</div>

			</div>
		);
	}
}
ResultsDisplay.propTypes = {};
//photo, first name, last name, email, title, company
ResultsDisplay.defaultProps = {
	defaultColumns:[
		'photo','first_name', 'last_name', 'email', 'title', 'company_name'
	]
};

// dispatch actions
function mapDispatchToProps(dispatch){
	return bindActionCreators({ paginationChange }, dispatch);
}
// connect to redux, only as dispatcher
export default connect(null, mapDispatchToProps)(ResultsDisplay);