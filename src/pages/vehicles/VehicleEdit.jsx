import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReduxFormConsumer from '../../hocs/reduxFormConsumer'
import { Field } from 'redux-form'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import IconButton from 'material-ui/IconButton'
import History from 'material-ui/svg-icons/action/history'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import _ from 'lodash'

import CollapsibleHeader from '../../shared/components/CollapsibleHeader'
import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar	from '../../shared/toolbars/PageTitleBar'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
//import { Input } from 'react-materialize'
import BaseComponent from '../../shared/components/BaseComponent'
import SimpleText 	from '../../shared/ui/SimpleText'
//import EditToolbar from './components/EditToolbar'

import {
	VEHICLES_EDIT_PAGE_DESCRIPTION,
	VEHICLES_EDIT_PHOTO_TITLE,
	VEHICLES_EDIT_DATA_TITLE,
	VEHICLES_EDIT_BUILD_TITLE
	} from '../../config/strings'

import styles from '../../styles/styles'
import globals from '../../config/globals'

import { renderTextField, renderSelectField } from '../../utils/form-field-renderers'
import { _valueAt, _getOptionsForField, _cySeries } 	from '../../utils/functions'

class VehicleDetails extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};

		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onFocusHandler = this.onFocusHandler.bind(this);
	}


	onFocusHandler(event){
		//console.log('onFocusHandler', event);
	}



	onFormSubmit(data){
		console.log('VehicleEdit form submit', data);
		// call action creator ...
	}

	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}


	render(){

		const { handleSubmit, pristine, reset, submitting } = this.props;

		const cys = _cySeries(this.props.vehicles.selectedItem);
		/* [];
		_.forEach(this.props.vehicles.selectedItem, function(value, key) {
			if(_.startsWith(key, 'cy_'))
				cys.push(_.set({}, 'name', key));
		})*/

		//console.log('cys',cys );

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<div className="comp-vehicleedit page vehicleedit-page">

					<Breadcrumbs />
					{/*<SimpleText text={ VEHICLES_EDIT_PAGE_DESCRIPTION } />*/}

					<PageTitleBar title="Vehicle Edit" >
						<IconButton className="tooltipped" data-position="bottom" data-delay="50" data-tooltip="History"><History /></IconButton>
					</PageTitleBar>

					{/*<EditToolbar styles={styles} />*/}
					<div className="row" style={{marginBottom:0}}>
						<div className="col s12">
							<h5 className="details-title">{ _valueAt(this.props.vehicles.selectedItem, 'manufacturer') } { _valueAt(this.props.vehicles.selectedItem, 'production_brand') }</h5>
						</div>
					</div>

					<form className="vehicle-edit-form" onSubmit={ handleSubmit(this.onFormSubmit) }>
						<ul className="collapsible" data-collapsible="expandable">
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title={ VEHICLES_EDIT_DATA_TITLE } />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
							           	<Field name="production_brand" options={_getOptionsForField(this.props.vehicles.filters, 'production_brand')} component={renderSelectField} label="Brand" />
										<Field name="Production_Nameplate" type="text" component={renderTextField} label="Nameplate" />
										<Field name="program" type="text" component={renderTextField} label="Program" />
										<Field name="manufacturer" options={_getOptionsForField(this.props.vehicles.filters, 'manufacturer')} component={renderSelectField} label="Manufacturer" />
										<Field name="platform" type="text" component={renderTextField} label="Platform" />
										<Field name="sop" type="text" component={renderTextField} label="SOP" />
										<Field name="eop" type="text" component={renderTextField} label="EOP" />
										<Field name="production_plant" type="text" component={renderTextField} label="Production Plant" />
										<Field name="location" type="text" component={renderTextField} label="Location" />
						        	</div>
						        </div>
						    </li>
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title={ VEHICLES_EDIT_BUILD_TITLE } />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
						            {
										_.map(cys, (item) =>
											<Field key={ item.name } name={ item.key } type="text" component={renderTextField} label={ item.label } />
										)
									}
						        	</div>
						        </div>
						    </li>
						    <li>
						        <div className="collapsible-header active">
						            <CollapsibleHeader title={ VEHICLES_EDIT_PHOTO_TITLE } />
						        </div>
						        <div className="collapsible-body">
						        	<div className="row">
							            <div className="col s12 m2">
											<img src={`${globals.basePath}/images/car_placeholder.jpg`} className="responsive-img form-edit-image" />
										</div>
										<div className="col s12 m4 file-field input-field file-upload">
											<div className="btn">
												<span>File</span>
												<Field type="file" component="input" name="vehicle_image" />
											</div>
											<div className="file-path-wrapper">
												<input className="file-path validate" type="text" />
											</div>
										</div>
						        	</div>
						        </div>
						    </li>
						</ul>
						<div className="row" style={{textAlign:'center'}}>
							<div className="col s10 offset-s1">
							<RaisedButton type="button" label="Reset Changes" primary={false} disabled={ pristine || submitting } onClick={reset} />
							<RaisedButton type="submit" label="Update Vehicle Data" primary={true} disabled={ pristine || submitting }  />
							</div>
						</div>
					</form>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
VehicleDetails.propTypes = {};
VehicleDetails.defaultProps = {};


// listen to state changes
function mapStateToProps( state ){
	return {
		vehicles: state.vehicles,
		initialValues: state.vehicles.selectedItem
	};
}
// dispatch actions
function mapDispatchToProps(dispatch){
	return bindActionCreators({}, dispatch);
}

VehicleDetails = ReduxFormConsumer(VehicleDetails, 'vehicleEditForm');
VehicleDetails = connect(mapStateToProps, mapDispatchToProps)(VehicleDetails);
export default VehicleDetails;