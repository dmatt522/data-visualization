import React, {Component} 		from 'react'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import Dialog from 'material-ui/Dialog'

import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar			from '../../shared/toolbars/PageTitleBar'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import BaseComponent 	from '../../shared/components/BaseComponent'
import FiltersGroup 	from '../../shared/search/FiltersGroup'
//import SimpleText 	from '../../shared/ui/SimpleText'
import ResultsDisplay 	from './components/ResultsDisplay'
import Details from './components/dialog/Details'
import CollapsibleHeader from '../../shared/components/CollapsibleHeader'

import _ from 'lodash'

// actions
import {
	fetchVehicles,
	resetFilterVehicles,
	applyFilterVehichles,
	bufferFilterVehicles } from './actions/index'
import { notify } from '../../shared/actions/index'
// shared actions
import { LOADING_DATA, UPDATING_DATA } from '../../config/messages'
// texts
import {
	VEHICLES_PAGE_DESCRIPTION,
	VEHICLES_RESULT_CALL_TO_ACTION,
	VEHICLES_RESULTS_TITLE,
	VEHICLES_FILTER_GROUP_TITLE } from '../../config/strings'

import styles from '../../styles/styles'


function whoIsTheChild(name, closeDialogHandler){
	switch(name){
		case 'details':
			return <Details closeHandler={ closeDialogHandler } />;
		default:
			return <Details closeHandler={ closeDialogHandler } />;
	}
}

class Vehicles extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {
			openDialog:false,
			actionName:''
		};

		this.applyFiltersHandler = this.applyFiltersHandler.bind(this);
		this.resetFiltersHandler = this.resetFiltersHandler.bind(this);
		this.didUpdateCallback = this.didUpdateCallback.bind(this);
		this.toolbarActionHandler = this.toolbarActionHandler.bind(this);
		this.closeDialogHandler = this.closeDialogHandler.bind(this);
	}

	closeDialogHandler(){
		this.setState({openDialog:false});
	}
	toolbarActionHandler(obj){
		console.log('action', obj.actionName);

		if(obj.actionName === 'add'){
			//`${globals.basePath}/projects/add/${this.props.data.selectedItem['vehicle_id']}`
			//console.log('--->', this.props.vehicles.selectedItem.vehicle_id);
			this.context.router.push(`/projects/add/${this.props.vehicles.selectedItem.vehicle_id}`);
		}
		else{
			this.setState({openDialog:true, actionName:obj.actionName});
		}
	}

	applyFiltersHandler(){
		//this.setState({activeResults:''});
		//_.delay((o) => this.setState(o) , 2000, {activeResults:'active'});
		//$(this.collapsibleResults).trigger('click');//.classList.toggle('active');
		//this.setState({loading:true});
		this.props.applyFilterVehichles();

	}
	resetFiltersHandler(){
		//this.setState({activeResults:''});
		//_.delay((o) => this.setState(o) , 2000, {activeResults:'active'});
		//$(this.collapsibleResults).trigger('click');//.classList.toggle('active');
		//this.setState({loading:true});
		this.props.resetFilterVehicles();
	}

	didUpdateCallback(c){
		//console.log('Vehicles.didUpdateCallback', c);
		//$('.collapsible').collapsible({accordion : false });
		//$(this.collapsibleFilters).addClass('active');
	}


	componentWillMount() {
		//console.log('Vehicles.componentWillMount');
		//console.log('--->', this.collapsibleFilters);
		//$('.collapsible').collapsible({accordion : false });
		//this.setState({loading:false});
		// action to load vehicles data from api
	}
	componentDidMount() {
		//console.log('Vehicles.componentDidMount');
		$('.collapsible').collapsible({accordion : false });
		if(_.isEmpty(this.props.vehicles.list)){
			this.props.fetchVehicles();
		}
		//this.props.notify({message:LOADING_DATA});
	}
	componentWillUpdate(nextProps, nextState) {
	    //console.log('Vehicles.componentWillUpdate');
	    //console.log('--->', this.collapsibleFilters);
	    //$('.collapsible').collapsible({accordion : false });
	    //$('.collapsible').collapsible({accordion : false });

	    //$(this.collapsibleResults).trigger('click');
	    //console.log('---->', 'nextProps', nextProps);
		//console.log('---->', 'nextState', nextState);
	}
	componentDidUpdate(prevProps, prevState) {
		//console.log('Vehicles.componentDidUpdate');
		$('.collapsible').collapsible({accordion : false });
		//console.log('@ ', this.refs.filtersGroupRef);
		//this.refs.filtersGroupRef.setLoadingStateFromParent(false);
		//$('.collapsible').collapsible({accordion : false });
		//console.log('---->', 'prevProps', prevProps);
		//console.log('---->', 'prevState', prevState);
		//_.delay((o) => this.setState(o) , 4000, {activeFilters:'active'});
	    //this.setState({activeFilters:'active'});
		//_.delay((o) => this.setState(o) , 2000, {activeFilters:'active', activeResults:'active'});
	}
	componentWillReceiveProps(nextProps) {
	    //console.log('Vehicles.componentWillReceiveProps');
	    //this.setState({loading:true});
	    //_.delay((o) => this.setState(o) , 4000, {activeFilters:''});
	   	//this.props.notify(UPDATING_DATA);
	    /*if(!_.isEmpty(nextProps.vehicles.list)){
	    	this.setState({loading:false});
	    }*/
	}
	shouldComponentUpdate(nextProps, nextState) {
		//console.log('Vehicles.shouldComponentUpdate');
	    return true;
	}
	componentWillUnmount() {
		this.props.resetFilterVehicles();
	    //console.log('Vehicles.componentWillUnmount');
	    //this.refs.filtersGroupRef.setStateFromParent({loading:true});
	}

	render(){
		console.log('Vehicles.render');
		console.log(this.props.vehicles.filters);

		/*if(this.state.loading){
			return <div>'loading...'</div>;
		}*/
		const dialogChild = whoIsTheChild(this.state.actionName, this.closeDialogHandler);

		return(


			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<div className="comp-vehicles page vehicles-page">

					<Breadcrumbs />

					<PageTitleBar title="Vehicles" />

					{/*<SimpleText text={ VEHICLES_PAGE_DESCRIPTION } />*/}

					<ul className="collapsible" data-collapsible="expandable">
						<li>
							<div className="collapsible-header">
								<CollapsibleHeader  title={VEHICLES_FILTER_GROUP_TITLE} />
							</div>
						  	<div className="collapsible-body">
								{/*<SimpleText text="lorem ipsum dolor" />*/}
								<FiltersGroup
									ref="filtersGroupRef"
									title={ VEHICLES_FILTER_GROUP_TITLE }
									data={ this.props.vehicles }
									onResetHandler={ this.resetFiltersHandler }
									onApplyHandler={ this.applyFiltersHandler }
									onSelectChange={ (filter) => this.props.bufferFilterVehicles(filter) } />
						  	</div>
						</li>
						<li>
						  	<div className="collapsible-header active">
								<CollapsibleHeader  title={VEHICLES_RESULTS_TITLE} /></div>
						  	<div className="collapsible-body">
								{/*<SimpleText text={VEHICLES_RESULT_CALL_TO_ACTION} />*/}
								<ResultsDisplay
									title={ VEHICLES_RESULTS_TITLE }
									toolbarActionHandler={ this.toolbarActionHandler }
									data={ this.props.vehicles } />
						  	</div>
						</li>
					</ul>
				</div>

				<Dialog
					autoScrollBodyContent={true}
					children={dialogChild}
					open={this.state.openDialog}
					contentStyle={styles.projectDialog}
					onRequestClose={ () => this.setState({openDialog:false}) }  />

			</ReactCSSTransitionGroup>
		);
	}
}
Vehicles.propTypes = {};
Vehicles.defaultProps = {
	numCollapsible: 2
};
Vehicles.contextTypes = {
	router: React.PropTypes.object
};

// recive data from app state reducer
function mapStateToProps({ vehicles, contacts }){
	//console.log('contacts', contacts);
	return { vehicles };
}
// dispatch actions
function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
			fetchVehicles,
			resetFilterVehicles,
			applyFilterVehichles,
			bufferFilterVehicles,
			notify
		},
		dispatch
	);
}
// connect to redux
export default connect(mapStateToProps, mapDispatchToProps)(Vehicles);