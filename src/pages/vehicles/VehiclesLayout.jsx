import React, {Component} from 'react';
import styles from '../../styles/styles'
class VehiclesLayout extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}
	render(){
		return(
			<div className="page-layout vehicles-page-layout">
				{this.props.children}
			</div>
		);
	}
}
VehiclesLayout.propTypes = {};
VehiclesLayout.defaultProps = {};
export default VehiclesLayout;