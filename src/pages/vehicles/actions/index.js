import axios 	from 'axios'
import globals 	from '../../../config/globals'
import {
	ALL_VEHICLES,
	SINGLE_VEHICLE,
	VEHICLE_EDIT,
	VEHICLE_DELETE
	} from '../../../config/api'
import {
	FETCH_VEHICLES,
	BUFFER_FILTERS_VEHICLES,
	APPLY_FILTERS_VEHICLES,
	RESET_FILTER_VEHICLES,
	PAGINATION_VEHICLES_CHANGE,
	SELECT_VEHICLE,
	SORT_VEHICLES,
	EDIT_VEHICLE,
	DELETE_VEHICLE
	} from '../../../actions/types'



// warning: use api urls not locals

/**
 * fetches all vehicles data
 * @return {[type]} [description]
 */
export function fetchVehicles(){

	//const URL = `${API_BASE_URL}${ALL_VEHICLES}`;
	//const URL = ALL_VEHICLES;//`${FAKE_API_BASE_URL}vehicles.json`;
	//const URL = 'https://commops.rekoware.com/demo/data/vehicles.json'
	const request = axios.get(ALL_VEHICLES);
	//console.log('Action: fetchVehicles', request);
	//debugger;
	return {
		type: FETCH_VEHICLES,
		payload: request
	};
}

/**
 * add/change the value of a display vehicle filter
 * @param  {object} data {label, value}
 * @return {[type]}      [description]
 */
export function bufferFilterVehicles(data){
	//console.log('Action: filterVehicles', data);
	return {
		type:BUFFER_FILTERS_VEHICLES,
		payload: data
	};
}

/**
 * [resetFilterVehicles description]
 * @return {[type]} [description]
 */
export function resetFilterVehicles(){
	//console.log('Action: resetFilterVehicles');
	return {
		type: RESET_FILTER_VEHICLES,
		payload: {}
	};
}

/**
 * [applyFilterVehichles description]
 * @return {[type]} [description]
 */
export function applyFilterVehichles(){
	//console.log('Action: applyFilterVehichles');
	return {
		type: APPLY_FILTERS_VEHICLES,
		payload: {}
	};
}

/**
 * [paginationChange description]
 * @param  {[type]} page [description]
 * @return {[type]}      [description]
 */
export function paginationChange(page){
	//console.log('Action: paginationChange');
	return {
		type: PAGINATION_VEHICLES_CHANGE,
		payload: page
	};
}

/**
 * [sortList description]
 * @param  {[type]} sortKey [description]
 * @return {[type]}         [description]
 */
export function sortList(sortKey){
	//console.log('Action: sortList', sortKey);
	return {
		type: SORT_VEHICLES,
		payload: sortKey
	}

}

/**
 * [selectVehicle description]
 * @param  {array of integers} rows: integers are the indexes of the selected row/rows
 * @return {[type]}      [description]
 */
export function selectVehicle(rows){
	//console.log('Action: selectItem', rows);
	return {
		type: SELECT_VEHICLE,
		payload: rows
	}
}

export function editVehicleData(formData){
	const request = axios.post(VEHICLE_EDIT, formData);
	return {
		type: EDIT_VEHICLE,
		payload: request
	};
}

/**
 * [deleteVehicle description]
 * @param  {[type]} payload [description]
 * @return {[type]}         [description]
 */
export function deleteVehicle(payload){
	console.log('deleting', payload);
	const request = axios.post(VEHICLE_DELETE, payload);
	return {
		type: DELETE_VEHICLE,
		payload: request
	};
}