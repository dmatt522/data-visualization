import React, { Component, PropTypes } 	from 'react'
import { connect } 						from 'react-redux'
import { bindActionCreators } 			from 'redux'

import Avatar 		from 'material-ui/Avatar'
import FlatButton 	from 'material-ui/FlatButton'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

import _ from 'lodash'

// this component actions to dispatch
import { sortList, selectVehicle } from '../actions/index'

// utils functions
import { refactorKey, mapKeyToHelpText, _cellValue } from '../../../utils/functions'

import HelpButton from '../../../shared/buttons/HelpButton'
import styles from '../../../styles/styles'


// data I do not watnt to display in table


class ResultsTable extends Component{
	constructor(props){
		super(props);
		this.state = {};

		this.headerClickHandler = this.headerClickHandler.bind(this);
		this.rowSelectionHandler = this.rowSelectionHandler.bind(this);
	}

	headerClickHandler(key){
		this.props.sortList(key);
	}

	rowSelectionHandler(rows){
		this.props.enableToolbarTools(rows)
		this.props.selectVehicle(rows);
	}

	componentDidMount() {
		$('.tooltipped').tooltip({delay: 50});
	}
	componentDidUpdate(prevProps, prevState) {
	    $('.tooltipped').tooltip({delay: 50});
	}
	componentWillUnmount() {
		$('.tooltipped').tooltip('remove');
	}

	render(){
		// no data to render ...
		//
		if(_.isEmpty(this.props.data)){
			return (
				<div className="no-search-result">Your search matched 0 items.</div>
			);
		}

		// data to render
		//
		// remove some props from the objects representing the single vehicle
		//const propsToRemove = ['vehicle_image', 'vehicle_mnemonic', 'vehicle_id'];
		const displayData = this.props.data.map( (item) => {
			return _.pick(item, this.props.defaultColumns);
		} );

		return(
			<Table
				bodyStyle={{overflow:'visible'}}
				fixedHeader={ false }
				fixedFooter={ false }
				onRowSelection={ this.rowSelectionHandler } >
				<TableHeader>
		        	<TableRow>
		        		{
		        			_.map(displayData[0], (value, key) => {
		        				return (
		        					<TableHeaderColumn  aria-label={ mapKeyToHelpText(key) } className={ `${key} hint--bottom hint--medium hint--bounce` } key={ key } style={{width: 160}}>
		        						{/*<FlatButton
		        							hoverColor="transparent"
		        							labelStyle={{textDecoration:'underline'}}
		        							style={{minWidth:40}}
		        							primary={true}
		        							label={ refactorKey(key) }
		        							onClick={ (event) => this.headerClickHandler(key) } />*/}
		        						<span className="tableHeaderColumnText" onClick={ (event) => this.headerClickHandler(key) }>{ _.upperFirst(refactorKey(key)) }</span>
		        					</TableHeaderColumn>
		        				);
		        			})
		        		}
			      	</TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} >
			    	{
		        		displayData.map((item, i) => {
		        			return (
		        				<TableRow key={ i } style={{cursor:'pointer'}}>
		        					{ _.map(item, (value, key) =>  <TableRowColumn className={ key } key={ key } style={ styles.td }>{ _cellValue(value, key) }</TableRowColumn> ) }
		        				</TableRow>
		        			)
		        		})
		        	}
			    </TableBody>
			</Table>
		);
	}
}
ResultsTable.displayName = 'ResultsTable';
ResultsTable.propTypes = {};

function mapDispatchToProps(dispatch){
	return bindActionCreators({ sortList, selectVehicle }, dispatch);
}

export default connect(null, mapDispatchToProps)(ResultsTable);
