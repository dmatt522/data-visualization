import React, {Component} from 'react'
import { Link } from 'react-router'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import History from 'material-ui/svg-icons/action/history'
import FlatButton from 'material-ui/FlatButton'

class DetailsToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
			<Toolbar>
				<ToolbarGroup firstChild={true}>
					<ToolbarTitle style={this.props.styles.title} text="Edit Vehicle" />
				</ToolbarGroup>
				<ToolbarGroup>
					<IconButton className="tooltipped" data-position="bottom" data-delay="50" data-tooltip="History"><History /></IconButton>
				</ToolbarGroup>
			</Toolbar>
		);
	}
}
DetailsToolbar.propTypes = {};
DetailsToolbar.defaultProps = {};

export default DetailsToolbar;