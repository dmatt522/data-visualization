import React, {Component} from 'react'
import { Link } from 'react-router'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'

import TooltippedIconButton from '../../../shared/ui/TooltippedIconButton'

import globals from '../../../config/globals'

class DetailsToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
			<Toolbar>
				<ToolbarGroup firstChild={true}>
					<ToolbarTitle style={this.props.styles.title} text="Vehicle" />
				</ToolbarGroup>
				<ToolbarGroup>
					<Link to={`${globals.basePath}/projects/add/00000`}>
						<TooltippedIconButton text="Add Project"><ContentAdd /></TooltippedIconButton>
					</Link>
					<Link to={`${globals.basePath}/vehicles/edit/${this.props.id}`}>
						<TooltippedIconButton text="Edit"><EditorModeEdit /></TooltippedIconButton>
					</Link>
				</ToolbarGroup>
			</Toolbar>
		);
	}
}
DetailsToolbar.propTypes = {};
DetailsToolbar.defaultProps = {};

export default DetailsToolbar;