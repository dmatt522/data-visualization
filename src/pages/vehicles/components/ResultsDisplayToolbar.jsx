import React, {Component} from 'react'
import {Link} from 'react-router'

import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton 			from 'material-ui/IconButton'
import IconMenu 			from 'material-ui/IconMenu'
import MenuItem 			from 'material-ui/MenuItem'
import FileDownloadIcon 	from 'material-ui/svg-icons/file/file-download'
import MoreVertIcon 		from 'material-ui/svg-icons/navigation/more-vert';
import Details		 		from 'material-ui/svg-icons/image/details'
import Add 				from 'material-ui/svg-icons/content/add'

class ResultsDisplayToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {
			isExpanded: false
		};

		this.expanderButtonClickHandler = this.expanderButtonClickHandler.bind(this);
		this.actionHandler = this.actionHandler.bind(this);
	}

	expanderButtonClickHandler(event){
		event.preventDefault();
		this.setState({isExpanded:!this.state.isExpanded});
		this.props.toggleColumns();
	}

	// menu item click sent to Projects to display dialog box
	actionHandler(event, actionName){
		//console.log('event', event.actionName);
		event.preventDefault();
		this.props.actionHandler({actionName:actionName});
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		//const btn = this.state.isExpanded ? <Contract /> : <Expand />;

		return(
			<Toolbar style={{backgroundColor:'#fff'}}>
				<ToolbarGroup firstChild={true} />
				<ToolbarGroup lastChild={true}>
					{/*<IconButton onClick={ this.expanderButtonClickHandler }>{ btn }</IconButton>*/}
					<IconButton><FileDownloadIcon /></IconButton>
					<IconMenu
						iconButtonElement={
							<IconButton
								className="tooltipped"
								data-position="bottom" data-delay="50" data-tooltip="Actions"
								disabled={ this.props.disableTools } >
								<MoreVertIcon />
							</IconButton> }
						anchorOrigin={{horizontal: 'right', vertical: 'top'}}
						targetOrigin={{horizontal: 'right', vertical: 'top'}} >
						<MenuItem leftIcon={<Add />} primaryText="Add Project"
							onTouchTap={ (e) => this.actionHandler(e, 'add') } />
						<MenuItem leftIcon={<Details />} primaryText="Details/Edit"
							onTouchTap={ (e) => this.actionHandler(e, 'details') } />
					</IconMenu>
				</ToolbarGroup>
			</Toolbar>
		);
	}
}
ResultsDisplayToolbar.propTypes = {};
ResultsDisplayToolbar.defaultProps = {};

export default ResultsDisplayToolbar;