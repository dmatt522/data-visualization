import React, {Component} 		from 'react'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'
import IconButton	 			from 'material-ui/IconButton'
import Pagination 				from 'react-js-pagination'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'
import ResultsTable 			from './ResultsTable'
import ResultsDisplayToolbar 	from './ResultsDisplayToolbar'
import ResultsCollapsible 		from '../../../shared/components/ResultsCollapsible'
import Preloader 				from '../../../shared/components/Preloader'
import NoResult 				from '../../../shared/components/NoResult'

import _ from 'lodash'
// actions
import { paginationChange, deleteVehicle } from '../actions/index'
import { notify } from '../../../shared/actions/index'
import { UPDATING_DATA } from '../../../config/messages'
// styles
import styles 	from '../../../styles/styles'
import globals from '../../../config/globals'

/**
 * todo: inline editing mode in the result row
 */

class ResultsDisplay extends Component{
	constructor(props){
		super(props);
		this.state = {
			disableTools: true/*,
			loading:true*/
		};

		this.paginationChangeHandler = this.paginationChangeHandler.bind(this);
		this.enableToolbarTools = this.enableToolbarTools.bind(this);
		this.deleteHandler = this.deleteHandler.bind(this);
	}

	paginationChangeHandler(pageNumber){
		//this.props.notify(UPDATING_DATA);
		// change the page
		this.props.paginationChange(pageNumber);
	}

	// enable/disable toolbar menu to view/edit ..
	enableToolbarTools(rows){
		//console.log('disableTools', (_.size(rows) == 0));
		this.setState({ disableTools: (_.size(rows) == 0) });
	}

	deleteHandler(event){
		this.props.deleteVehicle({
			scope: 'Vehicles',
			id: this.props.data.selectedItem['vehicle_id']
		});
	}

	componentWillMount() {
	    /*console.log('ResultsDisplay.componentWillMount');
	    if(!_.isEmpty(this.props.data.pageData))
	    	this.setState({loading:false});*/
	}
	componentDidMount() {
	    //console.log('ResultsDisplay.componentDidMount');

	}
	componentWillReceiveProps(nextProps) {
	    //console.log('ResultsDisplay.componentWillReceiveProps');
	    //console.log('@ current', this.props.data.pageData);
	    //console.log('# next', nextProps.data.pageData);
	    /*if(!_.isEqual(this.props.data.pageData, nextProps.data.pageData)){
	    	console.log('pageData changed ...');
	    	this.setState({loading:true});
	    }*/

	}
	componentWillUpdate(nextProps, nextState) {
	    //console.log('ResultsDisplay.componentWillUpdate');

	}
	componentDidUpdate(prevProps, prevState) {
	    /*console.log('ResultsDisplay.componentDidUpdate');
		if(!_.isEqual(this.props.data.pageData, prevProps.data.pageData)){
	    	console.log('pageData changed ...');
	    	this.setState({loading:false});
	    }*/
	}
	componentWillUnmount() {
	    /*console.log('ResultsDisplay.componentWillUnmount');
	    this.setState({loading:false});*/
	}

	render(){
		/*console.log('ResultsDisplay.render', 'is loading?', this.state.loading);

		if(_.isUndefined(this.state.loading) || this.state.loading){
			return (
				<Preloader />
			);
		}*/

		if(_.isEmpty(this.props.data)){
			return (
				<NoResult />
			);
		}

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >

				<div className="comp-resultsdisplay">

					<ResultsDisplayToolbar
						disableTools={ this.state.disableTools }
						deleteHandler={ this.deleteHandler }
						actionHandler={ this.props.toolbarActionHandler}
						actionPaths={{
							addprj: `${globals.basePath}/projects/add/${this.props.data.selectedItem['vehicle_id']}`,
							view: `${globals.basePath}/vehicles/${this.props.data.selectedItem['vehicle_id']}`,
							edit: `${globals.basePath}/vehicles/edit/${this.props.data.selectedItem['vehicle_id']}`,
							delete: "#"
						}} />

					<ResultsTable
						defaultColumns={ this.props.defaultColumns }
						data={ this.props.data.pageData }
						enableToolbarTools={ this.enableToolbarTools } />

					<div className="pagination-container">
						<Pagination
							activePage={ this.props.data.activePage }
							itemsCountPerPage={ this.props.data.itemsPerPage }
							totalItemsCount={ _.size(this.props.data.filteredList) }
							pageRangeDisplayed={ 5 }
							onChange={ this.paginationChangeHandler }
							/>
					</div>

				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
ResultsDisplay.propTypes = {};
//humbnail, manufacturer, brand, platform, program, nameplate, sop, eop
ResultsDisplay.defaultProps = {
	defaultColumns:[
		'vehicle_image', 'manufacturer', 'production_brand', 'platform', 'program', 'nameplate', 'sop', 'eop'
	]
};

// dispatch actions
function mapDispatchToProps(dispatch){
	return bindActionCreators({ paginationChange, deleteVehicle, notify }, dispatch);
}
// connect to redux, only as dispatcher
export default connect(null, mapDispatchToProps)(ResultsDisplay);