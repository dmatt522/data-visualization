import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field } from 'redux-form'

import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import { Toolbar, ToolbarGroup, ToolbarTitle, ToolbarSeparator } from 'material-ui/Toolbar'
import Toggle from 'material-ui/Toggle'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import Chart from 'material-ui/svg-icons/editor/show-chart'
import Edit from 'material-ui/svg-icons/editor/mode-edit'
import Eye from 'material-ui/svg-icons/action/visibility'
import Close from 'material-ui/svg-icons/navigation/close'

import TitleHeader from '../../../../shared/components/TitleHeader'
import ReduxFormConsumer from '../../../../hocs/reduxFormConsumer'
//import ComposableToolbar from '../../../../shared/toolbars/ComposableToolbar'
import TooltippedIconButton from '../../../../shared/ui/TooltippedIconButton'

//import { newItem } from '../../../../shared/actions/lookups'

//import { submitProject, editProject, fetchProjectChart1} 	from '../../actions/index'

import {
	_valueAt,
	_getOptionsForField,
	_cySeries,
	_cyTableData,
	_mapInitialValues,
	getUserName,
	getName } from '../../../../utils/functions'

import { genIconButton } from '../../../../utils/components-generators'

import {
	renderDialogTextFieldLookup,
	renderDialogSelectFieldLookup,
	userSelect,
	idSelect } from '../../../../utils/form-field-renderers'

const createColumn = (label, item, prop, unit='') => {
	return 	(
		<div className="col s12 m4">
			<span>{label}</span>: { `${_valueAt(item, prop)}${unit}` }
	    </div>
	)
};
/*
const createItem = (label, item, prop, unit='') => {
	return 	(
		<div className="column-item">
			<span className="as-label">{label}</span>
			<span>{ `${_valueAt(item, prop)}${unit}` }</span>
	    </div>
	)
}
*/

class Details extends Component{
	constructor(props){
		super(props);
		this.state = {
		};
		this.closeHandler = this.closeHandler.bind(this);
	}

	closeHandler(){
		this.props.closeHandler();
	}
	/**
	 * component creation phase methods
	 */
	componentWillMount() {
		//console.log('details will mount', this.props.projects.selectedItem);

	}
	componentDidMount() {
		//console.log('details did mount', this.props.projects.selectedItem.project_id);
		//$('.tooltipped').tooltip({delay: 50});
		$('.collapsible').collapsible({accordion : false });
	}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {
		//console.log('details will update', nextProps);
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
		//$('.tooltipped').tooltip({delay: 50});
	}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){

		const item = this.props.vehicles.selectedItem;


		const cyTableData = _cyTableData(item);

		console.log('cyTableData', cyTableData);

		return(
			<div className="dialog-content project-details">
				<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle className="toolbar-title" text={`${ _valueAt(item, 'manufacturer') } - ${ _valueAt(item, 'production_brand') }`} />

					</ToolbarGroup>
					<ToolbarGroup lastChild={true}>
						{ genIconButton( <Close />, this.closeHandler, 'Close') }
					</ToolbarGroup>
				</Toolbar>


			{/* start view details */}
				<div>



{/* start collapsible */}
<ul className="collapsible project-details" data-collapsible="expandable">
	<li>
		<div className="collapsible-header active">
            <TitleHeader title="Vehicle Media" />
        </div>
        <div className="collapsible-body">
        <div className="row">
            	<div className="col s12 m4">
        			<img src={ _valueAt(item, 'vehicle_image') } className="responsive-img" />
        		</div>
        	</div>
        </div>
	</li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Vehicle Data" />
        </div>
        <div className="collapsible-body">
            <div className="row">
            	<div className="col s12 m4">
					<span>Manufacturer</span> { _valueAt(item, 'manufacturer') }
				</div>
				<div className="col s12 m4">
					<span>Brand</span> { _valueAt(item, 'production_brand') }
				</div>
				<div className="col s12 m4">
					<span>Subsegment</span> { _valueAt(item, 'global_sales_subsegment') }
				</div>
				<div className="col s12 m4">
					<span>Segment</span> { _valueAt(item, 'regional_sales_segment') }
				</div>

				<div className="col s12 m4">
					<span>Nameplate</span> { _valueAt(item, 'production_nameplate') }
				</div>
				<div className="col s12 m4">
					<span>Platform</span> { _valueAt(item, 'platform') }
				</div>
				<div className="col s12 m4">
					<span>Program</span> { _valueAt(item, 'program') }
				</div>

				<div className="col s12 m4">
					<span>Production Plant</span> { _valueAt(item, 'production_plant') }
				</div>
				<div className="col s12 m4">
					<span>Country</span> { _valueAt(item, 'country') }
				</div>
				<div className="col s12 m4">
					<span>Location</span> { _valueAt(item, 'location') }
				</div>
				<div className="col s12 m4">
					<span>Start of Production</span> { _valueAt(item, 'sop') }
				</div>
				<div className="col s12 m4">
					<span>End of Production</span> { _valueAt(item, 'eop') }
				</div>
            </div>
        </div>
    </li>
    <li>
        <div className="collapsible-header active">
            <TitleHeader title="Vehicle Build" />
        </div>
        <div className="collapsible-body">
            <div className="row">
					<div className="col s12">
							<Table className="prj-details-table" style={{width:'100%'}} selectable={false}>
								<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
									<TableRow>
										<TableHeaderColumn>Calendar Year</TableHeaderColumn>
										<TableHeaderColumn>Vehicle Builds</TableHeaderColumn>
										{/*<TableHeaderColumn><h5>Part Volume</h5></TableHeaderColumn>*/}
									</TableRow>
								</TableHeader>
								<TableBody displayRowCheckbox={false}>
									{
										cyTableData.map( (row, index) => (
											<TableRow key={ index }>
												<TableRowColumn>{ row.year }</TableRowColumn>
												<TableRowColumn>{ row.tot }</TableRowColumn>
												{/*<TableRowColumn>{ `${row[uomAbbr]} (${uomAbbr})` }</TableRowColumn>*/}
											</TableRow>
										))
									}
								</TableBody>
							</Table>
						</div>
            </div>
        </div>
    </li>

</ul>
{/* finish collapsible */}


					{/* details vehicle data
					<div className="row dialog-view-edit-row-vehicle-data">
						<div className="col s12">
							<h5>Vehicle Data</h5>
						</div>
						<div className="col s12 m4">
							<span>Manufacturer</span> { _valueAt(item, 'manufacturer') }
						</div>
						<div className="col s12 m4">
							<span>Platform</span> { _valueAt(item, 'platform') }
						</div>
						<div className="col s12 m4">
							<span>Production Plant</span> { _valueAt(item, 'production_plant') }
						</div>
						<div className="col s12 m4">
							<span>Location</span> { _valueAt(item, 'location') }
						</div>
						<div className="col s12 m4">
							<span>Start of Production</span> { _valueAt(item, 'sop') }
						</div>
						<div className="col s12 m4">
							<span>End of Production</span> { _valueAt(item, 'eop') }
						</div>
					</div>*/}
					{/* details row 1
					<div className="row dialog-view-edit-row-project-data">
						<div className="col s12 m4">
							<h5>Commercial</h5>
							{createItem('Status', item, 'probability_status')}
							{createItem('Probability', item, 'probability_percent', '%')}
							{createItem('Year Moved to Current', item, 'year_moved_to_current')}
							{createItem('Current Material', item, 'current_material')}
							{createItem('Current Material Price ($/lb)', item, 'current_material_cost_per_lb')}
							{createItem('Target Material', item, 'mcpp_material')}
							{createItem('Target Material Price ($/lb)', item, '')}
						</div>
						<div className="col s12 m4">
							<h5>Production</h5>
							{createItem('Part Weight', item, 'part_weight_lbs')}
							{createItem('MCPP Plant', item, 'mcpp_plant')}
							{createItem('Tier 1', item, 'tier_1')}
							{createItem('Tier 2', item, 'tier_2')}
							{createItem('Molder', item, 'molder')}
						</div>
						<div className="col s12 m4">
							<h5>MCPP Team</h5>
							{createItem('Sales Manager', item, 'sales_manager')}
							{createItem('ADE', item, 'ade')}
							{createItem('Tech Service ', item, 'tech_service')}
							<h5>Specifications</h5>
							{createItem('OEM Material Spec', item, 'oem_material_spec')}
							{createItem('OEM Performance Spec', item, 'oem_performance_spec')}
						</div>
					</div>*/}
					{/* details row 2
					<div className="row dialog-view-edit-row-table-data">
						<div className="col s12">
							<Table className="prj-details-table" style={{width:'100%'}} selectable={false}>
								<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
									<TableRow>
										<TableHeaderColumn><h5>Calendar Year</h5></TableHeaderColumn>
										<TableHeaderColumn><h5>Vehicle Builds</h5></TableHeaderColumn>
										<TableHeaderColumn><h5>Part Volume</h5></TableHeaderColumn>
									</TableRow>
								</TableHeader>
								<TableBody displayRowCheckbox={false}>
									{
										this.cyTableData.map( (row, index) => (
											<TableRow key={ index }>
												<TableRowColumn>{ row.year }</TableRowColumn>
												<TableRowColumn>{ row.tot }</TableRowColumn>
												<TableRowColumn>{ `${row[uomAbbr]} (${uomAbbr})` }</TableRowColumn>
											</TableRow>
										))
									}
								</TableBody>
							</Table>
						</div>
					</div>*/}

				</div>



			{/* finish view details */}


			</div>
		);
	}
}
Details.propTypes = {};
Details.defaultProps = {};

function mapStateToProps({vehicles}){
	return { vehicles };
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({ }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);