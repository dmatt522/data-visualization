import React, {Component} 	from 'react'
import { connect } 			from 'react-redux'
import { Link } from 'react-router'
import IconButton from 'material-ui/IconButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
//import TitleHeader from '../../shared/components/TitleHeader'
import CollapsibleHeader from '../../shared/components/CollapsibleHeader'
import ComposableToolbar	from '../../shared/toolbars/ComposableToolbar'
import PageTitleBar	from '../../shared/toolbars/PageTitleBar'
import Breadcrumbs 			from '../../shared/components/Breadcrumbs'
import BaseComponent 	from '../../shared/components/BaseComponent'
import SimpleText 	from '../../shared/ui/SimpleText'
//import DetailsToolbar 	from './components/DetailsToolbar'
import TooltippedIconButton from '../../shared/ui/TooltippedIconButton'

import _ from 'lodash'

import { VEHICLES_DETAILS_PAGE_DESCRIPTION } from '../../config/strings'

import styles from '../../styles/styles'

import globals from '../../config/globals'
import { _valueAt, _cySeries } from '../../utils/functions'

/*	function _valueAt(obj, path, defaultValue = '-'){
		return _.get(obj, path, defaultValue);
	}

	function _foo(input){
		let output = [];
		_.forEach( input, function(value, key){
			if(_.startsWith(key, 'cy_'))
				output = _.concat(output, {key: key, value: value});
		});
		return output;//_.sortBy(output, ['key']);
	}
*/

class VehicleDetails extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}

	componentDidMount() {
		$('.collapsible').collapsible({accordion : false });
	}
	componentDidUpdate(prevProps, prevState) {
		$('.collapsible').collapsible({accordion : false });
	}
	render(){

		const item = this.props.vehicles.selectedItem;
		const years = _cySeries(item);

		return(
			<ReactCSSTransitionGroup
				transitionName="testTransition"
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false} >
				<div className="comp-vehicledetails page vehiclesdetails-page">

					<Breadcrumbs />

					{/*<PageTitleBar title="Vehicle Details" />*/}

					{/*<SimpleText text={ VEHICLES_DETAILS_PAGE_DESCRIPTION } />*/}

					<PageTitleBar title="Vehicle Details">
						<Link to={`${globals.basePath}/projects/add/${_valueAt(item, 'vehicle_id')}`}>
							<IconButton text="Add Project"><ContentAdd /></IconButton>
						</Link>
						<Link to={`${globals.basePath}/vehicles/edit/${_valueAt(item, 'vehicle_id')}`}>
							<IconButton><EditorModeEdit /></IconButton>
						</Link>
					</PageTitleBar>

					{/*<DetailsToolbar styles={styles} id={ _valueAt(item, 'Project_ID') } />*/}
					<div className="row" style={{marginBottom:0}}>
						<div className="col s12">
							<h5 className="details-title">{ _valueAt(item, 'manufacturer') } { _valueAt(item, 'production_brand') }</h5>
						</div>
					</div>
					<div className="row comp-vehicledetails">

						<div className="col s12 m4">
							<img src={`${globals.basePath}/images/car_placeholder.jpg`} className="responsive-img" />
						</div>


						<div className="col s12 m8">
							<ul className="collapsible" data-collapsible="expandable">
							    <li>
							        <div className="collapsible-header active">
							            <CollapsibleHeader title="Info" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
							                <div className="col s12 m6"><span className="as-label">Manufacturer</span> { _valueAt(item, 'manufacturer') }</div>
							                <div className="col s12  m6"><span>Platform</span> { _valueAt(item, 'platform') }</div>
							                <div className="col s12  m6"><span>Start of Production</span> { _valueAt(item, 'sop') }</div>
							                <div className="col s12 m6"><span>End of Production</span> { _valueAt(item, 'eop') }</div>
							                <div className="col s12 m6"><span>Production Plant</span> { _valueAt(item, 'production_plant') }</div>
							                <div className="col s12 m6"><span>Location</span> { _valueAt(item, 'location') }</div>
							            </div>
							        </div>
							    </li>
							    <li>
							        <div className="collapsible-header active">
							            <CollapsibleHeader title="Build" />
							        </div>
							        <div className="collapsible-body">
							            <div className="row">
											{
												years.map( (item, i) => <div className="col s12 m6" key={ i }><span>{ item.label }</span> - <span>{ item.value }</span></div> )
											}
							            </div>
							        </div>
							    </li>
							</ul>

						</div>
					</div>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
}
VehicleDetails.propTypes = {};
VehicleDetails.defaultProps = {};

function mapStateToProps({ vehicles }){
	return { vehicles: vehicles };
}

export default connect(mapStateToProps)(VehicleDetails);