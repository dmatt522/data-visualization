export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_ROLES = 'FETCH_ROLES';
export const FETCH_SCOPES = 'FETCH_SCOPES';