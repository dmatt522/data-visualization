import _ from 'lodash'
import moment from 'moment'


/**
 * ------------------------------------------------------------------------
 * ------------------------------------------------------------------------
 * 	STATE MODIFIERS
 * ------------------------------------------------------------------------
 * ------------------------------------------------------------------------
 */

/**
 * sets state after fetching data
 * @param  {[type]} state [description]
 * @param  {[type]} data  [description]
 * @return {[type]}       [description]
 */
export function init(state, data){
	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		chunkedFilteredList: chunks,
		/*filters: buildFiltersList(data),*/
		pageData: _.nth(chunks, state.activePage - 1)
	};
}

/**
 * buffer of filters
 * @param  {[type]} state  [description]
 * @param  {[type]} filter [description]
 * @return {[type]}        [description]
 */
export function bufferFilter(state, filter){
	// first remove the old name:value pair if it exists in the array
	_.remove(state.filtersBuffer, (item) => { return item.name == filter.name });
	return {
		filtersBuffer: (filter.value == 'any') ? state.filtersBuffer : _.concat(state.filtersBuffer, [filter])
	}
}


/**
 * [applyFilters description]
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
export function applyFilters(state){
	// start from the list of results
	let output = _.clone(state.list);
	// for each filter in the buffer ...
	_.forEach(state.filtersBuffer, function(filter){
		// if value is any do not apply that particular filter
		if(filter.value == 'any')
			return;
		// create the key for the search
		let value = _.set({}, filter.name, filter.value);
		// filter the current filtered output
		output = _.filter(output, value);
	});
	// prevent 0 data passing the full list
	//if(_.size(output) == 0)
	//	output = _.clone(state.list);
	// create the chunked version for pagination
	const chunked = _.chunk(output, state.itemsPerPage);
	// return the overriding state object
	return {
		filteredList: output,
		chunkedFilteredList: chunked,
		pageData: _.nth(chunked, 0),
		activePage: 1
	}
}


/**
 * clear the buffer of filters
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
export function clearFilters(state){
	const chunkedFilteredList = _.chunk(state.list, state.itemsPerPage)
	return {
		filtersBuffer: [],
		filteredList: state.list,
		chunkedFilteredList: chunkedFilteredList,
		pageData: _.nth(chunkedFilteredList, 0),
		activePage: 1
	}
}

/**
 * the item selected by click on a row in the current page data displayed
 * @param  {object} state        the state
 * @param  {array of integers} selectedRows [description]
 * @return {[type]}              [description]
 */
export function selectItem(state, selectedRows){
		// original
		//selectedItem: (!_.isEmpty(selectedRows)) ? _.nth(state.pageData, _.head(selectedRows)) : state.selectedItem
	return {
		selectedMultiItem: [],
		selectedItem: (!_.isEmpty(selectedRows)) ? _.nth(state.pageData, _.head(selectedRows)) : state.selectedItem
	};
}

export function selectMultiItem(state, selectedRows){
	let list = [];
	_.forEach(selectedRows, function(row){
		list.push(_.nth(state.pageData, row));
	});
	console.log('--->', list);
	return {
		selectedItem: {},
		selectedMultiItem: list
	}
}

export function pageChange(state, n){
	return {
		activePage: n,
		pageData: _.nth(state.chunkedFilteredList, n - 1)
	};
}


/**
 * [sortList description]
 * @param  {[type]} state   [description]
 * @param  {[type]} sortKey [description]
 * @return {[type]}         [description]
 */
export function sortList(state, sortKey){
	let output = _.clone(state.filteredList);
	//const sorter = _.concat([], sortKey);
	output = _.sortBy(output, _.concat([], sortKey));
	//console.log(output);
	if(state.isReversed)
		output = _.reverse(output);

	const chunked = _.chunk(output, state.itemsPerPage);
	//console.log(reversed);
	return {
		filteredList: output,
		chunkedFilteredList: chunked,
		pageData: _.nth(chunked, 0),
		activePage: 1,
		isReversed: !state.isReversed
	}
}



export function editSelectedItem(state, selectedItem, payload, key){
	const data = state.list.map(function(item){
		if(item[key] === selectedItem[key]){
			return Object.assign({}, selectedItem, payload);
		}
		return item;
	});

	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		chunkedFilteredList: chunks,
		selectedItem: Object.assign({}, selectedItem, payload),
		pageData: _.nth(chunks, state.activePage - 1)
	};
	//return { selectedItem: Object.assign({}, selectedItem, payload) };
}




// todo: implement remove selectedItem
/**
 * removes the selectedItem from state
 * @param  {[type]} state        [description]
 * @param  {[type]} selectedItem [description]
 * @return {[type]}              [description]
 */
export function deleteSelectedItem(state, selectedItem){
	console.log('remove ', selectedItem);

	_.remove(state.list, function(item){ return item === selectedItem; });
	const data = state.list;

	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		chunkedFilteredList: chunks,
		selectedItem: {},
		pageData: _.nth(chunks, state.activePage - 1)
	};
}

/**
 * ------------------------------------------------------------------------
 * ------------------------------------------------------------------------
 * 	UTILS
 * ------------------------------------------------------------------------
 * ------------------------------------------------------------------------
 */
/**
 * [buildOptions description]
 * @param  {[type]} array    [description]
 * @param  {[type]} propName [description]
 * @return {[type]}          [description]
 */
export function buildOptions(array, propName){
	return _.uniq(_.map(array, propName)).sort();
}


/**
 * create a moment in time
 * @return {[type]} [description]
 */
export function mms(){
	return moment().milliseconds();
}

/**
 * create random int
 * @return {[type]} [description]
 */
export function random(){
	const min = Math.ceil(1);
	const max = Math.floor(10000000000);
	return Math.floor(Math.random() * (max - min)) + min;
}



export function postResponseCodesHandler(state, data){
	//console.log('postResponseCodesHandler', data);
	if(_.isInteger(data) && data === 9){
		//console.log('integer');
		return Object.assign({}, state, {operationCode9:random()});
	}
	if(_.isInteger(data) && data === 0){
		return Object.assign({}, state, {operationCodeError:random()});
	}
	if(_.isInteger(data) && data === 1){
		return Object.assign({}, state, {operationCodeSuccess:random()});
	}
	if(_.isArray(data)){
		//console.log('array');
		return Object.assign({}, state, {operationCodeSuccess:random()});
	}
	return Object.assign({}, state);
}