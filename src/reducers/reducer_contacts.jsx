import {
	FETCH_CONTACTS,
	BUFFER_FILTER_CONTACTS,
	APPLY_FILTERS_CONTACTS,
	RESET_FILTER_CONTACTS,
	PAGINATION_CONTACTS_CHANGE,
	SELECT_CONTACT,
	SORT_CONTACTS,
	EDIT_CONTACT,
	ADD_CONTACT,
	DELETE_CONTACT
} from '../actions/types'

import { random } from './reducers_functions'
const defaultState = {
	list: [],
	filteredList: [],
	chunkedFilteredList: [],
	filters: [],
	filtersBuffer: [],
	activePage: 1,
	pageData: [],
	selectedItem: {},
	itemsPerPage: 10,
	isReversed: false,
	operationCodeSuccess:0,
	operationCode9:0
};

export default (state = defaultState, action = {}) => {
	switch(action.type){
		case FETCH_CONTACTS:
			return Object.assign({}, state, init(state, action.payload.data));
		case BUFFER_FILTER_CONTACTS:
			return Object.assign({}, state, bufferFilter(state, action.payload));
		case APPLY_FILTERS_CONTACTS:
			return Object.assign({}, state, applyFilters(state));
		case RESET_FILTER_CONTACTS:
			return Object.assign({}, state, clearFilters(state));
		case PAGINATION_CONTACTS_CHANGE:
			return Object.assign({}, state, pageChange(state, action.payload));
		case SELECT_CONTACT:
			return Object.assign({}, state, selectItem(state, action.payload));
		case SORT_CONTACTS:
			return Object.assign({}, state, sortList(state, action.payload));
		case EDIT_CONTACT:
			if(_.isArray(action.payload.data))
				return Object.assign({}, state, {operationCodeSuccess:random()});
			return Object.assign({}, state);
		case DELETE_CONTACT:
			return Object.assign({}, state);
		case ADD_CONTACT:
			//console.log('_.isArray(action.payload.data)',_.isArray(action.payload.data));
			if(_.isInteger(action.payload.data) && action.payload.data === 9)
				return Object.assign({}, state, {operationCode9:random()});
			if(_.isArray(action.payload.data))
				return Object.assign({}, state, {operationCodeSuccess:random()});
			return Object.assign({}, state);
		default:
			return state;
	}

}

/**
 * after-fetch-data state setup
 * @param  {[type]} state [description]
 * @param  {[type]} data  [description]
 * @return {[type]}       [description]
 */
function init(state, data){
	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		chunkedFilteredList: chunks,
		filters: buildFiltersList(data),
		pageData: _.nth(chunks, state.activePage - 1)
	};
}

/**
 * the item selected by click on a row in the current page data displayed
 * @param  {object} state        the state
 * @param  {array of integers} selectedRows [description]
 * @return {[type]}              [description]
 */
function selectItem(state, selectedRows){
	return {
		selectedItem: (!_.isEmpty(selectedRows)) ? _.nth(state.pageData, _.head(selectedRows)) : state.selectedItem
	};
}

function pageChange(state, n){
	return {
		activePage: n,
		pageData: _.nth(state.chunkedFilteredList, n - 1)
	};
}


/**
 * buffer of filters
 * @param  {[type]} state  [description]
 * @param  {[type]} filter [description]
 * @return {[type]}        [description]
 */
function bufferFilter(state, filter){
	// first remove the old name:value pair if it exists in the array
	_.remove(state.filtersBuffer, (item) => { return item.name == filter.name });
	return {
		filtersBuffer: (filter.value == 'any') ? state.filtersBuffer : _.concat(state.filtersBuffer, [filter])
	}
}

/**
 * clear the buffer of filters
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function clearFilters(state){
	const chunkedFilteredList = _.chunk(state.list, state.itemsPerPage)
	return {
		filters: buildFiltersList(state.list),
		filtersBuffer: [],
		filteredList: state.list,
		chunkedFilteredList: chunkedFilteredList,
		pageData: _.nth(chunkedFilteredList, 0),
		activePage: 1
	}
}


/**
 * [applyFilters description]
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function applyFilters(state){
	// start from the list of results
	let output = _.clone(state.list);
	// for each filter in the buffer ...
	_.forEach(state.filtersBuffer, function(filter){
		// if value is any do not apply that particular filter
		if(filter.value == 'any')
			return;
		// create the key for the search
		let value = _.set({}, filter.name, filter.value);
		// filter the current filtered output
		output = _.filter(output, value);
	});
	// prevent 0 data passing the full list
	//if(_.size(output) == 0)
	//	output = _.clone(state.list);
	// create the chunked version for pagination
	const chunked = _.chunk(output, state.itemsPerPage);
	// return the overriding state object
	return {
		filteredList: output,
		chunkedFilteredList: chunked,
		pageData: _.nth(chunked, 0),
		activePage: 1
	}
}


/**
 * [buildOptions description]
 * @param  {[type]} array    [description]
 * @param  {[type]} propName [description]
 * @return {[type]}          [description]
 */
function buildOptions(array, propName){
	return _.uniq(_.map(array, propName)).sort();
}

/**
 * [buildFiltersList description]
 * @param  {[type]} list [description]
 * @return {[type]}      [description]
 */
function buildFiltersList(list){
	// automatic creations
	// todo: ask Marco to create an api endpoint for filters
	// todo: for each prop in list[0] create an {label:'', options:[]}
	/*return _.forEach(list[0], (value, key) => {
			console.log(key, _.uniq( _.map( list, key ) ));
			return {
				label:key,
				options: _.uniq( _.map( list, key ) )
			}
		}
	);*/

	// manual creation
	return [
		{
			label:'First Name',
			name: 'first_name',
			options:buildOptions(list, 'first_name')
		},
		{
			label:'Last Name',
			name: 'last_name',
			options:buildOptions(list, 'last_name')
		},
		{	label:'Company Name',
			name: 'company_name',
			options:buildOptions(list, 'company_name')
		}
	]
}

/**
 * [sortList description]
 * @param  {[type]} state   [description]
 * @param  {[type]} sortKey [description]
 * @return {[type]}         [description]
 */
function sortList(state, sortKey){
	let output = _.clone(state.filteredList);
	//const sorter = _.concat([], sortKey);
	output = _.sortBy(output, _.concat([], sortKey));
	//console.log(output);
	if(state.isReversed)
		output = _.reverse(output);

	const chunked = _.chunk(output, state.itemsPerPage);
	//console.log(reversed);
	return {
		filteredList: output,
		chunkedFilteredList: chunked,
		pageData: _.nth(chunked, 0),
		activePage: 1,
		isReversed: !state.isReversed
	}
}