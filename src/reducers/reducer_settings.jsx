import {
	CHANGE_SETTINGS,
	FETCH_UOM,
	SIGN_IN }
	from '../actions/types'
import { US_UNIT } from '../config/values'

const defaultState = {
	uomList: [
		{ name: 'US', value: '1', abbr: 'lbs' },
		{ name: 'Metric', value: '2', abbr: 'kg' }
	],
	selectedUOM: {
		name: 'US', value: '1', abbr: 'lbs'
	},
	unselectedUOM:{
		name: 'Metric', value: '2', abbr: 'kg'
	},
	init:function(value){
		//console.log('value', value);
		initialize(value);
		//console.log('this.selectedUOM', this.selectedUOM);
	}
};

export default function(state = defaultState, action = {}){
	switch(action.type){
		//case FETCH_UOM:
		//	return Object.assign( {}, state/*, { uomList: action.payload.data }*/ );
		case CHANGE_SETTINGS:
			return Object.assign({}, state, changeUOM(state, action.payload));
		default:
			return state;
	}
}

function initialize(value){
	//console.log('initialize settings ... ', value);
	const list = _.clone(defaultState.uomList);
	defaultState.selectedUOM 	= _.head(_.filter(list, function(o){ return o.value == value }));
	defaultState.unselectedUOM 	= _.head(_.filter(list, function(o){ return o.value != value }));
}

function changeUOM(state, payload){
	//console.log('@', payload.uom);
	const list = _.clone(state.uomList);
	const selected = _.head(_.filter(list, function(o){ return o.value == payload.uom }));
	const unselected = _.head(_.filter(list, function(o){ return o.value != payload.uom; }));
	//console.log('@selected', selected.name);
	//console.log('@unselected', unselected.name);
	return {
		selectedUOM:  selected,
		unselectedUOM: unselected
	};
}