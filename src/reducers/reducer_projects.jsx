import _ from 'lodash'
import moment from 'moment'

import {
	init,
	bufferFilter,
	applyFilters,
	sortList,
	selectItem,
	selectMultiItem,
	clearFilters,
	pageChange,
	buildOptions,
	deleteSelectedItem,
	editSelectedItem,
	reset,
	random,
	postResponseCodesHandler
} from './reducers_functions'

// action types
import {
	FETCH_PROJECTS,
	FETCH_PROJECT_BY_ID,
	BUFFER_FILTERS_PROJECTS,
	APPLY_FILTERS_PROJECTS,
	RESET_FILTER_PROJECTS,
	PAGINATION_PROJECTS_CHANGE,
	SELECT_PROJECT,
	SELECT_MULTI_PROJECT,
	SORT_PROJECTS,
	SUBMIT_PROJECT,
	EDIT_PROJECT,
	EDIT_PROJECT_LOCAL,
	DELETE_PROJECT,
	RESET_PROJECTS_STATE,
	FETCH_PROJECTS_CHART_1,
	FETCH_CONTACTS_TO_PROJECT,
	ADD_CONTACT_TO_PROJECT,
	REMOVE_CONTACT_FROM_PROJECT,
	FETCH_PROJECT_HISTORY,
	FETCH_PROJECT_ACTION_PLANS,
	DELETE_PROJECT_ACTIONS_PLANS,
	ADD_PROJECT_ACTIONS_PLANS,
	SELECT_ACTION_PLAN
} from '../actions/types'

// default/init state
const defaultState = {
	list: [],
	filteredList: [],
	chunkedFilteredList: [],
	filters: [],
	filtersByVehicle:[],
	filtersByProject:[],
	hiddenFilters: [],
	filtersBuffer: [],
	activePage: 1,
	pageData: [],
	selectedItem: {},
	selectedMultiItem: [],
	selectedItemChart1: {},
	itemsPerPage: 10,
	isReversed: false,
	selectedItemContacts:[],
	selectedItemHistory:[],
	selectedItemActionPlans:[],
	selectedProjectActionPlan:{},
	operationCodeSuccess:0,
	operationCodeError:0,
	operationCode9:0/*already exists*/
};

export default function (state = defaultState, action = {}){
	switch(action.type){
		case FETCH_PROJECTS:
			return Object.assign({}, state, init(state, action.payload.data), {
					filters: buildFiltersList(action.payload.data),
					hiddenFilters:buildHiddenFiltersList(action.payload.data) ,
					filtersByVehicle:filtersByVehicle(action.payload.data),
					filtersByProject:filtersByProject(action.payload.data),
					operationCode9:0
				});
		case BUFFER_FILTERS_PROJECTS:
			return Object.assign({}, state, bufferFilter(state, action.payload));
		case APPLY_FILTERS_PROJECTS:
			return Object.assign({}, state, applyFilters(state));
		case RESET_FILTER_PROJECTS:
			return Object.assign({}, state, clearFilters(state), {filters: buildFiltersList(state.list)} );
		case PAGINATION_PROJECTS_CHANGE:
			return Object.assign({}, state, pageChange(state, action.payload));
		case SELECT_PROJECT:
			console.log('SELECT_PROJECT', action.payload);
			return Object.assign({}, state, selectItem(state, action.payload));
		case SELECT_MULTI_PROJECT:
			return Object.assign({}, state, selectMultiItem(state, action.payload));
		case SORT_PROJECTS:
			return Object.assign({}, state, sortList(state, action.payload));
		case SUBMIT_PROJECT:
			console.log('prj submitted payload', action);
			return postResponseCodesHandler(state, action.payload.data);
			return state;
		case EDIT_PROJECT:
			return postResponseCodesHandler(state, action.payload.data);
			return state;
		case EDIT_PROJECT_LOCAL:
			return Object.assign({}, state, editSelectedItem(state, action.selectedItem, action.payload, 'project_id'));
		case DELETE_PROJECT:
			return Object.assign({}, state, deleteSelectedItem(state, action.selectedItem));
		case FETCH_PROJECTS_CHART_1:
			return Object.assign({}, state, {selectedItemChart1: action.payload.data});
		case FETCH_CONTACTS_TO_PROJECT:
			return Object.assign({}, state, {selectedItemContacts: action.payload.data});
		case ADD_CONTACT_TO_PROJECT:
			if(_.isArray(action.payload.data))
				return Object.assign({}, state, {selectedItemContacts: action.payload.data});
			if(action.payload.data === 9){
				console.log('----------->', action.payload.data);
				return Object.assign({}, state, {operationCode9:random()});
			}
			return state;
		case REMOVE_CONTACT_FROM_PROJECT:
			return Object.assign({}, state, {selectedItemContacts: action.payload.data});
		case FETCH_PROJECT_HISTORY:
			return Object.assign({}, state, {selectedItemHistory: action.payload.data});
		case FETCH_PROJECT_ACTION_PLANS:
			return Object.assign({}, state, {selectedItemActionPlans: action.payload.data});
		case DELETE_PROJECT_ACTIONS_PLANS:
			return Object.assign({}, state, postResponseCodesHandler(state, action.payload.data));
		case ADD_PROJECT_ACTIONS_PLANS:
			return Object.assign({}, state, postResponseCodesHandler(state, action.payload.data));
		case SELECT_ACTION_PLAN:
			console.log('selectedProjectActionPlan:',action.payload);
			return Object.assign({}, state, {selectedProjectActionPlan:action.payload});
		/*case RESET_PROJECTS_STATE:
			return Object.assign({}, state, clearFilters(state));*/
		default:
			return state;
	}
}

/*
function onAddedProject(state, action){
	let promise = action.payload;
	//let output = {};
	promise.then(
		(data) => {
			console.log('success', data.data);
			state[list]: data;
			state[filteredList]: data;
			state[chunkedFilteredList]: chunks;
			state[pageData]: _.nth(chunks, state.activePage - 1);
			console.log('@', state[action.data.scope]);
		},
		(err) => console.log('rejected', err)
	);
}
*/

// this reducer utils

function buildHiddenFiltersList(list){
	return [

		{
			label:'probability_percent',
			name: 'probability_percent',
			options:buildOptions(list, 'probability_percent')
		}

	];
}
function filtersByVehicle(list){
	return [
		{
			label:'Manufacturer',
			name: 'manufacturer',
			options:buildOptions(list, 'manufacturer')
		},
		{
			label:'Program',
			name: 'program',
			options:buildOptions(list, 'program')
		},
		{
			label:'Brand',
			name: 'brand',
			options:buildOptions(list, 'brand')
		},
		{
			label:'Platform',
			name: 'platform',
			options:buildOptions(list, 'platform')
		},
		{
			label:'Nameplate',
			name: 'nameplate',
			options:buildOptions(list, 'nameplate')
		},
	];
}
function filtersByProject(list){
	return [
		{
			label:'Part Type',
			name: 'part_type',
			options:buildOptions(list, 'part_type')
		},
		{
			label:'Status',
			name: 'probability_status',
			options:buildOptions(list, 'probability_status')
		},

		{
			label:'Current Material',
			name: 'current_material',
			options:buildOptions(list, 'current_material')
		},
		{
			label:'Target Material',
			name: 'mcpp_material',
			options:buildOptions(list, 'mcpp_material')
		},

		{
			label:'MCPP Plant',
			name: 'mcpp_plant',
			options:buildOptions(list, 'mcpp_plant')
		},
		{
			label:'Tier 1',
			name: 'tier_1',
			options:buildOptions(list, 'tier_1')
		},
		{
			label:'Tier 2',
			name: 'tier_2',
			options:buildOptions(list, 'tier_2')
		},
		{
			label:'Molder',
			name: 'molder',
			options:buildOptions(list, 'molder')
		},
		{
			label:'SOP',
			name: 'sop',
			options:buildOptions(list, 'sop')
		},
		{
			label:'EOP',
			name: 'eop',
			options:buildOptions(list, 'eop')
		},

		{
			label:'Year to Current',
			name: 'year_moved_to_current',
			options:buildOptions(list, 'year_moved_to_current')
		},
		{
			label:'Next Facelift',
			name: 'next_facelift',
			options:buildOptions(list, 'next_facelift')
		},
		{
			label:'Segment',
			name: 'segment',
			options:buildOptions(list, 'segment')
		},
		{
			label:'Sub Segment',
			name: 'subsegment',
			options:buildOptions(list, 'subsegment')
		},

		{
			label:'Performance Spec',
			name: 'oem_performance_spec',
			options:buildOptions(list, 'oem_performance_spec')
		},
		{
			label:'Material Spec',
			name: 'oem_material_spec',
			options:buildOptions(list, 'oem_material_spec')
		},
		{
			label:'Sales Manager',
			name: 'sales_manager',
			options:buildOptions(list, 'sales_manager')
		},
		{
			label:'ADE',
			name: 'ade',
			options:buildOptions(list, 'ade')
		},
		{
			label:'Tech Service',
			name: 'tech_service',
			options:buildOptions(list, 'tech_service')
		}
	];
}
function buildFiltersList(list){
	return [
		{
			label:'Manufacturer',
			name: 'manufacturer',
			options:buildOptions(list, 'manufacturer')
		},
		{
			label:'Part Type',
			name: 'part_type',
			options:buildOptions(list, 'part_type')
		},
		{
			label:'Program',
			name: 'program',
			options:buildOptions(list, 'program')
		},
		{
			label:'Brand',
			name: 'brand',
			options:buildOptions(list, 'brand')
		},
		{
			label:'Platform',
			name: 'platform',
			options:buildOptions(list, 'platform')
		},
		{
			label:'Nameplate',
			name: 'nameplate',
			options:buildOptions(list, 'nameplate')
		},
		{
			label:'Status',
			name: 'probability_status',
			options:buildOptions(list, 'probability_status')
		},

		{
			label:'Current Material',
			name: 'current_material',
			options:buildOptions(list, 'current_material')
		},
		{
			label:'Target Material',
			name: 'mcpp_material',
			options:buildOptions(list, 'mcpp_material')
		},

		{
			label:'MCPP Plant',
			name: 'mcpp_plant',
			options:buildOptions(list, 'mcpp_plant')
		},
		{
			label:'Tier 1',
			name: 'tier_1',
			options:buildOptions(list, 'tier_1')
		},
		{
			label:'Tier 2',
			name: 'tier_2',
			options:buildOptions(list, 'tier_2')
		},
		{
			label:'Molder',
			name: 'molder',
			options:buildOptions(list, 'molder')
		},
		{
			label:'SOP',
			name: 'sop',
			options:buildOptions(list, 'sop')
		},
		{
			label:'EOP',
			name: 'eop',
			options:buildOptions(list, 'eop')
		},

		{
			label:'Year to Current',
			name: 'year_moved_to_current',
			options:buildOptions(list, 'year_moved_to_current')
		},
		{
			label:'Next Facelift',
			name: 'next_facelift',
			options:buildOptions(list, 'next_facelift')
		},
		{
			label:'Segment',
			name: 'segment',
			options:buildOptions(list, 'segment')
		},
		{
			label:'Sub Segment',
			name: 'subsegment',
			options:buildOptions(list, 'subsegment')
		},

		{
			label:'Performance Spec',
			name: 'oem_performance_spec',
			options:buildOptions(list, 'oem_performance_spec')
		},
		{
			label:'Material Spec',
			name: 'oem_material_spec',
			options:buildOptions(list, 'oem_material_spec')
		},
		{
			label:'Sales Manager',
			name: 'sales_manager',
			options:buildOptions(list, 'sales_manager')
		},
		{
			label:'ADE',
			name: 'ade',
			options:buildOptions(list, 'ade')
		},
		{
			label:'Tech Service',
			name: 'tech_service',
			options:buildOptions(list, 'tech_service')
		}
	]
}