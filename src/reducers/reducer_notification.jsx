import { NOTIFY } from '../actions/types'
// level: '', 'success', 'warn', 'error'
const defaultState = {
	message:'welcome user! :)',
	level:''
};

/**
  action.payload = defaultState = {
 	message: 'some text to display'
  }
 */

export default (state = defaultState, action = {}) => {
	switch(action.type){
		case NOTIFY:
			//console.log('reducer notification NOTIFY', action.payload);
			return Object.assign({}, state, action.payload);
		default:
			//console.log('reducer notification DEFAULT');
			return state;
	}
};