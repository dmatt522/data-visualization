import actions from '../config/action_types'

const defaultState = {
	result: false
};

export default function(state = defaultState, action = {}){
	switch(action.type){
		case actions.RESET_PASSWORD:
			console.log('Recived action', action);
			return action.payload.data;
		default:
			return state;
	}
	return state;
}