import _ from 'lodash'
import {LOGIN_TYPE} from '../actions/types'

const defaultState = {
	authType:'1'
};

export default function (state=defaultState, action={}){
	switch(action.type){
		case LOGIN_TYPE:
			console.log('authType:', action.payload.data);
			return Object.assign({}, state, {authType: action.payload.data});
		case AUTH_1:
			console.log('auth_1', action.payload.data);
			return state;
		case AUTH_2:
			return state;
		default:
			return state;
	}
	return state;
}