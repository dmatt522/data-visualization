import _ from 'lodash'
import {
	FETCH_LOOKUP_MATERIAL,
	FETCH_LOOKUP_STATUS,
	FETCH_LOOKUP_PERCENT,
	FETCH_LOOKUP_YTC,
	FETCH_LOOKUP_PART_TYPE,
	FETCH_LOOKUP_MCCP_PLANT,
	FETCH_LOOKUP_TIER_1,
	FETCH_LOOKUP_TIER_2,
	FETCH_LOOKUP_MOLDER,
	FETCH_LOOKUP_SALES_MANAGER,
	FETCH_LOOKUP_ADE,
	FETCH_LOOKUP_TECH_SERVICE,
	FETCH_LOOKUP_OEM_MATERIAL,
	FETCH_LOOKUP_OEM_PERFORMANCE,
	POST_LOOKUP,
	FETCH_USERS_ROLES,
	FETCH_LOOKUP_ACTION_PLANS_PRIORITY,
	FETCH_LOOKUP_ACTION_PLANS_STATUS
	 } from '../actions/types'

const defaultState = {
	materials:[],
	current_material: [],
	mcpp_material: [],
	status:[],
	probability_percent:[],
	probability_status:[],
	year_to_current:[],
	year_moved_to_current: [],
	mccp_plant:[],
	mcpp_plant:[],
	part_type:[],
	tier1:[],
	tier2:[],
	tier_1: [],
	tier_2: [],
	molder:[],
	sales_manager:[],
	ade:[],
	tech_service:[],
	oem_material_spec:[],
	oem_performance_spec:[],
	users:[],
	action_plans_priority: [],
	action_plans_status: [],
	roles: [],
	role_id: [],
	definedById:['ade','assigned_to' , 'current_material', 'materials','mcpp_plant','mcpp_plant','molder','oem_material_spec','oem_performance_spec','part_type','plan_status','priority' , 'probability_status','probability_percent', 'role_id', 'sales_manager','submitted_by' ,'target_material','tech_service','tier_1','tier_2','year_moved_to_current']
};
/*
const defaultStateNew = {
	ade:[],
	current_material: [],
	mcpp_plant: [],
	molder:[],
	oem_material_spec:[],
	oem_performance_spec:[],
	part_type: [],
	probability_status: [],
	probability_percent: [],
	sales_manager:[],
	target_material: [],
	tech_service:[],
	tier_1: [],
	tier_2: [],
	tier1: [],
	tier2: [],
	year_moved_to_current: []
};
*/
export default function (state = defaultState, action = {}){
	switch(action.type){
		case FETCH_LOOKUP_MATERIAL:
			//console.log(action.type, action.payload.data);
			return Object.assign({}, state, {materials: sortByName(action.payload.data),current_material: sortByName(action.payload.data),mcpp_material: sortByName(action.payload.data)});
		case FETCH_LOOKUP_STATUS:
			//console.log(action.type, action.payload.data);
			return Object.assign({}, state, {status: sortByName(action.payload.data),probability_status: sortByName(action.payload.data)});
		case FETCH_LOOKUP_PERCENT:
			//console.log(action.type, action.payload.data);
			return Object.assign({}, state, {probability_percent: action.payload.data});
		case FETCH_LOOKUP_YTC:
			//console.log(action.type, action.payload.data);
			return Object.assign({}, state, {year_to_current: sortByName(action.payload.data),year_moved_to_current: sortByName(action.payload.data)});
		case FETCH_LOOKUP_PART_TYPE:
			//console.log(action.type, action.payload.data);
			return Object.assign({}, state, {part_type: sortByName(action.payload.data)});
		case FETCH_LOOKUP_MCCP_PLANT:
			//console.log(action.type, action.payload.data);
			return Object.assign({}, state, {mccp_plant: sortByName(action.payload.data),mcpp_plant: sortByName(action.payload.data)});
		case FETCH_LOOKUP_TIER_1:
			return Object.assign({}, state, {tier1: sortByName(action.payload.data),tier_1: sortByName(action.payload.data)});
		case FETCH_LOOKUP_TIER_2:
			return Object.assign({}, state, {tier2: sortByName(action.payload.data),tier_2: sortByName(action.payload.data)});
		case FETCH_LOOKUP_MOLDER:
			return Object.assign({}, state, {molder: sortByName(action.payload.data)});
		case FETCH_LOOKUP_SALES_MANAGER:
			return Object.assign({}, state, {users: sortByName(action.payload.data), sales_manager: sortByName(action.payload.data)});
		case FETCH_LOOKUP_ADE:
			return Object.assign({}, state, {users: sortByName(action.payload.data), ade: sortByName(action.payload.data)});
		case FETCH_LOOKUP_TECH_SERVICE:
			return Object.assign({}, state, {users: sortByName(action.payload.data), tech_service: sortByName(action.payload.data)});
		case FETCH_LOOKUP_OEM_MATERIAL:
			return Object.assign({}, state, {oem_material_spec: sortByName(action.payload.data)});
		case FETCH_LOOKUP_OEM_PERFORMANCE:
			return Object.assign({}, state, {oem_performance_spec: sortByName(action.payload.data)});
		case POST_LOOKUP:
			console.log('POST_LOOKUP', action);
			mapActionScopeStateProps(state, action);
			return state;
		case FETCH_LOOKUP_ACTION_PLANS_PRIORITY:
			return Object.assign({}, state, {action_plans_priority:action.payload.data});
		case FETCH_LOOKUP_ACTION_PLANS_STATUS:
			return Object.assign({}, state, {action_plans_status:action.payload.data});

		case FETCH_USERS_ROLES:
			console.log(action.payload.data);
			return Object.assign({}, state, {roles: action.payload.data, role_id: action.payload.data});
			//return Object.assign({}, state, mapActionScopeStateProps(state, action));
		default:
			return state;
	}
}

function sortByName(array){
	return _.sortBy(array, ['name']);
}

function mapActionScopeStateProps(state, action){

	//console.log('action',action);
	//console.log('action.payload', action.payload);
	//console.log('action.payload.PromiseValue.data', action.payload.data);

	let promise = action.payload;
	//let output = {};
	promise.then(
		(data) => {
			console.log('success ->', action.data.scope, data.data);
			if(action.data.scope === 'material'){
				// scope === material in project -> Details edit mode
				state['mcpp_material'] = data.data;
				state['current_material'] = data.data;
			}
			else if(action.data.scope === 'spec'){
				state['oem_performance_spec'] = data.data;
				state['oem_material_spec'] = data.data;
			}
			else{
				state[action.data.scope] = data.data;
			}
			//console.log('@', state[action.data.scope]);
		},
		(err) => console.log('rejected', err)
	);
	//console.log('output', output);
	//return output;

	/*const prop = foo(action.data.scope);
	let val = _.concat(state[prop], {id:`${Math.ceil(Math.random()*1000000)}`, name:action.data.value})
	val = _.sortBy(val, ['name']);
	return {
		[prop]: val
	};*/
}


function foo(prop){
	switch(prop){
		case 'material':
			return 'materials';
		case 'tier_1':
			return 'tier1';
		case 'tier_1':
			return 'tier2';
		default:
			return prop;
	}
}