import {CONTEXT_FOR_EDIT_DELETE_DETAILS} from '../actions/types'

const defaultState = {
	item:{}
};

export default function(state=defaultState, action={}){
	switch(action.type){
		case CONTEXT_FOR_EDIT_DELETE_DETAILS:
			console.log('CONTEXT_FOR_EDIT_DELETE_DETAILS', action.payload);
			return Object.assign({}, state, {item:action.payload});
		default:
			return state;
	}
}