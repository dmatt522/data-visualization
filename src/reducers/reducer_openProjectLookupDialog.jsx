import { OPEN_PROJECT_LOOKUP_DIALOG } from '../actions/types'

const defaultState = {
	title: 'title',
	open: false
};

export default function (state = defaultState, action = {}){
	switch(action.type){
		case OPEN_PROJECT_LOOKUP_DIALOG:
			return Object.assign({}, state, {title: action.payload.title, open:true});
		default:
			return state;
	}
}