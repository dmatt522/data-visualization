import { combineReducers } from 'redux'
/**
 * represents a message to display:
 * see also:
 * 		src/config/messages.js
 * 		src/shared/actions/index.js -> notify
 */
import Notification from './reducer_notification'
/**
 * represents the user state, it holds info such as
 * if is signed in, his personal info, ...
 * see also:
 * 		src/pages/sign/actions/index.js -> signIn
 */
import User from './reducer_user'
/**
 * when the user needs to reset the pwd he doesn't remember
 * is it correct to sotre in the app state this info ???????????????????
 * see also:
 * 		src/pages/sign/actions/index.js -> resetPasssword
 */
//import ResetPassword from './reducer_reset_password'
/* all vehicles, contacts  */
//import Auth from './reducer_auth'
import Vehicles from './reducer_vehicles'
import Reports from './reducer_reports'
import Contacts from './reducer_contacts'
import Users from './reducer_users'
import Files from './reducer_files'
import Projects from './reducer_projects'
import Lookups from './reducer_lookups'
import Settings from './reducer_settings'
import ProjectLookupDialog from './reducer_openProjectLookupDialog'
import TheThing from './reducer_thething'

//import VehiclesFilters from './reducer_vehicles_filters'
/**
 * redux-form reducer
 */
import { reducer as formReducer } from 'redux-form'


const rootReducer = combineReducers({
	contacts: 		Contacts,
	files: 			Files,
	form: 			formReducer,
	lookupDialog: 	ProjectLookupDialog,
	lookups: 		Lookups,
	notification: 	Notification,
	projects: 		Projects,
	settings: 		Settings,
	theThing: 		TheThing,
	user: 			User,
	users: 			Users,
	vehicles: 		Vehicles,
    reports:        Reports
});


export default rootReducer;