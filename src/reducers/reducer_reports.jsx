import  axios  from 'axios'
import {
    ALL_REPORT_FILTERS
} from '../config/api'
import {
	FETCH_REPORTS_VOLUMNS,
    FETCH_REPORTS_FILTERS,
	FETCH_REPORTS_REVENUES,
	FETCH_REPORTS_LIGHTWEIGHTS_,
	BUFFER_FILTERS_REPORTS,
	APPLY_FILTERS_REPORTS,
	RESET_FILTER_REPORTS
} from '../actions/types'

import _ 		from 'lodash'


// private var to keep track of the filters to apply to the complete list
let filtersBuffer = [];

const defaultState = {
	// filters to display on <select>
	filters:[],
	// array of single reports, it never changes after fetching data
	list: [],
	// array of single  after filtering
	filteredList: [],
	// buffer of filters to apply to obtain a filtered list
	filtersBuffer: [],
	chartType : "Bar"
};

export default (state = defaultState, action = {}) => {

	switch(action.type){
		case FETCH_REPORTS_VOLUMNS:
			return Object.assign({}, state, response(state, action.payload.data));

		case FETCH_REPORTS_FILTERS:
			return Object.assign({}, state, getFilter(state, action.payload.data));

		case BUFFER_FILTERS_REPORTS:
			return Object.assign({}, state, bufferFilter(state, action.payload));

		case APPLY_FILTERS_REPORTS:
			return Object.assign({}, state, applyFilters(state));

		case RESET_FILTER_REPORTS:
			return Object.assign({}, state, clearFilters(state));

		default:
			return state;
	}

}

function response(state, data) {
	return {
		list: data,
		filteredList: data,
        chartType : "Bar"
	};
}

function getFilter(state, data) {
    return {
        filters: data[0].elements,
    };
}

/**
 * [parseJsonData description]
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */
function parseJsonData(str) {
	//return _.attempt(JSON.parse.bind(null, str));
	return JSON.parse(str);
}

/**
 * buffer of filters
 * @param  {[type]} state  [description]
 * @param  {[type]} filter [description]
 * @return {[type]}        [description]
 */
function bufferFilter(state, filter) {
	// first remove the old name:value pair if it exists in the array
	_.remove(state.filtersBuffer, (item) => { return item.name == filter.name });
	return {
		filtersBuffer: (filter.value == 'any') ? state.filtersBuffer : _.concat(state.filtersBuffer, [filter])
	}
}

/**
 * clear the buffer of filters
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function clearFilters(state) {
	return {
		filters: buildFiltersList(state.list),
		filtersBuffer: [],
		filteredList: state.list
	}
}

/**
 * [applyFilters description]
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function applyFilters(state) {
	// start from the list of results
	let output = _.clone(state.list);
    let chart = _.clone(state.chartType);
	// for each filter in the buffer ...
	_.forEach(state.filtersBuffer, function(filter){
		// if value is any do not apply that particular filter
        console.log("filter value",filter.value);
		if(filter.value == 'any')
			return;
		if(filter.value == 'Bar') {
            chart = "Bar";
            return;
        }
        if(filter.value == 'Line') {
            chart = "Line";
            return;
        }
		// create the key for the search
		let value = _.set({}, filter.name, filter.value);
		// filter the current filtered output
		output = _.filter(output, value);
	});
	// return the overriding state object
	return {
		filteredList: output,
        chartType: chart
	}
}

/**
 * [buildOptions description]
 * @param  {[type]} array    [description]
 * @param  {[type]} propName [description]
 * @return {[type]}          [description]
 */
function buildOptions(array, propName) {
	return _.uniq(_.map(array, propName)).sort();
}

function buildYears(array) {
    return array.map(function(val) {
        var obj = new Object();
        obj.name = val;
        obj.value = val;
        return obj;
    });
}
