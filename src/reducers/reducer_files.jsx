import {
	FETCH_FILES,
	PAGINATION_FILES_CHANGE,
	SORT_FILES,
	APPLY_FILTERS_FILES,
	RESET_FILTER_FILES,
	BUFFER_FILTER_FILES,
	SELECT_FILE,
	ADD_FILE,
	DELETE_FILE
} from '../actions/types'

import {
	buildOptions,
	pageChange,
	sortList,
	selectItem,
	clearFilters,
	bufferFilter,
	applyFilters,
	deleteSelectedItem
	} from './reducers_functions'

const defaultState = {
	list: [],
	filteredList:[],
	chunkedFilteredList:[],
	filters:[],
	activePage: 1,
	pageData:[],
	selectedItem: {},
	filtersBuffer:[],
	// pagination info
	itemsPerPage: 8,
	orderFlag:true
};

export default (state = defaultState, action = {}) => {

	switch(action.type){
		case FETCH_FILES:
			return Object.assign({}, state, response(state, action.payload.data));
		case PAGINATION_FILES_CHANGE:
			return Object.assign({}, state, pageChange(state, action.payload));
		case SORT_FILES:
			return Object.assign({}, state, sortList(state, action.payload));
		case APPLY_FILTERS_FILES:
			return Object.assign({}, state, applyFilters(state));
		case RESET_FILTER_FILES:
			return Object.assign({}, state, clearFilters(state), {filters: buildFiltersList(state.list)});
		case BUFFER_FILTER_FILES:
			return Object.assign({}, state, bufferFilter(state, action.payload));
		case SELECT_FILE:
			return Object.assign({}, state, selectItem(state, action.payload));
		case DELETE_FILE:
			return Object.assign({}, state, deleteSelectedItem(state, action.selectedItem));
		case ADD_FILE:
			console.log('reducer_files ADD_FILE', action);
			return Object.assign({}, state);
		default:
			return state;
	}

}


function response(state, data){
	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		filters: buildFiltersList(data),
		chunkedFilteredList: chunks,
		pageData: _.nth(chunks, state.activePage - 1)
	};
}

function buildFiltersList(list){
	return [
		{
			label:'File type',
			name: 'type',
			options:buildOptions(list, 'type')
		}
	]
}