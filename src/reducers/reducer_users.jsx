import {
	FETCH_USERS,
	PAGINATION_USERS_CHANGE,
	SORT_USERS,
	APPLY_FILTERS_USERS,
	RESET_FILTER_USERS,
	BUFFER_FILTER_USERS,
	SELECT_USER,
	ADD_USER,
	EDIT_USER,
	DELETE_USER
} from '../actions/types'

import {
	buildOptions,
	pageChange,
	sortList,
	selectItem,
	clearFilters,
	bufferFilter,
	applyFilters,
	deleteSelectedItem,
	random,
	postResponseCodesHandler
	} from './reducers_functions'

const defaultState = {
	list: [],
	filteredList:[],
	chunkedFilteredList:[],
	filters:[],
	activePage: 1,
	pageData:[],
	selectedItem: {},
	filtersBuffer:[],
	// pagination info
	itemsPerPage: 8,
	orderFlag:true,
	operationCodeSuccess:0,
	operationCode9:0
};

export default (state = defaultState, action = {}) => {

	switch(action.type){
		case FETCH_USERS:
			return Object.assign({}, state, response(state, action.payload.data));
		case PAGINATION_USERS_CHANGE:
			return Object.assign({}, state, pageChange(state, action.payload));
		case SORT_USERS:
			return Object.assign({}, state, sortList(state, action.payload));
		case APPLY_FILTERS_USERS:
			return Object.assign({}, state, applyFilters(state));
		case RESET_FILTER_USERS:
			return Object.assign({}, state, clearFilters(state), {filters: buildFiltersList(state.list)});
		case BUFFER_FILTER_USERS:
			return Object.assign({}, state, bufferFilter(state, action.payload));
		case SELECT_USER:
			return Object.assign({}, state, selectItem(state, action.payload));
		case DELETE_USER:
			return Object.assign({}, state, deleteSelectedItem(state, action.selectedItem));
		case EDIT_USER:
			//console.log('teducer users EDIT_USER',action.payload.data);
			return postResponseCodesHandler(state, action.payload.data);
		case ADD_USER:
			//console.log('reducer_users ADD_USER', action);
			if(_.isInteger(action.payload.data) && action.payload.data === 9){
				return Object.assign({}, state, {operationCode9:random()});
			}
			if(_.isArray(action.payload.data)){
				return Object.assign({}, state, {operationCodeSuccess:random()});
			}
			return Object.assign({}, state);
		default:
			return state;
	}

}


function response(state, data){
	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		filters: buildFiltersList(data),
		chunkedFilteredList: chunks,
		pageData: _.nth(chunks, state.activePage - 1)
	};
}

function buildFiltersList(list){
	return [
		{
			label:'First Name',
			name: 'first_name',
			options:buildOptions(list, 'first_name')
		},
		{
			label:'Last Name',
			name: 'last_name',
			options:buildOptions(list, 'last_name')
		}
	]
}