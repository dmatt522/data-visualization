import {
	FETCH_VEHICLES,
	BUFFER_FILTERS_VEHICLES,
	APPLY_FILTERS_VEHICLES,
	RESET_FILTER_VEHICLES,
	PAGINATION_VEHICLES_CHANGE,
	SELECT_VEHICLE,
	SORT_VEHICLES
	} from '../actions/types'

import _ 		from 'lodash'


// private var to keep track of the filters to apply to the complete list
let filtersBuffer = [];

const defaultState = {
	// filters to display on <select>
	filters:[],
	// array of single vehicles, it never changes after fetching data
	list: [],
	// array of single vehicles after filtering
	filteredList: [],
	// buffer of filters to apply to obtain a filtered list
	filtersBuffer: [],
	// filtered list good for pagination pourposes
	chunkedFilteredList:[],
	// page to display
	activePage: 1,
	// data for the active page, from filteredList
	pageData: [],
	// vehicle selected, from table result row selection
	selectedItem: {},
	// pagination info
	itemsPerPage: 10,
	orderFlag:true
};

export default (state = defaultState, action = {}) => {

	switch(action.type){
		case FETCH_VEHICLES:
			return Object.assign({}, state, response(state, action.payload.data));

		case BUFFER_FILTERS_VEHICLES:
			return Object.assign({}, state, bufferFilter(state, action.payload));

		case APPLY_FILTERS_VEHICLES:
			return Object.assign({}, state, applyFilters(state));

		case RESET_FILTER_VEHICLES:
			return Object.assign({}, state, clearFilters(state));

		case PAGINATION_VEHICLES_CHANGE:
			return Object.assign({}, state, pageChange(state, action.payload));

		case SELECT_VEHICLE:
			return Object.assign({}, state, selectVehicle(state, action.payload));

		case SORT_VEHICLES:
			return Object.assign({}, state, sortList(state, action.payload));

		default:
			return state;
	}

}

function response(state, data){
	const chunks = _.chunk(data, state.itemsPerPage);
	return {
		list: data,
		filteredList: data,
		chunkedFilteredList: chunks,
		filters: buildFiltersList(data),
		pageData: _.nth(chunks, state.activePage - 1)
	};
}


/**
 * the item selected by click on a row in the current page data displayed
 * @param {[type]} state [description]
 * @param {array of integers} rows [description]
 */
function selectVehicle(state, rows){
	//console.log('selectVehicle', rows);
	return {
		selectedItem: (!_.isEmpty(rows)) ? _.nth(state.pageData, _.head(rows)) : state.selectedItem
	};

}

/**
 * [sortList description]
 * @param  {[type]} state   [description]
 * @param  {[type]} sortKey [description]
 * @return {[type]}         [description]
 */
function sortList(state, sortKey){
	let output = _.clone(state.filteredList);
	//const sorter = _.concat([], sortKey);
	output = _.sortBy(output, _.concat([], sortKey));
	//console.log(output);
	if(state.isReversed)
		output = _.reverse(output);

	const chunked = _.chunk(output, state.itemsPerPage);
	//console.log(reversed);
	return {
		filteredList: output,
		chunkedFilteredList: chunked,
		pageData: _.nth(chunked, 0),
		activePage: 1,
		isReversed: !state.isReversed
	}
}

/**
 * [parseJsonData description]
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */
function parseJsonData(str){
	//return _.attempt(JSON.parse.bind(null, str));
	return JSON.parse(str);
}

function pageChange(state, n){
	return {
		activePage: n,
		pageData: _.nth(state.chunkedFilteredList, n - 1)
	};
}

/**
 * buffer of filters
 * @param  {[type]} state  [description]
 * @param  {[type]} filter [description]
 * @return {[type]}        [description]
 */
function bufferFilter(state, filter){
	// first remove the old name:value pair if it exists in the array
	_.remove(state.filtersBuffer, (item) => { return item.name == filter.name });
	return {
		filtersBuffer: (filter.value == 'any') ? state.filtersBuffer : _.concat(state.filtersBuffer, [filter])
	}
}

/**
 * clear the buffer of filters
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function clearFilters(state){
	const chunkedFilteredList = _.chunk(state.list, state.itemsPerPage)
	return {
		filters: buildFiltersList(state.list),
		filtersBuffer: [],
		filteredList: state.list,
		chunkedFilteredList: chunkedFilteredList,
		pageData: _.nth(chunkedFilteredList, 0),
		activePage: 1
	}
}

/**
 * [applyFilters description]
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function applyFilters(state){
	// start from the list of results
	let output = _.clone(state.list);
	// for each filter in the buffer ...
	_.forEach(state.filtersBuffer, function(filter){
		// if value is any do not apply that particular filter
		if(filter.value == 'any')
			return;
		// create the key for the search
		let value = _.set({}, filter.name, filter.value);
		// filter the current filtered output
		output = _.filter(output, value);
	});
	// prevent 0 data passing the full list
	//if(_.size(output) == 0)
	//	output = _.clone(state.list);
	// create the chunked version for pagination
	const chunked = _.chunk(output, state.itemsPerPage);
	// return the overriding state object
	return {
		filteredList: output,
		chunkedFilteredList: chunked,
		pageData: _.nth(chunked, 0),
		activePage: 1
	}
}

/**
 * [buildOptions description]
 * @param  {[type]} array    [description]
 * @param  {[type]} propName [description]
 * @return {[type]}          [description]
 */
function buildOptions(array, propName){
	return _.uniq(_.map(array, propName)).sort();
}

/**
 * [buildFiltersList description]
 * @param  {[type]} list [description]
 * @return {[type]}      [description]
 */
function buildFiltersList(list){
	// automatic creations
	// todo: ask Marco to create an api endpoint for filters
	// todo: for each prop in list[0] create an {label:'', options:[]}
	/*return _.forEach(list[0], (value, key) => {
			console.log(key, _.uniq( _.map( list, key ) ));
			return {
				label:key,
				options: _.uniq( _.map( list, key ) )
			}
		}
	);*/

	// manual creation
	return [
		{
			label:'Manufacturer',
			name: 'manufacturer',
			options:buildOptions(list, 'manufacturer')
		},
		{
			label:'Program',
			name: 'program',
			options:buildOptions(list, 'program')
		},
		{	label:'Brand',
			name: 'production_brand',
			options:buildOptions(list, 'production_brand')
		},
		{
			label:'Platform',
			name: 'platform',
			options:buildOptions(list, 'platform')
		},
		{
			label:'Nameplate',
			name: 'production_nameplate',
			options:buildOptions(list, 'production_nameplate')
		},
		{
			label:'SOP',
			name: 'sop',
			options:buildOptions(list, 'sop')
		},
		{
			label:'EOP',
			name: 'eop',
			options:buildOptions(list, 'eop')
		},
		{
			label:'Segment',
			name: 'regional_sales_segment',
			options:buildOptions(list, 'regional_sales_segment')
		},
		{
			label:'Subsegment',
			name: 'global_sales_subsegment',
			options:buildOptions(list, 'global_sales_subsegment')
		},
		{
			label:'Country',
			name: 'country',
			options:buildOptions(list, 'country')
		}
	]
}
