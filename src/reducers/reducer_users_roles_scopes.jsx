import { FETCH_USERS, FETCH_ROLES, FETCH_SCOPES } from '../actions/net'

const defaultState = {
	users:[],
	roles:[],
	scopes:[],
};

export default function (state = defaultState, action = {}){
	switch(action.type){
		case FETCH_USERS:
			console.log('users here');
			return Object.assign({}, state, {users: action.payload.data});
		case FETCH_ROLES:
			return Object.assign({}, state, {roles: action.payload.data});
		case FETCH_SCOPES:
			return Object.assign({}, state, {scopes: action.payload.data});
		default:
			return state;
	}
}