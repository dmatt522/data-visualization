import _ from 'lodash'

import {
	SIGN_IN,
	SIGN_OUT,
	FORGOT_PASSWORD,
	LOGIN_TYPE,
	AUTH_1,
	AUTH_2
	} from '../actions/types'

// todo: model defaultState on the data that will arrive from api endpoint and corrisponds to action.payload.data
//userInfo -> "User_ID": "1", "Employee_Name": "Chris Myers", "Email": "chris@pixantia.com", "Status": "1", "Employee": "0", "Manager": "-1"
const defaultState = {
	authType:'0',
	logged: false,
	reset: 0,
	userInfo:{},
	authInfo:{},
	authStep1Complete: false,
	authStep2Complete: false
};
// state argument is not application state, only the state this reducer is responsible for
export default function(state = defaultState, action = {}){
	switch(action.type){
		case SIGN_IN:
			return Object.assign({}, state, setUser(state, action));
		case SIGN_OUT:
			return Object.assign({}, state, {logged: false, userInfo: {}, reset: 0, authInfo:{}, authStep1Complete:false, authStep2Complete:false});
		case FORGOT_PASSWORD:
			return Object.assign({}, state, recoverPwd(state, action.payload));
		case LOGIN_TYPE:
			console.log('authType:', action.payload.data);
			return Object.assign({}, state, {authType: action.payload.data});
		case AUTH_1:
			console.log('auth_1', action.payload.data);
			if(_.isArray(action.payload.data))
				return Object.assign({}, state, {authInfo: _.head(action.payload.data), authStep1Complete: true});
			if(_.isInteger(action.payload.data) && action.payload.data === 0)
				return Object.assign({}, state, {authStep1Complete: false});
			return state;
		case AUTH_2:
			console.log('auth_2', action.payload.data);
			if(_.isArray(action.payload.data))
				return Object.assign({}, state, setUser(state, action), {authStep2Complete: true});
			if(_.isInteger(action.payload.data) && action.payload.data === 0)
				return Object.assign({}, state, {authStep2Complete: false});
			return state;
		default:
			return state;
	}
}

function setUser(state,  action){
	//
	// START RROR MODE
	//
	/*return {
		logged: true,
		userInfo: {
			"user_id":"1000","employee_name":"","first_name":"Giuseppe","last_name":"Leone","email":"gpleone@gmail.com","status":"1","role_id":"1","uom":"2",
			"scope":[{"id":"1","name":"Dashboard","target":"dashboard"},
				{"id":"2","name":"Projects","target":"projects"},
				{"id":"3","name":"Vehicles","target":"vehicles"},
				{"id":"4","name":"Reports","target":"reports"},
				{"id":"5","name":"Contacts","target":"contacts"},
				{"id":"11","name":"Pricing","target":"pricing"},
				{"id":"12","name":"Goals & Objectives","target":"goal_and_objectives"},
				{"id":"13","name":"Users","target":"users"},
				{"id":"14","name":"Files","target":"files"}]}
	}*/
	//
	// END ERROR MODE
	//



	const data =  action.payload.data;
	// start delete when using real api
	//return { logged: true, userInfo: _.head(data) };
	// end delete
	if(_.isEmpty(data)){
		//console.log('data empty');
		return { logged: false, userInfo: {} };
	}
	if(_.isArray(data) && !_.isUndefined(_.head(data).employee_name)){
		// warning: cross reducer call, update settings state
		_.get(action.getState(), 'settings').init(_.head(data).uom);
		return { logged: true, userInfo: _.head(data) };
	}
	else{
		//console.log('data not an array');
		return { logged: false, userInfo: {} };

	}
	return { logged: false, userInfo: {} };
}

function recoverPwd(state, payload){
	// start delete when using real api
	return { reset: (state.reset + 1) };
	// end delete
}