import actions from '../config/action_types'

const defaultState = [];

export default (state = defaultState, action = {}) => {
	switch(action.type){
		case actions.FILTER_VEHICLES:
			return state;
		default:
			return state
	}
}