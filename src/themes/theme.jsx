import {
  cyan500, cyan700,
  pinkA200,
  grey100, grey300, grey400, grey500, grey900,
  redA700,
  white, darkBlack, fullBlack,
} 				from 'material-ui/styles/colors';
import {fade} 	from 'material-ui/utils/colorManipulator';
import spacing 	from 'material-ui/styles/spacing';

export default {
	spacing: spacing,
	fontFamily: 'Roboto, sans-serif',
	palette: {
	    primary1Color: redA700, /*cyan500*/
	    primary2Color: redA700, /*cyan700*/
	    primary3Color: grey400,
	    accent1Color: redA700, /*pinkA200*/
	    accent2Color: grey100,
	    accent3Color: grey500,
	    textColor: darkBlack,
	    secondaryTextColor: darkBlack,/*fade(darkBlack, 0.54)*/
	    alternateTextColor: white,
	    canvasColor: white,
	    borderColor: grey300,
	    disabledColor: fade(darkBlack, 0.3),
	    pickerHeaderColor: redA700, /*cyan500*/
	    clockCircleColor: fade(darkBlack, 0.07),
	    shadowColor: fullBlack,
	},
	tooltip:{
		color:'#fff',
		rippleBackgroundColor:'#000'
	}
};