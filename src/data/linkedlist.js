export function LinkedList(head = null, tail = null){
	this.head = head;
	this.tail = tail;
}

export function Node(value, next, prev){
	this.value = value;
	this.next = next;
	this.prev = prev;
}