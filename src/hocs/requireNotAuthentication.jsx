import React, { Component } from 'react'
import { connect } from 'react-redux'

import globals from '../config/globals'

export default function(ComposedComponent){
	class RequireNotAuthentication extends Component{

		componentWillMount() {
			if(this.props.user.logged){
				this.context.router.push(`${globals.basePath}/dashboard`);
			}
		}
		componentWillUpdate(nextProps, nextState) {
			if(nextProps.user.logged){
				this.context.router.push(`${globals.basePath}/dashboard`);
			}
		}

		render(){
			return <ComposedComponent {...this.props} />
		}
	}

	RequireNotAuthentication.contextTypes = {
		router: React.PropTypes.object
	};

	function mapStateToProps({user}){
		return { user: user};
	}

	return connect(mapStateToProps)(RequireNotAuthentication);
}
