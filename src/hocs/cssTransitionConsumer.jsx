import React, { Component } from 'react'
import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

export default function(ComposedComponent){

	class CSSTransitionConsumer extends Component{
		render(){
			return (
				<ReactCSSTransitionGroup
					transitionName="compTransition"
					transitionAppear={true}
					transitionAppearTimeout={500}
					transitionEnter={false}
					transitionLeave={false} >
					{ this.props.children }
				</ReactCSSTransitionGroup>
			)
		}
	}

	return CSSTransitionConsumer;
}