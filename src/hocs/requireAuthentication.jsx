import React, { Component } from 'react'
import { connect } from 'react-redux'

import globals from '../config/globals'

export default function(ComposedComponent){
	class RequireAuthentication extends Component{
		componentWillMount() {
			if(!this.props.user.logged){
				this.context.router.push(globals.basePath+'/');
			}
		}
		componentWillUpdate(nextProps, nextState) {
			if(!nextProps.user.logged){
				this.context.router.push(globals.basePath+'/');

			}
		}

		render(){
			return <ComposedComponent {...this.props} />
		}
	}

	RequireAuthentication.contextTypes = {
		router: React.PropTypes.object
	};

	function mapStateToProps({user}){
		return { user: user};
	}

	return connect(mapStateToProps)(RequireAuthentication);
}
