import React, { Component } from 'react'

export default function(ComposedComponent){

	class ActionNotifier extends Component{
		render(){
			return <ComposedComponent { ...this.props } />
		}
	}

	return ActionNotifier;

}