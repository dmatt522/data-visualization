import React, { Component } from 'react'

export default function(ComposedComponent){

	class TooltipConsumer extends Component{
		componentDidMount() {
		    $('.tooltipped').tooltip({delay: 50});
		}
		componentDidUpdate(prevProps, prevState) {
		    $('.tooltipped').tooltip({delay: 50});
		}
		componentWillUnmount() {
		    $('.material-tooltip').tooltip('remove');//.attr({'style':''});
		}
		render(){
			return <ComposedComponent {...this.props} />
		}
	}

	return TooltipConsumer;

}