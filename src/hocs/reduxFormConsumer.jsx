import React, { Component } from 'react'
import { reduxForm } from 'redux-form'
import _ from 'lodash'

export default function(ComposedComponent, formName, validateFunction){

	class ReduxFormConsumer extends Component{

		render(){
			return <ComposedComponent {...this.props} />
		}
	}

	//console.log('ReduxFormConsumer  _.isFunction(validateFunction)', _.isFunction(validateFunction));

	if(_.isFunction(validateFunction))
		return reduxForm({form:formName, validateFunction})(ReduxFormConsumer);
	else
		return reduxForm({form:formName})(ReduxFormConsumer);

}