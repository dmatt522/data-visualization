import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton'

const FilterAlt = ({ label, openDialog }) => {
	const lbl = openDialog ? <label><FlatButton style={ {textAlign: 'left', height:'auto', lineHeight: 1} } onClick={openDialog}>{ label }</FlatButton></label> : <label>{ label }</label> ;
    return (
    	<div className="input-field" style={ {marginBottom: 32} }>
	        <select>
		      <option value="0">unassigned</option>
		      <option value="1">Option 1</option>
		      <option value="2">Option 2</option>
		      <option value="3">Option 3</option>
		    </select>
		    { lbl }
	    </div>
    );
};

FilterAlt.displayName = 'FilterAlt';

FilterAlt.propTypes = {
    label: PropTypes.string,
};

export default FilterAlt;
