/**
 * redux-form Field with label
 */
import React, { PropTypes } from 'react'
import { Field } from 'redux-form'

const FieldInput = ({ label, type, name }) => {
	return (
		<label>{ label }</label>
		<Field name={ name } type={ type } placeholder="Please enter your email" component="input" />
	);
}

FieldInput.propTypes = {
	name: PropTypes.string.isRequired
};
FieldInput.defaultProps = {
	type: 'text',
	label:'label'
};

export default FieldInput;