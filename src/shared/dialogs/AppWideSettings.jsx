import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import { changeSettings } from '../actions/settings'
import { US_UNIT, EU_UNIT } from '../../config/values'

class AppWideSettings extends Component{
	constructor(props){
		super(props);
		this.state = {
			open: false,
		};
		this.closeHandler = this.closeHandler.bind(this);
	}

	closeHandler(){
		// close dialog
		this.setState({ open:false });
		// tell it to parent
		this.props.resetOpen();
	}

	confirmSettings(){
		console.log('confirm settings');
		// dispatch the action
		this.props.changeSettings({
			uom: this.state.uom || this.props.settings.selectedUOM.value
		});
		this.closeHandler();
	}

	/**
	 * component creation phase methods
	 */
	componentWillMount() {}
	componentDidMount() {}
	/*
	 * state changes: changes in state triggers lifecycle events
	 */
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	componentWillUpdate(nextProps, nextState) {}
	componentDidUpdate(prevProps, prevState) {}
	/*
	 * props changes: changes in props triggers lifecycle events
	 */
	componentWillReceiveProps(nextProps) {
		this.setState({ open: nextProps.open});
	}
	/*
	 * component destruction phase methods
	 */
	componentWillUnmount() {}

	render(){
		const actions = [
			<FlatButton label="Confirm" primary={ true } onTouchTap={ (event) => this.confirmSettings() } />,
			<FlatButton label="Cancel" primary={ true } onTouchTap={ (event) => this.closeHandler() } />
		];
		return(
			<Dialog
				title="Application Settings"
				modal={ true }
				open={ this.state.open }
				actions={ actions }>
				<div>
					<h5>Measure/Units</h5>
					<RadioButtonGroup
						name="unit"
						defaultSelected={this.props.settings.selectedUOM.value}
						onChange={ (event, value) => this.setState({uom: value}) }
					>
						{
							this.props.settings.uomList.map( (item) => <RadioButton value={item.value} key={item.value} label={`${item.name} (${item.abbr})`} /> )
						}
					</RadioButtonGroup>
				</div>
			</Dialog>
		);
	}
}
AppWideSettings.propTypes = {};
AppWideSettings.defaultProps = {};

function mapStateToProps({settings}){
	return {settings};
}
function mapDispatchToProps(dispatch){
	return bindActionCreators({changeSettings}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AppWideSettings);