import React from 'react'

const SimpleText = ({ text, className }) => <p className={ className }>{ text }</p>

SimpleText.defaultProps = {
	text: 'simple text',
	className: 'simple-text'
};

export default SimpleText;