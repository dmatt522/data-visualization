import React from 'react'

const createSlides = (slides) => {
	return slides.map( (slide, i) => {
		return(
			<li key={`slide-${i}`}>
				<img src={ slide.image } />
				<div className="caption center-align">
	        		{ slide.title && <h3>{ slide.title }</h3>}
	        		{ slide.subtitle && <h5 className="light grey-text text-lighten-3">{ slide.subtitle }</h5>}
	        	</div>
			</li>
		);
	})
}

const HeroSlider = ({slides}) => {
	return (
		<div className="slider">
			<ul className="slides">
				{ createSlides(slides) }
			</ul>
		</div>
	);
}

export default HeroSlider;