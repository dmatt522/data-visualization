import React, {Component} from 'react'
import Toggle from 'material-ui/Toggle'

class ControlledToggler extends Component{
	constructor(props){
		super(props);
		this.state = {
			isToggled: true
		};
	}

	onSelection(){
		this.setState({isToggled:!this.state.isToggled});
	}

	componentDidUpdate(prevProps, prevState) {
		this.props.onToggle({
			id:this.props.id,
			status: this.state.isToggled
		});
	}

	render(){
		return(
			<Toggle
				label={this.props.label}
				labelPosition={this.props.labelPosition}
				toggled={this.state.isToggled}
				onToggle={this.onSelection.bind(this)} />
		);
	}
}
ControlledToggler.propTypes = {};
ControlledToggler.defaultProps = {
	label:'default text',
	labelPosition:'right'
};

export default ControlledToggler;