import React, { PropTypes } from 'react';

const TooltippedToggle = ({ text, children }) => {
    return (
        <Toggle className="tooltipped" data-position="top" data-delay="50" data-tooltip={ text }>
        	{ children }
        </Toggle>
    );
};

TooltippedToggle.displayName = 'TooltippedToggle';

TooltippedToggle.propTypes = {
    text: PropTypes.string,
};

export default TooltippedToggle;
