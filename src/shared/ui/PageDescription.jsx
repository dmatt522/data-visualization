import React from 'react'

const PageDescription = ({text}) => <p className="comp-pagedescription">{text}</p>

export default PageDescription;