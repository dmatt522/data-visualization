import React, { PropTypes } from 'react'
import IconButton from 'material-ui/IconButton'

const TooltippedIconButton = ({ text, position, children, clickHandler }) => {
    return (
        <IconButton className="tooltipped" data-position={ position } data-delay="50" data-tooltip={ text } onClick={ clickHandler }>
        	{ children }
        </IconButton>
    );
};

TooltippedIconButton.displayName = 'TooltippedIconButton';

TooltippedIconButton.propTypes = {
    text: PropTypes.string,
};

TooltippedIconButton.defaultProps = {
    position:'bottom',
    //clickHandler: (event) => { console.log('TooltippedIconButton default clickHandler', event.currentTarget); }
};

export default TooltippedIconButton;
