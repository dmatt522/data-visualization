import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { signOut } from '../../pages/sign/actions/index'

// material-ui
import AppBar from 'material-ui/AppBar'
import Drawer from 'material-ui/Drawer'
import Avatar from 'material-ui/Avatar'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import ExitToApp from 'material-ui/svg-icons/action/exit-to-app'
//import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
//import CloseIcon from 'material-ui/svg-icons/navigation/close';
// app
import AppLayoutMenu from './AppLayoutMenu'
import TooltipConsumer from '../../hocs/tooltipConsumer'
import AppToolbar from '../components/AppToolbar'
import TooltippedIconButton from '../ui/TooltippedIconButton'
import Nav from '../nav/Nav'
import BackButton from '../buttons/BackButton'

import AppWideSettings from '../dialogs/AppWideSettings'

import { APP_TITLE, SIGN_OUT } from '../../config/strings'
import { DELAY } from '../../config/values'

class AppLayout extends Component{
	constructor(props){
		super(props);
		this.state = {
			open : false,
			openSecondary : false,
			openSettings: false
		};

		this.toggleDrawer = this.toggleDrawer.bind(this);
		this.closeDrawers = this.closeDrawers.bind(this);
		this.signOutHandler = this.signOutHandler.bind(this);
		this.settingsHandler = this.settingsHandler.bind(this);
	}

	settingsHandler(){
		//console.log('show settings', this.refs.foo, this.refs.foo.openFromParent);
		//this.refs.foo.openFromParent();
		this.setState({openSettings:true});
	}
	signOutHandler(){
		//console.log(event);
		//this.props.signOut();
		//_.delay(this.props.signOut, DELAY);
		this.props.signOut();
	}

	toggleDrawer(){
		this.setState({open:!this.state.open});
		//console.log('toggleDrawer');
	}
	toggleSecondaryDrawer(){
		this.setState({openSecondary:!this.state.openSecondary});
	}

	closeDrawers(){
		this.setState({open:false});
		this.setState({openSecondary:false});

		//console.log('closeDrawer');
	}


	render(){
			//console.log('logged? ' + this.props.user.logged);
		return(

				<div className={ this.props.displayName }>
					<AppToolbar className="comp-apptoolbar"/>
					<AppBar
						className="comp-appbar"
						title={ APP_TITLE }
						onLeftIconButtonTouchTap={ this.toggleDrawer }
						iconElementRight={
							<AppLayoutMenu settingsTouchHandler={this.settingsHandler} logoutTouchHandler={this.signOutHandler} />
						}
					/>
					<div className="page-content">
						{/*<div className="backbuttonbar"><BackButton /></div>*/}
						{this.props.children}
					</div>
					<Drawer
						docked={ false }
						open={ this.state.open }
						onRequestChange={ this.toggleDrawer } >
						<Nav closeDrawer={ this.closeDrawers } />
					</Drawer>
					<AppWideSettings open={this.state.openSettings} resetOpen={() => this.setState({openSettings:false}) } />
				</div>
		);
	}
}
AppLayout.propTypes = {

};
AppLayout.defaultProps = {
	displayName:'AppLayout'
};

function mapDispatchToProps(dispatch){
	return bindActionCreators({ signOut }, dispatch);
};

export default connect(null, mapDispatchToProps)(TooltipConsumer(AppLayout));