import React, {Component} 	from 'react'
import MuiThemeProvider 	from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme 			from 'material-ui/styles/getMuiTheme'


import theme 		from '../../themes/theme'
import FakeNav		from '../nav/FakeNav'
import SnackbarMessage 		from '../components/SnackbarMessage'

class BaseLayout extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return(
			<MuiThemeProvider muiTheme={getMuiTheme(theme)}>
				<div>
					{/*<FakeNav/>*/}
					{this.props.children}
					<SnackbarMessage />
				</div>
			</MuiThemeProvider>
		);
	}
}
BaseLayout.propTypes = {};
BaseLayout.defaultProps = {};

export default BaseLayout;