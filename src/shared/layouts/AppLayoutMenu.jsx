import React, { PropTypes } from 'react';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const AppLayoutMenu = ({settingsTouchHandler, logoutTouchHandler}) => {
    return (
        <IconMenu
        	iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        	targetOrigin={{horizontal:'right',vertical:'top'}}
        	anchorOrigin={{horizontal:'right',vertical:'top'}}
        >
        	<MenuItem primaryText="Settings" onTouchTap={ (event) => settingsTouchHandler() } />
        	<MenuItem primaryText="LogOut" onTouchTap={ (event) => logoutTouchHandler() } />
        </IconMenu>
    );
};

AppLayoutMenu.displayName = 'AppLayoutMenu';

AppLayoutMenu.propTypes = {

};

export default AppLayoutMenu;
