import { CHANGE_SETTINGS } from '../../actions/types'

export function changeSettings(payload){
	return {
		type: CHANGE_SETTINGS,
		payload: payload
	}
}