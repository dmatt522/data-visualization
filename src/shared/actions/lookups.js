import axios from 'axios'
import {
	POST_LOOKUP,
	FETCH_LOOKUP_MATERIAL,
	FETCH_LOOKUP_STATUS,
	FETCH_LOOKUP_PERCENT,
	FETCH_LOOKUP_PART_TYPE,
	FETCH_LOOKUP_MCCP_PLANT,
	FETCH_LOOKUP_YTC,
	FETCH_LOOKUP_TIER_1,
	FETCH_LOOKUP_TIER_2,
	FETCH_LOOKUP_MOLDER,
	FETCH_LOOKUP_SALES_MANAGER,
	FETCH_LOOKUP_ADE,
	FETCH_LOOKUP_TECH_SERVICE,
	FETCH_LOOKUP_OEM_MATERIAL,
	FETCH_LOOKUP_OEM_PERFORMANCE,
	FETCH_USERS_ROLES,
	FETCH_LOOKUP_ACTION_PLANS_PRIORITY,
	FETCH_LOOKUP_ACTION_PLANS_STATUS
	 } from  '../../actions/types'
import {
	USERS,
	ADD_LOOKUP,
	MATERIALS,
	STATUS,
	PERCENT,
	PART_TYPE,
	MCCP_PLANT,
	YTC,
	TIER_1,
	TIER_2,
	MOLDER,
	SALES_MANAGER,
	ADE,
	TECH_SERVICE,
	OEM_MATERIAL,
	OEM_PERFORMANCE,
	USERS_ROLES,
	ACTION_PLANS_PRIORITY,
	ACTION_PLANS_STATUS
	} from '../../config/api'


export function fecthAllLookups(){
	console.log('go and fecthAllLookups');
	return [
		fetchMaterials(),
		fetchPartType(),
		fetchMcppPlant(),
		fetchStatus(),
		fetchProbabilityPercent(),
		fetchYTC(),
		fetchTier1(),
		fetchTier2(),
		fetchMolder(),
		fetchAde(),
		fetchSalesManager(),
		fetchTechService(),
		fetchOemMaterial(),
		fetchOemPerformance(),
		fetchUsesRoles(),
		fetchActionPlansPriority(),
		fetchActionPlansStatus()

		/*{ type: FETCH_LOOKUP_MATERIAL, 		payload: axios.get(MATERIALS) },
		{ type: FETCH_LOOKUP_PART_TYPE, 	payload: axios.get(PART_TYPE) },
		{ type: FETCH_LOOKUP_MCCP_PLANT, 	payload: axios.get(MCCP_PLANT) },
		{ type: FETCH_LOOKUP_STATUS, 		payload: axios.get(STATUS) },
		{ type: FETCH_LOOKUP_PERCENT, 		payload: axios.get(PERCENT) },
		{ type: FETCH_LOOKUP_YTC, 			payload: axios.get(YTC) },
		{ type: FETCH_LOOKUP_TIER_1, 		payload: axios.get(TIER_1) },
		{ type: FETCH_LOOKUP_TIER_2, 		payload: axios.get(TIER_2) },
		{ type: FETCH_LOOKUP_MOLDER, 		payload: axios.get(MOLDER) },
		{ type: FETCH_LOOKUP_SALES_MANAGER, payload: axios.get(SALES_MANAGER) },
		{ type: FETCH_LOOKUP_ADE, 			payload: axios.get(ADE) },
		{ type: FETCH_LOOKUP_TECH_SERVICE, 	payload: axios.get(TECH_SERVICE) },
		{ type: FETCH_LOOKUP_OEM_MATERIAL, 	payload: axios.get(OEM_MATERIAL) },
		{ type: FETCH_LOOKUP_OEM_PERFORMANCE, 	payload: axios.get(OEM_PERFORMANCE) },
		{ type: FETCH_LOOKUP_TIER_2, 		payload: axios.get(TIER_2) }*/
	];
}

export function fetchActionPlansPriority(){
	const request = axios.get(ACTION_PLANS_PRIORITY);
	return {
		type: FETCH_LOOKUP_ACTION_PLANS_PRIORITY,
		payload: request
	};
}
export function fetchActionPlansStatus(){
	const request = axios.get(ACTION_PLANS_STATUS);
	return {
		type: FETCH_LOOKUP_ACTION_PLANS_STATUS,
		payload: request
	};
}

export function fetchUsesRoles(){
	const request = axios.get(USERS_ROLES);
	return {
		type: FETCH_USERS_ROLES,
		payload: request
	};
}

export function newItem(formData){
	//console.log('....', formData);
	const data = formData;//{ scope: mapScopeToTableName(formData.scope), value: formData.value };
	const request = axios.post(ADD_LOOKUP, data);
	return {
		type: POST_LOOKUP,
		payload: request,
		data: formData
	}
}

export function fetchLookup({url, type, payload}){
	const request = axios.get(url);
	return {
		type: type,
		payload: request
	};
}

export function fetchMaterials(){
	const request = axios.get(MATERIALS);
	return {
		type: FETCH_LOOKUP_MATERIAL,
		payload: request
	};
}

export function fetchPartType(){
	const request = axios.get(PART_TYPE);
	return {
		type: FETCH_LOOKUP_PART_TYPE,
		payload: request
	};
}

export function fetchMcppPlant(){
	const request = axios.get(MCCP_PLANT);
	return {
		type: FETCH_LOOKUP_MCCP_PLANT,
		payload: request
	};
}

export function fetchStatus(){
	const request = axios.get(STATUS);
	return {
		type: FETCH_LOOKUP_STATUS,
		payload: request
	};
}

export function fetchProbabilityPercent(){
	const request = axios.get(PERCENT);
	return {
		type: FETCH_LOOKUP_PERCENT,
		payload: request
	};
}

export function fetchYTC(){
	const request = axios.get(YTC);
	return {
		type: FETCH_LOOKUP_YTC,
		payload: request
	};
}

export function fetchTier1(){
	const request = axios.get(TIER_1);
	return {
		type: FETCH_LOOKUP_TIER_1,
		payload: request
	};
}

export function fetchTier2(){
	const request = axios.get(TIER_2);
	return {
		type: FETCH_LOOKUP_TIER_2,
		payload: request
	};
}


export function fetchMolder(){
	const request = axios.get(MOLDER);
	return {
		type: FETCH_LOOKUP_MOLDER,
		payload: request
	};
}


export function fetchSalesManager(){
	const request = axios.get(USERS);
	return {
		type: FETCH_LOOKUP_SALES_MANAGER,
		payload: request
	};
}

export function fetchAde(){
	const request = axios.get(USERS);
	return {
		type: FETCH_LOOKUP_ADE,
		payload: request
	};
}

export function fetchTechService(){
	const request = axios.get(USERS);
	return {
		type: FETCH_LOOKUP_TECH_SERVICE,
		payload: request
	};
}

export function fetchOemMaterial(){
	const request = axios.get(OEM_MATERIAL);
	return {
		type: FETCH_LOOKUP_OEM_MATERIAL,
		payload: request
	};
}

export function fetchOemPerformance(){
	const request = axios.get(OEM_PERFORMANCE);
	return {
		type: FETCH_LOOKUP_OEM_PERFORMANCE,
		payload: request
	};
}

/*

function mapScopeToTableName(scope){
	switch(scope){
	 	case 'mccp_material':
	 		return 'material';
	 	case 'current_material':
	 		return 'material';
	 	default:
	 		return 'AAA';
	 }
}*/