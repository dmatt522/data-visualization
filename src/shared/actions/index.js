import {
	NOTIFY,
	FETCH_UOM,
	CONTEXT_FOR_EDIT_DELETE_DETAILS,
	CLEAR_CONTEXT_FOR_EDIT_DELETE_DETAILS
	/*OPEN_PROJECT_LOOKUP_DIALOG*/
	} from '../../actions/types'
import { UNIT_OF_MEASURE } from '../../config/api'

export function setContextForEditDeleteDetails(item){
	return {
		type: CONTEXT_FOR_EDIT_DELETE_DETAILS,
		payload: item
	}
}

export function clearContextForEditDeleteDetails(){
	return {
		type: CONTEXT_FOR_EDIT_DELETE_DETAILS,
		payload: {}
	}
}

/**
 * [notify description]
 * @param  {object} obj hold some props, {msg, level, ...}
 * @return {[type]}     [description]
 */
export function notify(payload){
	//console.log('Shared action notify', msg);
	return {
		type: NOTIFY,
		payload: payload
	};
}

export function fetchUOM(){
	//const request = axios.get(UNIT_OF_MEASURE);
	return {
		type:FETCH_UOM,
		payload: {}//request
	}
}

/*
export function openProjectLookupDialog(payload){
	return {
		type: OPEN_PROJECT_LOOKUP_DIALOG,
		payload: payload
	}
}*/