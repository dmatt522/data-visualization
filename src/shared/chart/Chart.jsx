import React, { Component } from 'react'
var Line = require('react-chartjs').Line;
var Bar = require('react-chartjs').Bar;

class Chart extends Component {
	constructor (props) {
		super(props)
		this.chartData = {
			labels: [],
			datasets: [
				{
					label: "bar1",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: []
				},
				{
					label: "bar2",
					fillColor: "rgba(151,187,205,0.2)",
					strokeColor: "rgba(151,187,205,1)",
					pointColor: "rgba(151,187,205,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(151,187,205,1)",
					data: []
				}
			]
		}
		this.chartOptions = {
			responsive: true,
			scaleShowGridLines : true,
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			scaleShowHorizontalLines: true,
			pointDot : true,
			pointDotRadius : 4,
			pointDotStrokeWidth : 1,
			pointHitDetectionRadius : 20,
			datasetStroke : true,
			datasetStrokeWidth : 2,
			offsetGridLines: false,
            multiTooltipTemplate: "<%if (datasetLabel){%><%=datasetLabel%>: <%}%><%= value %>"
		}
	}

    getChartData() {
        console.log("get chart data funciton !");
        this.chartData.labels = [];
        this.chartData.datasets[0].data = [];
        this.chartData.datasets[1].data = [];
        for (var key in this.props.data.values) {
            console.log(key);
            this.chartData.labels.push(key);
            var obj = this.props.data.values[key];
            this.chartData.datasets[0].data.push(parseFloat(obj['bar1_1'].replace(/,/g, '')));
            this.chartData.datasets[1].data.push  (parseFloat(obj['bar1_2'].replace(/,/g, '')));
        }
    }

	render () {
		this.getChartData();
        console.log("chart data", this.chartData);
        console.log("original data", this.props.data);
        if(this.props.chartType == "Line")
            return(
                <Line data={ this.chartData } options={ this.chartOptions } />
            )
        else if(this.props.chartType == "Bar")
            return (
                <Bar data={ this.chartData } options={ this.chartOptions } />
            )
	}
}
export default Chart;