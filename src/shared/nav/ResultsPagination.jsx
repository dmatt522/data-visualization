import React, {Component} from 'react';

class ResultsPagination extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
			<ul className="pagination">
			    <li className="disabled"><a href="#!"><i className="material-icons">chevron_left</i></a></li>
			    <li className="active"><a href="#!">1</a></li>
			    <li className="waves-effect"><a href="#!">2</a></li>
			    <li className="waves-effect"><a href="#!">3</a></li>
			    <li className="waves-effect"><a href="#!">4</a></li>
			    <li className="waves-effect"><a href="#!">5</a></li>
			    <li className="waves-effect"><a href="#!"><i className="material-icons">chevron_right</i></a></li>
			</ul>
		);
	}
}
ResultsPagination.propTypes = {};
ResultsPagination.defaultProps = {};

export default ResultsPagination;

