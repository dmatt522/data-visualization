import React, {Component} from 'react';
import {Link} from 'react-router'
import globals from '../../config/globals'

	const style={
		li:{display:'inline-block',marginRight:20}
	};

class FakeNav extends Component{
	constructor(props){
		super(props);
	}


	render(){
		return(
			<ul>
				<li style={style.li}><Link to={`${globals.basePath}/`}>login</Link></li>
				<li style={style.li}><Link to={`${globals.basePath}/dashboard`}>enter</Link></li>
				{/*<li style={style.li}><Link to={`${globals.basePath}/calendar`}>calendar</Link></li>*/}
				<li style={style.li}><Link to={`${globals.basePath}/vehicles`}>vehicles</Link></li>
				{/*<li style={style.li}><Link to="/vehicles">vehicles</Link></li>
				<li style={style.li}><Link to="/projects/100">prj</Link></li>*/}
			</ul>
		);
	}
}
FakeNav.propTypes = {

};
FakeNav.defaultProps = {

};

export default FakeNav;