import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'

import _ from 'lodash'

//import Paper from 'material-ui/Paper'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'

import Dashboard from 'material-ui/svg-icons/action/dashboard'
import Projects from 'material-ui/svg-icons/action/work'
import Vehicles from 'material-ui/svg-icons/maps/directions-car'
import Report from 'material-ui/svg-icons/content/report'
import Calendar from 'material-ui/svg-icons/action/date-range'
import TrendUp from 'material-ui/svg-icons/action/trending-up'
import CModel from 'material-ui/svg-icons/action/view-module'
import Pricing from 'material-ui/svg-icons/editor/attach-money'
import Folder from 'material-ui/svg-icons/file/folder'
import Contacts from 'material-ui/svg-icons/communication/contacts'
import PieChart from 'material-ui/svg-icons/editor/pie-chart-outlined'
import Person from 'material-ui/svg-icons/social/person-outline'


import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right'
import ActionSearch from 'material-ui/svg-icons/action/search'
import Help from 'material-ui/svg-icons/action/help'
import ContentAdd from 'material-ui/svg-icons/content/add'
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit'

// routes
import PATHS from '../../routes/paths'

import globals from '../../config/globals'

function getIcon(foo){
	switch(foo){
		case 'Dashboard': return <Dashboard />;
		case 'Projects': return <Projects />;
		case 'Vehicles': return <Vehicles />;
		case 'Reports': return <Report />;
		case 'Calendar': return <Calendar />;
		case 'Pricing': return <Pricing />;
		case 'Cost Model': return <CModel />;
		case 'Quotes': return <PieChart />;
		case 'Goals & Objectives': return <TrendUp />;
		case 'Contacts': return <Contacts />;
		case 'Users': return <Person />;
		default: return <Dashboard />;
	}
}


function createMenuItems(scope, closeHandler){
	if(_.isUndefined(scope))
		return false;
	return scope.map(function(item, index) {
		// projects with submenu
		if(item.name === 'Projects'){
			return <MenuItem key={index} leftIcon={<Projects />} primaryText={item.name} rightIcon={<ArrowDropRight />}
						menuItems={[
							<MenuItem primaryText="Search" leftIcon={<ActionSearch />} containerElement={<Link to={PATHS.projects} />}onTouchTap={() => closeHandler()} />,
							<MenuItem primaryText="Add to Vehicle" leftIcon={<ContentAdd />} containerElement={<Link to={PATHS.vehicles} />} onTouchTap={() => closeHandler()} />
						]}
					/>
		}
	/*	if(item.name === 'Reports'){
			return <MenuItem key={index} leftIcon={<Reports />} primaryText={item.name} rightIcon={<ArrowDropRight />}
				menuItems={[
					<MenuItem primaryText="Volumn Chart"  containerElement={<Link to={PATHS.volumns} />}onTouchTap={() => closeHandler()} />,
					<MenuItem primaryText="Revenue Chart"  containerElement={<Link to={PATHS.revenues} />} onTouchTap={() => closeHandler()} />,
					<MenuItem primaryText="Lightweight Production Chart"  containerElement={<Link to={PATHS.lightweights} />} onTouchTap={() => closeHandler()} />
				]}
			/>
		}*/
		// reports with submenu
		if(item.name === 'Reports'){
			return <MenuItem key={index} leftIcon={<Report />} primaryText={item.name} rightIcon={<ArrowDropRight />}
                menuItems={[
                    <MenuItem primaryText="Volumn Chart" leftIcon={<PieChart />} containerElement={<Link to={PATHS.volumns} />}onTouchTap={() => closeHandler()} />,
                    <MenuItem primaryText="Revenue Chart" leftIcon={<PieChart />} containerElement={<Link to={PATHS.revenues} />} onTouchTap={() => closeHandler()} />,
                    <MenuItem primaryText="Lightweight Production Chart" leftIcon={<PieChart />} containerElement={<Link to={PATHS.lightweights} />} onTouchTap={() => closeHandler()} />
                ]}
            />
		}



		let text = (item.name === 'Contacts') ? 'Directory' : item.name;
		return <MenuItem  key={index} primaryText={text} leftIcon={getIcon(item.name)} containerElement={<Link to={
			`/${item.target}`} />} onTouchTap={ () => closeHandler()} />
	} );
}

class Nav extends Component{
	constructor(props){
		super(props);
	}

	render(){

		return(
			<div className="comp-nav">
				<div style={ {padding: 16} }>
					<img src={`${globals.basePath}/images/MPA_logo_Alpha_117x60.png`} />
				</div>
				<Menu
					desktop={ true }>

					{ createMenuItems(this.props.user.userInfo.scope, this.props.closeDrawer) }

					{/*<MenuItem
						primaryText="Dashboard"
						leftIcon={<Dashboard />}
						containerElement={<Link to={PATHS.dashboard} />}
						onTouchTap={() => this.props.closeDrawer()} />
					<MenuItem
						leftIcon={<Work />}
						primaryText="Projects"
						rightIcon={<ArrowDropRight />}
						menuItems={[
							<MenuItem
								primaryText="Search"
								leftIcon={<ActionSearch />}
								containerElement={<Link to={PATHS.projects} />}
								onTouchTap={() => this.props.closeDrawer()} />,
							<MenuItem
								primaryText="Add to Vehicle"
								leftIcon={<ContentAdd />}
								containerElement={<Link to={PATHS.vehicles} />}
								onTouchTap={() => this.props.closeDrawer()} />
						]} />
					<MenuItem
						leftIcon={<DirectionsCar />}
						primaryText="Vehicles"
						containerElement={<Link to={PATHS.vehicles} />}
						onTouchTap={() => this.props.closeDrawer()} />

					<MenuItem
						leftIcon={<Report />}
						primaryText="Reports"
						containerElement={<Link to={PATHS.reports} />}
						onTouchTap={() => this.props.closeDrawer()} />
					<MenuItem
						leftIcon={<Folder />}
						primaryText="Contacts"
						containerElement={<Link to={PATHS.contacts} />}
						onTouchTap={() => this.props.closeDrawer()} />
					<MenuItem
						leftIcon={<DateRange />}
						primaryText="Calendar"
						containerElement={<Link to={PATHS.calendar} />}
						onTouchTap={() => this.props.closeDrawer()} />
					<MenuItem
						leftIcon={<Help />}
						primaryText="Help/Guide"
						containerElement={<Link to="#" />}
						onTouchTap={() => this.props.closeDrawer()} />*/}

				</Menu>
			</div>
		);
	}
}
Nav.propTypes = {};
Nav.defaultProps = {};
function mapStateToProps({user}){
	return {user};
}
export default connect(mapStateToProps)(Nav);