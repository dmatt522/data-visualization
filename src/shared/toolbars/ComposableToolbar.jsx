import React, { PropTypes } from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'

const ComposableToolbar = ({ title, children, tgstyle }) => {
    return (
    	<Toolbar className="composable-toolbar">
			<ToolbarGroup firstChild={true}>
				<ToolbarTitle className="toolbar-title" text={ title } />
			</ToolbarGroup>
			<ToolbarGroup lastChild={true} style={tgstyle}>
				{ children }
			</ToolbarGroup>
		</Toolbar>
    );
};

ComposableToolbar.defaultProps={
	tgstyle:{}
}

export default ComposableToolbar;
