import React, { PropTypes } from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle, ToolbarSeparator } from 'material-ui/Toolbar'
import BackButton from '../buttons/BackButton'
const PageTitleBar = ({ title, children }) => {
    return (
    	<Toolbar className="page-title-toolbar">
			<ToolbarGroup firstChild={true}>
				<BackButton />
				<ToolbarSeparator />
				<ToolbarTitle className="toolbar-title" text={ title } />
			</ToolbarGroup>
			<ToolbarGroup lastChild={true}>
				{ children }
			</ToolbarGroup>
		</Toolbar>
    );
};


export default PageTitleBar;
