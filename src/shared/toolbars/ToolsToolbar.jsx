import React, { PropTypes } from 'react';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'

const ToolsToolbar = ({children}) => {
    return (
    	<Toolbar>
			<ToolbarGroup firstChild={true} />
			<ToolbarGroup>
				{ children }
			</ToolbarGroup>
		</Toolbar>
    );
};

ToolsToolbar.ToolsToolbar = {

};

export default ToolsToolbar;
