import React, {Component} from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'

import TooltippedIconButton from '../ui/TooltippedIconButton'
import styles from '../../styles/styles'

class ToolbarExpand extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
			<Toolbar>
					<ToolbarGroup firstChild={true}>
						<ToolbarTitle  style={styles.title} text={this.props.title} />
					</ToolbarGroup>
					<ToolbarGroup lastChild={ true }>
						<TooltippedIconButton text="Toggle" clickHandler={ this.props.clickHandler }>
							<ExpandMoreIcon />
						</TooltippedIconButton>
					</ToolbarGroup>
				</Toolbar>
		);
	}
}
ToolbarExpand.propTypes = {};
ToolbarExpand.defaultProps = {
	//clickHandler: (event) => { console.log('ToolbarExpand default clickHandler', event.currentTarget); }
};

export default ToolbarExpand;