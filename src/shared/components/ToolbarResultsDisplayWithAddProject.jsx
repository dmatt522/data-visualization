import React, {Component} 	from 'react'
import { connect } from 'react-redux'

import _ from 'lodash'

import { Link }				from 'react-router'
import IconButton 			from 'material-ui/IconButton'
import IconMenu 			from 'material-ui/IconMenu'
import MenuItem 			from 'material-ui/MenuItem'
import FileDownloadIcon 	from 'material-ui/svg-icons/file/file-download'
import ReorderIcon 			from 'material-ui/svg-icons/action/reorder'
import MoreVertIcon 		from 'material-ui/svg-icons/navigation/more-vert';
import Add 				from 'material-ui/svg-icons/content/add'
import Delete 				from 'material-ui/svg-icons/action/delete'
import EditorModeEdit 		from 'material-ui/svg-icons/editor/mode-edit'
import Details		 		from 'material-ui/svg-icons/image/details'

import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'

import TooltippedIconButton from '../ui/TooltippedIconButton'
import styles 	from '../../styles/styles'
import globals 	from '../../config/globals'

class ToolbarResultsDisplayWithAddProject extends Component{
	constructor(props){
		super(props);
		this.state = {
			addprjUrl: '#',
			editUrl: '#',
			viewUrl:'#',
			deleteUrl:'#'
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.actionPaths){
			//console.log('nextProps.actionPaths', nextProps.actionPaths);
		    this.setState({
		    	addprjUrl: nextProps.actionPaths.addprj,
		    	viewUrl: nextProps.actionPaths.view,
		    	editUrl: nextProps.actionPaths.edit,
		    	deleteUrl: nextProps.actionPaths.delete
		    });
		}

	}

	render(){

		return(
			<Toolbar className="comp-toolbar" style={{backgroundColor:'#fff'}}>
					<ToolbarGroup firstChild={true}>
						{/*<ToolbarTitle style={ styles.title } text={ this.props.title } />*/}
					</ToolbarGroup>
					<ToolbarGroup lastChild={ true }>

						<TooltippedIconButton text="Download Data"><FileDownloadIcon /></TooltippedIconButton>

						<IconMenu
							iconButtonElement={
								<IconButton
									className="tooltipped"
									data-position="bottom" data-delay="50" data-tooltip="Actions"
									disabled={ this.props.disableTools }>
									<MoreVertIcon />
								</IconButton> }
							anchorOrigin={{horizontal: 'right', vertical: 'top'}}
							targetOrigin={{horizontal: 'right', vertical: 'top'}} >
							<MenuItem leftIcon={<Add />}>
								<Link to={ this.state.addprjUrl }>Add Project</Link>
							</MenuItem>
							<MenuItem leftIcon={<Details />}>
								<Link to={ this.state.viewUrl }>Details</Link>
							</MenuItem>
							<MenuItem leftIcon={<EditorModeEdit />}>
								<Link to={ this.state.editUrl }>Edit</Link>
							</MenuItem>
							<MenuItem primaryText="Delete" onTouchTap={ this.props.deleteHandler } leftIcon={<Delete />} />
						</IconMenu>

					</ToolbarGroup>
				</Toolbar>
		);
	}
}

ToolbarResultsDisplayWithAddProject.propTypes = {};
ToolbarResultsDisplayWithAddProject.defaultProps = {};

export default ToolbarResultsDisplayWithAddProject;