import React, { PropTypes } from 'react';

const NoMatch = ({ className }) => {
    return (
        <div>404 - no match</div>
    );
};

NoMatch.displayName = 'NoMatch';

NoMatch.propTypes = {
    className: PropTypes.string,
};

export default NoMatch;
