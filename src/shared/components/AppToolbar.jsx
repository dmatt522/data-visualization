import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import AutoComplete from 'material-ui/AutoComplete'
import SearchIcon from 'material-ui/svg-icons/action/search'
import IconButton from 'material-ui/IconButton'



class AppToolbar extends Component{
	constructor(props){
		super(props);
		this.state = {
			employeeName: 'Mr. Black',
			dataSource:[]
		};
		this.handleUpdateInput = this.handleUpdateInput.bind(this);
	}

	handleUpdateInput(value){
		this.setState({ dataSource : [ value, value + value, value + value + value ]});
	}


	componentWillReceiveProps(nextProps) {
	    //this.setState({employeeName:nextProps.user.userInfo['Employee_Name']});
	}

	render(){
		const info = this.props.user.userInfo;

		return(
			<Toolbar style={{backgroundColor:'#fff', maxWidth:'98%', margin:'auto'}}>
				<ToolbarGroup firstChild={ true }>
					<ToolbarTitle text={`${info.first_name} ${info.last_name}`} style={{paddingLeft:16}} />
				</ToolbarGroup>
				<ToolbarGroup lastChild={ true }>
					<AutoComplete
						hintText="Search box"
						dataSource={ this.state.dataSource }
						onUpdateInput={ this.handleUpdateInput } />
						<IconButton children={<SearchIcon />} onClick={ (event) => { console.log('searching...');} } />
				</ToolbarGroup>
			</Toolbar>
		);
	}
}
AppToolbar.propTypes = {};
AppToolbar.defaultProps = {};

function mapStateToProps({ user }){
	return { user };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AppToolbar);