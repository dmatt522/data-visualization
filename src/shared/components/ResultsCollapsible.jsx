import React, {Component} from 'react';
import { Link } from 'react-router'

import globals from '../../config/globals'
import FakeTable from './FakeTable'

const id = '3';
const st = {
	ul:{

	},
	li:{
		display:'inline-block',
		padding:16
	}
};

class ResultsCollapsible extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
			<div>
					<ul className="collapsible" data-collapsible="expandable">
					    <li>
					      <div className="collapsible-header"><i className="material-icons">expand_more</i>First expandable</div>
					      <div className="collapsible-body">
					      	<FakeTable />
					      	{/*<div>
					      		<ul style={ st.ul }>
					      			<li style={ st.li }>sample pages</li>
									<li style= { st.li }><Link to={`${globals.basePath}/${this.props.path}/${id}`}>view</Link></li>
									<li style= { st.li }><Link to={`${globals.basePath}/${this.props.path}/edit/${id}`}>edit</Link></li>
									<li style= { st.li }><Link to="#">delete</Link></li>
								</ul>
					      	</div>*/}
					      </div>
					    </li>
					    <li>
					      <div className="collapsible-header"><i className="material-icons">expand_more</i>Second expandable</div>
					      <div className="collapsible-body">
					      	<FakeTable />
					      </div>
					    </li>
					    <li>
					      <div className="collapsible-header"><i className="material-icons">expand_more</i>Third expandable</div>
					      <div className="collapsible-body">
					      	<FakeTable />
					      </div>
					    </li>
					</ul>

					{/*<ul>
						<li><Link to={`/vehicles/${id}`}>view</Link></li>
						<li><Link to={`/vehicles/edit/${id}`}>edit</Link></li>
						<li><Link to="#">delete</Link></li>
					</ul>*/}
				</div>
		);
	}
}
ResultsCollapsible.propTypes = {};
ResultsCollapsible.defaultProps = {};

export default ResultsCollapsible;