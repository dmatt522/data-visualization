import React from 'react'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'

const TitleHeader = ({ title }) => {
	return (
		<Toolbar>
			<ToolbarGroup firstChild={ true }>
				<ToolbarTitle className="collapsible-header-title" text={title} />
			</ToolbarGroup>
		</Toolbar>
	);
}

export default TitleHeader;