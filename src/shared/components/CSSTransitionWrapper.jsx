import React, {Component} from 'react';
import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

class CSSTransitionWrapper extends Component{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(

				<ReactCSSTransitionGroup
					transitionName="compTransition"
					transitionAppear={this.props.transitionAppear}
					transitionAppearTimeout={this.props.transitionAppearTimeout}
					transitionEnter={this.props.transitionEnter}
					transitionLeave={this.props.transitionLeave} >
					{ this.props.children }
				</ReactCSSTransitionGroup>

		);
	}
}
CSSTransitionWrapper.propTypes = {};
CSSTransitionWrapper.defaultProps = {
	transitionName:'compTransition',
	transitionAppear:true,
	transitionAppearTimeout:500,
	transitionEnter:false,
	transitionLeave:false
};

export default CSSTransitionWrapper;