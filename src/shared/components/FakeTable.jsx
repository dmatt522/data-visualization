import React, { PropTypes } from 'react';
import Avatar from 'material-ui/Avatar'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

const FakeTable = () => {
    return (
        <Table
        	bodyStyle={{overflow:'visible'}}
        	fixedHeader={ false }
        	fixedFooter={ false }>
        <TableHeader>
        	<TableRow>
	        <TableHeaderColumn tooltip="avataro">Avatar</TableHeaderColumn>
	        <TableHeaderColumn>Name</TableHeaderColumn>
	        <TableHeaderColumn>Lastname</TableHeaderColumn>
	        <TableHeaderColumn>Email</TableHeaderColumn>
	        <TableHeaderColumn>Phone</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	        <TableHeaderColumn>Location</TableHeaderColumn>
	      </TableRow>
	    </TableHeader>
	    <TableBody showRowHover={true} stripedRows={true} >
	      <TableRow >
	        <TableRowColumn><Avatar src="images/ok-128.jpg" /></TableRowColumn>
	        <TableRowColumn>John</TableRowColumn>
	        <TableRowColumn>Smith</TableRowColumn>
	        <TableRowColumn>johnsmith@johnsmithdomain.com</TableRowColumn>
	        <TableRowColumn>012345678</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	      </TableRow>
	      <TableRow>
	        <TableRowColumn><Avatar src="images/uxceo-128.jpg" /></TableRowColumn>
	        <TableRowColumn>Maria</TableRowColumn>
	        <TableRowColumn>Rose</TableRowColumn>
	        <TableRowColumn>mariarose@mariarosedomain.com</TableRowColumn>
	        <TableRowColumn>87654321</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	        <TableRowColumn>Somewhere</TableRowColumn>
	      </TableRow>

	    </TableBody>
        </Table>
    );
};


export default FakeTable;