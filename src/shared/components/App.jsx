import React, {Component} from 'react'
/*
class App extends Component{
	constructor(props){
		super(props);
	}
	render(){
		return (
			<div>
				<p>I am your App component</p>
				{this.props.children}
			</div>
		)
	}
}
*/
const App = ({children}) => {
	return (
		<div>
			<p>I am your functional App component</p>
			{children}
		</div>
	);
}

export default App;