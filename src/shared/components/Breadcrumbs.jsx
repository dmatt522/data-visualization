import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import paths from '../../routes/paths'

import _ from 'lodash'

class Breadcrumbs extends Component{

    render(){

		//console.log('items', this.props.items);
	    return (
	        <div>
	        	{ this.props.items }
	        	{/*items.map( function(name) { <Link to={_.get(paths, name)} >{ name }</Link> } )*/}
	        </div>
	    )
    }
};

Breadcrumbs.displayName = 'Breadcrumbs';

Breadcrumbs.defaultProps = {
	items: []
}
Breadcrumbs.propTypes = {};
/*Breadcrumbs.contextTypes = {
	router: React.PropTypes.object
}*/

export default Breadcrumbs;
