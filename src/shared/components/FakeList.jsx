import React, { PropTypes } from 'react';
import {List, ListItem} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'

const FakeList = () => {
    return (
        <List>
        	<ListItem
	          leftAvatar={<Avatar src="images/ok-128.jpg" />}
	          primaryText="Project A"
	          secondaryText={
	            <p>
	              <span style={{color: '#000'}}>project manager</span> --
	             this is a short project description
	            </p>
	          }
	          secondaryTextLines={2}
	        />
	        <ListItem
	          leftAvatar={<Avatar src="images/uxceo-128.jpg" />}
	          primaryText="Project B"
	          secondaryText={
	            <p>
	              <span style={{color: '#000'}}>project manager</span> --
	             this is a short project description
	            </p>
	          }
	          secondaryTextLines={2}
	        />
	        <ListItem
	          leftAvatar={<Avatar src="images/kerem-128.jpg" />}
	          primaryText="Project C"
	          secondaryText={
	            <p>
	              <span style={{color: '#000'}}>project manager</span> --
	             this is a short project description
	            </p>
	          }
	          secondaryTextLines={2}
	        />
	        <ListItem
	          leftAvatar={<Avatar src="images/ok-128.jpg" />}
	          primaryText="Project D"
	          secondaryText={
	            <p>
	              <span style={{color: '#000'}}>project manager</span> --
	             this is a short project description
	            </p>
	          }
	          secondaryTextLines={2}
	        />
        </List>
    );
};

FakeList.displayName = 'FakeList';

FakeList.propTypes = {
    className: PropTypes.string,
};

export default FakeList;
