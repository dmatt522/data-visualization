import React, { PropTypes } from 'react';

const Preloader = ({ message }) => {
    return (
    	<div className="comp-Preloader">
    		<div className="comp-Preloader_message">{ message }</div>
        	<div className="progress">
			    <div className="indeterminate"></div>
			</div>
		</div>
    );
};

Preloader.displayName = 'Preloader';

Preloader.propTypes = {

};

Preloader.defaultProps = {
	message: 'Loading data.'
};

export default Preloader;
