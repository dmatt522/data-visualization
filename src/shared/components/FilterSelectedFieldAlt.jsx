import React, {Component} 		from 'react';


class FilterSelectedFieldAlt extends Component{
	constructor(props){
		super(props);
		this.state = {
			value: 'any'
		};
		this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleReportsSelectChange = this.handleReportsSelectChange.bind(this);
	}


	componentWillMount() {
		//console.log('FilterSelectedFieldAlt.componentWillMount');
	}
	componentDidMount() {
		//console.log('FilterSelectedFieldAlt.componentDidMount');
	}
	componentWillUpdate(nextProps, nextState) {
	    //console.log('FilterSelectedFieldAlt.componentWillUpdate');
	}
	componentDidUpdate(prevProps, prevState) {
		//console.log('FilterSelectedFieldAlt.componentDidUpdate');
	}
	/**
	 * [componentWillReceiveProps description]
	 * @param  {[type]} nextProps [description]
	 * @return {[type]}           [description]
	 */
	componentWillReceiveProps(nextProps) {
		//console.log('FilterSelectedFieldAlt.componentWillReceiveProps');
		// reset state props
	    //if(_.isEmpty(nextProps.vehicles.filtersBuffer))
	    if(nextProps.bufferSize == 0)
	    	this.setState({ value:'any' });

	}
	componentWillUnmount() {
	    //console.log('FilterSelectedFieldAlt.componentWillUnmount');
	}



	handleSelectChange(event){
		console.log('event', event);
		this.setState({ value:event.target.value });
		this.props.onChange({name:this.props.name, value: event.target.value});
	}

    handleReportsSelectChange(event){
        console.log('event', event);
        this.setState({ value:event.target.value });
        this.props.onChange({name:this.props.label, value: event.target.value});
    }

	render(){

        console.log("this.prop",this.props);

			if(this.props.id) {
                if(this.props.type == "datepicker"){
                    return (
                    <div className="browser-default-input-field">
                        <label>{ this.props.label }</label>
                        <select className="browser-default" value={ this.state.value } onChange={ this.handleReportsSelectChange }>
                            <option key="any" value="any">any</option>
                        </select>
                    </div>
                    );
                }else {
                    return (
                        <div className="browser-default-input-field">
                        <label>{ this.props.label }</label>
                        <select className="browser-default" value={ this.state.value } onChange={ this.handleReportsSelectChange }>
                        <option key="any" value="any">any</option>
                        {
                            this.props.options.map( (opt, i) => <option key={ opt.value } value={ opt.name }>{ opt.name }</option> )
                        }
                        </select>
                        </div>
                        );
                }

            }else {
                return (
                <div className="browser-default-input-field">
                    <label>{ this.props.label }</label>
                    <select className="browser-default" value={ this.state.value } onChange={ this.handleSelectChange }>
                        <option key="any" value="any">any</option>
                        {
                        this.props.options.map( (opt, i) => <option key={ i } value={ opt }>{ opt }</option> )
                        }
                    </select>
                </div>
                );
            }
	}
}
FilterSelectedFieldAlt.propTypes = {};
FilterSelectedFieldAlt.defaultProps = {};


export default FilterSelectedFieldAlt;