/**
 * this component is a Toolbar with:
 * 	- title: the container title
 * 	- toggle: to act as its contaainer content toggler
 */

import React, { PropTypes } from 'react';
import Toggle from 'material-ui/Toggle'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import TooltippedIconButton from '../ui/TooltippedIconButton'

import styles from '../../styles/styles'

const ToolbarToggler = ({ title, toggleCallback }) => {
    return (
		<Toolbar>
			<ToolbarGroup firstChild={true}>
				<ToolbarTitle text={title} style={styles.title} />
			</ToolbarGroup>
			<ToolbarGroup lastChild={true}>
				{/*<Toggle defaultToggled={true} onToggle={ () => toggleCallback() } />*/}
				<TooltippedIconButton onClick={ () => toggleCallback() } text="Open Context Menu">
					<MoreVertIcon />
				</TooltippedIconButton>
			</ToolbarGroup>
		</Toolbar>
    );
};

ToolbarToggler.displayName = 'ToolbarToggler';

ToolbarToggler.propTypes = {
    title: PropTypes.string
};
ToolbarToggler.defaultProps = {
    title: 'The Title'
};

export default ToolbarToggler;

