/**
 * displays app state messages,
 * see ../../reducers/index.jsx
 */

import React, {Component} from 'react'
import { connect } from 'react-redux'
import Snackbar from 'material-ui/Snackbar'

class SnackbarMessage extends Component{
	constructor(props){
		super(props);
		this.state = {
			open:false
		};
	}

	componentWillReceiveProps(nextProps) {
	    if(nextProps.notification){
	    	this.setState({open:true})
	    }
	}

	render(){

		return(
			<Snackbar
				className="comp-snackbar"
				open={ this.state.open }
				message={ this.props.notification.message }
				autoHideDuration={ 3000 }
				bodyStyle={{paddingTop:8, paddingBottom:8,textAlign:'center', height:70,fontSize:'18px !important'}}
				style={{fontSize:'18px !important'}}
				onRequestClose={ () => this.setState({open:false}) } />
		);
	}
}
SnackbarMessage.propTypes = {};
SnackbarMessage.defaultProps = {
	notification:{
		message: 'no notifications'
	}
};

function mapStateToProps({ notification }){
	return { notification };
}

export default connect(mapStateToProps)(SnackbarMessage);