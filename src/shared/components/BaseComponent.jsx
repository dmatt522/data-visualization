import React, {Component} from 'react';

class BaseComponent extends Component{
	constructor(props){
		super(props);
	}
	componentDidMount() {
		$('.tooltipped').tooltip({delay: 50});
		//$('.materialboxed').materialbox();
		//console.log('componentDidMount parent');
	}
	componentDidUpdate(prevProps, prevState) {
		$('.tooltipped').tooltip({delay: 50});
		//$('.materialboxed').materialbox();
		//console.log('componentDidUpdate parent');
	}
	componentWillUnmount() {
		//console.log(this, 'componentWillUnmount parent');
		$('.tooltipped').tooltip('remove');
	}
	render(){
		return( <div>base component</div>);
	}
}
BaseComponent.propTypes = {};
BaseComponent.defaultProps = {};

export default BaseComponent;