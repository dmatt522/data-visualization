import React, { Component } 	from 'react'
import { connect } 				from 'react-redux'
import { bindActionCreators } 	from 'redux'

import _ from 'lodash'

import SelectField 	from 'material-ui/SelectField'
import MenuItem 	from 'material-ui/MenuItem'
// some colors
import { redA700, white, darkBlack, fullBlack } from 'material-ui/styles/colors';
// action to filter results to display
import { bufferFilterVehicles } from '../../pages/vehicles/actions/index'


class FilterSelectedField extends Component{
	constructor(props){
		super(props);
		this.state = {
			value: 'any'
		};

		//this.handleSelectChange = this.handleSelectChange.bind(this);
	}


	handleSelectChange(event, index, value){
		this.setState({ value:value });
		// dispacth action filter_vehicles con payload = {label, value}
		//this.props.bufferFilterVehicles(_.set({}, this.props.name, value));
		this.props.bufferFilterVehicles({name:this.props.name, value: value});

		//{this.props.name:value});
		// managed by Vehicles.jsx, passed by Vehicles.jsx -> FiltersGroup.jsx -> FilterSelectedField
		//this.props.onFilterChange({label:this.props.label, value:value});
	}

	render(){
		return(
			<SelectField
	    		floatingLabelText={ this.props.label }
	    		floatingLabelFixed={true}
	    		floatingLabelStyle={ {color: redA700, fontSize: '1.2rem', fontWeight:'400'} }
	    		onChange={ this.handleSelectChange.bind(this) }
	    		value={ this.state.value }
	    		maxHeight={ 300 }
	    		fullWidth={ true }>
	          	<MenuItem value="any" primaryText="any" />
	    		{
	    			this.props.options.map( (opt, i) => <MenuItem key={ i } value={ opt } primaryText={ opt } /> )
	    		}
	        </SelectField>
		);
	}
}
FilterSelectedField.propTypes = {};
FilterSelectedField.defaultProps = {};

function mapDispatchToProps(dispatch){
	return bindActionCreators({ bufferFilterVehicles }, dispatch);
}
export default connect(null, mapDispatchToProps)(FilterSelectedField);