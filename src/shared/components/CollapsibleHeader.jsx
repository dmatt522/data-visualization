import React from 'react'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'
import IconButton from 'material-ui/IconButton'
import styles from '../../styles/styles'
//import TooltippedIconButton from '../ui/TooltippedIconButton'

const CollapsibleHeader = ({ title }) => {
	return (
		<Toolbar>
			<ToolbarGroup firstChild={ true }>
				<ToolbarTitle className="collapsible-header-title" text={title} />
			</ToolbarGroup>
			<ToolbarGroup lastChild={false}>
				{/*<TooltippedIconButton text="Open/Close"><ExpandMoreIcon /></TooltippedIconButton>*/}
				<IconButton tooltip="Open/Close" tooltipStyles={styles.tooltip}><ExpandMoreIcon /></IconButton>
			</ToolbarGroup>
		</Toolbar>
	);
}

export default CollapsibleHeader;