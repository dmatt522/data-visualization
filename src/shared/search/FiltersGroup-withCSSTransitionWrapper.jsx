import React, {Component} from 'react'



import _ from 'lodash'

import ActionNotifier from '../../hocs/actionNotifier'
import CSSTransitionWrapper from '../components/CSSTransitionWrapper'

import Toggle from 'material-ui/Toggle'
import Divider from 'material-ui/Divider'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'

import TooltippedIconButton from '../ui/TooltippedIconButton'
import Preloader 				from '../components/Preloader'
import NoResult 				from '../components/NoResult'

import Filter 					from './Filter'
import FilterAlt 				from './FilterAlt'
import FilterSelectFieldAlt 	from '../components/FilterSelectedFieldAlt'
//import Selector					from './Selector'
import styles 				from '../../styles/styles'

/**
 * todo: add a paper toolbar with the title and a collapse action button on the right
 */


function createColumns ({filters, bufferSize, onChange}) {
// todo: use this.props.filters array, fetched from api call
		return filters.map( (item, i) => {
			return (
				<CSSTransitionWrapper>
					<div className="col l3 m4 s12 filter-col" key={ i }>
						{/*<FilterAlt key={ i } {...item} />*/}
						<FilterSelectFieldAlt
							{ ...item }
							bufferSize={bufferSize}
							onChange={ onChange }/>
					</div>
				</CSSTransitionWrapper>
			);
		});
};

class FiltersGroup extends Component{
	constructor(props){
		super(props);
		this.state = {
			loading: undefined
		};
	}

	componentWillMount() {
		//console.log('FiltersGroup.componentWillMount');
		if(!_.isEmpty(this.props.data.filters))
	    	this.setState({loading:false});
	}
	componentDidMount() {
		//console.log('FiltersGroup.componentDidMount');
	}
	componentWillUpdate(nextProps, nextState) {
	}
	componentDidUpdate(prevProps, prevState) {
		//console.log('FiltersGroup.componentDidUpdate');
		if(!_.isEqual(this.props.data.filters, prevProps.data.filters)){
	    	console.log('pageData changed ...');
	    	this.setState({loading:false});
	    }
	}
	componentWillReceiveProps(nextProps) {
	    //console.log('FiltersGroup.componentWillReceiveProps');
	    if(!_.isEqual(this.props.data.filters, nextProps.data.filters)){
	    	console.log('pageData changed ...');
	    	this.setState({loading:true});
	    }
	}
	componentWillUnmount() {
	    //console.log('FiltersGroup.componentWillUnmount');
	    this.setState({loading:false});
	}


	componentWillReceiveProps(nextProps) {
		//console.log('FiltersGroup.componentWillReceiveProps', nextProps);
	    /*if(_.isEqual(this.props.data.filtersBuffer, nextProps.data.filtersBuffer)){
	    	this.props.notify('Filters Update');
	    }*/
	}

	render(){

		console.log('FiltersGroup.render', 'is loading?', this.state.loading);


		let el = <div className="comp-filtersgroup" style={styles.filtersGroup}>
				<div className="filters">
					<div className="row filters-selects">
						{
							createColumns({
								filters:this.props.data.filters,
								bufferSize:_.size(this.props.data.filtersBuffer),
								onChange: this.props.onSelectChange
							})
						}
					</div>
					<div className="filters-buttons">
						<RaisedButton label="Reset" primary={false} onClick={ this.props.onResetHandler } />
						<RaisedButton label="Search" primary={true} onClick={ this.props.onApplyHandler } />
					</div>
					<div>
						<div className="filters-recap">
							<p>Selected Filters: { _.size(this.props.data.filtersBuffer) > 0 ? '' : 'no filter selected yet.'}</p>
							<ul>
								{ this.props.data.filtersBuffer.map((item) => <li className="selected-filters-item" key={item.name}><span>{_.replace(item.name, /_/g, ' ')}</span> <span>{item.value}</span></li>) }
							</ul>
						</div>
					</div>
				</div>
			</div>;
		// else show preloader
		if(_.isUndefined(this.state.loading) || this.state.loading){
			el = <Preloader />
		}

		return (
			<CSSTransitionWrapper>
				{el}
			</CSSTransitionWrapper>
		);

	}
}
FiltersGroup.propTypes = {};
FiltersGroup.defaultProps = {};

export default FiltersGroup;