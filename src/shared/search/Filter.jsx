import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

import styles from '../../styles/styles'

const data = [
	{ key: 'x', value: 'x', primaryText: 'any'},
	{ key: 'a', value: 'a', primaryText: 'option a'},
	{ key: 'b', value: 'b', primaryText: 'option b'},
	{ key: 'c', value: 'c', primaryText: 'option c'},
	{ key: 'd', value: 'd', primaryText: 'option d'},
	{ key: 'db', value: 'd', primaryText: 'option d'},
	{ key: 'dg', value: 'd', primaryText: 'option d'},
	{ key: 'dz', value: 'd', primaryText: 'option d'},
	{ key: 'df', value: 'd', primaryText: 'option d'},
	{ key: 'dd', value: 'd', primaryText: 'option d'},
	{ key: 'dq', value: 'd', primaryText: 'option d'}
];

class Filter extends Component{
	constructor(props){
		super(props);
		this.state = { value: null };
	}

	handleChange(event, index, value){
		this.setState({value});
	}

	render(){

		const items = data.map((item) => {
			return <MenuItem { ...item } />
		});

		return(
			<div className="col l2 m3 s12 comp-filter">
				<SelectField
					floatingLabelText={this.props.label}
					floatingLabelFixed={true}
					floatingLabelStyle={styles.filter.label}
					fullWidth={true}
					maxHeight={200}
					onChange={this.handleChange.bind(this)}
					style={styles.filter}
					value={this.state.value}
					>
					{items}
				</SelectField>
			</div>
		);
	}
}
Filter.propTypes = {};
Filter.defaultProps = {};

export default Filter;