import React, {Component} from 'react'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'

import _ from 'lodash'

import ActionNotifier from '../../hocs/actionNotifier'

import Toggle from 'material-ui/Toggle'
import Divider from 'material-ui/Divider'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'
import Search from 'material-ui/svg-icons/action/search'
import Clear from 'material-ui/svg-icons/content/clear'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'

import TooltippedIconButton from '../ui/TooltippedIconButton'
import Preloader 				from '../components/Preloader'
import NoResult 				from '../components/NoResult'

import Filter 					from './Filter'
import FilterAlt 				from './FilterAlt'
import FilterSelectFieldAlt 	from '../components/FilterSelectedFieldAlt'
//import Selector					from './Selector'
import styles 				from '../../styles/styles'

/**
 * todo: add a paper toolbar with the title and a collapse action button on the right
 */


function createColumns ({filters, bufferSize, onChange}) {
// todo: use this.props.filters array, fetched from api call
		return filters.map( (item, i) => {
			return (
				<div className="col l3 m4 s12 filter-col" key={ i }>
					{/*<FilterAlt key={ i } {...item} />*/}
					<FilterSelectFieldAlt
						{ ...item }
						bufferSize={bufferSize}
						onChange={ onChange }/>
				</div>
			);
		});
};

class FiltersGroupPart extends Component{
	constructor(props){
		super(props);
		this.state = {
			/*loading: true*/
		};
	}

	setLoadingStateFromParent(bool){
		//console.log('---- setLoadingStateFromParent', bool);
		//this.setState({loading: bool});
	}

	componentWillMount() {
		//console.log('FiltersGroupPart.componentWillMount');
	}
	componentDidMount() {
		//console.log('FiltersGroupPart.componentDidMount');
	}
	componentWillReceiveProps(nextProps) {
	    //console.log('FiltersGroupPart.componentWillReceiveProps');
	    /*if(!_.isEqual(this.props.data.filters, nextProps.data.filters)){
	    	console.log('filters changed ...');
	    	this.setState({loading:true});
	    }*/
	}
	componentWillUpdate(nextProps, nextState) {
		//console.log('FiltersGroupPart.componentWillUpdate');
	}
	componentDidUpdate(prevProps, prevState) {
		//console.log('FiltersGroupPart.componentDidUpdate');

		//this.props.didUpdateCallback(this);

		//if(!_.isEqual(this.props.data.filters, prevProps.data.filters)){
	    	//console.log('filters changed ...');
	    	//this.setState({loading:false});
	    //}
	}
	shouldComponentUpdate(nextProps, nextState) {
		//console.log('FiltersGroupPart.shouldComponentUpdate');
	    return true;//(!_.isEmpty(nextProps.data.filters));
	}
	componentWillUnmount() {
	    //console.log('FiltersGroupPart.componentWillUnmount');
	    //this.setState({loading: true});
	}


	render(){

		//console.log('FiltersGroupPart.render', 'is loading?', this.state.loading);



		// else show preloader
		/*if(this.state.loading){
			return <Preloader />
		}*/

		return (
						<ReactCSSTransitionGroup
							transitionName="testTransition"
							transitionAppear={true}
							transitionAppearTimeout={500}
							transitionEnter={true}
							transitionEnterTimeout={500}
							transitionLeave={false} >
			<div className="comp-filtersgroup" style={styles.filtersGroup}>
				<div className="filters">
					<div className="row filters-selects">
							{
								createColumns({
									filters:this.props.data.filters,
									bufferSize:_.size(this.props.data.filtersBuffer),
									onChange: this.props.onSelectChange
								})
							}
					</div>
					<div className="filters-buttons">
						<RaisedButton label="Reset" primary={false} onClick={ this.props.onResetHandler } icon={<Clear />} />
						<RaisedButton label="Search" primary={true} onClick={ this.props.onApplyHandler } icon={<Search />} />
					</div>
					<div>
						<div className="filters-recap">
							<p>Selected Filters: { _.size(this.props.data.filtersBuffer) > 0 ? '' : 'no filter selected yet.'}</p>
							<ul>
								{ this.props.data.filtersBuffer.map((item) => <li className="selected-filters-item" key={item.name}><span>{_.replace(item.name, /_/g, ' ')}</span> <span>{item.value}</span></li>) }
							</ul>
						</div>
					</div>
				</div>
			</div>
						</ReactCSSTransitionGroup>
		);

	}
}
FiltersGroupPart.propTypes = {};
FiltersGroupPart.defaultProps = {};

export default FiltersGroupPart;