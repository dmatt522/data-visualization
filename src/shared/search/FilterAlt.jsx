import React, { PropTypes } from 'react';

function createOptions(options){
	return (
		options.map( (opt, i) => <option key={ i } value={ opt }>{ opt }</option> )
	)
}


const FilterAlt = ({ label, options }) => {

    return (
    	<div className="input-field">
	        <select>
	        	<option key="any" value="any">any</option>
	        	{ createOptions(options) }
		    </select>
		    <label>{ label }</label>
	    </div>
    );
};

FilterAlt.displayName = 'FilterAlt';

FilterAlt.propTypes = {
    label: PropTypes.string,
};

export default FilterAlt;
