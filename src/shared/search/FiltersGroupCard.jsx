import React, {Component} from 'react'

import ReactCSSTransitionGroup 	from 'react-addons-css-transition-group'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import _ from 'lodash'

import ActionNotifier from '../../hocs/actionNotifier'

import Toggle from 'material-ui/Toggle'
import Divider from 'material-ui/Divider'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar'

import TooltippedIconButton from '../ui/TooltippedIconButton'
import Preloader 				from '../components/Preloader'
import NoResult 				from '../components/NoResult'

import Filter 					from './Filter'
import FilterAlt 				from './FilterAlt'
import FilterSelectFieldAlt 	from '../components/FilterSelectedFieldAlt'
//import Selector					from './Selector'
import styles 				from '../../styles/styles'

/**
 * todo: add a paper toolbar with the title and a collapse action button on the right
 */


function createColumns ({filters, bufferSize, onChange}) {
// todo: use this.props.filters array, fetched from api call
		return filters.map( (item, i) => {
			return (
				<div className="col l3 m4 s12 filter-col" key={ i }>
					{/*<FilterAlt key={ i } {...item} />*/}
					<FilterSelectFieldAlt
						{ ...item }
						bufferSize={bufferSize}
						onChange={ onChange }/>
				</div>
			);
		});
};

class FiltersGroupCard extends Component{
	constructor(props){
		super(props);
		this.state = {
			expanded: false
		};
		this.handleExpandChange = this.handleExpandChange.bind(this);
	}

	handleExpandChange(expanded){
		console.log('expanded', expanded);
		this.setState({expanded:expanded});
	}

	componentWillMount() {
		console.log('FiltersGroupCard.componentWillMount');
	}
	componentDidMount() {
		console.log('FiltersGroupCard.componentDidMount');
	}
	componentWillReceiveProps(nextProps) {
	    console.log('FiltersGroupCard.componentWillReceiveProps');
	}
	componentWillUpdate(nextProps, nextState) {
		console.log('FiltersGroupCard.componentWillUpdate');
	}
	componentDidUpdate(prevProps, prevState) {
		console.log('FiltersGroupCard.componentDidUpdate');
	}
	shouldComponentUpdate(nextProps, nextState) {
		console.log('FiltersGroupCard.shouldComponentUpdate');
	    return true;
	}
	componentWillUnmount() {
	    console.log('FiltersGroupCard.componentWillUnmount');
	}


	render(){

		console.log('FiltersGroupCard.render', 'is loading?', this.state.loading);


		return (

			<div className="comp-FiltersGroupCard" style={styles.FiltersGroupCard}>

				<Card  expanded={this.state.expanded} onExpandChange={this.handleExpandChange}>
					<CardHeader
						title="card title"
						actAsExpander={true}
          				showExpandableButton={true} />
					<CardText expandable={true}>

						<div className="filters">
							<div className="row filters-selects">
									{
										createColumns({
											filters:this.props.data.filters,
											bufferSize:_.size(this.props.data.filtersBuffer),
											onChange: this.props.onSelectChange
										})
									}
							</div>
							<div className="filters-buttons">
								<RaisedButton label="Reset" primary={false} onClick={ this.props.onResetHandler } />
								<RaisedButton label="Search" primary={true} onClick={ this.props.onApplyHandler } />
							</div>
							<div>
								<div className="filters-recap">
									<p>Selected Filters: { _.size(this.props.data.filtersBuffer) > 0 ? '' : 'no filter selected yet.'}</p>
									<ul>
										{ this.props.data.filtersBuffer.map((item) => <li className="selected-filters-item" key={item.name}><span>{_.replace(item.name, /_/g, ' ')}</span> <span>{item.value}</span></li>) }
									</ul>
								</div>
							</div>
						</div>
					</CardText>
				</Card>

					</div>

		);

	}
}
FiltersGroupCard.propTypes = {};
FiltersGroupCard.defaultProps = {};

export default FiltersGroupCard;