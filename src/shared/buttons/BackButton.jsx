import React from 'react'
import { browserHistory } from 'react-router'
import IconButton from 'material-ui/IconButton'
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back'
import styles from '../../styles/styles'

//<TooltippedIconButton text="Back" clickHandler={ browserHistory.goBack }><ArrowBack /></TooltippedIconButton>
const BackButton = () => {
    return (
        <IconButton className="backbutton" onClick={ browserHistory.goBack } tooltip="Back" tooltipStyles={styles.tooltip}><ArrowBack /></IconButton>
    );
};

export default BackButton;
