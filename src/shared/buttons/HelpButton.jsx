import React, { PropTypes } from 'react';
import IconButton from 'material-ui/IconButton'
import HelpOutline from 'material-ui/svg-icons/action/help-outline'

import BaseComponent from '../components/BaseComponent'

class HelpButton extends BaseComponent{
	constructor(props){
		super(props);
		this.state = {};
	}

	render(){
		return(
	        <IconButton
	        	className="tooltipped"
	        	data-position="top"
	        	data-delay="20"
	        	data-tooltip={this.props.text}
	        	style={ {textAlign: 'sub', width: 'auto', height: 'auto', padding: 0} }>
	        	<HelpOutline />
	        </IconButton>
		);
	}
}
HelpButton.propTypes = {};
HelpButton.defaultProps = {
	text: 'No help for this element'
};

export default HelpButton;