//import { SIGN_IN } from '../actions/types'

export default function(store){
	return next => action => {
		//console.log('UOM Setter', store, action);
		/*
		if(action.type != SIGN_IN){
			return next(action);
		}
		ok, the action is interesting for me

		1 - work on the action.payload
		2 - create a new action { ...action, payload: the new payload}
		3 - dispact(new action);

		*/
		next(Object.assign({}, action, {getState:store.getState}));
	}
}