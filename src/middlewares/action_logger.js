export default function({ dispatch }){
	return next => action => {
		//console.log('ActionLogger', action);

		/*

		if(set a condition for which u refuse to handle this action){
			return next(action);
		}

		ok, the action is interesting for me

		1 - work on the action.payload
		2 - create a new action { ...action, payload: the new payload}
		3 - dispact(new action);

		*/
		next(action);
	}
}