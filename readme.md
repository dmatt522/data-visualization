# **Data Visualization App**
###### RestAPI data visualization professional tool on React.js (Redux).
***

## **Installation**

- Install global packages

```
npm install -g babel
npm install -g babel-cli
```

- Install the dependencies & plugins

```
npm install
```

- Running the Server

```
npm start
```